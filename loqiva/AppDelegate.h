//
//  AppDelegate.h
//  loqiva
//
//  Created by Manuel Manzanera on 19/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ATLocationBeacon/ATLocationBeacon.h>
#import <ATConnectionHttp/ATConnectionHttp.h>
#import <UserNotifications/UserNotifications.h>

@class NetworkRequestMonitor;

@interface AppDelegate : ATBeaconAppDelegate <UIApplicationDelegate, ATBeaconNotificationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UIImageView *windowImageView;
@property (strong, nonatomic) NetworkRequestMonitor *networkRequestMonitor;

+ (AppDelegate *)sharedDelegate;
+ (BOOL)showAlertIfDisconnected;

@end

