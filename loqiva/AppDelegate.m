//
//  AppDelegate.m
//  loqiva
//
//  Created by Manuel Manzanera on 19/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "AppDelegate.h"

#import "SplashViewController.h"

#import "AFNetworkReachabilityManager.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "LocationManager.h"
#import "BeaconNotificationStrategyFilter.h"
#import "BlueCatsSDK.h"
#import "JCNotificationCenter.h"
#import "JCNotificationBannerPresenterSmokeStyle.h"
#import "BlueCatsSDK/BlueCatsSDK.h"
#import "BeaconData.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"
#import "FlurrySessionBuilder.h"
#import "Flurry.h"

#define APP_TOKEN_BLUECATS @"c1b7f931-200f-4831-b87a-e9befe61c4d8"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self startObservingNetworkRequests];
    
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    BOOL isBluecats = infoPlistDict[@"AppUsesBlueCat"];
    
    if(isBLUECAT && isBluecats) {
        [BlueCatsSDK requestAlwaysLocationAuthorization];
        [self initBlueCats];
    }
    else {
        [self initAdtagInstanceWithUrlType:ATUrlTypeItg userLogin:infoPlistDict[@"AppUserLogin"] userPassword:infoPlistDict[@"AppUserPassword"] userCompany:infoPlistDict[@"AppUserCompany"] beaconUuid:infoPlistDict[@"AppUserBluetoothUUID"]];
    }
    
    [self addNotificationStrategy:[[BeaconNotificationStrategyFilter alloc] initWithMinTimeBetweenNotification:1 * 60 * 1000]];
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
        }];
    }
    else {
        if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
            [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [application registerForRemoteNotifications];
        }
    }
    
    //Flurry Api Session
    //NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    NSString *flurryAPIKey = infoPlistDict[@"FlurryApi"];
    FlurrySessionBuilder *builder = [[[[[FlurrySessionBuilder new]
                                        withLogLevel:FlurryLogLevelAll]
                                       withCrashReporting:YES]
                                      withSessionContinueSeconds:10]
                                     withAppVersion:@"0.1.2"];
    
    [Flurry startSession:flurryAPIKey withSessionBuilder:builder];
    //end
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[LocationManager sharedInstance] startUpdatingLocation];
    
    [Fabric with:@[[Crashlytics class]]];
    
    SplashViewController *splashViewController = [[SplashViewController alloc] init];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [self.window setBackgroundColor:[UIColor orangeAppColor]];
    [self.window setRootViewController:splashViewController];
    [self.window makeKeyAndVisible];
    
    self.windowImageView = [[UIImageView alloc] init];
    self.windowImageView.frame = self.window.frame;
    
    [self.window addSubview:self.windowImageView];
    [self.window sendSubviewToBack:self.windowImageView];
    
    return YES;
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [WebServiceManager postDevToken:nil];
}

- (void)initBlueCats{
    [BlueCatsSDK startPurringWithAppToken:APP_TOKEN_BLUECATS completion:^(BCStatus status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (status == kBCStatusPurringWithErrors) {
                BCAppTokenVerificationStatus appTokenVerificationStatus = [BlueCatsSDK appTokenVerificationStatus];
                if (appTokenVerificationStatus == kBCAppTokenVerificationStatusNotProvided || appTokenVerificationStatus == kBCAppTokenVerificationStatusInvalid) {
                    //kBCAppTokenVerificationStatusNotProvided - Use setAppToken to set the app token. Get an app token from app.bluecats.com
                    //kBCAppTokenVerificationStatusInvalid - App token invalid.
                    NSLog(@"Token Invalid");
                }
                if (![BlueCatsSDK isLocationAuthorized]) {
                    [BlueCatsSDK requestAlwaysLocationAuthorization]; //Requests location use from the user even when the app is not in use.
                    //[BlueCatsSDK requestWhenInUseLocationAuthorization]; //Requests location use when the app is in use.
                    NSLog(@"Location UnAuthorized");
                }
                if (![BlueCatsSDK isNetworkReachable]) {
                    //The BlueCats SDK must have network connectivity at least once before ranging beacons. If this is the only error and the SDK has never reached the network purring will occur with network connectivity.
                    NSLog(@"Network Reachable");
                }
                if (![BlueCatsSDK isBluetoothEnabled]) {
                    //Prompt user to enable bluetooth in settings. If BLE is required for current functionality a modal is recommended.
                    NSLog(@"Bluetooth Enabled");
                }
            } else {
                NSLog(@"BINGO");
            }
        });
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to register Remote Notifications");
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    const unsigned *tokenBytes = [deviceToken bytes];
    NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    
    NSLog(@"Token %@", hexToken);
    [[NSUserDefaults standardUserDefaults] setObject:hexToken forKey:@"devToken"];
    [WebServiceManager postDevToken:nil];
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    
    if ([UIApplication sharedApplication].applicationState!=UIApplicationStateActive) {
        NSLog(@"%@", notification.userInfo);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:REWARD_DETAIL_NOTIFICATION object:notification.userInfo];
        
    } else {
        if(isBLUECAT){
            if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
                NSLog(@"Old notification method");
            }
            else if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"9.0")){
                BeaconData *beaconData = [BeaconData getBeaconData];
                if (beaconData) {
                    [JCNotificationCenter enqueueNotificationWithTitle:beaconData.titleNotification message:beaconData.bodyNotification tapHandler:^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:REWARD_DETAIL_NOTIFICATION object:nil];
                        [UIApplication  sharedApplication].applicationIconBadgeNumber --;
                    }];
                }
                else {
                    NSDictionary *aps = [notification.userInfo valueForKey:@"aps"];
                    NSDictionary *alert = [aps valueForKey:@"alerttype"];
                    NSString *title = [alert valueForKey:@"msgtitle"];
                    NSString *message = [alert valueForKey:@"welcomemsg"];
                    //notification.alertTitle
                    //notification.alertBody
                    [JCNotificationCenter enqueueNotificationWithTitle:title message:message tapHandler:^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:ACTION_NOTIFICATION object:nil];
                        [UIApplication  sharedApplication].applicationIconBadgeNumber --;
                    }];
                    
                }
            }
        }
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateTheBellSystem" object:nil];
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        if(isBLUECAT){
            
            if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
                NSLog(@"Old notification method");
            }
            else if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"9.0")){
                BeaconData *beaconData = [BeaconData getBeaconData];
                if (beaconData) {
                    [JCNotificationCenter enqueueNotificationWithTitle:beaconData.titleNotification message:beaconData.bodyNotification tapHandler:^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:REWARD_DETAIL_NOTIFICATION object:nil];
                        [UIApplication  sharedApplication].applicationIconBadgeNumber --;
                    }];
                }
                else {
                    NSDictionary *aps = [userInfo valueForKey:@"aps"];
                    NSDictionary *alert = [aps valueForKey:@"alert"];
                    NSString *title = [alert valueForKey:@"title"];
                    NSString *message = [alert valueForKey:@"body"];

                    [JCNotificationCenter enqueueNotificationWithTitle:message message:title tapHandler:^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:ACTION_NOTIFICATION object:nil];
                        [UIApplication  sharedApplication].applicationIconBadgeNumber --;
                    }];
                }
            }
            
        } else {
            ATBeaconContent *beaconContent = [adtagBeaconManager getNotificationBeaconContent];
            NSDictionary* dict = [NSDictionary dictionaryWithObject: beaconContent forKey:@"beaconContent"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LocalNotificationMessageReceivedNotification" object:nil userInfo:dict];
            
            //Here we can make a call to Reward Detail Method
            
            [[NSNotificationCenter defaultCenter] postNotificationName:REWARD_DETAIL_NOTIFICATION object:beaconContent];
            
        }
        
    } else {
        NSLog(@"I received a beacon with App Close or background");
    }
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateTheBellSystem" object:nil];
    
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    if(state == UIApplicationStateActive){
        
        if ([[AppContext sharedInstance] geoLocationMessage]) {
            GeoLocationMessage *geoloCationMessage = [[AppContext sharedInstance] geoLocationMessage];
            
            [JCNotificationCenter enqueueNotificationWithTitle:geoloCationMessage.msgtitle message:geoloCationMessage.welcomeMsg tapHandler:^{
                if (geoloCationMessage.url && geoloCationMessage.url > 0) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:geoloCationMessage.url]];
                }
                else {
                    [[NSNotificationCenter defaultCenter] postNotificationName:REWARD_DETAIL_NOTIFICATION object:nil];
                }
            }];
            //here setting geolocationMessage object --Wasiq
            [[AppContext sharedInstance] setGeoLocationMessage:geoloCationMessage];
            
        }
        else if ([BeaconData getBeaconData]) {
            BeaconData *beaconData = [BeaconData getBeaconData];
            
            [JCNotificationCenter enqueueNotificationWithTitle:beaconData.titleNotification message:beaconData.bodyNotification tapHandler:^{
                [[NSNotificationCenter defaultCenter] postNotificationName:REWARD_DETAIL_NOTIFICATION object:nil];
                [UIApplication  sharedApplication].applicationIconBadgeNumber --;
            }];
        }
        else {
            NSString *title = [[notification.request content] title];
            NSString *message = [[notification.request content] body];
            NSNumber *badge = [[notification.request content] badge];
            
            [JCNotificationCenter enqueueNotificationWithTitle:message message:title tapHandler:^{
                [[NSNotificationCenter defaultCenter] postNotificationName:ACTION_NOTIFICATION object:nil];
                [UIApplication  sharedApplication].applicationIconBadgeNumber --;
            }];
        }
    }
    else {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:REWARD_DETAIL_NOTIFICATION object:nil];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        
        completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    }
}

//Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateTheBellSystem" object:nil];
    
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    if(state == UIApplicationStateActive){
        if([[AppContext sharedInstance] geoLocationMessage])
        {
            GeoLocationMessage *geoloCationMessage = [[AppContext sharedInstance] geoLocationMessage];
            
            [JCNotificationCenter enqueueNotificationWithTitle:geoloCationMessage.msgtitle message:geoloCationMessage.welcomeMsg tapHandler:^{
                if (geoloCationMessage.url && geoloCationMessage.url > 0) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:geoloCationMessage.url]];
                }
                else {
                    [[NSNotificationCenter defaultCenter] postNotificationName:REWARD_DETAIL_NOTIFICATION object:nil];
                }
            }];
            
            [[AppContext sharedInstance] setGeoLocationMessage:nil];
            
        }
        else{
            BeaconData *beaconData = [BeaconData getBeaconData];
            
            [JCNotificationCenter enqueueNotificationWithTitle:beaconData.titleNotification message:beaconData.bodyNotification tapHandler:^{
                [[NSNotificationCenter defaultCenter] postNotificationName:REWARD_DETAIL_NOTIFICATION object:nil];
            }];
        }
    }
    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:REWARD_DETAIL_NOTIFICATION object:nil];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
    
    completionHandler();
}

-(UILocalNotification *)createNotification:(ATBeaconContent *)_beaconContent{
    
    UILocalNotification *notification = [[UILocalNotification alloc]init];
    [notification setAlertBody:[_beaconContent getNotificationDescription]];
    
    if(SYSTEM_VERSION_GREATER_THAN(@"7.99")){
        [notification setAlertTitle:[_beaconContent getAlertTitle]];
    }
    
    return notification;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
   
    NSString *urlScheme = url.absoluteString;
    NSArray *splitUrl = [urlScheme componentsSeparatedByString:@"/"];
    NSArray *urlToken =  [splitUrl[2] componentsSeparatedByString:@"="] ;
    NSString *userToken = urlToken[2];
    [UserDefaultManager removeKey:@"usernewtoken"];
   
    NSLog(@"%@", urlToken);
    [UserDefaultManager setWithKey:@"usernewtoken" WithObject:userToken];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ResetPassWord" object:nil];
    return YES;
}

+ (AppDelegate *)sharedDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

+ (BOOL)showAlertIfDisconnected {
    if (![self connectedToInternet]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please make sure you're connected to the internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return YES;
    }
    else {
        return NO;
    }
}

+ (BOOL)connectedToInternet {
    return [[DataConnectionManager shared] isConnected];
}

@end
