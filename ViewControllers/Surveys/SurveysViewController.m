//
//  SurveysViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "SurveysViewController.h"
#import "SurveysDetailViewController.h"
#import "SurveysCell.h"
#import "NavCustomView.h"
#import "UIColor+Helper.h"
#import "WebServiceManager.h"
#import "TOWebViewController.h"
#import "WebViewController.h"
#import "Survey.h"
#import "loqiva-Swift.h"

@interface SurveysViewController ()<UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate>

@property (nonatomic, strong) UITableView *surveysTableView;
@property (nonatomic, strong) NSArray *dataArray;

@end

@implementation SurveysViewController

@synthesize surveysTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dataArray = [[AppContext sharedInstance] surveys];
    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated {
    [WebServiceManager getSurveys:self];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    if (!surveysTableView) {
        surveysTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., -20., WIDTH, HEIGHT + 20) style:UITableViewStylePlain];
        [surveysTableView setDelegate:self];
        [surveysTableView setDataSource:self];
        [surveysTableView registerClass:[SurveysCell class] forCellReuseIdentifier:@"surveysCell"];
        [surveysTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.view addSubview:surveysTableView];
    }
    
    [surveysTableView reloadData];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [surveysTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.dataArray.count > 0) {
        Survey *survey = self.dataArray[indexPath.row];
        
        WebViewController *webViewController = [[WebViewController alloc] init];
        [webViewController setUrlString:survey.link];
        [webViewController setTitleBar:survey.title];
        [webViewController setIsAlternative:YES];
        [webViewController setIsBackAvailable:YES];
        [self.navigationController pushViewController:webViewController animated:YES];
    }
}

#pragma mark UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.dataArray.count == 0) {
        return 1;
    }
    
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [UtilManager height:70];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return NAV_BAR_HEIGHT;
            break;
        case 1:
            return 0;
            break;
        default:
            return 0;
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return [self navigationView];
            break;
        default:
            return nil;
            break;
    }
}

- (UIView *)headerTitleView:(NSString *)title{
    UIView *headerTitleView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:120])];
    [headerTitleView setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], 0, headerTitleView.frame.size.width - 2 * [UtilManager width:20], [UtilManager height:110])];
    [titleLabel setNumberOfLines:0];
    [titleLabel setText:title];
    [titleLabel setTextAlignment:NSTextAlignmentLeft];
    [titleLabel setTextColor:[UIColor orangeButtonColor]];
    [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:28]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    [headerTitleView addSubview:titleLabel];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., [UtilManager height:119], WIDTH, 1)];
    [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
    [headerTitleView addSubview:separatorView];
    
    return headerTitleView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SurveysCell *cell = [surveysTableView dequeueReusableCellWithIdentifier:@"surveysCell"];
    
    if (self.dataArray.count == 0) {
        cell.titleLabel.text = @"No surveys to complete";
        cell.leftLabel.text = @"";
        cell.rigthArrow.hidden = YES;
        return cell;
    }

    
    Survey *survey = self.dataArray[indexPath.row];
    cell.titleLabel.text = survey.title;
    cell.rigthArrow.hidden = NO;
    
    NSString *expireString = [NSString stringWithFormat:@"%@ left", survey.expires];
    cell.leftLabel.text = expireString;

    return cell;
}

- (UIView *)navigationView{
    NavCustomView *containerView = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight :NO];
    [[containerView icon] setImage:[UIImage imageNamed:@"icon-surveys-orange"]];
    [[containerView iconLabel] setTextColor:[UIColor orangeButtonColor]];
    [[containerView iconLabel] setText:NSLocalizedString(@"Surveys", nil)];
    [[containerView slideButton]setImage:[UIImage imageNamed:@"btn-slider-orange"]];
    [[containerView notificationButtonImage] setImage:[UIImage imageNamed:@"btn-notification-orange"]];
    [[containerView separatorView] setBackgroundColor:[UIColor orangeButtonColor]];
    [[containerView separatorView] setAlpha:0.8];
    containerView.parentController = self;
    [containerView setBackgroundColor:[UIColor whiteColor]];
    
    return containerView;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object {
    if (webServiceType == WebServiceTypeSurveyList) {
        self.dataArray = [[AppContext sharedInstance] surveys];
        [self configureView];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType {
    if (webServiceType == WebServiceTypeSurveyList) {
        self.dataArray = [[AppContext sharedInstance] surveys];
        [self configureView];
    }
}

@end
