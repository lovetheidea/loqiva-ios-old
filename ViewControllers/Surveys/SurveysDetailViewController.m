//
//  SurveysDetailViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 26/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "SurveysDetailViewController.h"
#import "NavCustomView.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface SurveysDetailViewController ()

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIButton *questionButton;

@property (nonatomic, strong) UIImageView *firstImageView;
@property (nonatomic, strong) UIImageView *secondImageView;


@end

@implementation SurveysDetailViewController

@synthesize scrollView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0., -20., WIDTH, HEIGHT + 20)];
    
    [scrollView addSubview:[self navigationView]];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], NAV_BAR_HEIGHT + [UtilManager height:16], [UtilManager width:320], [UtilManager height:74])];
    [titleLabel setText:NSLocalizedString(@"Duthrie Park Playground 16/2/2016", nil)];
    [titleLabel setNumberOfLines:0];
    [titleLabel setTextColor:[UIColor orangeButtonColor]];
    [titleLabel setTextAlignment:NSTextAlignmentLeft];
    [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    [scrollView addSubview:titleLabel];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y + titleLabel.frame.size.height + [UtilManager height:17.], [UtilManager width:335], 1)];
    [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
    [scrollView addSubview:separatorView];
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(separatorView.frame.origin.x, separatorView.frame.origin.y + [UtilManager height:15], separatorView.frame.size.width, [UtilManager height:96])];
    [descriptionLabel setNumberOfLines:0];
    [descriptionLabel setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [descriptionLabel setTextColor:GRAY_TEXT_COLOR];
    [descriptionLabel setText:NSLocalizedString(@"On 7th July a new playground will be installed at Duthrie Park, feel free to answer a few questions to have your say in the decision process", nil)];
    [scrollView addSubview:descriptionLabel];
    
    CGFloat origin = descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height + [UtilManager height:25];
    
    [scrollView addSubview:[self questionViewWithOrigin:origin]];
    
    _questionButton = [[UIButton alloc] initWithFrame:CGRectMake(0, origin + [UtilManager height:290], [UtilManager width:142], [UtilManager height:40])];
    [_questionButton setImage:[UIImage imageNamed:@"btn-next-question"] forState:UIControlStateNormal];
    [_questionButton setCenter:CGPointMake(self.view.center.x, _questionButton.frame.origin.y)];
    
    [scrollView addSubview:_questionButton];
    
    [self.view addSubview:scrollView];
}

- (UIView *)questionViewWithOrigin:(CGFloat)origin{
    UIView *questionView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20], origin, [UtilManager width:330], [UtilManager height:230])];
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:15], [UtilManager width:300], [UtilManager height:66])];
    [infoLabel setTextColor:ORANGE_TEXT_COLOR];
    [infoLabel setText:@"Q1 Please click the preferred climbing frame you would like to see used in the new Duthrie Park Playground"];
    [infoLabel setTextAlignment:NSTextAlignmentLeft];
    [infoLabel setNumberOfLines:0];
    [infoLabel setFont:[UIFont fontWithName:LIGHT_FONT size:14]];
    [infoLabel setAdjustsFontSizeToFitWidth:YES];
    [questionView addSubview:infoLabel];
    
    _firstImageView = [[UIImageView alloc] initWithFrame:CGRectMake(infoLabel.frame.origin.x, infoLabel.frame.origin.y + infoLabel.frame.size.height + [UtilManager height:24], [UtilManager width:140.], [UtilManager height:110])];
    [_firstImageView setBackgroundColor:GRAY_TEXT_COLOR];
    [_firstImageView setImage:[UIImage imageNamed:@"temp-surveys-firstimage"]];
    [_firstImageView setUserInteractionEnabled:YES];
    [_firstImageView setTag:1];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(firstImageTap)];
    [_firstImageView addGestureRecognizer:tapGesture];
    
    [questionView addSubview:_firstImageView];
    
    _secondImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_firstImageView.frame.origin.x + _firstImageView.frame.size.width + [UtilManager width:10], _firstImageView.frame.origin.y, [UtilManager width:140], [UtilManager height:110])];
    [_secondImageView setBackgroundColor:GRAY_TEXT_COLOR];
    [_secondImageView setImage:[UIImage imageNamed:@"temp-surveys-secondimage"]];
    [_secondImageView setTag:2];
    [_secondImageView setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *secondtapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(secondImageTap)];
    [_secondImageView addGestureRecognizer:secondtapGesture];
    
    [questionView addSubview:_secondImageView];
    
    [[questionView layer] setCornerRadius:4];
    [[questionView layer] setBorderWidth:1];
    [[questionView layer] setBorderColor:GRAY_TEXT_COLOR.CGColor];
    
    return questionView;
}

- (UIView *)navigationView{
    NavCustomView *containerView = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT)];
    [[containerView icon] setImage:[UIImage imageNamed:@"icon-surveys-orange"]];
    [[containerView iconLabel] setTextColor:[UIColor orangeButtonColor]];
    [[containerView iconLabel] setText:NSLocalizedString(@"Surveys", nil)];
    [[containerView slideButton]setImage:[UIImage imageNamed:@"btn-slider-orange"]];
    [[containerView  notificationButtonImage] setImage:[UIImage imageNamed:@"btn-notification-orange"]];
    [[containerView separatorView] setBackgroundColor:[UIColor HeaderTextColour]];
    [[containerView separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];
    [containerView setBackgroundColor:[UIColor whiteColor]];
    [[containerView backButtonImage] setHidden:FALSE];
    [[containerView backButtonImage] setImage:[UIImage imageNamed:@"btn-back-orange"]];
    containerView.parentController = self;
    return containerView;
}

- (void)firstImageTap{
    [self removeSubViews];
    [_firstImageView addSubview:[self yesView]];
    [_secondImageView addSubview:[self noView]];
}

- (void)secondImageTap{
    [self removeSubViews];
    [_firstImageView addSubview:[self noView]];
    [_secondImageView addSubview:[self yesView]];
}

- (void)removeSubViews{
    for(UIView *view in _firstImageView.subviews){
        [view removeFromSuperview];
    }
    for(UIView *view in _secondImageView.subviews){
        [view removeFromSuperview];
    }
}

- (UIView *)yesView{
    UIView *yesView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., _firstImageView.frame.size.width,  _firstImageView.frame.size.height)];
    
    UIView *maskView = [[UIView alloc] initWithFrame:yesView.frame];
    [maskView setBackgroundColor:[UIColor greenColor]];
    [maskView setAlpha:0.6];
    [yesView addSubview:maskView];
    
    UILabel*yesLabel = [[UILabel alloc] initWithFrame:yesView.frame];
    [yesLabel setBackgroundColor:[UIColor clearColor]];
    [yesLabel setTextColor:[UIColor whiteColor]];
    [yesLabel setTextAlignment:NSTextAlignmentCenter];
    [yesLabel setText:NSLocalizedString(@"YES", nil)];
    [yesLabel setFont:[UIFont fontWithName:REGULAR_FONT size:26]];
    [yesView addSubview:yesLabel];
    
    return yesView;
    
}

- (UIView *)noView{
    UIView *noView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., _secondImageView.frame.size.width,  _secondImageView.frame.size.height)];
    
    UIView *maskView = [[UIView alloc] initWithFrame:noView.frame];
    [maskView setBackgroundColor:GRAY_TEXT_COLOR    ];
    [maskView setAlpha:0.6];
    [noView addSubview:maskView];
    
    UILabel*noLabel = [[UILabel alloc] initWithFrame:noView.frame];
    [noLabel setBackgroundColor:[UIColor clearColor]];
    [noLabel setTextColor:[UIColor whiteColor]];
    [noLabel setTextAlignment:NSTextAlignmentCenter];
    [noLabel setText:NSLocalizedString(@"NO", nil)];
    [noLabel setFont:[UIFont fontWithName:REGULAR_FONT size:26]];
    [noView addSubview:noLabel];
    
    return noView;
}

@end
