//
//  SurveysCell.m
//  loqiva
//
//  Created by Manuel Manzanera on 29/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "SurveysCell.h"

#import "AppConstants.h"
#import "UtilManager.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@implementation SurveysCell

@synthesize titleLabel, leftLabel, rigthArrow;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:5], WIDTH - [UtilManager width:20] * 2, [UtilManager height:30])];
        [titleLabel setTextAlignment:NSTextAlignmentLeft];
        [titleLabel setTextColor:[UIColor darkGrayColor]];
        [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:18]];
        [self.contentView addSubview:titleLabel];
        
        leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y + titleLabel.frame.size.height, titleLabel.frame.size.width, [UtilManager height:20])];
        [leftLabel setTextColor:[UIColor orangeButtonColor]];
        [leftLabel setText:@"1 day left"];
        [leftLabel setFont:[UIFont fontWithName:LIGHT_FONT size:12]];
        
        [self.contentView addSubview:leftLabel];
        
        rigthArrow  =[[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:9], [UtilManager height:17])];
        [rigthArrow setImage:[UIImage imageNamed:@"image-arrow-orange"]];
        [rigthArrow setCenter:CGPointMake(WIDTH - [UtilManager width:20], [UtilManager height:35])];
        
        [self.contentView addSubview:rigthArrow];
        
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., [UtilManager height:69], WIDTH, 1)];
        [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [self.contentView addSubview:separatorView];
    }
    
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
