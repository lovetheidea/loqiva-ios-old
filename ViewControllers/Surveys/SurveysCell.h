//
//  SurveysCell.h
//  loqiva
//
//  Created by Manuel Manzanera on 29/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SurveysCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UIImageView *rigthArrow;

@end
