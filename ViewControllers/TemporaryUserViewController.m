//
//  TemporaryUserViewController.m
//  loqiva
//
//  Created by juan Jimenez on 08/12/2016.
//  Copyright © 2016 inup. All rights reserved.
//

#import "TemporaryUserViewController.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "loqiva-Swift.h"

@interface TemporaryUserViewController ()

@property(strong, nonatomic) UIImageView * messageImage;
@property(strong, nonatomic) UIView * darkBG;
@property(strong, nonatomic) UIButton *signUpFreeBtn;
@property(strong, nonatomic) UIButton *continueBrowsingBtn;
@property(nonatomic)DismmissBlock dismissBlock;

@property(nonatomic)CallToSignForFreeBlock calltosignForFreeBlock;

@end

@implementation TemporaryUserViewController


-(id)initWithDismissBlock:(DismmissBlock)dismissBlock andCallToSignForFree:(CallToSignForFreeBlock)callToSignforFreeBlock{
    if(self=[super init]){
        
        _dismissBlock = dismissBlock;
        _calltosignForFreeBlock = callToSignforFreeBlock;
        
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _darkBG = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [_darkBG setBackgroundColor:[UIColor blackColor]];
    _darkBG.alpha=0.7;
    
    [self.view addSubview:_darkBG];
    _messageImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popup-restricted-user"]];
    _messageImage.frame = CGRectMake(WIDTH/2-[UtilManager width:330]/2, HEIGHT/2-[UtilManager width:448]/2, [UtilManager width:330], [UtilManager height:488]);
    [self.view addSubview:_messageImage];
    
    _continueBrowsingBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 400, WIDTH, [UtilManager height:40])];
    
    
    [_continueBrowsingBtn setBackgroundColor:[UIColor clearColor]];
    [_continueBrowsingBtn addTarget:self action:@selector(continueBrowsing:) forControlEvents:UIControlEventTouchDown];
    [_continueBrowsingBtn setTitleColor:GRAY_TEXT_COLOR forState:UIControlStateNormal];
    [_continueBrowsingBtn setTitle:NSLocalizedString(@"Continue browsing", nil) forState:UIControlStateNormal];
    [_continueBrowsingBtn setFont:[UIFont fontWithName:REGULAR_FONT size:17]];
    
    
    [self.view addSubview:_continueBrowsingBtn];
    
    _signUpFreeBtn = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH/2-[UtilManager height:170]/2,350,  [UtilManager width:170], [UtilManager height:40])];
    [_signUpFreeBtn setBackgroundImage:[UIImage imageNamed:@"btn-signup-modal"] forState:UIControlStateNormal];
    [_signUpFreeBtn addTarget:self action:@selector(pushSignUpController) forControlEvents:UIControlEventTouchDown];
 

    
    
    
    [self.view addSubview:_signUpFreeBtn];
    // Do any additional setup after loading the view.
}


-(void)continueBrowsing:(id)sender{
    _dismissBlock();
    
}
-(void)pushSignUpController{
    NSLog(@"FRFRFRFR111");
    _calltosignForFreeBlock();
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
