//
//  ParentViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ParentViewController.h"
#import "RegisterUserViewController.h"
#import "SignNavController.h"
#import "AppDelegate.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface ParentViewController ()

@end

@implementation ParentViewController

- (void)loadView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0., 0., [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    [view setBackgroundColor:[UIColor clearColor]];
    
    [self setView:view];
}

- (void)presentSignForFreeController{
    RegisterUserViewController *viewController = [[RegisterUserViewController alloc] init];
    SignNavController *customNavigationController = [[SignNavController alloc] initWithRootViewController:viewController];
    customNavigationController.comesFromApp = YES;
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:customNavigationController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:customNavigationController];
     }];
    
}

-(void)setupHeaderForSection:(CMSectionType)section{
    
    if(_headerNav){
        NSLog(@"ALready _headerNav");
        return;
    }
    
    switch (section) {
        case CMSectionTypePayments:
            self.headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight :true];
            self.headerNav.parentController = self;
            [[self.headerNav icon] setImage:[UIImage imageNamed:@"icon-payments-orange"]];
            [[self.headerNav iconLabel] setAttributedText:NSLocalizedString(@"Payments", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            
            [[self.headerNav slideButton]setImage:[UIImage imageNamed:@"btn-slider-orange"]];
            [[self.headerNav  notificationButtonImage] setImage:[UIImage imageNamed:@"btn-notification-orange"]];
            [[self.headerNav separatorView] setBackgroundColor:[UIColor HeaderTextColour]];
            [[self.headerNav separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];
            
            break;
        case CMSectionTypeTransportation:
            self.headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:false];
            self.headerNav.parentController = self;
            [[self.headerNav icon] setImage:[UIImage imageNamed:@"icon-transport-orange"]];
            
            [[self.headerNav iconLabel] setAttributedText:NSLocalizedString(@"Transportation", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];

            
            [[self.headerNav slideButton]setImage:[UIImage imageNamed:@"btn-slider-orange"]];
            [[self.headerNav  notificationButtonImage] setImage:[UIImage imageNamed:@"btn-notification-orange"]];
            [[self.headerNav separatorView] setBackgroundColor:[UIColor HeaderTextColour]];
            [[self.headerNav separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];
            
            break;
        case CMSectionTypeWebView:
            self.headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:true];
            self.headerNav.parentController = self;
            
            [[self.headerNav iconLabel] setAttributedText:_titleBar withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor whiteColor] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            [[self.headerNav iconLabel] setAttributedText:NSLocalizedString(@"Attraction", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor whiteColor] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            
            [[_headerNav separatorView] setBackgroundColor:[UIColor HeaderTextColour]];
            [[_headerNav separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];
            [[_headerNav backButton] setHidden:TRUE];
            [[_headerNav blurView] setHidden:YES];
            [[self.headerNav icon] setImage:[UIImage imageNamed:@"icon-info"]];
            [[self.headerNav slideButton] setImage:[UIImage imageNamed:@"btn-slider"]];
            [[self.headerNav notificationButtonImage]setImage:[UIImage imageNamed:@"btn-notification"]];
            
            
            [_headerNav setBackgroundColor:[UIColor colorWithRed:0.137 green:0.122 blue:0.125 alpha:1.00]];
            
            if(self.navigationController.viewControllers.count > 1)
                [self.headerNav setBackOption];
            
            break;
            
        case CMSectionTypeWebViewAlternative:
            self.headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:NO];
            self.headerNav.parentController = self;
            
//            [[self.headerNav iconLabel] setAttributedText:_titleBar withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor darkGrayColor] andKern:NAV_CUSTOM_VIEW_KERN];
            [_headerNav.iconLabel setText:_titleBar];
            [[_headerNav iconLabel] setTextColor:[UIColor darkGrayColor]];
            
            
            [[_headerNav separatorView] setBackgroundColor:[UIColor navigationSeparatorColor]];
            [[_headerNav separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];
            [[_headerNav backButton] setHidden:TRUE];
            [[_headerNav blurView] setHidden:YES];
            [[_headerNav icon] setImage:[[UIImage imageNamed:@"icon-info"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
            [[_headerNav slideButton] setImage:[[UIImage imageNamed:@"btn-slider"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
            [[_headerNav notificationButtonImage]setImage:[[UIImage imageNamed:@"btn-notification"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
            
            _headerNav.tintColor = [UIColor darkGrayColor];
            [_headerNav setBackgroundColor:[UIColor whiteColor]];
            
            if(self.navigationController.viewControllers.count > 1) {
                [_headerNav setBackOption];
            }
            
            [_headerNav changeBackColor];
            
            break;
            
            
        case CMSectionTypeAttracction:
            self.headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:true];
            self.headerNav.parentController = self;
            [[self.headerNav icon] setImage:[UIImage imageNamed:@"icon-attraction"]];
            [[self.headerNav iconLabel] setAttributedText:_titleBar withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor whiteColor] andKern:-NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            [[self.headerNav iconLabel] setAttributedText:NSLocalizedString(@"Attraction", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor whiteColor] andKern: NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            
            [[_headerNav slideButton] setImage:[UIImage imageNamed:@"btn-slider"]];
            
            [[self.headerNav notificationButtonImage]setImage:[UIImage imageNamed:@"btn-notification"]];
            [[_headerNav blurView] setHidden:NO];
            
            if(self.navigationController.viewControllers.count > 1)
                [self.headerNav setBackOption];
            
            
            break;
        case CMSectionTypeReporting:
            
            self.headerNav  = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:true];
            self.headerNav.parentController = self;
            [[self.headerNav  icon] setImage:[UIImage imageNamed:@"icon-reporting"]];
            [[self.headerNav  iconLabel] setAttributedText:NSLocalizedString(@"Reporting", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor whiteColor] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            
            [[self.headerNav  slideButton]setImage:[UIImage imageNamed:@"btn-slider"]];
            [[self.headerNav   notificationButtonImage] setImage:[UIImage imageNamed:@"btn-notification"]];
            //            [[self.headerNav  separatorView] setBackgroundColor:[UIColor whiteColor]];
            //            [[self.headerNav  separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];
            [[self.headerNav  backButtonImage] setHidden:YES];
            
            break;
            
        case CMSectionTypeService:
            self.headerNav  = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:true];
            self.headerNav.parentController = self;
            [[_headerNav icon] setImage:[UIImage imageNamed:@"icon-services"]];
            [[_headerNav iconLabel] setAttributedText:NSLocalizedString(@"Services", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor whiteColor] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            [[_headerNav backButton] setHidden:FALSE];
            [_headerNav setBackOption];
            break;
            
            
        case CMSectionTypeInfoDetail:
            self.headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:false];
            self.headerNav.parentController = self;
            [[self.headerNav icon] setImage:[UIImage imageNamed:@"icon-info-orange"]];
            [[self.headerNav  iconLabel] setAttributedText:NSLocalizedString(@"Information", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            
            
            
            [[self.headerNav slideButton]setImage:[UIImage imageNamed:@"btn-slider-orange"]];
            [[self.headerNav  notificationButtonImage] setImage:[UIImage imageNamed:@"btn-notification-orange"]];
            [[self.headerNav separatorView] setBackgroundColor:[UIColor HeaderTextColour]];
            [[self.headerNav separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];
            [[_headerNav backButtonImage] setImage:[UIImage imageNamed:@"btn-back-orange"]];
            [_headerNav setBackgroundColor:WHITE_TEXT_COLOR];
            [_headerNav setBackOption];
            
            break;
            
        case CMSectionTypeRewards:
            
            
            self.headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:false];
            self.headerNav.parentController = self;
            [_headerNav setBackgroundColor:[UIColor whiteColor]];
            [[_headerNav iconLabel] setAttributedText:NSLocalizedString(@"Rewards", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            // [[_headerNav separatorView] setBackgroundColor:[UIColor navigationSeparatorColor]];
            // [[_headerNav separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];
            [[_headerNav backButton] setHidden:TRUE];
            [[_headerNav blurView] setHidden:YES];
            [[self.headerNav icon] setImage:[UIImage imageNamed:@"icon-rewards-orange"]];
            [[self.headerNav slideButton] setImage:[UIImage imageNamed:@"btn-slider-orange"]];
            [[self.headerNav notificationButtonImage]setImage:[UIImage imageNamed:@"btn-notification-orange"]];
            [[_headerNav separatorView] setBackgroundColor:[UIColor HeaderTextColour]];
            [[_headerNav separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];

            
            break;
        case CMSectionTypeScheduleMonth:
            
            self.headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:false];
            self.headerNav.parentController = self;
            [[_headerNav icon] setImage:[UIImage imageNamed:@"icon-schedule-accent"]];
            [[_headerNav iconLabel] setAttributedText:NSLocalizedString(@"Events", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            [[_headerNav slideButton]setImage:[UIImage imageNamed:@"btn-slider-orange"]];
            [[_headerNav  notificationButtonImage] setImage:[UIImage imageNamed:@"btn-notification-orange"] ];
            [[_headerNav separatorView] setBackgroundColor:[UIColor HeaderTextColour]];
            [[_headerNav separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];

            [[_headerNav backButtonImage] setHidden:YES];
            [[_headerNav backButtonImage] setImage:[UIImage imageNamed:@"btn-back-orange"]];
            break;
            
            
        case CMSectionTypeSchedule:
            
            self.headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:true];
            self.headerNav.parentController = self;
            [[_headerNav icon] setImage:[UIImage imageNamed:@"icon-schedule"]];
            [[_headerNav iconLabel] setAttributedText:NSLocalizedString(@"Events", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor whiteColor] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            [[_headerNav backButton] setHidden:TRUE];
            [[_headerNav  separatorView] setHidden:NO];
            [[self.headerNav slideButton] setImage:[UIImage imageNamed:@"btn-slider"]];
            [[self.headerNav notificationButtonImage]setImage:[UIImage imageNamed:@"btn-notification"]];
            break;
            
        case CMSectionTypeRewardsDetail:
            
            _headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:true];
            self.headerNav.parentController = self;
            [[_headerNav icon] setImage:[UIImage imageNamed:@"icon-rewards"]];
            [[_headerNav iconLabel] setAttributedText:NSLocalizedString(@"Rewards", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor whiteColor] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            
            
            [_headerNav setBackOption];
            
            break;
            
        case CMSectionTypeProfile:
            _headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:false];
            self.headerNav.parentController = self;
            [[_headerNav icon] setImage:[UIImage imageNamed:@"icon-profile-orange"]];
            [[_headerNav iconLabel] setAttributedText:NSLocalizedString(@"My Profile", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            [[_headerNav slideButton] setImage:[UIImage imageNamed:@"btn-slider-orange"]];
            [[_headerNav notificationButtonImage] setImage:[UIImage imageNamed:@"btn-notification-orange"]];
            [[_headerNav separatorView] setBackgroundColor:[UIColor HeaderTextColour]];
            [[_headerNav separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];
            [_headerNav setBackgroundColor:[UIColor whiteColor]];
            break;
            
        case CMSectionTypeTour:
            self.headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:true];
            self.headerNav.parentController = self;
            [[_headerNav icon] setImage:[UIImage imageNamed:@"icon-tours"]];
            [[_headerNav iconLabel] setAttributedText:NSLocalizedString(@"Tours", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor whiteColor] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            [[_headerNav slideButton] setImage:[UIImage imageNamed:@"btn-slider"]];
            
            [[self.headerNav notificationButtonImage]setImage:[UIImage imageNamed:@"btn-notification"]];
            [[_headerNav blurView] setHidden:NO];
            if(self.navigationController.viewControllers.count > 1)
                [self.headerNav setBackOption];
            
            break;
        case CMSectionTypeInformation:
            self.headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:false];
            self.headerNav.parentController = self;
            [[self.headerNav icon] setImage:[UIImage imageNamed:@"icon-info-orange"]];
            [[self.headerNav iconLabel] setAttributedText:NSLocalizedString(@"Information", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            
            [[self.headerNav slideButton]setImage:[UIImage imageNamed:@"btn-slider-orange"]];
            [[self.headerNav  notificationButtonImage] setImage:[UIImage imageNamed:@"btn-notification-orange"]];
            [[self.headerNav separatorView] setBackgroundColor:[UIColor HeaderTextColour]];
            [[self.headerNav separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];
            [_headerNav setBackgroundColor:WHITE_TEXT_COLOR];
                
            break;
            //Home
        default: {
            
            NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
            NSString *appName = infoPlistDict[@"AppName"];
            if (appName.length == 0) {
                appName = @"Loqiva";
            }
            
            NSString *hubName = infoPlistDict[@"HubName"];
            if (hubName.length == 0) {
                hubName = @"My Hub";
            }
            
            self.headerNav = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:true];
            self.headerNav.parentController = self;
            [[_headerNav iconLabel] setAttributedText:[NSString stringWithFormat:@"%@", hubName] withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor whiteColor] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
            [[_headerNav icon] setFrame:CGRectMake(NAV_ICON_WIDTH, [UtilManager height:35.], [UtilManager width:21], [UtilManager height:21])];
            [[_headerNav icon] setImage:[UIImage imageNamed:@"icon-myhub"]];
            [[_headerNav slideButton] setImage:[UIImage imageNamed:@"btn-slider"]];
            
            [[self.headerNav notificationButtonImage]setImage:[UIImage imageNamed:@"btn-notification"]];
            
            
            [[_headerNav blurView] setHidden:NO];
            
            
            break;
        }
    }
    
    
    
    [self.headerNav setHidden:NO];
    [self.view addSubview:self.headerNav];
    
    
}

@end
