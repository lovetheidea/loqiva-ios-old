//
//  CalloutContentMapView.m
//  loqiva
//
//  Created by Manuel Manzanera on 13/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "CalloutContentMapView.h"

#import "UtilManager.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "loqiva-Swift.h"

@implementation CalloutContentMapView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        
        _titleTextView = [[UITextView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:10], [UtilManager width:160], [UtilManager height:60])];
        [_titleTextView setBackgroundColor:TEST_BG_ONE];
        [_titleTextView setTextContainerInset:UIEdgeInsetsZero];
        _titleTextView.textContainer.lineFragmentPadding = 0;
        _titleTextView.editable=NO;
        
        [self addSubview:_titleTextView];

        
        
        
        _infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleTextView.frame.origin.x, _titleTextView.frame.size.height + _titleTextView.frame.origin.y, [UtilManager width:160], [UtilManager height:20])];
        [_infoLabel setBackgroundColor:TEST_BG_TWO];
        
        [self addSubview:_infoLabel];
        
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:175], 0, [UtilManager width:100], self.frame.size.height)];
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        [[_imageView layer] setMasksToBounds:YES];
        
        [self addSubview:_imageView];
        
        
        UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image-arrow-down-map"]];
        [arrow setFrame:CGRectMake(0., self.frame.size.height, arrow.frame.size.width, arrow.frame.size.height)];
        [arrow setCenter:CGPointMake(self.center.x, arrow.center.y)];
        [self addSubview:arrow];
    }
    
    return self;
}

@end
