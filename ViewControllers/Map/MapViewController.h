//
//  MapViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "ParentViewController.h"
#import "Reward.h"
#import "InterestDetail.h"
#import "Event.h"
#import "Tours.h"

@class MapRoundView;

typedef enum {
    MapSelectTypeRewards,
    MapSelectTypeTours,
    MapSelectTypeInfo,
    MapSelectTypeFriends,
    MapSelectTypeNone
} MapSelectType;

@interface MapViewController : ParentViewController


@property (nonatomic, strong) NSMutableArray *mapAnnotations;
@property (nonatomic, strong) MKMapView *mapView ;
@property (nonatomic, weak) MKAnnotationView *selectedAnnotationView;

@property (nonatomic, strong) InterestDetail *interestDetail;
@property (nonatomic, strong) Reward *currentReward;
@property (nonatomic, strong) InterestDetail *currentInterestDetail;
@property (nonatomic, strong) Event *currentEvent;

@property (nonatomic, strong) CLLocationManager *manager;
@property (nonatomic, strong) MapRoundView *mapRoundView;
@property (nonatomic, assign) MapSelectType mapSelectType;
@property (nonatomic, assign) BOOL userIsLocated;

@property (nonatomic, strong) UIButton *mapButton;
@property (nonatomic, strong) MKRoute *route;

@property (nonatomic, assign) BOOL centerAnimation;

@property (nonatomic, strong) UIView *containerMapView;


- (void)loadMapId:(NSString *)mapId;

- (void)toursInterestAnnotation:(NSArray *)toursInterestAnnotations;
- (void)configureView;
@end
