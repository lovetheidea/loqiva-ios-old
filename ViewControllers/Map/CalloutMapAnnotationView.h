//
//  CalloutMapAnnotationView.h
//  loqiva
//
//  Created by Manuel Manzanera on 12/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "CalloutMapAnnotation.h"

@protocol CalloutMapAnnotationViewDelegate;

@interface CalloutMapAnnotationView : MKAnnotationView
{
    MKAnnotationView *_parentAnnotationView;
    MKMapView *_mapView;
    CGRect _endFrame;
    UIView *_contentView;
    CGFloat _yShadowOffset;
    CGPoint _offsetFromParent;
    CGFloat _contentHeight;
}

@property (nonatomic, strong) MKAnnotationView *parentAnnotationView;
@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) CalloutMapAnnotation *calloutMapAnnotation;
@property (nonatomic, readonly) UIView *contentView;
@property (nonatomic) CGPoint offsetFromParent;
@property (nonatomic) CGFloat contentHeight;
@property (nonatomic, assign) id<CalloutMapAnnotationViewDelegate>delegate;

- (void)animateIn;
- (void)animateInStepTwo;
- (void)animateInStepThree;

@end


@protocol CalloutMapAnnotationViewDelegate <NSObject>

- (void)calloutDidPush:(CalloutMapAnnotation *)calloutMapAnnotation;

@end
