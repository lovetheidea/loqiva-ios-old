//
//  MapViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "MapViewController.h"
#import "NavCustomView.h"
#import "MapRoundView.h"
#import "AppContext.h"
#import "CustomAnnotation.h"
#import "CustomPinAnnotationView.h"
#import "CalloutMapAnnotation.h"
#import "CalloutMapAnnotationView.h"
#import "Reward.h"
#import "Tours.h"
#import "MapItem.h"
#import "WebServiceManager.h"
#import "InterestDetail.h"
#import "TemporaryUserViewController.h"
#import "RegisterUserViewController.h"
#import "SignNavController.h"
#import "UIColor+Helper.h"
#import "AttractionViewController.h"
#import "RewardsDetailViewController.h"
#import "ServiceDetailViewController.h"
#import "ToursDetailViewController.h"
#import "UITextView+Extra.h"
#import "Utility.h"
#import "LocationManager.h"
#import "loqiva-Swift.h"

@interface MapViewController ()<CLLocationManagerDelegate, MKMapViewDelegate, MKAnnotation,MapRoundViewDelegate, WebServiceManagerDelegate>

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _userIsLocated = FALSE;
    _centerAnimation = FALSE;
    
    self.manager = [[CLLocationManager alloc] init];
    self.manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    self.manager.distanceFilter = kCLDistanceFilterNone;
    [self.manager startUpdatingLocation];
    [self.manager requestWhenInUseAuthorization];
    
    if (![AppDelegate showAlertIfDisconnected]) {

        [self requestRewards];
        [self requestAttractions];
        [self requestServices];
        [self requestTours];
    }
    
    [self configureView];
    [self createMenuDrawer];
    [self setupMenuDrawer];
}

- (void)loadMapId:(NSString *)mapId {
    [self loadInterestDetailsWithIdentifier:mapId];
}

- (void)configureView{
    
    if (!self.mapView) {
        self.mapView = [[MKMapView alloc] initWithFrame:self.view.frame];
        [self.view addSubview:self.mapView];
    }
    [self.mapView setMapType:MKMapTypeStandard];
    [self.mapView setDelegate:self];
    [self.mapView setShowsCompass:FALSE];
    [self.mapView setShowsTraffic:FALSE];
    [self.mapView setShowsBuildings:YES];
    [self.mapView setShowsUserLocation:YES];
    [self.mapView setShowsPointsOfInterest:YES];
}

- (void)createMenuDrawer {
    
    if (!self.mapRoundView) {
        self.mapRoundView = [[MapRoundView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:300], [UtilManager width:300])];
        self.mapRoundView.isCollapsed = true;
        [self.view addSubview:self.mapRoundView];
    }
    
    [[self.mapRoundView layer] setBorderColor:[UIColor whiteColor].CGColor];
    [[self.mapRoundView layer] setBorderWidth:1];
    [[self.mapRoundView layer] setCornerRadius:[UtilManager width:300/2]];
    [self.mapRoundView setDelegate:self];
    [self.mapRoundView setCenter:CGPointMake(self.view.center.x, HEIGHT - [UtilManager height:65])];
    
    UIImage *mapButtonImage =  [UIImage imageNamed:@"btn-map"];
    
    if (!_containerMapView) {
        _containerMapView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., mapButtonImage.size.width + [UtilManager width:15], mapButtonImage.size.width + [UtilManager width:15])];
//        [_containerMapView setBackgroundColor:[UIColor whiteColor]];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapAction)];
        [_containerMapView addGestureRecognizer:tapGesture];
        
        [self.view addSubview:_containerMapView];
    }
    
    [self.view addSubview:[self navigationView]];
    
    [_containerMapView setCenter:self.mapRoundView.center];
    [[_containerMapView layer] setBorderColor:[UIColor whiteColor].CGColor];
    [[_containerMapView layer] setBorderWidth:1];
    [[_containerMapView layer] setCornerRadius:_containerMapView.frame.size.width/2];
    
    _containerMapView.layer.shadowColor = [GRAY_TEXT_HOME_COLOR CGColor];
    _containerMapView.layer.shadowOffset = CGSizeMake(.0f, .0f);
    _containerMapView.layer.shadowRadius = 4;
    _containerMapView.layer.shadowOpacity = 0.8f;
    _containerMapView.layer.masksToBounds = NO;
    
    if (!_mapButton) {
        _mapButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., mapButtonImage.size.width * 0.8, mapButtonImage.size.height * 0.8)];
        [_containerMapView addSubview:_mapButton];
    }
    [_mapButton setCenter:CGPointMake(_containerMapView.frame.size.width/2, _containerMapView.frame.size.height/2)];
    [_mapButton setImage:mapButtonImage forState:UIControlStateNormal];
    [_mapButton addTarget:self action:@selector(mapAction) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.mapSelectType == MapSelectTypeNone) {
        [self.mapRoundView setMapSelectType:MapSelectTypeNone];
    }
    else {
        [self.mapRoundView setMapSelectType:self.mapSelectType];
    }
    
    [self addAnnotations];
    [self zoomToFitMapAnnotations:self.mapView];
}

- (UIView *)navigationView {
    
    // Mitch Header nav for map
    NavCustomView *navCustomView = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight:true];
    [[navCustomView icon] setImage:[UIImage imageNamed:@"icon-map"]];
    [[navCustomView iconLabel] setAttributedText:NSLocalizedString(@"Map", nil) withLineHeight:NAV_CUSTOM_VIEW_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE] andColor:[UIColor whiteColor] andKern:NAV_CUSTOM_VIEW_KERN withLineBreak:NO];
    navCustomView.parentController = self;
    NSLog(@"FRFRFRFRF ::: %lu",(long unsigned)self.navigationController.viewControllers.count);
    if(self.navigationController.viewControllers.count > 1)
        [navCustomView setBackOption];
    
        
    
    return navCustomView;
}

- (void)mapAction{
    
    [self toggleMenuDrawer];
}

- (void)addAnnotations{
    
    if(self.mapAnnotations) {
        [self.mapView removeAnnotations:self.mapAnnotations];
        self.mapAnnotations = nil;
    }
    
    [self.mapView removeOverlays:self.mapView.overlays];
    self.mapAnnotations = [[NSMutableArray alloc] init];
    
    if(self.mapSelectType == MapSelectTypeRewards)
    {
        if(self.currentReward){
            [self addRewardRoute];
        } else
            [self addMapAnnotationsForRewards];
    }
    else if(self.mapSelectType == MapSelectTypeTours)
    {
        if(self.currentInterestDetail)
            [self addInterestDetailRoute];
        else
            [self.mapAnnotations addObjectsFromArray:[self toursAnnotation]];
    }
    else if(self.mapSelectType == MapSelectTypeInfo){
        if(self.currentInterestDetail)
            [self addInterestDetailRoute];
        else if(self.currentEvent)
            [self addEventRoute];
        else
            [self.mapAnnotations addObjectsFromArray:[self attractionsAnnotation]];
    }
    else if(self.mapSelectType == MapSelectTypeFriends)
        [self.mapAnnotations addObjectsFromArray:[self servicesAnnotation]];
    else if (self.mapSelectType == MapSelectTypeNone){
        [self addMapAnnotationsForRewards];
        [self.mapAnnotations addObjectsFromArray:[self toursAnnotation]];
        [self.mapAnnotations addObjectsFromArray:[self attractionsAnnotation]];
        [self.mapAnnotations addObjectsFromArray:[self servicesAnnotation]];
    }
    [self.mapView addAnnotations:self.mapAnnotations];
    
    [self zoomToFitMapAnnotations:self.mapView];
}

- (void)addEventRoute{
    CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(self.currentEvent.latitude.doubleValue, self.currentEvent.longitude.doubleValue) title:self.currentEvent.title summary:self.currentEvent.summary subtitle:@"" pin:@"image-pin-info"];
    [annotation setAccessibilityLabel:self.currentEvent.title];
    [annotation setImage:self.currentEvent.mediaURL];
    [annotation setType:@"Attraction"];
    [annotation setInterestId:[NSString stringWithFormat:@"%d",self.currentEvent.eventId.intValue]];
    [annotation setData:self.currentEvent];
     
    [self.mapView addAnnotations:@[annotation]];
    
    MKPlacemark *placemarkSrc = [[MKPlacemark alloc] initWithCoordinate:[[[LocationManager sharedInstance]currentLocation] coordinate] addressDictionary:nil];
    MKMapItem *mapItemSrc = [[MKMapItem alloc] initWithPlacemark:placemarkSrc];
    MKPlacemark *placemarkDest = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(self.currentEvent.latitude.doubleValue, self.currentEvent.longitude.doubleValue) addressDictionary:nil];
    MKMapItem *mapItemDest = [[MKMapItem alloc] initWithPlacemark:placemarkDest];
    [mapItemSrc setName:self.currentEvent.title];
    [mapItemDest setName:[[SessionManager shared] user].firstName];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    [request setSource:mapItemSrc];
    [request setDestination:mapItemDest];
    [request setTransportType:MKDirectionsTransportTypeWalking];
    request.requestsAlternateRoutes = NO;
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     ^(MKDirectionsResponse *response, NSError *error) {
         if (error) {
             // Handle Error
         } else {
             [self.mapView removeOverlays:self.mapView.overlays];
             [self showRoute:response];
         }
     }];
}

- (void)addInterestDetailRoute{
    CustomAnnotation *annotation;
    if(self.currentInterestDetail.imageType.length > 0)
        annotation = [[CustomAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(self.currentInterestDetail.latitude.doubleValue, self.currentInterestDetail.longitude.doubleValue) title:self.currentInterestDetail.title summary:self.currentInterestDetail.summary subtitle:@"" pin:[NSString stringWithFormat:@"image-pin-%@",self.currentInterestDetail.imageType]];
    else
        annotation = [[CustomAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(self.currentInterestDetail.latitude.doubleValue, self.currentInterestDetail.longitude.doubleValue) title:self.currentInterestDetail.title summary:self.currentInterestDetail.summary subtitle:@"" pin:@"image-pin-info"];
    [annotation setAccessibilityLabel:self.currentInterestDetail.title];
    [annotation setImage:self.currentInterestDetail.mediaURL];
    [annotation setType:@"Attraction"];
    [annotation setInterestId:[NSString stringWithFormat:@"%d",self.currentInterestDetail.interestId.intValue]];
    [annotation setData:self.currentInterestDetail];
    
    [self.mapView addAnnotations:@[annotation]];
    
    MKPlacemark *placemarkSrc = [[MKPlacemark alloc] initWithCoordinate:[[[LocationManager sharedInstance]currentLocation] coordinate] addressDictionary:nil];
    MKMapItem *mapItemSrc = [[MKMapItem alloc] initWithPlacemark:placemarkSrc];
    MKPlacemark *placemarkDest = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(self.currentInterestDetail.latitude.doubleValue, self.currentInterestDetail.longitude.doubleValue) addressDictionary:nil];
    MKMapItem *mapItemDest = [[MKMapItem alloc] initWithPlacemark:placemarkDest];
    [mapItemSrc setName:self.currentInterestDetail.title];
    [mapItemDest setName:[[SessionManager shared] user].firstName];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    [request setSource:mapItemSrc];
    [request setDestination:mapItemDest];
    [request setTransportType:MKDirectionsTransportTypeWalking];
    request.requestsAlternateRoutes = NO;
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     ^(MKDirectionsResponse *response, NSError *error) {
         if (error) {
             // Handle Error
         } else {
             [self.mapView removeOverlays:self.mapView.overlays];
             [self showRoute:response];
         }
     }];
}

- (void)addRewardRoute{
    
    CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(self.currentReward.latitude.doubleValue, self.currentReward.longitude.doubleValue) title:self.currentReward.title  summary:self.currentReward.summary subtitle:@"" pin:@"image-pin-reward"];
    [annotation setAccessibilityLabel:self.currentReward.title];
    [annotation setImage:self.currentReward.mediaurl];
    [annotation setType:@"Reward"];
    [annotation setInterestId:[NSString stringWithFormat:@"%d",self.currentReward.mapitempoint.intValue]];
    [annotation setData:self.currentReward];
    
    [self.mapView addAnnotations:@[annotation]];
    
    MKPlacemark *placemarkSrc = [[MKPlacemark alloc] initWithCoordinate:[[[LocationManager sharedInstance]currentLocation] coordinate] addressDictionary:nil];
    MKMapItem *mapItemSrc = [[MKMapItem alloc] initWithPlacemark:placemarkSrc];
    MKPlacemark *placemarkDest = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(self.currentReward.latitude.doubleValue, self.currentReward.longitude.doubleValue) addressDictionary:nil];
    MKMapItem *mapItemDest = [[MKMapItem alloc] initWithPlacemark:placemarkDest];
    [mapItemSrc setName:self.currentReward.title];
    [mapItemDest setName:[[SessionManager shared] user].firstName];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    [request setSource:mapItemSrc];
    [request setDestination:mapItemDest];
    [request setTransportType:MKDirectionsTransportTypeWalking];
    request.requestsAlternateRoutes = NO;
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     ^(MKDirectionsResponse *response, NSError *error) {
         if (error) {
             // Handle Error
         } else {
             [self.mapView removeOverlays:self.mapView.overlays];
             [self showRoute:response];
         }
     }];
}

-(void)showRoute:(MKDirectionsResponse *)response
{
    for (MKRoute *route in response.routes)
    {
        _route = route;
        [self.mapView
         addOverlay:route.polyline level:MKOverlayLevelAboveRoads];
        
        for (MKRouteStep *step in route.steps)
        {
            NSLog(@"%@", step.instructions);
        }
    }
}

- (NSArray *)toursAnnotation{
    NSArray *toursList = [[TourCache shared] cachedTours];
    NSMutableArray *auxList = [[NSMutableArray alloc] init];
    for(Tours *tours in toursList)
    {
        if(tours.startPointLat.doubleValue != 0 && tours.startPointLong.doubleValue != 0)
        {
            InterestDetail *toursInterestAnnotations = tours.interestPoints.firstObject;
            CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(tours.startPointLat.doubleValue, tours.startPointLong.doubleValue) title:tours.title summary:toursInterestAnnotations.summary subtitle:@"" pin:@"image-pin-tours"];
            [annotation setAccessibilityLabel:tours.title];
            [annotation setType:@"Tours"];
            [annotation setImage:tours.imageURL];
            [annotation setInterestId:[NSString stringWithFormat:@"%d",tours.walkId.intValue]];
            [annotation setData:tours];
            
            [auxList addObject:annotation];
        }
    }
    return auxList;
}

- (void)toursInterestAnnotation:(Tours *)tours{
    
    NSArray *toursInterestAnnotations = tours.interestPoints;
    NSMutableArray *auxList = [[NSMutableArray alloc] init];
    for(InterestDetail *tours in toursInterestAnnotations)
    {
        if(tours.latitude.doubleValue != 0 && tours.longitude.doubleValue != 0)
        {
            CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(tours.latitude.doubleValue, tours.longitude.doubleValue) title:tours.title summary:tours.summary subtitle:@"" pin:@"image-pin-tour-information"];
            [annotation setAccessibilityLabel:tours.title];
            [annotation setType:@"Tours Information"];
            [annotation setImage:@""];
            [annotation setInterestId:[NSString stringWithFormat:@"%d",tours.interestId.intValue]];
            [annotation setData:tours];
            
            [auxList addObject:annotation];
        }
    }
    
    if(auxList.count > 0){
        [self.mapView removeAnnotations:self.mapAnnotations];
        [self.mapAnnotations removeAllObjects];
        [self.mapAnnotations addObjectsFromArray:auxList];
        [self.mapView addAnnotations:self.mapAnnotations];
        [self zoomToFitMapAnnotations:self.mapView];
    }
    
    [self plotRoutesOverlay: tours];
}

- (NSArray *)attractionsAnnotation {
    NSArray *mapItems = [[AttractionCache shared] cachedMapItems];
    NSMutableArray *auxList = [[NSMutableArray alloc] init];
    for(MapItem *mapItem in mapItems)
    {
        if(mapItem.latitude.doubleValue != 0 && mapItem.longitude.doubleValue != 0)
        {
            CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(mapItem.latitude.doubleValue, mapItem.longitude.doubleValue) title:mapItem.title summary:mapItem.title subtitle:@"" pin:[NSString stringWithFormat:@"image-pin-%@",mapItem.imageType]];
            [annotation setAccessibilityLabel:mapItem.title];
            [annotation setType:@"Attraction"];
            [annotation setImage:@""];
            [annotation setInterestId:[NSString stringWithFormat:@"%d",mapItem.mapItemId.intValue]];
            [annotation setData:mapItem];
            
            [self.mapAnnotations addObject:annotation];
        }
    }
    return auxList;
}

- (NSArray *)servicesAnnotation{
    NSArray *mapItems =  [[ServiceCache shared] cachedMapItems];
    NSMutableArray *auxList = [[NSMutableArray alloc] init];
    for(MapItem *mapItem in mapItems)
    {
        if(mapItem.latitude.doubleValue != 0 && mapItem.longitude.doubleValue != 0)
        {
            CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(mapItem.latitude.doubleValue, mapItem.longitude.doubleValue) title:mapItem.title summary:mapItem.title subtitle:@"" pin:[NSString stringWithFormat:@"image-pin-%@",mapItem.imageType]];
            [annotation setType:@"Service"];
            [annotation setImage:@""];
            [annotation setInterestId:[NSString stringWithFormat:@"%d",mapItem.mapItemId.intValue]];
            [annotation setData:mapItem];
            
            [self.mapAnnotations addObject:annotation];
        }
    }
    return auxList;
}

-(void)zoomToFitMapAnnotations:(MKMapView*)mapView
{
    if([mapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(CustomAnnotation* annotation in mapView.annotations) {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.5; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.5; // Add a little extra space on the sides
    
    region = [mapView regionThatFits:region];
    
    NSInteger paddingFromBottom = 100;
    UIEdgeInsets paddingInsets = UIEdgeInsetsMake(0, 0, paddingFromBottom, 0);
    [mapView setVisibleRegionWithMapRegion:region edgePadding:paddingInsets animated:YES];
}

- (void)plotRoutesOverlay:(Tours *)tours{
    
    for(int i = 1;i<tours.interestPoints.count; i++){
    
        InterestDetail *interestDetail = [tours.interestPoints objectAtIndex:i];
        InterestDetail *interestDetailOrigin = [tours.interestPoints objectAtIndex:i-1];
        
        MKPlacemark *placemarkSrc = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(interestDetailOrigin.latitude.doubleValue, interestDetailOrigin.longitude.doubleValue) addressDictionary:nil];
        MKMapItem *mapItemSrc = [[MKMapItem alloc] initWithPlacemark:placemarkSrc];
        MKPlacemark *placemarkDest = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(interestDetail.latitude.doubleValue, interestDetail.longitude.doubleValue) addressDictionary:nil];
        MKMapItem *mapItemDest = [[MKMapItem alloc] initWithPlacemark:placemarkDest];
        [mapItemSrc setName:interestDetailOrigin.title];
        [mapItemDest setName:interestDetail.title];
        
        MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
        [request setSource:mapItemSrc];
        [request setDestination:mapItemDest];
        [request setTransportType:MKDirectionsTransportTypeWalking];
        request.requestsAlternateRoutes = NO;
        
        MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
        
        [directions calculateDirectionsWithCompletionHandler:
         ^(MKDirectionsResponse *response, NSError *error) {
             if (error) {
                 // Handle Error
             } else {
                 [self showRoute:response];
             }
         }];
    }
}


#pragma mark MKMapViewDelegate Methods

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{

}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{

}

- (void)mapViewWillStartLoadingMap:(MKMapView *)mapView{

}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView{
    
}

- (void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error{

}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    printf("map view annotion selected");
    [self mapViewDidSelectAnnotationViewWithAnnotationView:view];
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    [self clearSelectedAnnotationData];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation NS_AVAILABLE(10_9, 4_0)
{
    if(!_userIsLocated)
    {
        _userIsLocated = true;
        [self zoomToFitMapAnnotationsWithUser:self.mapView];
    }
}

-(void)zoomToFitMapAnnotationsWithUser:(MKMapView*)mapView
{
    if([self.mapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(CustomAnnotation* annotation in self.mapView.annotations)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.5; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.5; // Add a little extra space on the sides
    
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *identifier = @"pinAnnotation";
    if ([annotation isKindOfClass:[CustomAnnotation  class]])
    {
        CustomPinAnnotationView *annotationView = (CustomPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];

        if(!annotationView)
        {
            annotationView = [[CustomPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        }

        RewardCalloutDetailView *detailCalloutAccessoryView = [self createRewardCalloutDetailViewFrom:annotation];
        [annotationView setDetailCalloutAccessoryView:detailCalloutAccessoryView];
        [annotationView setCanShowCallout:YES];

        return annotationView;
    } else {
        return [self.mapView viewForAnnotation:self.mapView.userLocation];
    }
}

- (void)viewTap
{
    printf("viewTap selected");
    NSLog(@"%s", "viewTap selected");
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    printf("didAddAnnotionViews tapped");
    NSLog(@"%s", "didAddAnnotionViews tapped");
//    for (MKAnnotationView * annView in views) {
//        [[annView superview] bringSubviewToFront:annView];
//    }
}

- (void)mapView:(MKMapView *)mapview annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
}


// Mitch Master Map Variables
- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay NS_AVAILABLE(10_9, 7_0){
    
    MKPolylineRenderer* lineView;
    
    if(_route)
        lineView = [[MKPolylineRenderer alloc] initWithPolyline:_route.polyline];
    else
        lineView = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
    lineView.strokeColor = [UtilManager colorwithHexString:@"#00b4ff" alpha:1];
    lineView.lineWidth = 7;
    lineView.lineDashPattern = @[@1, @14];
    return lineView;
}

- (void)mapView:(MKMapView *)mapView didAddOverlayRenderers:(NSArray<MKOverlayRenderer *> *)renderers{

}

#pragma mark MapVRoundViewDelegate Methods

- (void)mapSelectType:(MapSelectType)mapSelectType{
    [self.mapView removeAnnotations:self.mapAnnotations];
    self.mapSelectType = mapSelectType;
    [self addAnnotations];
}

#pragma mark WebServiceDelegate Methods

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    if(webServiceType == WebServiceTypeToursDetail) {
        Tours *tours = (Tours *)object;
        [self.mapView removeAnnotations:self.mapAnnotations];
        [self toursInterestAnnotation: tours];
    }
    else if (webServiceType == WebServiceTypeTours) {
        [self configureView];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{

}

@synthesize coordinate;

@end
