//
//  MapRoundView.m
//  loqiva
//
//  Created by Manuel Manzanera on 3/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "MapRoundView.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "RNBlurModalView.h"
#import "loqiva-Swift.h"

@implementation MapRoundView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        UIView *blurView = [[UIView alloc] initWithFrame:self.frame];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        [blurEffectView setFrame:self.bounds];
        [[blurView layer] setCornerRadius:frame.size.width/2];
        [[blurEffectView layer] setCornerRadius:frame.size.width/2];
        [[blurEffectView layer] setMasksToBounds:YES];
        [[blurView layer] setMasksToBounds:YES];
        blurView = blurEffectView;
        
        [self addSubview:blurView];
        
        UIImage *rewardImage = [UIImage imageNamed:@"btn-reward-map"];
        _rewardsButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:15], 0., rewardImage.size.width * 0.9, rewardImage.size.height * 0.9)];
        [_rewardsButton setImage:rewardImage forState:UIControlStateNormal];
        [_rewardsButton setCenter:CGPointMake(_rewardsButton.center.x, self.center.y)];
        [_rewardsButton addTarget:self action:@selector(mapSelectAction:) forControlEvents:UIControlEventTouchUpInside];
        [_rewardsButton setTag:MapSelectTypeRewards];
        
        [self addSubview:_rewardsButton];
        
        UIImage *friendImage = [UIImage imageNamed:@"btn-friend-map"];
        _friendButton = [[UIButton alloc] initWithFrame:CGRectMake(self.bounds.size.width - ([UtilManager width:15] + _rewardsButton.frame.size.width), 0., _rewardsButton.frame.size.width, _rewardsButton.frame.size.height)];
        [_friendButton setImage:friendImage forState:UIControlStateNormal];
        [_friendButton setCenter:CGPointMake(_friendButton.center.x, self.center.y)];
        [_friendButton addTarget:self action:@selector(mapSelectAction:) forControlEvents:UIControlEventTouchUpInside];
        [_friendButton setTag:MapSelectTypeFriends];
        [_friendButton setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin & UIViewAutoresizingFlexibleTopMargin];
        [self addSubview:_friendButton];
        
        UIImage *infoImage = [UIImage imageNamed:@"btn-info-map"];
        _infoButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:63], [UtilManager height:25], _rewardsButton.frame.size.width, _rewardsButton.frame.size.height)];
        [_infoButton setImage:infoImage forState:UIControlStateNormal];
        [_infoButton addTarget:self action:@selector(mapSelectAction:) forControlEvents:UIControlEventTouchUpInside];
        [_infoButton setTag:MapSelectTypeInfo];
        
        [self addSubview:_infoButton];
        
        UIImage *routeImage = [UIImage imageNamed:@"btn-tours-map"];
        _tourButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:159], _infoButton.frame.origin.y, _rewardsButton.frame.size.width, _rewardsButton.frame.size.height)];
        [_tourButton setImage:routeImage forState:UIControlStateNormal];
        [_tourButton addTarget:self action:@selector(mapSelectAction:) forControlEvents:UIControlEventTouchUpInside];
        [_tourButton setTag:MapSelectTypeTours];
        
        [self addSubview:_tourButton];
    }
    
    return self;


}

- (void)mapSelectAction:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    
    switch (button.tag) {
        case MapSelectTypeRewards:
            if(_mapSelectType == MapSelectTypeRewards)
                [self unselectedMapView];
            else {
                [_rewardsButton setImage:[UIImage imageNamed:@"btn-reward-map-selected"] forState:UIControlStateNormal];
                [_tourButton setImage:[UIImage imageNamed:@"btn-tours-tours-unselected"] forState:UIControlStateNormal];
                [_infoButton setImage:[UIImage imageNamed:@"btn-info-map-unselected"] forState:UIControlStateNormal];
                [_friendButton setImage:[UIImage imageNamed:@"btn-friend-map-unselected"] forState:UIControlStateNormal];
                _mapSelectType = MapSelectTypeRewards;
            }
            break;
        case MapSelectTypeTours:
            if(_mapSelectType == MapSelectTypeTours)
                [self unselectedMapView];
            else {
                [_rewardsButton setImage:[UIImage imageNamed:@"btn-reward-map-unselected"] forState:UIControlStateNormal];
                [_tourButton setImage:[UIImage imageNamed:@"btn-tours-map-selected"] forState:UIControlStateNormal];
                [_infoButton setImage:[UIImage imageNamed:@"btn-info-map-unselected"] forState:UIControlStateNormal];
                [_friendButton setImage:[UIImage imageNamed:@"btn-friend-map-unselected"] forState:UIControlStateNormal];
                _mapSelectType = MapSelectTypeTours;
            }
            break;
        case MapSelectTypeInfo:
            if(_mapSelectType == MapSelectTypeInfo)
                [self unselectedMapView];
            else {
                [_rewardsButton setImage:[UIImage imageNamed:@"btn-reward-map-unselected"] forState:UIControlStateNormal];
                [_tourButton setImage:[UIImage imageNamed:@"btn-tours-tours-unselected"] forState:UIControlStateNormal];
                [_infoButton setImage:[UIImage imageNamed:@"btn-venues-selected"] forState:UIControlStateNormal];
                [_friendButton setImage:[UIImage imageNamed:@"btn-friend-map-unselected"] forState:UIControlStateNormal];
                _mapSelectType = MapSelectTypeInfo;
            }
            break;
        case MapSelectTypeFriends:
            if(_mapSelectType == MapSelectTypeFriends)
                [self unselectedMapView];
            else {
                [_rewardsButton setImage:[UIImage imageNamed:@"btn-reward-map-unselected"] forState:UIControlStateNormal];
                [_tourButton setImage:[UIImage imageNamed:@"btn-tours-tours-unselected"] forState:UIControlStateNormal];
                [_infoButton setImage:[UIImage imageNamed:@"btn-info-map-unselected"] forState:UIControlStateNormal];
                [_friendButton setImage:[UIImage imageNamed:@"btn-friend-map-selected"] forState:UIControlStateNormal];
                _mapSelectType = MapSelectTypeFriends;
            }
            break;
        default:
            break;
    }
    
    if([_delegate respondsToSelector:@selector(mapSelectType:)])
        [_delegate mapSelectType:_mapSelectType];
}

- (void)unselectedMapView{
     _mapSelectType = MapSelectTypeNone;
    [_rewardsButton setImage:[UIImage imageNamed:@"btn-reward-map"] forState:UIControlStateNormal];
    [_tourButton setImage:[UIImage imageNamed:@"btn-tours-map"] forState:UIControlStateNormal];
    [_infoButton setImage:[UIImage imageNamed:@"btn-info-map"] forState:UIControlStateNormal];
    [_friendButton setImage:[UIImage imageNamed:@"btn-friend-map"] forState:UIControlStateNormal];
}

- (void)setMapSelectType:(MapSelectType)mapSelectType{
    _mapSelectType = mapSelectType;
    
    if(_mapSelectType == MapSelectTypeRewards){
        [_rewardsButton setImage:[UIImage imageNamed:@"btn-reward-map-selected"] forState:UIControlStateNormal];
        [_tourButton setImage:[UIImage imageNamed:@"btn-tours-tours-unselected"] forState:UIControlStateNormal];
        [_infoButton setImage:[UIImage imageNamed:@"btn-info-map-unselected"] forState:UIControlStateNormal];
        [_friendButton setImage:[UIImage imageNamed:@"btn-friend-map-unselected"] forState:UIControlStateNormal];
    }
}

@end
