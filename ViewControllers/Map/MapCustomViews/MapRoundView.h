//
//  MapRoundView.h
//  loqiva
//
//  Created by Manuel Manzanera on 3/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapViewController.h"

@protocol MapRoundViewDelegate;


@interface MapRoundView : UIView

@property (nonatomic, strong) UIButton *rewardsButton;
@property (nonatomic, strong) UIButton *infoButton;
@property (nonatomic, strong) UIButton *tourButton;
@property (nonatomic, strong) UIButton *friendButton;
@property (nonatomic, assign) MapSelectType mapSelectType;
@property (nonatomic, assign) BOOL isCollapsed;
@property (nonatomic, assign) id<MapRoundViewDelegate>delegate;
@end

@protocol MapRoundViewDelegate <NSObject>

- (void)mapSelectType:(MapSelectType)mapSelectType;

@end
