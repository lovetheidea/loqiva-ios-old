//
//  CustomAnnotation.m
//  loqiva
//
//  Created by Manuel Manzanera on 9/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "CustomAnnotation.h"
#import "loqiva-Swift.h"

@implementation CustomAnnotation

@synthesize coordinate;
@synthesize title,subtitle, summary;
@synthesize pin, interestId;
@synthesize data;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coord
                   title:(NSString *)aTitle
                 summary:(NSString *)aSummary
                subtitle:(NSString *)asubtitle
                     pin:(NSString *)_pin
{
    self = [super init];
    if (self != nil)
    {
        coordinate = coord;
        title = @"";    // this removes the title from the callout view
        subtitle=aTitle;
        summary=aSummary;
        pin = _pin;
    }
    return self;
}

- (NSString *)title
{
    return title;
}

- (NSString *)subtitle
{
    return subtitle;
}

- (NSString *)summary {
    return summary;
}

@end
