//
//  CustomPinAnnotationView.h
//  loqiva
//
//  Created by Manuel Manzanera on 9/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface CustomPinAnnotationView : MKAnnotationView

@property (nonatomic, strong) UIImageView *iconView;

@end
