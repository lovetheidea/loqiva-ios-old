//
//  CalloutMapAnnotationView.m
//  loqiva
//
//  Created by Manuel Manzanera on 12/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "CalloutMapAnnotationView.h"
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>
#import "AppConstants.h"
#import "UtilManager.h"
#import "loqiva-Swift.h"

#define CalloutMapAnnotationViewBottomShadowBufferSize 6.0f
#define CalloutMapAnnotationViewContentHeightBuffer 8.0f
#define CalloutMapAnnotationViewHeightAboveParent 2.0f
#define CallaoutWidth [UtilManager width:250.]
#define CallaoutHeight [UtilManager height:100.]

@interface CalloutMapAnnotationView()

@property (nonatomic, readonly) CGFloat yShadowOffset;
@property (nonatomic) BOOL animateOnNextDrawRect;
@property (nonatomic) CGRect endFrame;

- (void)prepareContentFrame;
- (void)prepareFrameSize;
- (void)prepareOffset;
- (CGFloat)relativeParentXPosition;
- (void)adjustMapRegionIfNeeded;

@end

@implementation CalloutMapAnnotationView

- (id) initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier])
    {
        self.contentHeight = CallaoutHeight;
        self.offsetFromParent = CGPointMake([UtilManager width:25],0); //this works for MKPinAnnotationView
        self.enabled = YES;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)setAnnotation:(id <MKAnnotation>)annotation
{
    [super setAnnotation:annotation];
    [self prepareFrameSize];
    [self prepareOffset];
    [self prepareContentFrame];
    [self setNeedsDisplay];
}

- (void)prepareFrameSize
{
    CGRect frame = self.frame;
    CGFloat height =	CallaoutHeight +
    CalloutMapAnnotationViewContentHeightBuffer +
    CalloutMapAnnotationViewBottomShadowBufferSize -
    self.offsetFromParent.y;
    
    frame.size = CGSizeMake(CallaoutWidth, height);
    self.frame = frame;
}

- (void)prepareContentFrame
{
    CGRect contentFrame = CGRectMake(0,
                                     self.bounds.origin.y + 3,
                                     CallaoutWidth,
                                     CallaoutHeight);
    
    self.contentView.frame = contentFrame;
}

- (void)prepareOffset
{
    CGPoint parentOrigin = [self.mapView convertPoint:self.parentAnnotationView.frame.origin
                                             fromView:self.parentAnnotationView.superview];
    
    CGFloat xOffset =	(self.mapView.frame.size.width / 2) - (parentOrigin.x + self.offsetFromParent.x);
    
    //Add half our height plus half of the height of the annotation we are tied to so that our bottom lines up to its top
    //Then take into account its offset and the extra space needed for our drop shadow
    CGFloat yOffset = -(self.frame.size.height / 2 +
                        self.parentAnnotationView.frame.size.height / 2) +
    self.offsetFromParent.y +
    CalloutMapAnnotationViewBottomShadowBufferSize;
    
    self.centerOffset = CGPointMake(xOffset, yOffset);
}

//if the pin is too close to the edge of the map view we need to shift the map view so the callout will fit.
- (void)adjustMapRegionIfNeeded
{
    //Longitude
    CGFloat xPixelShift = 0;
    if ([self relativeParentXPosition] < 38)
    {
        xPixelShift = 38 - [self relativeParentXPosition];
    }
    else if ([self relativeParentXPosition] > self.frame.size.width - 38)
    {
        xPixelShift = (self.frame.size.width - 38) - [self relativeParentXPosition];
    }
    
    
    //Latitude
    CGPoint mapViewOriginRelativeToParent = [self.mapView convertPoint:self.mapView.frame.origin toView:self.parentAnnotationView];
    CGFloat yPixelShift = 0;
    CGFloat pixelsFromTopOfMapView = -(mapViewOriginRelativeToParent.y + self.frame.size.height - CalloutMapAnnotationViewBottomShadowBufferSize);
    CGFloat pixelsFromBottomOfMapView = self.mapView.frame.size.height + mapViewOriginRelativeToParent.y - self.parentAnnotationView.frame.size.height;
    if (pixelsFromTopOfMapView < 7)
    {
        yPixelShift = 7 - pixelsFromTopOfMapView;
    }
    else if (pixelsFromBottomOfMapView < 10)
    {
        yPixelShift = -(10 - pixelsFromBottomOfMapView);
    }
    
    //Calculate new center point, if needed
    if (xPixelShift || yPixelShift)
    {
        CGFloat pixelsPerDegreeLongitude = self.mapView.frame.size.width / self.mapView.region.span.longitudeDelta;
        CGFloat pixelsPerDegreeLatitude = self.mapView.frame.size.height / self.mapView.region.span.latitudeDelta;
        
        CLLocationDegrees longitudinalShift = -(xPixelShift / pixelsPerDegreeLongitude);
        CLLocationDegrees latitudinalShift = yPixelShift / pixelsPerDegreeLatitude;
        
        CLLocationCoordinate2D newCenterCoordinate = {self.mapView.region.center.latitude + latitudinalShift,
            self.mapView.region.center.longitude + longitudinalShift};
        
        [self.mapView setCenterCoordinate:newCenterCoordinate animated:YES];
        
        //fix for now
        self.frame = CGRectMake(self.frame.origin.x - xPixelShift,
                                self.frame.origin.y - yPixelShift,
                                self.frame.size.width,
                                self.frame.size.height);
        //fix for later (after zoom or other action that resets the frame)
        self.centerOffset = CGPointMake(self.centerOffset.x - xPixelShift, self.centerOffset.y);
    }
}

- (CGFloat)xTransformForScale:(CGFloat)scale
{
    CGFloat xDistanceFromCenterToParent = self.endFrame.size.width / 2 - [self relativeParentXPosition];
    return (xDistanceFromCenterToParent * scale) - xDistanceFromCenterToParent;
}

- (CGFloat)yTransformForScale:(CGFloat)scale
{
    CGFloat yDistanceFromCenterToParent = (((self.endFrame.size.height) / 2) + self.offsetFromParent.y + CalloutMapAnnotationViewBottomShadowBufferSize + CalloutMapAnnotationViewHeightAboveParent);
    return yDistanceFromCenterToParent - yDistanceFromCenterToParent * scale;
}

- (void)animateIn
{
    self.endFrame = self.frame;
    CGFloat scale = 0.001f;
    self.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, [self xTransformForScale:scale], [self yTransformForScale:scale]);
    [UIView beginAnimations:@"animateIn" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.075];
    [UIView setAnimationDidStopSelector:@selector(animateInStepTwo)];
    [UIView setAnimationDelegate:self];
    scale = 1.1;
    self.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, [self xTransformForScale:scale], [self yTransformForScale:scale]);
    [UIView commitAnimations];
}

- (void)animateInStepTwo
{
    [UIView beginAnimations:@"animateInStepTwo" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDidStopSelector:@selector(animateInStepThree)];
    [UIView setAnimationDelegate:self];
    
    CGFloat scale = 0.95;
    self.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, [self xTransformForScale:scale], [self yTransformForScale:scale]);
    
    [UIView commitAnimations];
}

- (void)animateInStepThree
{
    [UIView beginAnimations:@"animateInStepThree" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.075];
    
    CGFloat scale = 1.0;
    self.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, [self xTransformForScale:scale], [self yTransformForScale:scale]);
    
    [UIView commitAnimations];
}

- (void)didMoveToSuperview
{
    [self adjustMapRegionIfNeeded];
    [self animateIn];
}

- (void)drawRect:(CGRect)rect
{
}

- (CGFloat)yShadowOffset
{
    if (!_yShadowOffset)
    {
        float osVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        if (osVersion >= 3.2)
        {
            _yShadowOffset = 6;
        }
        else
        {
            _yShadowOffset = -6;
        }
        
    }
    return _yShadowOffset;
}

- (CGFloat)relativeParentXPosition
{
    CGPoint parentOrigin = [self.mapView convertPoint:self.parentAnnotationView.frame.origin
                                             fromView:self.parentAnnotationView.superview];
    return parentOrigin.x + self.offsetFromParent.x -[UtilManager width:65];
}

- (UIView *)contentView
{
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self addSubview:self.contentView];
        
        self.contentView.userInteractionEnabled = NO;
    }
    return _contentView;
}

- (void)calloutTouch
{
    [_delegate calloutDidPush:_calloutMapAnnotation];
}

- (void)dealloc
{
    self.parentAnnotationView = nil;
    self.mapView = nil;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint locationPoint = [[touches anyObject] locationInView:self.contentView];
    CGPoint viewPoint = [self.contentView convertPoint:locationPoint fromView:self.contentView];
    if ([self.contentView pointInside:viewPoint withEvent:event]) {
        NSLog(@"Tapped");
    }
}

@end
