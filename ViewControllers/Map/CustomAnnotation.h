//
//  CustomAnnotation.h
//  loqiva
//
//  Created by Manuel Manzanera on 9/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface CustomAnnotation : NSObject{
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString * subtitle;
    NSString * pin;
    NSString * summary;
    NSObject * data;
}

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;
@property (nonatomic, readonly, copy) NSString *summary;
@property (nonatomic, readonly, copy) NSString *pin;

@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *interestId;

@property (nonatomic, strong) NSObject * data;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coord
                   title:(NSString *)aTitle
                 summary:(NSString *)aSummary
                subtitle:(NSString *)asubtitle
                     pin:(NSString *)_pin;

@end
