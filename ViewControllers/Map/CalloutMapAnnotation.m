//
//  CalloutMapAnnotation.m
//  loqiva
//
//  Created by Manuel Manzanera on 12/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "CalloutMapAnnotation.h"
#import "loqiva-Swift.h"

// TODO remove this class
@implementation CalloutMapAnnotation

- (id)initWithLatitude:(CLLocationDegrees)latitude
          andLongitude:(CLLocationDegrees)longitude
              andTitle:(NSString *)titleAnnotation
{
    if (self = [super init])
    {
        self.latitude = latitude;
        self.longitude = longitude;
        self.title = titleAnnotation;
    }
    return self;
}

- (CLLocationCoordinate2D)coordinate
{
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = self.latitude;
    coordinate.longitude = self.longitude;
    return coordinate;
}

- (NSString *)title
{
    return _title;
}


@end

