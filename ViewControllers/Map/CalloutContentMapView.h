//
//  CalloutContentMapView.h
//  loqiva
//
//  Created by Manuel Manzanera on 13/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"

@interface CalloutContentMapView : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextView *titleTextView;
@property (nonatomic, strong) UILabel *infoLabel;
@property (nonatomic, strong) UIImageView *imageView;

@end
