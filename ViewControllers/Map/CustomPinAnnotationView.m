//
//  CustomPinAnnotationView.m
//  loqiva
//
//  Created by Manuel Manzanera on 9/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "CustomPinAnnotationView.h"
#import "CustomAnnotation.h"
#import "UtilManager.h"
#import "loqiva-Swift.h"

@implementation CustomPinAnnotationView

id<MKAnnotation> _annotation;

- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        [self setCalloutOffset:CGPointMake(0., -5)];
        _annotation=annotation;
    }
    return self;
}

- (void)setAnnotation:(id<MKAnnotation>)annotation
{
    [super setAnnotation:annotation];
    
    
    CustomAnnotation *customAnnotation = (CustomAnnotation *)annotation;
    
    UIImage *pinImage;
    pinImage = [UIImage imageNamed:customAnnotation.pin];
    
    UIImageView *iconView=[[UIImageView alloc] initWithImage:pinImage];
    [iconView setBackgroundColor:[UIColor clearColor]];
    [iconView setUserInteractionEnabled:YES];
    [self setUserInteractionEnabled:YES];
    [iconView setFrame:CGRectMake(0, 0, iconView.frame.size.width, iconView.frame.size.height)];
    
    [self setCenterOffset:CGPointMake(0, -(iconView.frame.size.height / 4)-2)];
    
    for (UIView * vista in self.subviews)
    {
        [vista removeFromSuperview];
    }
    
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, iconView.frame.size.width, iconView.frame.size.height)];
    
    [self addSubview:iconView];
}

- (void)setImage:(UIImage *)image
{
    [super setImage:nil];
}

@end
