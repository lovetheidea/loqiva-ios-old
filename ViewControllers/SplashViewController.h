//
//  SplashViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "ParentViewController.h"

@interface SplashViewController : ParentViewController {
    CLLocationManager *locationManager;
}
- (void)presentSignViewController;
- (void)presentHomeViewController;
@end
