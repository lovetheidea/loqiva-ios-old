//
//  TransportCell.m
//  loqiva
//
//  Created by Manuel Manzanera on 14/9/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "TransportCell.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "loqiva-Swift.h"

@implementation TransportCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        _button = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:260], [UtilManager height:50])];
        
        [_button setCenter:CGPointMake(WIDTH/2, _button.center.y)];
        [_button setUserInteractionEnabled:FALSE];
        
        [[self contentView] addSubview:_button];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
