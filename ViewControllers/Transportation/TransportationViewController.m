//
//  TransportationViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 14/9/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "TransportationViewController.h"
#import "NavCustomView.h"
#import "Transport.h"
#import "TransportCell.h"
#import "AppContext.h"
#import "WebViewcontroller.h"
#import "UIColor+Helper.h"
#import "WebServiceManager.h"
#import "AppDelegate.h"
#import "loqiva-Swift.h"

@interface TransportationViewController ()<UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate>

@property (nonatomic, strong) NSArray *transportTypes;
@property (nonatomic, strong) UITableView *trasnportationTableView;
@property (nonatomic, strong) UIView *separatorView;

@end

@implementation TransportationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    _transportTypes = [[[AppContext sharedInstance] transports] mutableCopy];
    
    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (![AppDelegate showAlertIfDisconnected]) {
        [WebServiceManager getPaymentTransports:self];
    }
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object {
    if (webServiceType == WebServiceTypeTransportationTypes) {
        _transportTypes = [[[AppContext sharedInstance] transports] mutableCopy];
        [self.trasnportationTableView reloadData];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    
    [self.view addSubview:[self headerTitleView:NSLocalizedString(@"Discover the different travel options available to you.", nil)]];
    
    CGFloat descriptionSize = [UtilManager heightForText:NSLocalizedString(@"Select your mode of transport below.", nil) havingWidth:(WIDTH - [UtilManager width:40.]) andFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], NAV_BAR_HEIGHT + [UtilManager height:170], WIDTH - [UtilManager width:40.], descriptionSize)];
    [descriptionLabel setTextAlignment:NSTextAlignmentLeft];
    [descriptionLabel setTextColor:[UIColor darkGrayColor]];
    [descriptionLabel setText:NSLocalizedString(@"Select your mode of transport below.", nil)];
    [descriptionLabel setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [descriptionLabel setNumberOfLines:0];
    [descriptionLabel setAdjustsFontSizeToFitWidth:YES];
    
    [self.view addSubview:descriptionLabel];
    
    _separatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20], descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height + [UtilManager height:20], WIDTH - [UtilManager width:40], 1)];
    [_separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
    [self.view addSubview:_separatorView];
    
    _trasnportationTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _separatorView.frame.origin.y + [UtilManager height:20], WIDTH, self.view.frame.size.height - _separatorView.frame.origin.y - [UtilManager height:20]) style:UITableViewStylePlain];
    [_trasnportationTableView setDelegate:self];
    [_trasnportationTableView setDataSource:self];
    [_trasnportationTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_trasnportationTableView registerClass:[TransportCell class] forCellReuseIdentifier:@"cell"];
    [_trasnportationTableView setBackgroundColor:[UIColor whiteColor]];
    [_trasnportationTableView setBounces:TRUE];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:50])];
    [_trasnportationTableView setTableFooterView:footerView];
    
    [self.view addSubview:_trasnportationTableView];
    
    [self setupHeaderForSection:CMSectionTypeTransportation];
}

- (UIView *)headerTitleView:(NSString *)title{
    UIView *headerTitleView = [[UIView alloc] initWithFrame:CGRectMake(0., NAV_BAR_HEIGHT+ [UtilManager height:20], WIDTH, [UtilManager height:140])];
    [headerTitleView setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], 0, headerTitleView.frame.size.width - 2 * [UtilManager width:20], [UtilManager height:130])];
    [titleLabel setNumberOfLines:0];
    [titleLabel setText:title];
    [titleLabel setTextAlignment:NSTextAlignmentLeft];
    [titleLabel setTextColor:[UIColor HeaderTextColour]];
    [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:34]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    [headerTitleView addSubview:titleLabel];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:139], WIDTH - [UtilManager width:40], 1)];
    [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
    [headerTitleView addSubview:separatorView];
    
    return headerTitleView;
}

- (UIView *)navigationView{
    NavCustomView *containerView = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT)];
    [[containerView icon] setImage:[UIImage imageNamed:@"icon-transport-black"]];
    [[containerView iconLabel] setTextColor:[UIColor orangeButtonColor]];
    [[containerView iconLabel] setText:NSLocalizedString(@"Transportation", nil)];
    [[containerView slideButton]setImage:[UIImage imageNamed:@"btn-slider-orange"]];
    [[containerView  notificationButtonImage] setImage:[UIImage imageNamed:@"btn-notification-orange"]];
    [[containerView separatorView] setBackgroundColor:[UIColor orangeButtonColor]];
    [[containerView separatorView] setAlpha:0.8];
    containerView.parentController = self;
    [containerView setBackgroundColor:[UIColor whiteColor]];
    
    return containerView;
}


#pragma mark UITableViewDataSource Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _transportTypes.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return [UtilManager height:30];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return [UtilManager height:30];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return [UtilManager height:0];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [UtilManager height:50];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [UtilManager height:50];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:30])];
    
    return backgroundView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TransportCell *cell = [_trasnportationTableView dequeueReusableCellWithIdentifier:@"cell"];
  
    Transport *currentTransport = [_transportTypes objectAtIndex:indexPath.section];
    
    [[cell button] setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"btn-%@",currentTransport.icon]] forState:UIControlStateNormal];
    [[cell button] setTitle:currentTransport.title forState:UIControlStateNormal];
    [[cell button] setTintColor:[UIColor whiteColor]];
    
    return cell;
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if([[SessionManager shared] isLoggedIn] == false) {
        
        __weak TransportationViewController * weakSelf = self;
        self.tempUserViewController = [[TemporaryUserViewController alloc] initWithDismissBlock:^{
            
            [weakSelf.tempUserViewController.view removeFromSuperview];
            
        } andCallToSignForFree:^{
            [weakSelf.tempUserViewController.view removeFromSuperview];
            [weakSelf presentSignForFreeController];
            
        }];
        
        [self.view addSubview:self.tempUserViewController.view];
        
        return;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:FALSE];
    
    Transport *currentTransport = [_transportTypes objectAtIndex:indexPath.section];
    
     WebViewController *webViewcontroller = [[WebViewController alloc] init];
     [webViewcontroller setUrlString:currentTransport.url];
     [webViewcontroller setTitleBar:currentTransport.title];
     [webViewcontroller setIsBackAvailable:TRUE];
     [self.navigationController pushViewController:webViewcontroller animated:YES];
    
}

@end
