//
//  TransportCell.h
//  loqiva
//
//  Created by Manuel Manzanera on 14/9/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransportCell : UITableViewCell

@property (nonatomic, strong) UIButton *button;

@end
