//
//  HomeViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "HomeViewController.h"
#import "MenuContainerNavController.h"
#import "HomeRewardsCell.h"
#import "AtractionViewCell.h"
#import "LowerViewCell.h"
#import "PreferecesViewCell.h"
#import "AppContext.h"
#import "WebViewController.h"
#import "UtilManager.h"
#import "UIImageView+AFNetworking.h"
#import "ContextualMenu.h"
#import "HomePage.h"
#import "LowerMenu.h"
#import "LowerMenuItem.h"
#import "WebServiceManager.h"
#import "LocationManager.h"
#import "InterestDetail.h"
#import "AttractionViewController.h"
#import "UIImage+Tint.h"
#import "UIImage+Crop.h"
#import "AFNetworkReachabilityManager.h"
#include "JDStatusBarNotification.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface HomeViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,WebServiceManagerDelegate,CBCentralManagerDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) HomePage *homePage;
@property (nonatomic, assign) NSInteger collectionSections;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) CBCentralManager *centralManager;
@property (nonatomic) float lastScrollOffsetY;
@property (nonatomic) float lastHeaderOffsetY;
@end

@implementation HomeViewController

@synthesize collectionView;

- (void)viewDidLoad {
    [super viewDidLoad];
    _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    
    NSLog(@"VFVFVFV : %@",[[AppContext sharedInstance] homePage]);
    _homePage = [[AppContext sharedInstance] homePage];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection =  UICollectionViewScrollDirectionVertical;
    flowLayout.minimumLineSpacing = [UtilManager height:10];
    
    collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0., -20., WIDTH, HEIGHT + 20) collectionViewLayout:flowLayout];
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    [collectionView setBackgroundColor:TEST_BG_ONE];
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    [collectionView registerClass:[HomeRewardsCell class] forCellWithReuseIdentifier:@"HomeRewardsCell"];
    [collectionView registerClass:[AtractionViewCell class] forCellWithReuseIdentifier:@"AtractionViewCell"];
    [collectionView registerClass:[LowerViewCell class] forCellWithReuseIdentifier:@"LowerViewCell"];
    [collectionView registerClass:[PreferecesViewCell class] forCellWithReuseIdentifier:@"PreferecesViewCell"];
    [collectionView setBounces:TRUE];
    [collectionView setAlwaysBounceVertical:YES];
    
    _refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.tintColor = [UIColor orangeAppColor];
    [_refreshControl addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
    
    [self refershControlAction];
    [self.collectionView addSubview:_refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    
    [self.view addSubview:collectionView];
    
    [self setupHeaderForSection:-1];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"UpdateTheBellSystem" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [WebServiceManager getNotificationsDelegate:self];
    }];
    
    [[DataConnectionManager shared] addObserverWithClosure:^(BOOL isConnected) {
        if (isConnected) {
            [JDStatusBarNotification dismissAnimated:YES];
            [self refershControlAction];
        }
        else {
            [JDStatusBarNotification showWithStatus:@"No Internet Connection" styleName:JDStatusBarStyleError];
        }
    }];

}
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    if (central.state == CBCentralManagerStatePoweredOn) {
        //Do what you intend to do
    } else if(central.state == CBCentralManagerStatePoweredOff) {
       // [UtilManager presentAlertWithMessage:@"Bluetooth is off, please swith on."];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [WebServiceManager getNotificationsDelegate:self];
    [collectionView reloadData];
}

- (void)launchWebViewWithURL:(NSString *)url andTitle:(NSString *)title{
    NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:WEBVIEW_NOTIFICATION,@"viewController",
        url,@"url",title,@"title",nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:WEBVIEW_NOTIFICATION object:notifDictionary];
}

- (void)refershControlAction {
    if (![AppDelegate showAlertIfDisconnected]) {
        [_refreshControl beginRefreshing];
        if(![[LocationManager sharedInstance] currentLocation]) {
            [WebServiceManager getHomePageResourcesWithLatitude:@(0.0) andLongitude:@(0.0) andDelegate:self];
        }
        else {
            [WebServiceManager getHomePageResourcesWithLatitude:[NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.latitude]  andLongitude:[NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.longitude] andDelegate:self];
        }
    }
}


#pragma mark UICollectionView DataSource Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   
    if(section == 0)
        return 1;
    if(_collectionSections == 2)
        return 1;
    else{
        if(section == _collectionSections -1)
            return 1;
        return [[_homePage.lowerMenu objectAtIndex:section-1] lowerMenuItems].count + 1;
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    HomeRewardsCell *homeCell;
    AtractionViewCell *atractionCell;
    LowerViewCell *lowerViewCell;
    PreferecesViewCell *preferencesCell;
    
    if(indexPath.section == 0)
    {
        homeCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"HomeRewardsCell" forIndexPath:indexPath];
        [homeCell configureView];
        return homeCell;
    }
    if(_collectionSections == 2 && indexPath.section == 1)
    {
        preferencesCell = (PreferecesViewCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:@"PreferecesViewCell" forIndexPath:indexPath];
        return preferencesCell;
    }
    else{
        if(indexPath.section == _collectionSections -1)
        {
            preferencesCell = (PreferecesViewCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:@"PreferecesViewCell" forIndexPath:indexPath];
            return preferencesCell;
        }
        if(indexPath.row != 0)
        {
            lowerViewCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"LowerViewCell" forIndexPath:indexPath];
            
            LowerMenu *lowerMenu = [_homePage.lowerMenu objectAtIndex:indexPath.section - 1];
            LowerMenuItem *lowerMenuItem = [[lowerMenu lowerMenuItems] objectAtIndex:indexPath.row -1];
            
            if(indexPath.row == 2){
                if(lowerViewCell.frame.origin.x - [UtilManager width:10] > 160)
                    [lowerViewCell setFrame:CGRectMake(lowerViewCell.frame.origin.x - [UtilManager width:10], lowerViewCell.frame.origin.y, lowerViewCell.frame.size.width, lowerViewCell.frame.size.height)];
            }
            
            NSURLRequest *imageRequest;
            CGRect rssIconFrame = CGRectMake(8, 191, 12, 12);
            [[lowerViewCell icon] setFrame:rssIconFrame];
            [[lowerViewCell icon] setImage:[UIImage imageNamed:@"icon-rss"]];
            
            if([lowerMenuItem.type isEqualToString:@"twitter"]) {
                rssIconFrame = CGRectMake(8, 193, 12, 12);
                [[lowerViewCell icon] setFrame:rssIconFrame];
                imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",lowerMenuItem.image]]
                                                cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                            timeoutInterval:60];
                [[lowerViewCell icon] setImage:[UIImage imageNamed:@"icon-twitter"]];
            }
            else if ([lowerMenuItem.type isEqualToString:@"youtube"]) {
                imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",lowerMenuItem.image]]
                                                cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                            timeoutInterval:60];
                rssIconFrame = CGRectMake(8, 194, 12, 12);
                [[lowerViewCell icon] setFrame:rssIconFrame];
                [[lowerViewCell icon] setImage:[UIImage imageNamed:@"icon-youtube"]];
            }
            else {
                imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",lowerMenuItem.image]]
                                                cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                            timeoutInterval:60];
      
                rssIconFrame = CGRectMake(8, 194, 10, 10);
                [[lowerViewCell icon] setFrame:rssIconFrame];

                [[lowerViewCell icon] setImage:[UIImage imageNamed:@"icon-rss"]];
            }
            
                [[lowerViewCell agoLabel] setText:lowerMenuItem.time];
            
            if(![lowerMenuItem.content isKindOfClass:[NSNull class]]) {
                 [[lowerViewCell titleLabel] setAttributedText:lowerMenuItem.content withLineHeight:14 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:REGULAR_FONT size:12] andColor:[UIColor colorWithRed:0.278 green:0.278 blue:0.278 alpha:1.00] andKern:-0.4 withLineBreak:NO];
            }
            
            if([lowerMenuItem.type isEqualToString:@"twitter"]) {
                NSString *stringFormated = [NSString stringWithFormat:@"%@",lowerMenuItem.sourceTitle];
                [[lowerViewCell feedLabel] setAttributedText:stringFormated withLineHeight:15 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:REGULAR_FONT size:9] andColor:[UIColor FeedTextColour] andKern:-0.1 withLineBreak:NO];
            } else {
                [[lowerViewCell feedLabel] setAttributedText:lowerMenuItem.sourceTitle withLineHeight:15 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:REGULAR_FONT size:9] andColor:[UIColor FeedTextColour] andKern:-0.1 withLineBreak:NO];
            }
            
            if (!lowerMenuItem.image.length) {
                [[lowerViewCell contentImageView] setImage:nil];
                [lowerViewCell setContentLabelText:lowerMenuItem.content];
            }
            else {
                [lowerViewCell setContentLabelText:@""];
                [[lowerViewCell contentImageView] setImageWithURLRequest:imageRequest placeholderImage:[UtilManager imageWithColor:[UIColor lightGrayColor]] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                    [[lowerViewCell contentImageView] setImage:image];
                    [[lowerViewCell contentImageView] setContentMode:UIViewContentModeScaleAspectFill];
                } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                    [[lowerViewCell contentImageView] setImage:nil];
                    [lowerViewCell setContentLabelText:lowerMenuItem.content];
                }];
            }
           
            return lowerViewCell;
        }
        else
        {
            atractionCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"AtractionViewCell" forIndexPath:indexPath];
            
            LowerMenu *lowerMenu = [_homePage.lowerMenu objectAtIndex:indexPath.section - 1];
            
            NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],lowerMenu.mediaurl]]
                                                          cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                      timeoutInterval:60];
            [[atractionCell contentImageView] setImageWithURLRequest:imageRequest placeholderImage:[UtilManager imageWithColor:[UIColor lightGrayColor]] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
   
                
                [[atractionCell contentImageView] setImage: image];
               

            }failure:nil];
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"", nil),lowerMenu.title]];
            
            [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:LIGHT_FONT size:16] range:NSMakeRange(0, attributedString.length - 1)];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setAlignment:NSTextAlignmentLeft];
            
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length - 1)];
            
            NSRange range1 = [attributedString.string rangeOfString:@"Featured"];
#ifdef LOQIVA
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor orangeButtonColor] range:NSMakeRange(range1.location,range1.length)];
#elif OLDSEAPORT
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(range1.location,range1.length)];
#endif
            [[atractionCell subtitleLabel] setAttributedText:attributedString];
            return atractionCell;
        }
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    _collectionSections = 2 + _homePage.lowerMenu.count;
    return _collectionSections;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(9_0){
    return NO;
}

#pragma mark UICollectionView Delegate Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    if ([AppDelegate showAlertIfDisconnected]) {
        return;
    }
    
    if(indexPath.section == 0)
    {
        //Reward
    }
    else if(_collectionSections == 2 && indexPath.section == 1)
    {
        //Preferences
    }
    else{
        if(indexPath.section == _collectionSections -1)
        {
            //Preferences
        }
        else
        {

            LowerMenu *lowerMenu = [_homePage.lowerMenu objectAtIndex:indexPath.section - 1];
            
            if(indexPath.row == 0){
                
                if(![[SessionManager shared] user]){
                    
                    __weak HomeViewController * weakSelf = self;
                    self.tempUserViewController = [[TemporaryUserViewController alloc] initWithDismissBlock:^{
                        
                        [weakSelf.tempUserViewController.view removeFromSuperview];
                        
                    } andCallToSignForFree:^{
                        [weakSelf.tempUserViewController.view removeFromSuperview];
                        [weakSelf presentSignForFreeController];
                        
                    }];
                    
                    [self.view addSubview:self.tempUserViewController.view];
                    
                    return;
                }
        
                InterestDetail *interestDetail  =[[InterestDetail alloc] init];
                
                [interestDetail setSummary:lowerMenu.summary];
                [interestDetail setAddress:lowerMenu.address];
                [interestDetail setLatitude:lowerMenu.latitude];
                [interestDetail setLongitude:lowerMenu.longitude];
                [interestDetail setMediaURL:lowerMenu.mediaurl];
                [interestDetail setMediaType:lowerMenu.MediaType];
                [interestDetail setTitle:lowerMenu.title];
                [interestDetail setDistance:lowerMenu.distance];
                [interestDetail setInterestId:lowerMenu.lowerMenuId];
                
                AttractionViewController *atractionViewController = [[AttractionViewController alloc] initWithCurrentInterestDetail:interestDetail andInterestDetailID:lowerMenu.lowerMenuId];
                
                
                double delayInSeconds = 0.4;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [self.navigationController pushViewController:atractionViewController animated:YES];
                });
                
                
                
            } else {
            
                LowerMenuItem *lowerMenuItem = [[lowerMenu lowerMenuItems] objectAtIndex:indexPath.row - 1];
                
                if([lowerMenuItem.type isEqualToString:@"twitter"])
                {
                    if(lowerMenu.title.length > 0)
                        [self launchWebViewWithURL:lowerMenuItem.sourceLink andTitle:lowerMenuItem.sourceTitle];
                    else
                        [self launchWebViewWithURL:lowerMenuItem.sourceLink andTitle:lowerMenuItem.sourceTitle];
                }else if ([lowerMenuItem.type isEqualToString:@"youtube"]){
                    
                    
                    if(lowerMenu.title.length > 0)
                        [self launchWebViewWithURL:lowerMenuItem.sourceLink andTitle:lowerMenuItem.title];
                    else
                        [self launchWebViewWithURL:lowerMenuItem.sourceLink andTitle:lowerMenuItem.sourceTitle];
                }else{
                    if(lowerMenu.title.length > 0)
                        [self launchWebViewWithURL:lowerMenuItem.sourceLink andTitle:lowerMenuItem.sourceTitle];
                    else
                        [self launchWebViewWithURL:lowerMenuItem.sourceLink andTitle:lowerMenuItem.sourceTitle];
                }
            }
        }
    }
    
}

#pragma mark UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
        return CGSizeMake(WIDTH, [UtilManager height:590]);
    if(_collectionSections == 2)
    {
        if(![[SessionManager shared] user])
            return CGSizeMake(WIDTH, [UtilManager height:140]);
        else return CGSizeMake(WIDTH,0);
    }
    else{
        if(indexPath.section == _collectionSections -1)
        {
            if(![[SessionManager shared] user]) {
                return CGSizeMake(WIDTH, [UtilManager height:140]);
            }
            else {
                return CGSizeMake(WIDTH,0);
            }
        }
        if(indexPath.row != 0){
            return CGSizeMake([UtilManager width:172], [UtilManager height:250]);
        } else {
            return CGSizeMake([UtilManager width:354], [UtilManager height:224]);
        }
    }
    
    return CGSizeMake(WIDTH, 180.);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    
     if(section == 0)
     {
         return UIEdgeInsetsMake(0., 0., 0., 0.);
     }
     else if(_collectionSections == 2 && section == 1)
     {
     //Preferences
         return UIEdgeInsetsMake(5., 5., 0., 5.);
     }
     else {
         if(section == _collectionSections -1) {
             return UIEdgeInsetsMake(5., 5., 0., 5.);
         } else {
             return UIEdgeInsetsMake([UtilManager height:10], [UtilManager width:10], [UtilManager height:10], [UtilManager width:10]);
         }
     }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    switch (section) {
        case 0:
            return 0;
            break;
    
        default:
            return [UtilManager height:10];
            break;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    switch (section) {
        case 0:
            return 0;
            break;
        default:
            return [UtilManager height:10];
            break;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0.,0.);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeMake(0.,0.);
}

#pragma mark WebServiceManager delegate methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    [_refreshControl endRefreshing];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    if (webServiceType == WebServiceTypeProfileSave) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateTheBadgeIcon" object:nil];
    }
    else {
        _homePage = [[AppContext sharedInstance] homePage];

        [_refreshControl endRefreshing];
        [self.collectionView reloadData];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    [_refreshControl endRefreshing];
}

@end
