//
//  HomeRewardsCell.m
//  loqiva

//  Home Template Minus feeds
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "HomeRewardsCell.h"

#import "UtilManager.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "HomeGalleryItem.h"
#import "NSString+HTML.h"
#import "NavCustomView.h"
#import "WebServiceManager.h"
#import "RewardsDetailViewController.h"
#import "ScheduleDetailViewController.h"
#import "UIColor+Helper.h"
#import "UIImageView+AFNetworking.h"
#import "ContextualMenu.h"
#import "HomePage.h"
#import "AppDelegate.h"

#import "UIImageEffects.h"

#import "UIImage+Crop.h"

#import "BlurView.h"
#import "loqiva-Swift.h"

#ifdef LOQIVA

#define GALLERY_TEXT_COLOR [UIColor whiteColor]

#elif OLDSEAPORT

#define GALLERY_TEXT_COLOR [UIColor whiteColor]

#endif

@interface HomeRewardsCell ()

@property (nonatomic, strong) HomePage *homePage;
@property (nonatomic, strong) NSArray  *contextualMenus;
-(void)configureView;
@end

@implementation HomeRewardsCell

@synthesize backgroundImage, titleLabel, feedLabel, feedSubLabel, galleryView, pageControl;

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor whiteColor]];
    }
    return self;
}

-(void)configureView {
    _homePage = [[AppContext sharedInstance] homePage];
    _contextualMenus = [HomePage activateContextualMenus];
    
    ContextualMenu *contextualMenu;
    if([_homePage contextualMenus].count>0){
        contextualMenu = [[_homePage contextualMenus] objectAtIndex:1];
    }else{
        NSLog(@"NNNNNNNNNN  ::: %@",[[AppContext sharedInstance] homePage]);
    }
    
    if (!backgroundImage) {
        backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:480])];
        [backgroundImage setImage:[UIImage imageNamed:@"Header"]];
        [backgroundImage setClipsToBounds:YES];
        [self.contentView addSubview:backgroundImage];
    }
    
    if (!titleLabel) {
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:30], NAV_BAR_HEIGHT + [UtilManager height:40], [UtilManager width:315], [UtilManager height:200] - NAV_BAR_HEIGHT)];
        [self.contentView addSubview:titleLabel];
    }
    
    titleLabel.hidden = YES;
    [titleLabel setNumberOfLines:0];
    if(contextualMenu){
        [titleLabel setAttributedText:contextualMenu.headlinetext withLineHeight:32 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:30] andColor:[UIColor whiteColor] andKern:TITLE_FONT_KERN withLineBreak:NO];
        [titleLabel setBackgroundColor:TEST_BG_ONE];
        titleLabel.hidden = NO;
    }
    
    if (!galleryView) {
        galleryView = [[GalleryView alloc] initWithFrame:CGRectMake(0., titleLabel.frame.origin.y + titleLabel.frame.size.height + [UtilManager height:20], self.frame.size.width, [UtilManager height:180])];
        [self.contentView addSubview:galleryView];
    }
    
    [galleryView setType:GalleryTypeLinear];
    [galleryView setDelegate:self];
    [galleryView setDataSource:self];
    [galleryView setCurrentItemIndex:1];
    [galleryView setBackgroundColor:TEST_BG_TWO];
    [galleryView reloadData];

    if (!pageControl) {
        pageControl = [[PageControl alloc] initWithFrame:CGRectMake(100.,backgroundImage.frame.size.height + backgroundImage.frame.origin.y - [UtilManager height:40], 100., 25.)];
        [self.contentView addSubview:pageControl];
    }
    [pageControl setBackgroundColor:[UIColor clearColor]];
    [pageControl setCenter:CGPointMake(self.center.x, pageControl.center.y)];
    [pageControl setDelegate:self];
    [pageControl setNumberOfPages:_contextualMenus.count];
    [pageControl setCurrentPage:2];
    [pageControl setBackgroundColor:TEST_BG_ONE];
    
    
    
//Mitch this is where the position of the feed label is set
    if (!feedLabel) {
        feedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., self.frame.size.height - [UtilManager height:85], WIDTH, [UtilManager height:33])];
        [self.contentView addSubview:feedLabel];
    }
    
    [feedLabel setCenter:CGPointMake(self.center.x, feedLabel.center.y)];
    User *currentUser = [[SessionManager shared] user];
    
    if(currentUser){
        
        NSString *data = currentUser.firstName;
        if([data hasSuffix:@"s"]){
           
            data =[NSString stringWithFormat:@"%@%@",data,@"'"];
            
        }
        else{
            data =[NSString stringWithFormat:@"%@%@",data,@"'s"];
        }
        
        [feedLabel setAttributedText:[NSString stringWithFormat:@"%@%@",data,NSLocalizedString(@" feed", nil)] withLineHeight:30 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:26] andColor:[UIColor FeedTextColour] andKern:TITLE_FONT_KERN withLineBreak:NO];
        [feedSubLabel setBackgroundColor:TEST_BG_ONE];
    }
    

    [feedLabel setFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE]];
    [feedLabel setNumberOfLines:1];
    [feedLabel setTextColor:[UIColor orangeAppColor]];
    [feedLabel setTextAlignment:NSTextAlignmentCenter];
    [feedLabel setAdjustsFontSizeToFitWidth:YES];
    [feedLabel setBackgroundColor:TEST_BG_ONE];
    
    if (!feedSubLabel) {
        feedSubLabel = [UILabel new];
        [self.contentView addSubview:feedSubLabel];
    }
    
    [feedSubLabel setBackgroundColor:[UIColor clearColor]];
    
    if(currentUser){
        feedSubLabel.frame=CGRectMake(0., feedLabel.frame.size.height + feedLabel.frame.origin.y, WIDTH, [UtilManager height:40]);
        [feedSubLabel setAttributedText:NSLocalizedString(_homePage.lowerMenuText, nil) withLineHeight:30 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:GRAY_DARK_TEXT_COLOR andKern:TITLE_FONT_KERN withLineBreak:NO];
    }
    else{
         feedSubLabel.frame=CGRectMake([UtilManager width:10], feedLabel.frame.origin.y - [UtilManager height:25], WIDTH -[UtilManager width:20], [UtilManager height:120]);
        feedSubLabel.numberOfLines=0;
        
        NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
        NSString *locationText = _homePage.lowerMenuText;//infoPlistDict[@"AppHeaderText"];
        if (locationText.length == 0) {
            locationText = _homePage.lowerMenuText;
        }
        [feedSubLabel setAttributedText:locationText withLineHeight:30 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:GRAY_DARK_TEXT_COLOR andKern:TITLE_FONT_KERN withLineBreak:NO];

    }
    [feedSubLabel setBackgroundColor:TEST_BG_ONE];
}

- (void)slideAction{
    [[NSNotificationCenter defaultCenter] postNotificationName:SLIDE_NOTIFICATION object:nil];
}

- (void)notificationAction{
    [[NSNotificationCenter defaultCenter] postNotificationName:ACTION_NOTIFICATION object:nil];
}

#pragma mark GalleryViewDelegate methods

- (void)gallery:(GalleryView *)Gallery didSelectItemAtIndex:(NSInteger)index
{
    if ([AppDelegate showAlertIfDisconnected]) {
        return;
    }
    
    ContextualMenu *contextualMenu = [[_homePage contextualMenus] objectAtIndex:index];
    
    if(index == 0){
        NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"attractionDetailViewcontroller",VIEWCONTROLLER_NOTIFICATION,
                                         [NSNumber numberWithBool:TRUE],@"firstParam",contextualMenu.contextualMenuId,@"attractionId",nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
    } else if (index == 1) {
        if(_contextualMenus.count == 3)
        {
            NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"rewardsDetailViewController",VIEWCONTROLLER_NOTIFICATION,
                                             [NSNumber numberWithBool:TRUE],@"firstParam",contextualMenu.contextualMenuId,@"rewardtitle",nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
        } else {
            NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"attractionDetailViewcontroller",VIEWCONTROLLER_NOTIFICATION,
                                             [NSNumber numberWithBool:TRUE],@"firstParam",contextualMenu.contextualMenuId,@"attractionId",nil];
                
            [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
        }
    } else if(index == 2){
        NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"attractionDetailViewcontroller",VIEWCONTROLLER_NOTIFICATION,
                                         [NSNumber numberWithBool:TRUE],@"firstParam",contextualMenu.contextualMenuId,@"attractionId",nil];
    
        [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
    }
    
}

// Text Styles

- (void)galleryDidScroll:(GalleryView *)Gallery{
    
    if(galleryView.currentItemIndex + 1 < 3 && galleryView.currentItemIndex >= 0){
        ContextualMenu *contextualMenu = [[_homePage contextualMenus] objectAtIndex:galleryView.currentItemIndex];
        //[titleLabel setText:contextualMenu.headlinetext];
        
        [titleLabel setAttributedText:contextualMenu.headlinetext withLineHeight:32 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:30] andColor:[UIColor whiteColor] andKern:TITLE_FONT_KERN withLineBreak:NO];
    
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],contextualMenu.image]]
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    
        __weak UIImageView *imageView = backgroundImage;
        
        [backgroundImage setImageWithURLRequest:imageRequest placeholderImage:[UtilManager imageWithColor:[UIColor lightGrayColor]] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            [imageView setImage:[UIImageEffects imageByApplyingBlurToImage:[UtilManager imageWithImage:image scaledToWidth:imageView.frame.size.width andMinHeigth:imageView.frame.size.height] withRadius:0 tintColor: ORANGE_BG_CONTEXTUAL_COLOR saturationDeltaFactor:1 maskImage:nil]];
            [imageView setContentMode:UIViewContentModeScaleAspectFill];
            } failure:nil];
        
        [pageControl setCurrentPage:galleryView.currentItemIndex + 1];
    } else {
        if (galleryView.currentItemIndex >= 0) {
            ContextualMenu *contextualMenu = [[_homePage contextualMenus] objectAtIndex:galleryView.currentItemIndex];
            
            if([contextualMenu.headlinetext isKindOfClass:[NSNull class]])
                [titleLabel setText:@""];
            else{
                [titleLabel setAttributedText:contextualMenu.headlinetext withLineHeight:30 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:30] andColor:[UIColor whiteColor] andKern:0 withLineBreak:NO];
                
        
            }
            
            NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],contextualMenu.image]]
                                                          cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                      timeoutInterval:60];
            __weak UIImageView *imageView = backgroundImage;
            
            // Backgorund Image slider gallery
            [backgroundImage setImageWithURLRequest:imageRequest placeholderImage:[UtilManager imageWithColor:[UIColor lightGrayColor]] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                [imageView setImage:[UIImageEffects imageByApplyingBlurToImage:[UtilManager imageWithImage:image scaledToWidth:imageView.frame.size.width andMinHeigth:imageView.frame.size.height] withRadius:0 tintColor:ORANGE_BG_CONTEXTUAL_COLOR saturationDeltaFactor:0.5 maskImage:nil]];
                [imageView setContentMode:UIViewContentModeScaleAspectFill];
            } failure:nil];
           
            [pageControl setCurrentPage:galleryView.currentItemIndex + 1];
        }
    }
}

#pragma mark GalleryViewDataSource methods

- (NSUInteger)numberOfItemsInGallery:(GalleryView *)gallery
{
    return _contextualMenus.count;
}

- (NSUInteger)numberOfPlaceholdersInGallery:(GalleryView *)Gallery
{
    return 0;
}

- (CGFloat)gallery:(GalleryView *)Gallery valueForOption:(GalleryOption)option withDefault:(CGFloat)value
{
    switch (option){
        case GalleryOptionWrap:
            return true;
        case GalleryOptionFadeMin:
            return -0.2;
        case GalleryOptionFadeMax:
            return 0.2;
        case GalleryOptionFadeRange:
            return 1.5;
        default:
            break;
    }
    return value * 1.05;
}


- (UIView *)gallery:(GalleryView *)gallery viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    HomeGalleryItem *homeGalleryItem = (HomeGalleryItem *)view;
    ContextualMenu *contextualMenu ;
    
    
    
    if(homeGalleryItem == nil){
        homeGalleryItem = [[HomeGalleryItem alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:250], [UtilManager height:160])];
        [homeGalleryItem setBackgroundColor:[UIColor clearColor]];
    }
    
    if(_homePage && _homePage.contextualMenus && _homePage.contextualMenus.count != 0){
       
        contextualMenu = [_contextualMenus objectAtIndex:index];
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],contextualMenu.image]]
                                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                          timeoutInterval:60];

        
        [[homeGalleryItem imageView] setImageWithURLRequest:imageRequest placeholderImage:[UtilManager imageWithColor:[UIColor lightGrayColor]] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            
            
            homeGalleryItem.imageView.image = image;
            
                UIBezierPath *maskPath = [UIBezierPath
                                      bezierPathWithRoundedRect:[homeGalleryItem imageView].bounds
                                      byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                                      cornerRadii:CGSizeMake(3, 3)
                                      ];
            
                CAShapeLayer *maskLayer = [CAShapeLayer layer];
            
                maskLayer.frame = self.bounds;
                maskLayer.path = maskPath.CGPath;
            
                [homeGalleryItem imageView].layer.mask = maskLayer;
    
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            
        }];
    } else {
        [[homeGalleryItem imageView] setImage:[UtilManager imageWithColor:[UIColor lightGrayColor]]];
    }
    
    if(_contextualMenus.count == 3)
    {
    switch (index) {
        case 0:{
            
            [[homeGalleryItem icon] setImage:[UIImage imageNamed:@"icon-home-location"]];
            [homeGalleryItem setBackgroundColor:[UIColor homeContextualFirstItemBGColor]];
            
            NSString *distance = contextualMenu.distance;
            if ([distance isKindOfClass:[NSNumber class]]) {
                distance = [(NSNumber *)distance stringValue];
            }
            [[homeGalleryItem titleLabel] setText:[distance distanceString]];
            [[homeGalleryItem titleLabel] setAdjustsFontSizeToFitWidth:YES];
            [[homeGalleryItem titleLabel] setTextColor:[UIColor homeContextualSecondItemBGColor]];
            
            [[homeGalleryItem subTitleLabel] setAttributedText:contextualMenu.title withLineHeight:17 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:REGULAR_FONT size:14] andColor:WHITE_TEXT_COLOR andKern:-0.4 withLineBreak:NO];
            [[homeGalleryItem subTitleLabel] setAdjustsFontSizeToFitWidth:NO];
            [[homeGalleryItem subTitleLabel] sizeToFit];

            [[homeGalleryItem infoLabel] setText:NSLocalizedString(@"Attraction", nil)];
            [[homeGalleryItem infoLabel] setTextColor:[UIColor homeContextualSecondItemBGColor]];
            
        }
            
     break;
        case 1:{
            [homeGalleryItem setBackgroundColor:[UIColor homeContextualSecondItemBGColor]];
            [[homeGalleryItem icon] setImage:[UIImage imageNamed:@"icon-home-reward"]];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"HH:mm a"];
            NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
            NSArray* words = [dateString componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            dateString = [words componentsJoinedByString:@""];
            [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
            [[homeGalleryItem titleLabel] setText:dateString];
            [[homeGalleryItem titleLabel] setTextColor:[UIColor homeContextualFirstItemBGColor]];

            [[homeGalleryItem subTitleLabel] setAttributedText:contextualMenu.title withLineHeight:17 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:REGULAR_FONT size:14] andColor:WHITE_TEXT_COLOR andKern:-0.4 withLineBreak:NO];
            [[homeGalleryItem subTitleLabel] setAdjustsFontSizeToFitWidth:NO];
            [[homeGalleryItem subTitleLabel] sizeToFit];

            [[homeGalleryItem infoLabel] setText:NSLocalizedString(@"Rewards", nil)];
            [[homeGalleryItem infoLabel] setTextColor:[UIColor homeContextualFirstItemBGColor]];
    }
     break;
        case 2:{
            [homeGalleryItem setBackgroundColor:[UIColor homeContextualThirdItemBGColor]];
            [[homeGalleryItem icon] setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icon-%@",contextualMenu.icon]]];
           // [[homeGalleryItem titleLabel] setText:[NSString stringWithFormat:@"%.1f°F %@",[self fahrenheit:contextualMenu.temp.floatValue],contextualMenu.weather]];
            [[homeGalleryItem titleLabel] setText:[self fahrenheitFormatted:contextualMenu.temp.floatValue]];
            [[homeGalleryItem titleLabel] setTextColor:[UIColor homeContextualSecondItemBGColor]];
            
            [[homeGalleryItem subTitleLabel] setAttributedText:contextualMenu.title withLineHeight:17 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:REGULAR_FONT size:14] andColor:WHITE_TEXT_COLOR andKern:-0.4 withLineBreak:NO];
            [[homeGalleryItem subTitleLabel] setAdjustsFontSizeToFitWidth:NO];
            [[homeGalleryItem subTitleLabel] sizeToFit];

            [[homeGalleryItem infoLabel] setText:NSLocalizedString(@"Attraction", nil)];
            [[homeGalleryItem infoLabel] setTextColor:[UIColor homeContextualSecondItemBGColor]];
        }
     break;
     default:
     break;
     }
    } else if(_contextualMenus.count == 2){
        if(index == 0){
            
            [[homeGalleryItem icon] setImage:[UIImage imageNamed:@"icon-pin-small"]];
            [homeGalleryItem setBackgroundColor:[UIColor orangeButtonColor]];
            [[homeGalleryItem subTitleLabel] setText:[NSString stringWithFormat:@"%@",contextualMenu.title]];
            [[homeGalleryItem subTitleLabel] setAdjustsFontSizeToFitWidth:NO];
            [[homeGalleryItem subTitleLabel] sizeToFit];
            [[homeGalleryItem infoLabel] setText:NSLocalizedString(@"Attraction", nil)];
            [[homeGalleryItem infoLabel] setTextColor:[UIColor orangeButtonColor]];
            
            NSString *distance = contextualMenu.distance;
            if ([distance isKindOfClass:[NSNumber class]]) {
                distance = [(NSNumber *)distance stringValue];
            }
            [[homeGalleryItem titleLabel] setText:[distance distanceString]];
            [[homeGalleryItem titleLabel] setAdjustsFontSizeToFitWidth:YES];
        } else if (index == 1){
            [homeGalleryItem setBackgroundColor:[UIColor homeContextualThirdItemBGColor]];
            [[homeGalleryItem icon] setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icon-%@",contextualMenu.icon]]];
            [[homeGalleryItem infoLabel] setText:NSLocalizedString(@"Attraction", nil)];
            [[homeGalleryItem infoLabel] setTextColor:[UIColor yellowColor]];
            [[homeGalleryItem titleLabel] setText:[self fahrenheitFormatted:contextualMenu.temp.floatValue]];
            [[homeGalleryItem subTitleLabel] setText:contextualMenu.title];
        }
    }
    
    [[homeGalleryItem titleLabel] setAdjustsFontSizeToFitWidth:YES];
    
    return homeGalleryItem;
}

- (NSString *)fahrenheitFormatted:(CGFloat)celsius {
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    BOOL usesCentigrade = infoPlistDict[@"AppUsesCentigrade"];
    if (usesCentigrade) {
        return [NSString stringWithFormat:@"%.1f°C", celsius];
    }
    
    return [NSString stringWithFormat:@"%.1f°C", (celsius * 1.8) + 32];
}


@end
