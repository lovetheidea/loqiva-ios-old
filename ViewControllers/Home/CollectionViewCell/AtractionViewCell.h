//
//  AtractionViewCell.h
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
//#import "GradientOverlayView.h"

@interface AtractionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *contentImageView;
@property (nonatomic, strong) UILabel *subtitleLabel;
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *titleLabel;

@end
