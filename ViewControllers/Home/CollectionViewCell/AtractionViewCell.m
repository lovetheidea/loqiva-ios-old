//
//  AtractionViewCell.m
//  loqiva
//  Template = Attraction cell in Feed
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "AtractionViewCell.h"
#import <Quartzcore/Quartzcore.h>
#import "UtilManager.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "UIImageView+AFNetworking.h"
#import "UIView+Framing.h"
#import "loqiva-Swift.h"

@implementation AtractionViewCell

@synthesize contentImageView, subtitleLabel, icon;

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        
        contentImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [contentImageView setContentMode:UIViewContentModeScaleAspectFill];
        [self.contentView addSubview:contentImageView];
        
        subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10.], self.contentView.frame.size.height - [UtilManager height:44.], WIDTH - 2 * [UtilManager width:15.], [UtilManager height:40])];
        [subtitleLabel setTextAlignment:NSTextAlignmentLeft];
        [subtitleLabel setTextColor:[UIColor whiteColor]];
        [subtitleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:16]];
        [subtitleLabel setNumberOfLines:2];
        
        [self.contentView addSubview:subtitleLabel];
        
        icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-attraction"]];
        [icon setFrame:CGRectMake([UtilManager width:10], [UtilManager height:5], [UtilManager width:20], [UtilManager height:20])];
        [self.contentView addSubview:icon];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:35], 0, [UtilManager width:120], icon.frame.size.height)];
        [_titleLabel setCenter:CGPointMake(_titleLabel.center.x, icon.center.y)];
        [_titleLabel setText:NSLocalizedString(@"Attraction", nil)];
        [_titleLabel setTextColor:[UIColor whiteColor]];
        [_titleLabel setFont:[UIFont fontWithName:REGULAR_FONT size:10]];
        [self.contentView addSubview:_titleLabel];
        
        self.contentImageView.layer.masksToBounds = YES;
        self.contentImageView.layer.cornerRadius = 2.f;

        [self addGradientToImage];
    }
    
    return self;
}

- (void)addGradientToImage {
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(self.contentImageView.frame.origin.x, 0, self.contentImageView.frame.size.width, 30)];
    [self.contentView insertSubview:topView belowSubview:self.icon];
    topView.layer.cornerRadius = 2;
    topView.layer.masksToBounds = YES;
    [topView addFadeOut:NO];
    
    
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(self.contentImageView.frame.origin.x, self.contentImageView.frame.size.height - 40, self.contentImageView.frame.size.width, 40)];
    [self.contentView insertSubview:bottomView belowSubview:self.subtitleLabel];
    bottomView.layer.cornerRadius = 2;
    bottomView.layer.masksToBounds = YES;
    [bottomView addFadeOut:YES];
}

@end
