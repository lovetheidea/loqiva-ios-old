//
//  HomeGalleryItem.m
//  loqiva
//
//  Created by Manuel Manzanera on 29/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "HomeGalleryItem.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "loqiva-Swift.h"


@implementation HomeGalleryItem

@synthesize imageView, icon, titleLabel, subTitleLabel, infoLabel;

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        
        icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:9], [UtilManager height:10], [UtilManager width:18], [UtilManager width:18])];
        [icon setContentMode:UIViewContentModeScaleAspectFit];
        [icon setBackgroundColor:TEST_BG_TWO];
        [self addSubview:icon];
        
        // Contextual text for time/temp/distance
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(icon.frame.origin.x + icon.frame.size.width + [UtilManager width:3], 0., [UtilManager width:95], [UtilManager height:20])];
        [titleLabel setCenter:CGPointMake(titleLabel.center.x, icon.center.y)];
        [titleLabel setTextAlignment:NSTextAlignmentLeft];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setFont:[UIFont fontWithName:REGULAR_FONT size:12]];
        [titleLabel setBackgroundColor:TEST_BG_ONE];
        
        [self addSubview:titleLabel];
        
        // Contextual text for Description
        subTitleLabel  = [[UILabel alloc] initWithFrame:CGRectMake(icon.frame.origin.x, icon.frame.origin.y + icon.frame.size.height + [UtilManager height:10], [UtilManager width:115], [UtilManager height:80])];
        [subTitleLabel setNumberOfLines:4];
        [subTitleLabel setBackgroundColor:TEST_BG_TWO];
        [self addSubview:subTitleLabel];
        
//        Mitch - Tried to add elipses, but couldnt, below is my code
        
//        subTitleLabel  = [[UILabel alloc] initWithFrame:CGRectMake(icon.frame.origin.x, icon.frame.origin.y + icon.frame.size.height + [UtilManager height:10], [UtilManager width:120], [UtilManager height:80])];
//        [subTitleLabel setNumberOfLines:4];
//        [subTitleLabel setBackgroundColor:TEST_BG_TWO];
//        [subTitleLabel setAdjustsFontSizeToFitWidth:(NO)];
//        [subTitleLabel setLineBreakMode:(NSLineBreakByTruncatingTail)];
//        [self addSubview:subTitleLabel];

        
        // Contextual text for type (Reward/Attraction etc)
        infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(icon.frame.origin.x, frame.size.height - [UtilManager height:25], [UtilManager width:100], [UtilManager height:18])];
        [infoLabel setFont:[UIFont fontWithName:REGULAR_FONT size:10]];
        [infoLabel setBackgroundColor:TEST_BG_ONE];
        [self addSubview:infoLabel];
        
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:130], 0., [UtilManager width:120], [UtilManager height:160])];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        
        imageView.autoresizingMask =
        ( UIViewAutoresizingFlexibleBottomMargin
         | UIViewAutoresizingFlexibleHeight
         | UIViewAutoresizingFlexibleLeftMargin
         | UIViewAutoresizingFlexibleRightMargin
         | UIViewAutoresizingFlexibleTopMargin
         | UIViewAutoresizingFlexibleWidth );
        
        
        
        
        
        
        [self addSubview:imageView];
        
        
        
        // border radius
        [self.layer setCornerRadius:3.0f];
        

        
        // drop shadow
        [self.layer setShadowColor:[UIColor blackColor].CGColor];
        [self.layer setShadowOpacity:0.8];
        [self.layer setShadowRadius:10.0];
        [self.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    }
    
    return self;
}

@end
