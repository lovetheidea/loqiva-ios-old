//
//  PreferecesViewCell.m
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "PreferecesViewCell.h"
#import "SignUpViewController.h"
#import "SignNavController.h"
#import "UIColor+Helper.h"
#import "AppDelegate.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "loqiva-Swift.h"

@implementation PreferecesViewCell


- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        
        [self setBackgroundColor:[UIColor footerColor]];
        [self.contentView setBackgroundColor:[UIColor footerColor]];
        
        UILabel *newsLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:15.], self.contentView.frame.size.width - [UtilManager width:6], [UtilManager height:22])];
        [newsLabel setTextColor:[UIColor footerTextAcentcolour]];
        [newsLabel setTextAlignment:NSTextAlignmentLeft];
        [newsLabel setText:NSLocalizedString(@"Want news more relevant to you?", nil)];
        [newsLabel setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
        
        [self.contentView addSubview:newsLabel];
        
        UILabel *updateLabel = [[UILabel alloc] initWithFrame:CGRectMake(newsLabel.frame.origin.x, newsLabel.frame.origin.y + newsLabel.frame.size.height, newsLabel.frame.size.width, newsLabel.frame.size.height)];
        [updateLabel setTextAlignment:NSTextAlignmentLeft];
        [updateLabel setTextColor:[UIColor whiteColor]];
        [updateLabel setText:NSLocalizedString(@"Update your news feed preferences.", nil)];
        [updateLabel setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
        
        [self.contentView addSubview:updateLabel];
        
        UIImage *orangeButtonImage = [UIImage imageNamed:@"btn-update-preferences"];
        
        UIButton *preferencesButton = [[UIButton alloc] initWithFrame:CGRectMake(newsLabel.frame.origin.x, updateLabel.frame.origin.y + updateLabel.frame.size.height + [UtilManager height:10], orangeButtonImage.size.width * 0.85, orangeButtonImage.size.height * 0.85)];
        [preferencesButton setImage:orangeButtonImage forState:UIControlStateNormal];
        [preferencesButton addTarget:self action:@selector(preferencesAction) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:preferencesButton];
    }
    
    return self;
}

- (void)preferencesAction{
    SignUpViewController *viewController = [[SignUpViewController alloc] init];
    SignNavController *customNavigationController = [[SignNavController alloc] initWithRootViewController:viewController];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:customNavigationController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:customNavigationController];
     }];
}

@end
