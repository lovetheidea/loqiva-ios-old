//
//  LowerViewCell.m
//  loqiva
//  Template = Social Media cell in feed
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "LowerViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "UIColor+Helper.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "loqiva-Swift.h"

@implementation LowerViewCell

@synthesize contentImageView, titleLabel, icon, feedLabel, agoLabel, contentLabel;

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        
        [[self contentView] setBackgroundColor: WHITE_TEXT_COLOR];
        
        contentImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., self.contentView.frame.size.width,[UtilManager height:180.])];
        [[contentImageView layer] setMasksToBounds:YES];
        [[self.contentView layer ] setMasksToBounds:YES];
        [self.contentView addSubview:contentImageView];
        
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], contentImageView.frame.size.height + [UtilManager height:7], [UtilManager width:160], [UtilManager height:36])];
        [titleLabel setTextAlignment:NSTextAlignmentLeft];
        [titleLabel setNumberOfLines:2];
        [titleLabel setTextColor:[UIColor colorWithRed:0.278 green:0.278 blue:0.278 alpha:1.00]];
        [titleLabel setFont:[UIFont fontWithName:REGULAR_FONT size:11]];
        [titleLabel setAdjustsFontSizeToFitWidth:NO];
        
        [self.contentView addSubview:titleLabel];
        
        agoLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y + titleLabel.frame.size.height, titleLabel.frame.size.width, titleLabel.frame.size.height)];
        [agoLabel setFont:titleLabel.font];
        [agoLabel setTextColor:titleLabel.textColor];
        [agoLabel setAdjustsFontSizeToFitWidth:YES];
        
        icon = [[UIImageView alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, self.contentView.frame.size.height - [UtilManager height:25], [UtilManager width:15], [UtilManager width:15])];
        [icon setBackgroundColor:[UIColor clearColor]];
        [icon setContentMode:UIViewContentModeScaleAspectFit];
        icon.autoresizingMask =
        ( UIViewAutoresizingFlexibleBottomMargin
         | UIViewAutoresizingFlexibleHeight
         | UIViewAutoresizingFlexibleLeftMargin
         | UIViewAutoresizingFlexibleRightMargin
         | UIViewAutoresizingFlexibleTopMargin
         | UIViewAutoresizingFlexibleWidth );
        [[icon layer] setMasksToBounds:YES];
        [self.contentView addSubview:icon];
        
        feedLabel = [[UILabel alloc] initWithFrame:CGRectMake(icon.frame.origin.x + icon.frame.size.width + [UtilManager width:4.], icon.frame.origin.y, self.contentView.frame.size.width - (icon.frame.origin.x + icon.frame.size.width + [UtilManager width:6.]), [UtilManager height:16])];
        [feedLabel setCenter:CGPointMake(feedLabel.center.x, icon.center.y)];
        [feedLabel setTextColor:[UIColor FeedTextColour]];
        [feedLabel setTextAlignment:NSTextAlignmentLeft];
        [feedLabel setFont:[UIFont fontWithName:REGULAR_FONT size:10]];
        [feedLabel setAdjustsFontSizeToFitWidth:YES];
        [feedLabel setBackgroundColor:TEST_BG_ONE];

        [self.contentView addSubview:feedLabel];
        
        self.contentView.layer.cornerRadius = 2.f;
        self.layer.cornerRadius = 2.f;
        self.layer.shadowColor = [GRAY_LOWER_MENU_BORDER_COLOR CGColor];
        self.layer.shadowOffset = CGSizeMake(.0f, .0f);
        self.layer.shadowRadius = 2.0f;
        self.layer.shadowOpacity = 0.5f;
        self.layer.masksToBounds = NO;
        self.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.contentView.layer.cornerRadius].CGPath;   
    }
    
    return self;
}

- (void)setContentLabelText:(NSString *)contentText
{
    if (contentText.length) {
        float height = [contentText boundingRectWithSize:CGSizeMake(self.contentImageView.frame.size.width - 20, CGFLOAT_MAX)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:[UIFont fontWithName:REGULAR_FONT size:16]}
                                                 context:nil].size.height;
        if (height > 22 * 7) {
            height = 22 * 7;
        }
        
        CGRect frame = CGRectMake(10, 8, self.contentImageView.frame.size.width - 20, height);
        if (!self.contentLabel) {
            self.contentLabel = [[UILabel alloc] initWithFrame:frame];
        }
        
        self.contentLabel.numberOfLines = 0;
        self.contentLabel.lineBreakMode = NSLineBreakByTruncatingTail | NSLineBreakByWordWrapping;
        self.contentLabel.font = [UIFont fontWithName:REGULAR_FONT size:16];
        self.contentLabel.backgroundColor = [UIColor clearColor];
        self.contentLabel.text = contentText;
        [self.contentView addSubview:self.contentLabel];
        
        self.contentImageView.hidden = YES;
        self.titleLabel.hidden = YES;
    }
    else {
        self.contentImageView.hidden = NO;
        self.contentLabel.hidden = YES;
        self.titleLabel.hidden = NO;
        [self.contentLabel removeFromSuperview];
        self.contentLabel = nil;
    }
}

- (void)prepareForReuse {
    self.contentImageView.hidden = NO;
    self.contentLabel.hidden = YES;
    self.titleLabel.hidden = NO;
    [self.contentLabel removeFromSuperview];
}

@end
