//
//  HomeRewardsCell.h
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GalleryView.h"
#import "PageControl.h"
#import "UILabel+Extra.h"

@interface HomeRewardsCell : UICollectionViewCell <GalleryDataSource, GalleryDelegate, PageControlDelegate>

@property (nonatomic, strong) UIImageView *backgroundImage;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *feedLabel;
@property (nonatomic, strong) UILabel *feedSubLabel;

@property (nonatomic, strong) GalleryView *galleryView;
@property (nonatomic, strong) PageControl *pageControl;

-(void)configureView;

@end
