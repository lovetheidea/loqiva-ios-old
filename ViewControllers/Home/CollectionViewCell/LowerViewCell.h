//
//  LowerViewCell.h
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"

@interface LowerViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *contentImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *agoLabel;
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *feedLabel;
@property (nonatomic, strong) UILabel *contentLabel;

- (void)setContentLabelText:(NSString *)contentText;

@end
