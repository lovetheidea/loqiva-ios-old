//
//  ProfileView.h
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileViewController.h"

@interface ProfileView : UIView <UIImagePickerControllerDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) UIImageView *profileImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, strong) UIImagePickerController *picker;

@property (nonatomic, assign) ProfileViewController *profileViewController;

@end
