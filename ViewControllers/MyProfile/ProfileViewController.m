//
//  ProfileViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ProfileViewController.h"

#import "ProfileView.h"
#import "NavCustomView.h"
#import "InterestView.h"
#import "ProfileFormView.h"
#import "WebServiceManager.h"
#import "SignNavController.h"
#import "SignUpViewController.h"
#import "AppDelegate.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface ProfileViewController ()<UITableViewDelegate, UITableViewDataSource, InterestViewDelegate, UIImagePickerControllerDelegate, ProfileFormViewDelegate, WebServiceManagerDelegate,UINavigationControllerDelegate>

@end

@implementation ProfileViewController

@synthesize profileTableView;

@synthesize isFromPreferences;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isSectionOpen = FALSE;
    _openSectionIndex = NSNotFound;
    
    [self configureView];
    
    _refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.tintColor = [UIColor orangeAppColor];
    [_refreshControl addTarget:self action:@selector(refreshControlAction) forControlEvents:UIControlEventValueChanged];
    [self.profileTableView addSubview:_refreshControl];
    self.profileTableView.alwaysBounceVertical = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}

- (void)keyboardWasHide:(NSNotification *)notification
{

        
        self.profileTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        CGPoint bottomOffset = CGPointMake(0, self.profileTableView.contentSize.height - self.profileTableView.bounds.size.height);
        [self.profileTableView setContentOffset:bottomOffset animated:YES];
    
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    int kbheight = MIN(keyboardSize.height,keyboardSize.width);
    // int width = MAX(keyboardSize.height,keyboardSize.width);
    
        
        self.profileTableView.contentInset = UIEdgeInsetsMake(0, 0, kbheight, 0);
    
    //your other code here..........
}

- (void)refreshControlAction {
    if (![AppDelegate showAlertIfDisconnected]) {
        [_refreshControl beginRefreshing];
        [self updateProfile];
        [WebServiceManager getInterests:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(isFromPreferences){
        isFromPreferences = FALSE;
        _isSectionOpen = TRUE;
        _openSectionIndex = 3;
        [self openListInSection:3];
    }
}

- (void)configureView{
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    profileTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., 40, WIDTH, HEIGHT -40) style:UITableViewStylePlain];
    [profileTableView setBackgroundColor:[UIColor whiteColor]];
    [profileTableView setDelegate:self];
    [profileTableView setDataSource:self];
    [profileTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [profileTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:0])];
    [footerView setBackgroundColor:[UIColor whiteColor]];
    
    [profileTableView setTableFooterView:footerView];
    
    [self.view addSubview:profileTableView];
    
    [self setupHeaderForSection:CMSectionTypeProfile];
    
}

#pragma mark UITableViewDelegate Methods

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

#pragma mark UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 0;
            break;
        case 1:
            return 0;
            break;
        case 2:
            if(_isSectionOpen && _sectionSelected == section)
                return 1;
            else
                return 0;
            break;
        case 3:
            if(_isSectionOpen && _sectionSelected == section)
                return 1;
            else
                return 0;
            break;
        default:
            return 0;
            break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [profileTableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell setBackgroundColor:[UIColor orangeAppColor]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    switch (indexPath.section) {
        case 2:{
            ProfileFormView *profileFormView = [[ProfileFormView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:830])];
            [profileFormView setDelegate:self];
            [[cell contentView] addSubview:profileFormView];
        }
            break;
        case 3:{
            if (!self.interestView) {
                NSInteger heightOfView = [InterestView heightOfView];
                self.interestView = [[InterestView alloc] initWithFrame:CGRectMake(0., 0, WIDTH, heightOfView)];
            }
            
            [[self.interestView useAppButton] setTitle:NSLocalizedString(@"Save Preferences", nil)  forState:UIControlStateNormal];
            [self.interestView setDelegate:self];
            [[cell contentView] addSubview:self.interestView];
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 0;
            break;
        case 1:
            return [UtilManager height:340];
            break;
        case 2:
            return [UtilManager height:60];
            break;
        case 3:
            return [UtilManager height:60];
            break;
        case 4:
            return [UtilManager height:80];
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 2:
            return [UtilManager height:830];
            break;
        case 3:
            return [InterestView heightOfView];
            break;
        default:
            return 0;
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (section) {
       
        case 1:
            return [self profileView];
            break;
        case 2:{
            ProfileHeaderView *profileHeaderView = [[ProfileHeaderView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:60]) title:NSLocalizedString(@"Your Details", nil) image:@"" section:section delegate:self];
            if(_isSectionOpen && _sectionSelected == section)
            {
                [[profileHeaderView separatorView] setHidden:FALSE];
                [[profileHeaderView icon] setImage:[UIImage imageNamed:@"btn-arrow-up"]];
            }
            return profileHeaderView;
        }
            break;
        case 3:{
            ProfileHeaderView *profileHeaderView = [[ProfileHeaderView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:60]) title:NSLocalizedString(@"Your Preferences", nil) image:@"" section:section delegate:self];
            if(_isSectionOpen && _sectionSelected == section)
            {
                [[profileHeaderView separatorView] setHidden:FALSE];
                [[profileHeaderView icon] setImage:[UIImage imageNamed:@"btn-arrow-up"]];
            }
            return profileHeaderView;
        }
            break;
        case 4:
            return [self logView];
            break;
        default:
            return nil;
            break;
    }
}

- (UIView *)logView{
    UIView *logView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:170])];
    [logView setBackgroundColor:[UIColor whiteColor]];
    UIImage *button = [UIImage imageNamed:@"btn-logout"];
    UIButton *logButton = [[UIButton alloc] initWithFrame:CGRectMake(0., [UtilManager height:20], [UtilManager width:195], [UtilManager height:40])];
    [logButton setCenter:CGPointMake(self.view.center.x, logButton.center.y)];
    [logButton setImage:button forState:UIControlStateNormal];
    [logButton addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    [logView addSubview:logButton];
    
    return logView;
}

- (UIView *)profileView{
    _profileView = [[ProfileView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:300])];
    [_profileView setProfileViewController:self];
    return _profileView;
}

- (void)slideAction{
    [[NSNotificationCenter defaultCenter] postNotificationName:SLIDE_NOTIFICATION object:self];
}

- (void)notificationAction{
    [[NSNotificationCenter defaultCenter] postNotificationName:ACTION_NOTIFICATION object:self];
}

#pragma mark ProfileSectionHeaderView

- (void)openListInSection:(NSInteger)section
{
    if ([AppDelegate showAlertIfDisconnected]) {
        return;
    }
    
    _isSectionOpen = TRUE;
    
    _sectionSelected = section;
    
    NSInteger countOfRowsToInsert;
    
    countOfRowsToInsert = 1;
    
    if(countOfRowsToInsert != 0)
    {
        NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToInsert; i++)
        {
            [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        NSInteger previousOpenSectionIndex = self.openSectionIndex;
        
        UITableViewRowAnimation insertAnimation;
        UITableViewRowAnimation deleteAnimation;
        if (previousOpenSectionIndex == NSNotFound || section < previousOpenSectionIndex)
        {
            insertAnimation = UITableViewRowAnimationTop;
            deleteAnimation = UITableViewRowAnimationBottom;
        }
        else
        {
            insertAnimation = UITableViewRowAnimationBottom;
            deleteAnimation = UITableViewRowAnimationTop;
        }
        
        // Apply the updates.
        [profileTableView beginUpdates];
        [profileTableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
        [profileTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
        [profileTableView endUpdates];
        _openSectionIndex = section;
    }
}

- (void)profileHeaderView:(ProfileHeaderView *)profileHeaderView sectionOpened:(NSInteger)section
{
    if(_isSectionOpen)
    {
        if(section == _sectionSelected)
        {
            _isSectionOpen = FALSE;
            [self profileHeaderView:profileHeaderView sectionClosed:section];
        }
        else
        {
            _isSectionOpen = FALSE;
            [self profileHeaderView:profileHeaderView sectionClosed:_sectionSelected];
            [self openListInSection:section];
        }
    }
    else
    {
        [self openListInSection:section];
    }
    
    [profileTableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)profileHeaderView:(ProfileHeaderView *)profileHeaderView sectionClosed:(NSInteger)section
{
    NSInteger countOfRowsToDelete;
    
    countOfRowsToDelete = 1;
    
    if (countOfRowsToDelete > 0)
    {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++)
        {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        
        [profileTableView beginUpdates];
        [profileTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
        [profileTableView endUpdates];
    }
    _openSectionIndex = NSNotFound;
}

#pragma mark InterestViewDelegate Methods

- (void)actionDidPush{
    ProfileHeaderView *profileHeaderView;
    [self profileHeaderView:profileHeaderView sectionOpened:_sectionSelected];

}

#pragma mark ProfileFormViewdelegate Methods

- (void)savePreferencesAction{
    ProfileHeaderView *profileHeaderView;
    [self profileHeaderView:profileHeaderView sectionOpened:_sectionSelected];
}

#pragma mark UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    [[_profileView profileImageView] setImage:image];
    [[_profileView profileImageView] setContentMode:UIViewContentModeScaleAspectFit];
    
    [self uploadProfilePicture: image];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    }];
}

#pragma mark WebServiceDelegaMethods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
    [_refreshControl endRefreshing];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    if (webServiceType == WebServiceTypeInterests) {
        _interestView = nil;
    }
    
    [_refreshControl endRefreshing];
    [self.profileTableView reloadData];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)objec{
    [_refreshControl endRefreshing];
}

- (void)backToStartApplication{
    SignUpViewController *viewController = [[SignUpViewController alloc] init];
    SignNavController *customNavigationController = [[SignNavController alloc] initWithRootViewController:viewController];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:customNavigationController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:customNavigationController];
     }];
}

@end
