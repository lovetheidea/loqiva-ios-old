//
//  ProfileViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "ProfileHeaderView.h"
@class ProfileView;
@class InterestView;

@interface ProfileViewController : ParentViewController<ProfileHeaderViewDelegate>

@property (nonatomic, assign) BOOL isFromPreferences;
@property (nonatomic, strong) UITableView *profileTableView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic) BOOL isSectionOpen;
@property (nonatomic, assign) NSInteger sectionSelected;
@property (nonatomic, assign) NSInteger openSectionIndex;
@property (nonatomic, strong) ProfileView *profileView;
@property (nonatomic, strong) InterestView *interestView;

- (void)backToStartApplication;
@end
