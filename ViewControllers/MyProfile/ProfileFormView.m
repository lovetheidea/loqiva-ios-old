//
//  ProfileFormView.m
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ProfileFormView.h"
#import "RegularExpressionsManager.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "User.h"
#import "UIColor+Helper.h"
#import "AppContext.h"
#import "AppDelegate.h"
#import "loqiva-Swift.h"

@implementation ProfileFormView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor orangeAppColor]];
        
        User *currentUser = [[SessionManager shared] user];
        
        
        
        UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20.], [UtilManager height:40], [UtilManager width:290], [UtilManager height:120])];
        [infoLabel setCenter:CGPointMake(self.frame.size.width / 2, infoLabel.center.y)];
        [infoLabel setAttributedText: NSLocalizedString(@"Have you changed your details? \n Update them here.", nil) withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:WHITE_TEXT_COLOR andKern:TITLE_FONT_KERN withLineBreak:NO];
        [infoLabel setNumberOfLines:0];
        [infoLabel setBackgroundColor:[UIColor clearColor]];
        [infoLabel setAdjustsFontSizeToFitWidth:YES];
        [self addSubview:infoLabel];

        
        
        
        
        
        
        
        UILabel *firstNameLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:180], [UtilManager width:300], [UtilManager height:20])];
        [firstNameLabel setTextColor:[UIColor whiteColor]];
        [firstNameLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:14]];
        [firstNameLabel setText:NSLocalizedString(@"First Name", nil)];
        
        [self addSubview:firstNameLabel];
        
        
        
        UIImageView *firstNameTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:335.], [UtilManager height:50])];
        [firstNameTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
        [firstNameTextFieldBacgroundView setCenter:CGPointMake(self.center.x, firstNameLabel.frame.origin.y + firstNameLabel.frame.size.height + [UtilManager height:10] + firstNameTextFieldBacgroundView.frame.size.height/2)];
        [self addSubview:firstNameTextFieldBacgroundView];
        
        _firstNameTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:15],[UtilManager height:12], firstNameTextFieldBacgroundView.frame.size.width, UITEXTFIELD_HEIGHT)];
        [_firstNameTextField setBackgroundColor:[UIColor clearColor]];
        [_firstNameTextField setCenter:CGPointMake(self.center.x + 5,firstNameTextFieldBacgroundView.center.y)];
        [_firstNameTextField setBorderStyle:UITextBorderStyleNone];
        [_firstNameTextField setTextColor:GRAY_TEXT_COLOR];
        [_firstNameTextField setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
        [_firstNameTextField setText:currentUser.firstName];
        [_firstNameTextField setKeyboardType:UIKeyboardTypeEmailAddress];
        [_firstNameTextField setReturnKeyType:UIReturnKeyNext];
        [_firstNameTextField setLeftViewMode:UITextFieldViewModeAlways];
        [_firstNameTextField setDelegate:self];
        
        UIView *leftFirstNameTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
        leftFirstNameTextFieldView.backgroundColor = [UIColor clearColor];
        [_firstNameTextField setLeftView:leftFirstNameTextFieldView];
        [self addSubview:_firstNameTextField];
        
        
        
        
        
        
        
        
        UILabel *lastNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(firstNameLabel.frame.origin.x, _firstNameTextField.frame.origin.y + _firstNameTextField.frame.size.height + [UtilManager height:12], [UtilManager width:300], [UtilManager height:20])];
        [lastNameLabel setTextColor:[UIColor whiteColor]];
        [lastNameLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:14]];
        [lastNameLabel setText:NSLocalizedString(@"Last Name", nil)];
        
        [self addSubview:lastNameLabel];
        
        UIImageView *lastNameTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(firstNameTextFieldBacgroundView.frame.origin.x, 0, firstNameTextFieldBacgroundView.frame.size.width, firstNameTextFieldBacgroundView.frame.size.height)];
        [lastNameTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
        [lastNameTextFieldBacgroundView setCenter:CGPointMake(self.center.x, lastNameLabel.frame.origin.y + lastNameLabel.frame.size.height + [UtilManager height:10] + firstNameTextFieldBacgroundView.frame.size.height/2)];
        [self addSubview:lastNameTextFieldBacgroundView];
        
        _lastNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(_firstNameTextField.frame.origin.x, _firstNameTextField.frame.origin.y + _firstNameTextField.frame.size.height + [UtilManager height:12], _firstNameTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
        [_lastNameTextField setBorderStyle:UITextBorderStyleNone];
        [_lastNameTextField setBackgroundColor:[UIColor clearColor]];
        [_lastNameTextField setCenter:CGPointMake(self.center.x + 5,lastNameTextFieldBacgroundView.center.y)];
        [_lastNameTextField setTextColor:GRAY_TEXT_COLOR];
        [_lastNameTextField setTextAlignment:NSTextAlignmentLeft];
        [_lastNameTextField setBorderStyle:UITextBorderStyleNone];
        [_lastNameTextField setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
        [_lastNameTextField setText:NSLocalizedString(@"Last Name", nil)];
        [_lastNameTextField setText:currentUser.lastName];
        [_lastNameTextField setKeyboardType:UIKeyboardTypeEmailAddress];
        [_lastNameTextField setReturnKeyType:UIReturnKeyNext];
        [_lastNameTextField setLeftViewMode:UITextFieldViewModeAlways];
        [_lastNameTextField setDelegate:self];

        UIView *leftLastNameTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
        leftLastNameTextFieldView.backgroundColor = [UIColor clearColor];
        [_lastNameTextField setLeftView:leftLastNameTextFieldView];
        [self addSubview:_lastNameTextField];
        
        
        
        
        
        
        
        
        UILabel *mailLabel = [[UILabel alloc] initWithFrame:CGRectMake(firstNameLabel.frame.origin.x, _lastNameTextField.frame.origin.y + _lastNameTextField.frame.size.height + [UtilManager height:12], [UtilManager width:300], [UtilManager height:20])];
        [mailLabel setTextColor:[UIColor whiteColor]];
        [mailLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:14]];
        [mailLabel setText:NSLocalizedString(@"Email", nil)];
        
        [self addSubview:mailLabel];
        
        UIImageView *mailTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(firstNameTextFieldBacgroundView.frame.origin.x, 0, firstNameTextFieldBacgroundView.frame.size.width, firstNameTextFieldBacgroundView.frame.size.height)];
        [mailTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
        [mailTextFieldBacgroundView setCenter:CGPointMake(self.center.x, mailLabel.frame.origin.y + mailLabel.frame.size.height + [UtilManager height:10] + mailTextFieldBacgroundView.frame.size.height/2)];
        [self addSubview:mailTextFieldBacgroundView];
        
        _mailTextField = [[UITextField alloc] initWithFrame:CGRectMake(_firstNameTextField.frame.origin.x, mailTextFieldBacgroundView.frame.origin.y + mailTextFieldBacgroundView.frame.size.height + [UtilManager height:12], _firstNameTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
        [_mailTextField setCenter:CGPointMake(_mailTextField.center.x, mailTextFieldBacgroundView.center.y)];
        [_mailTextField  setBackgroundColor:[UIColor clearColor]];
        [_mailTextField setBorderStyle:UITextBorderStyleNone];
        [[_mailTextField layer] setMasksToBounds:YES];
        [_mailTextField setTextColor:GRAY_TEXT_COLOR];
        [_mailTextField setTextAlignment:NSTextAlignmentLeft];
        [_mailTextField setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
        [_mailTextField setText:NSLocalizedString(@"Email", nil)];
        [_mailTextField setText:currentUser.mail];
        [_mailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
        [_mailTextField setReturnKeyType:UIReturnKeyNext];
        [_mailTextField setLeftViewMode:UITextFieldViewModeAlways];
        [_mailTextField setDelegate:self];
        
        UIView *mailTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
        [_mailTextField setLeftView:mailTextFieldView];
        [self addSubview:_mailTextField];
        
        
        
        
        
        
        UILabel *postcodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(firstNameLabel.frame.origin.x, _mailTextField.frame.origin.y + _mailTextField.frame.size.height+ [UtilManager height:12], [UtilManager width:300], [UtilManager height:20])];
        [postcodeLabel setTextColor:[UIColor whiteColor]];
        [postcodeLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:14]];
        [postcodeLabel setText:NSLocalizedString(@"Post Code", nil)];
        
        [self addSubview:postcodeLabel];
        
        UIImageView *postcodeTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(firstNameTextFieldBacgroundView.frame.origin.x, 0, firstNameTextFieldBacgroundView.frame.size.width, firstNameTextFieldBacgroundView.frame.size.height)];
        [postcodeTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
        [postcodeTextFieldBacgroundView setCenter:CGPointMake(self.center.x, postcodeLabel.frame.origin.y + postcodeLabel.frame.size.height + [UtilManager height:10] + postcodeTextFieldBacgroundView.frame.size.height/2)];
        [self addSubview:postcodeTextFieldBacgroundView];
        
        _postcodeTextField = [[UITextField alloc] initWithFrame:CGRectMake(_firstNameTextField.frame.origin.x, _mailTextField.frame.origin.y + _mailTextField.frame.size.height + [UtilManager height:12], _firstNameTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
        [_postcodeTextField setBackgroundColor:[UIColor clearColor]];
        [_postcodeTextField setBorderStyle:UITextBorderStyleNone];
        [[_postcodeTextField layer] setMasksToBounds:YES];
        [_postcodeTextField setKeyboardType:UIKeyboardTypeEmailAddress];
        [_postcodeTextField setCenter:CGPointMake(self.center.x + 5,postcodeTextFieldBacgroundView.center.y)];
        [_postcodeTextField setTextColor:GRAY_TEXT_COLOR];
        [_postcodeTextField setTextAlignment:NSTextAlignmentLeft];
        [_postcodeTextField setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
        [_postcodeTextField setText:NSLocalizedString(@"Postcode", nil)];
        [_postcodeTextField setText:currentUser.postcode];
        [_postcodeTextField setReturnKeyType:UIReturnKeyNext];
        [_postcodeTextField setLeftViewMode:UITextFieldViewModeAlways];
        [_postcodeTextField setDelegate:self];
        
        UIView *postcodeTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
        [_postcodeTextField setLeftView:postcodeTextFieldView];
        [self addSubview:_postcodeTextField];
        
        
        
        
        
        
        
        UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(firstNameLabel.frame.origin.x, _postcodeTextField.frame.origin.y + _postcodeTextField.frame.size.height+ [UtilManager height:12], [UtilManager width:300], [UtilManager height:20])];
        [phoneLabel setTextColor:[UIColor whiteColor]];
        [phoneLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:14]];
        [phoneLabel setText:NSLocalizedString(@"Telephone Number", nil)];
        [self addSubview:phoneLabel];
        
        
        
        UIImageView *phoneTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., firstNameTextFieldBacgroundView.frame.size.width, firstNameTextFieldBacgroundView.frame.size.height)];
        [phoneTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
        [phoneTextFieldBacgroundView setCenter:CGPointMake(self.center.x, phoneLabel.frame.origin.y + phoneLabel.frame.size.height + [UtilManager height:10] + firstNameTextFieldBacgroundView.frame.size.height/2)];
        [self addSubview:phoneTextFieldBacgroundView];
        
        self.telephoneTextField = [[UITextField alloc] initWithFrame:CGRectMake(_firstNameTextField.frame.origin.x, _postcodeTextField.frame.origin.y + _postcodeTextField.frame.size.height + [UtilManager height:12], _firstNameTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
        [self.telephoneTextField setCenter:CGPointMake(self.center.x + 5, phoneTextFieldBacgroundView.center.y)];
        [self.telephoneTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [self.telephoneTextField setBorderStyle:UITextBorderStyleNone];
        [self.telephoneTextField setTextAlignment:NSTextAlignmentLeft];
        [self.telephoneTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
        [self.telephoneTextField setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
        [self.telephoneTextField setTextColor:GRAY_TEXT_COLOR];
        [self.telephoneTextField setText:currentUser.phoneNumber];
        [self.telephoneTextField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
        [self.telephoneTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        [self.telephoneTextField setReturnKeyType:UIReturnKeyNext];
        [self.telephoneTextField setLeftViewMode:UITextFieldViewModeAlways];
        [self.telephoneTextField setDelegate:self];
        
        UIView *phoneTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
        [self.telephoneTextField setLeftView:phoneTextFieldView];
        [self addSubview:self.telephoneTextField];
        
        
        
        
        
        
        
        
        UILabel *passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(firstNameLabel.frame.origin.x, _telephoneTextField.frame.origin.y + _telephoneTextField.frame.size.height+ [UtilManager height:12], [UtilManager width:300], [UtilManager height:20])];
        [passwordLabel setTextColor:[UIColor whiteColor]];
        [passwordLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:14]];
        [passwordLabel setText:NSLocalizedString(@"Password", nil)];
        
        [self addSubview:passwordLabel];
        
        UIImageView *passwordTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(firstNameTextFieldBacgroundView.frame.origin.x, 0, firstNameTextFieldBacgroundView.frame.size.width, firstNameTextFieldBacgroundView.frame.size.height)];
        [passwordTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
        [passwordTextFieldBacgroundView setCenter:CGPointMake(self.center.x, passwordLabel.frame.origin.y + passwordLabel.frame.size.height + [UtilManager height:10] + passwordTextFieldBacgroundView.frame.size.height/2)];
        [self addSubview:passwordTextFieldBacgroundView];
        
        _passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(_firstNameTextField.frame.origin.x, _telephoneTextField.frame.origin.y + _telephoneTextField.frame.size.height + [UtilManager height:12], _firstNameTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
        [_passwordTextField setCenter:CGPointMake(_passwordTextField.center.x + 5, passwordTextFieldBacgroundView.center.y)];
        [_passwordTextField setBackgroundColor:[UIColor clearColor]];
        [_passwordTextField setBorderStyle:UITextBorderStyleNone];
        [[_passwordTextField layer] setMasksToBounds:YES];
        [_passwordTextField setTextColor:GRAY_TEXT_COLOR];
        [_passwordTextField setTextAlignment:NSTextAlignmentLeft];
        [_passwordTextField setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
        [_passwordTextField setText:currentUser.password];
        [_passwordTextField setSecureTextEntry:TRUE];
        [_passwordTextField setKeyboardType:UIKeyboardTypeEmailAddress];
        [_passwordTextField setReturnKeyType:UIReturnKeyDone];
        [_passwordTextField setLeftViewMode:UITextFieldViewModeAlways];
        [_passwordTextField setRightViewMode:UITextFieldViewModeWhileEditing];
        [_passwordTextField setDelegate:self];
        
        UIView *passwordFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
        [_passwordTextField setLeftView:passwordFieldView];
        
        UIView *passwordTextViewContainer = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:-55],0,[UtilManager width:45],UITEXTFIELD_HEIGHT)];
        
        UIButton *showPasswordFieldView = [[UIButton alloc] initWithFrame:CGRectMake(0.,0,[UtilManager width:25],[UtilManager height:15])];
        [showPasswordFieldView setCenter:CGPointMake(showPasswordFieldView.center.x, passwordTextViewContainer.center.y)];
        [showPasswordFieldView setBackgroundColor:[UIColor clearColor]];
        [showPasswordFieldView setImage:[UIImage imageNamed:@"btn-show-password"] forState:UIControlStateNormal];
        [showPasswordFieldView addTarget:self action:@selector(showPassword) forControlEvents:UIControlEventTouchUpInside];
        [passwordTextViewContainer addSubview:showPasswordFieldView];
        [_passwordTextField setRightView:passwordTextViewContainer];
        [self addSubview:_passwordTextField];
        
        
        
        
        
//        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(postcodeTextFieldView.frame.origin.x, self.passwordTextField.frame.origin.y + self.passwordTextField.frame.size.height + [UtilManager height:17], self.frame.size.width - 24, 1)];
//        [separatorView setBackgroundColor:[UIColor whiteColor]];
//        [separatorView setAlpha:0.8];
//
//        [self addSubview:separatorView];
        
        
        
        
        
        UIButton *saveButton = [[UIButton alloc] initWithFrame:CGRectMake(0., passwordTextFieldBacgroundView.frame.origin.y + passwordTextFieldBacgroundView.frame.size.height + [UtilManager height:17], [UtilManager width:124.], [UtilManager width:40])];
        [saveButton setCenter:CGPointMake(self.center.x, saveButton.center.y)];
        [saveButton setBackgroundImage:[UIImage imageNamed:@"btn-save-details"] forState:UIControlStateNormal];
        [saveButton addTarget:self action:@selector(saveAction) forControlEvents:UIControlEventTouchUpInside];
        [[saveButton titleLabel] setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
        
        [self addSubview:saveButton];
    }
    
    return self;

}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if([textField isEqual:_passwordTextField])
        [_passwordTextField setSecureTextEntry:YES];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:_passwordTextField]) {
        [_passwordTextField resignFirstResponder];
    }
    if([textField isEqual:_firstNameTextField]) {
        [_lastNameTextField becomeFirstResponder];
    }
    if([textField isEqual:_lastNameTextField]) {
        [_mailTextField becomeFirstResponder];
    }
    if([textField isEqual:_postcodeTextField]) {
        [_telephoneTextField becomeFirstResponder];
    }
    if([textField isEqual:_mailTextField]) {
        [_postcodeTextField becomeFirstResponder];
    }
    if ([textField isEqual:_telephoneTextField]) {
        [_passwordTextField becomeFirstResponder];
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength;
    
    newLength = [textField.text length] + [string length] - range.length;
    
    if([textField isEqual:_firstNameTextField])
        return (newLength > 30) ? NO : YES;
    if([textField isEqual:_lastNameTextField])
        return (newLength > 30) ? NO : YES;
    if([textField isEqual:_passwordTextField])
        return (newLength > 25) ? NO : YES;
    if([textField isEqual:_mailTextField])
        return (newLength > 100) ? NO : YES;
    if([textField isEqual:_postcodeTextField])
        return (newLength > 10) ? NO : YES;
    return YES;
}

- (void)showPassword{
    if(_passwordTextField.isSecureTextEntry)
        [_passwordTextField setSecureTextEntry:FALSE];
    else
        [_passwordTextField setSecureTextEntry:TRUE];
}

- (void)saveAction{
    
    if([self checkFields]){
        
        if (![AppDelegate showAlertIfDisconnected]) {
            NSDictionary *profileDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:_firstNameTextField.text,@"firstname",_lastNameTextField.text,@"lastname",_passwordTextField.text,@"password",_postcodeTextField.text,@"postcode",_mailTextField.text,@"email", _telephoneTextField.text,@"mobile", nil];
            [WebServiceManager saveProfile:profileDictionary andDelegate:self];
        }
    }
}

- (BOOL)checkFields{
        
        if(_firstNameTextField.text.length == 0){
            [UtilManager presentAlertWithMessage:NSLocalizedString(@"First Name cannot be empty", nil)];
            return FALSE;
        }
        
        if(_firstNameTextField.text.length < 3){
            [UtilManager presentAlertWithMessage:NSLocalizedString(@"First Name cannot be less that 3 letters", nil)];
            return FALSE;
        }
        
        if(_lastNameTextField.text.length == 0){
            [UtilManager presentAlertWithMessage:NSLocalizedString(@"Last Name cannot be empty", nil)];
            return FALSE;
        }
        
        if(_firstNameTextField.text.length < 2){
            [UtilManager presentAlertWithMessage:NSLocalizedString(@"Last Name cannot be less that 2 letters", nil)];
            return FALSE;
        }
        
        if(_postcodeTextField.text.length == 0){
            [UtilManager presentAlertWithMessage:NSLocalizedString(@"Zip Code cannot be empty", nil)];
            return FALSE;
        }
        
        if(_postcodeTextField.text.length < 5){
            [UtilManager presentAlertWithMessage:NSLocalizedString(@"Zip Code cannot be less that 5 letters", nil)];
            return FALSE;
        }
        
        if(_mailTextField.text.length == 0){
            [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please enter a valid email address", nil)];
            return FALSE;
        }
        
        if(![RegularExpressionsManager checkMail:_mailTextField.text]){
            [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please enter a valid email address", nil)];
            return FALSE;
        }
        
        if(_passwordTextField.text.length == 0){
            [UtilManager presentAlertWithMessage:NSLocalizedString(@"Password cannot be empty", nil)];
            return FALSE;
        }
        if(_passwordTextField.text.length < 6 ){
            [UtilManager presentAlertWithMessage:NSLocalizedString(@"Passwords need to be a minimum of 6 characters. No special characters are allowed.", nil)];
            return FALSE;
        }
    
        if (![self validatePhone:_telephoneTextField.text]) {
            [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please enter a valid phone number", nil)];
            return FALSE;
        }
    
        return TRUE;

}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^[0-9]{8,12}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:[phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""]];
}

#pragma mark WebServiceManagerDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    if([_delegate respondsToSelector:@selector(savePreferencesAction)])
        [_delegate savePreferencesAction];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    [[[SessionManager shared] user] setFirstName:_firstNameTextField.text];
    [[[SessionManager shared] user] setLastName:_lastNameTextField.text];
    [[[SessionManager shared] user] setMail:_mailTextField.text];
    [[[SessionManager shared] user] setPostcode:_postcodeTextField.text];
    [[[SessionManager shared] user] setPassword:_passwordTextField.text];
    [[[SessionManager shared] user] setPhoneNumber:_telephoneTextField.text];
    
    [[SessionManager shared] storeLoggedInUserToDisk];

    if([_delegate respondsToSelector:@selector(savePreferencesAction)])
        [_delegate savePreferencesAction];
}

@end
