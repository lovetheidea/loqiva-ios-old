//
//  ProfileFormView.h
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceManager.h"

@protocol ProfileFormViewDelegate;

@interface ProfileFormView : UIView <UITextFieldDelegate, WebServiceManagerDelegate>

@property (nonatomic, assign) id<ProfileFormViewDelegate>delegate;
@property (nonatomic, strong) UITextField *firstNameTextField;
@property (nonatomic, strong) UITextField *lastNameTextField;
@property (nonatomic, strong) UITextField *mailTextField;
@property (nonatomic, strong) UITextField * passwordTextField;
@property (nonatomic, strong) UITextField *postcodeTextField;
@property (nonatomic, strong) UITextField *telephoneTextField;

@end

@protocol ProfileFormViewDelegate <NSObject>

- (void)savePreferencesAction;

@end
