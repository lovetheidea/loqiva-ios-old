//
//  ProfileView.m
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ProfileView.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "User.h"
#import "AppContext.h"
#import "AFNetworkReachabilityManager.h"
#import "UIImageView+AFNetworking.h"
#import "UIColor+Helper.h"
#import "WebServiceManager.h"
#import "AppDelegate.h"
#import "loqiva-Swift.h"

@interface ProfileView ()

@end

@implementation ProfileView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor whiteColor]];
        
        _profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0., [UtilManager height:60], [UtilManager height:130], [UtilManager height:130])];
        [_profileImageView setCenter:CGPointMake(self.center.x, _profileImageView.center.y)];
        [[_profileImageView layer] setMasksToBounds:YES];
        [[_profileImageView layer] setCornerRadius:_profileImageView.frame.size.width/2];
        [_profileImageView setContentMode:UIViewContentModeScaleAspectFill];
        
        NSString *profileImageURL = [[[SessionManager shared] user] sourceImage];
        
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],profileImageURL]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        [_profileImageView setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageNamed:@"btn-profile-photo"] success:nil failure:nil];
        
        //[_profileImageView setImage:[UIImage imageNamed:@"temp-profile-photo"]];
        [_profileImageView setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadImage)];
        [_profileImageView addGestureRecognizer:tapGestureRecognizer];
        
        [self addSubview:_profileImageView];
        
        
        
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., _profileImageView.frame.origin.y + _profileImageView.frame.size.height + [UtilManager height:20], WIDTH, [UtilManager height:30])];
        
                User *currentUser = [[SessionManager shared] user];
        
        [_nameLabel setAttributedText:[NSString stringWithFormat:@"Hi %@", currentUser.firstName] withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:TITLE_FONT_KERN withLineBreak:NO];
        [_nameLabel setAdjustsFontSizeToFitWidth:YES];
        [self addSubview:_nameLabel];
        
        _descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:40], _nameLabel.frame.origin.y + _nameLabel.frame.size.height + [UtilManager height:5], [UtilManager width:335], [UtilManager height:58])];
        [_descriptionLabel setText:NSLocalizedString(@"Update your preferences\n for a richer app experience ", nil)];
        [_descriptionLabel setCenter:CGPointMake(self.center.x, _descriptionLabel.center.y)];
        [_descriptionLabel setNumberOfLines:2];
        [_descriptionLabel setTextAlignment:NSTextAlignmentCenter];
        [_descriptionLabel setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
        [_descriptionLabel setTextColor:[UtilManager colorwithHexString:@"787878" alpha:1]];
        [_descriptionLabel setAdjustsFontSizeToFitWidth:YES];
        
        [self addSubview:_descriptionLabel];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20], _descriptionLabel.frame.origin.y + _descriptionLabel.frame.size.height + [UtilManager height:30], WIDTH - 2 *[UtilManager width:20], 1)];
        [_separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        
        [self addSubview:_separatorView];
    }

    return self;
}

- (void)uploadImage{
    
    if ([AppDelegate showAlertIfDisconnected]) {
        return;
    }
    
    [_profileImageView setUserInteractionEnabled:FALSE];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select an option.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Take a picture", nil)];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Select from gallery", nil)];
    
    [actionSheet showInView:self];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0){
        NSLog(@"Cancel Action Sheet Did Push");
        [_profileImageView setUserInteractionEnabled:TRUE];
    }
    else
    {
        if(buttonIndex == 1)
        {
            _picker = [[UIImagePickerController alloc] init];
            [_picker setAllowsEditing:YES];
            [_picker setDelegate:_profileViewController];
            [_picker setSourceType:UIImagePickerControllerSourceTypeCamera];
            
            [_profileViewController presentViewController:_picker animated:YES completion:^{
                [_profileImageView setUserInteractionEnabled:TRUE];
            }];
        }
        else if(buttonIndex == 2)
        {
            _picker = [[UIImagePickerController alloc] init];
            [_picker setAllowsEditing:YES];
            [_picker setDelegate:_profileViewController];
            [_picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            [_profileViewController presentViewController:_picker animated:YES completion:^{
                [_profileImageView setUserInteractionEnabled:TRUE];
            }];
            
            
        }
    }
}

@end
