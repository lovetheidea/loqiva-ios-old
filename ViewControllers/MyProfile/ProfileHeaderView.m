//
//  ProfileHeaderView.m
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ProfileHeaderView.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "UILabel+Extra.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@implementation ProfileHeaderView

@synthesize delegate, section, titleLabel, isOpen, icon;
@synthesize separatorView;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title image:(NSString *)image section:(NSInteger)sectionNumber delegate:(id<ProfileHeaderViewDelegate>)sectionDelegate
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleOpen:)];
        [self addGestureRecognizer:tapGesture];
        
        self.delegate = sectionDelegate;
        self.userInteractionEnabled = YES;
        self.section = sectionNumber;
        
        [self setBackgroundColor:[UIColor whiteColor]];
        
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20.], 0., WIDTH - [UtilManager width:40], [UtilManager height:60])];
        
        [titleLabel setAttributedText:title withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:TITLE_FONT_KERN withLineBreak:NO];
        [titleLabel setNumberOfLines:0.];
        [titleLabel setUserInteractionEnabled:YES];
        
        
        
        [self addSubview:titleLabel];
        
        UIImage *arrow = [UIImage imageNamed:@"btn-arrow-down"];
        
        icon = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., arrow.size.width,arrow.size.height)];
        [icon setContentMode:UIViewContentModeScaleAspectFit];
        [icon setCenter:CGPointMake(titleLabel.frame.origin.x + titleLabel.frame.size.width - arrow.size.width/2, titleLabel.center.y)];
        [icon setImage:arrow];
        
        [self addSubview:icon];
        
        separatorView = [[UIView alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, [UtilManager height:59], titleLabel.frame.size.width, 1)];
        [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        
        [self addSubview:separatorView];
    }
    return self;
}

- (void)toggleOpen:(id)sender
{
    if(isOpen)
    {
        if([self.delegate respondsToSelector:@selector(profileHeaderView:sectionClosed:)])
        {
            [delegate profileHeaderView:self sectionClosed:self.section];
        }
    }
    else
    {
        if([delegate respondsToSelector:@selector(profileHeaderView:sectionOpened:)])
        {
            [delegate profileHeaderView:self sectionOpened:self.section];
        }
    }
}

- (void)setNeedsDisplay
{
    
}


@end
