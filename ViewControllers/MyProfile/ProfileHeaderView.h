//
//  ProfileHeaderView.h
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol ProfileHeaderViewDelegate;

@interface ProfileHeaderView : UIControl

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic) NSInteger section;
@property (nonatomic) BOOL isOpen;
@property (nonatomic, weak) id <ProfileHeaderViewDelegate>delegate;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title image:(NSString *)image section:(NSInteger)sectionNumber delegate:(id<ProfileHeaderViewDelegate>)delegate;

@end

@protocol ProfileHeaderViewDelegate <NSObject>

@optional
- (void)profileHeaderView:(ProfileHeaderView *)profileHeaderView sectionOpened:(NSInteger)section;
- (void)profileHeaderView:(ProfileHeaderView *)profileHeaderView sectionClosed:(NSInteger)section;

@end
