//
//  ToursDetailViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 29/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InterestDetail.h"
#import "POIDetailViewController.h"

@interface ToursDetailViewController : POIDetailViewController

@property (nonatomic, strong) InterestDetail *currentInterestDetail;

@end
