//
//  ToursDetailViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 29/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ToursDetailViewController.h"
#import "AFNetworkReachabilityManager.h"
#import "WebServiceManager.h"
#import "UIImageView+AFNetworking.h"
#import "NavCustomView.h"
#import "AppContext.h"
#import "LocationManager.h"
#import "WebViewController.h"
#import "MapViewController.h"
#import "Utility.h"
#import "NSString+HTML.h"
#import "CustomAnnotation.h"
#import "CustomPinAnnotationView.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface ToursDetailViewController ()<CLLocationManagerDelegate, MKMapViewDelegate, MKAnnotation, WebServiceManagerDelegate, UIScrollViewDelegate>

@property (nonatomic, assign) BOOL isRedeemPush;
@property (nonatomic, assign) BOOL isFirstLoad;

@property (nonatomic, strong) UIView *navCustomView;

@property (nonatomic, strong) UIView *redeemUserView;
@property (nonatomic, strong) UIView *fourSeparatorView;
@property (nonatomic, strong) UIView *footerView;

@property (nonatomic, assign) CGFloat originY;

@property (nonatomic, strong) UIImageView *rewardImageView;
@property (nonatomic, strong) UILabel *distanceLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *dateLabel;

@property (nonatomic, strong) UILabel *trailLabel;
@property (nonatomic, strong) UILabel *estimatedLabel;

@property (nonatomic, strong) UIButton *confirmButton;

@property (nonatomic, strong) MKAnnotationView *selectedAnnotationView;

@end

@implementation ToursDetailViewController

@synthesize scrollView, mapView, coordinate, manager;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isRedeemPush = FALSE;
    _isFirstLoad = TRUE;
    
    [self createLocationManager];
    
    [self configureView];
    [self addAnnotations];
    [super zoomToFitMapAnnotations];
}

- (void)addAnnotations{
    CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(_currentInterestDetail.latitude.doubleValue, _currentInterestDetail.longitude.doubleValue) title:_currentInterestDetail.title summary:_currentInterestDetail.summary subtitle:@"" pin:@"image-pin-tour-information"];
    [annotation setAccessibilityLabel:_currentInterestDetail.title];
    [annotation setType:@"Tour"];
    [annotation setImage:@""];
    
    [mapView addAnnotations:@[annotation]];
}

- (void)configureView{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    if(!_isRedeemPush && _isFirstLoad){
        
        scrollView = [[LoqivaScrollView alloc] initWithFrame:CGRectMake(0., -20., WIDTH, HEIGHT + 20)];
        
        _rewardImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,WIDTH,[UtilManager height:290])];
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],_currentInterestDetail.mediaURL]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        [_rewardImageView setImageWithURLRequest:imageRequest placeholderImage:[UtilManager imageWithColor:GRAY_TEXT_COLOR] success:nil failure:nil];
        [_rewardImageView setContentMode:UIViewContentModeScaleAspectFill];
        [[_rewardImageView layer] setMasksToBounds:YES];
        
        [scrollView addSubview:_rewardImageView];
        scrollView.delegate = self;
        
        self.whiteView = [[UIView alloc] init];
        self.whiteView.frame = CGRectMake(0, _rewardImageView.frame.size.height, self.view.frame.size.width, 100);
        [scrollView addSubview:self.whiteView];
        [self.whiteView setBackgroundColor:[UIColor whiteColor]];
        
        
        [scrollView addSubview:_navCustomView];
        
        UIImage *pinImage;
        pinImage = [UIImage imageNamed:@"icon-pin-small"];
        
        UIImageView *pinImageView;
        pinImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:20.], [UtilManager height:15], pinImage.size.width, pinImage.size.height)];
        [pinImageView setImage:pinImage];
        [self.whiteView addSubview:pinImageView];
        
        _distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x + pinImageView.frame.size.width + [UtilManager width:10.], pinImageView.frame.origin.y, [UtilManager width:180.], [UtilManager height:20])];
        [_distanceLabel setTextColor:[UIColor DistanceLabelTextColor]];
        [_distanceLabel setCenter:CGPointMake(_distanceLabel.center.x, pinImageView.center.y)];
        [_distanceLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:11]];
        
        NSString *distance = _currentInterestDetail.distance;
        if ([distance isKindOfClass:[NSNumber class]]) {
            distance = [(NSNumber *)distance stringValue];
        }
        [_distanceLabel setText:[distance distanceString]];
        [_distanceLabel setAdjustsFontSizeToFitWidth:YES];
        
        [self.whiteView addSubview:_distanceLabel];
        
        
        
        
        
        // Separator First
        UIView *firstSeparatorView;
        firstSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _distanceLabel.frame.origin.y + _distanceLabel.frame.size.height + [UtilManager height:10], WIDTH - 2 * pinImageView.frame.origin.x, 1)];
        [firstSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [self.whiteView addSubview:firstSeparatorView];
        
        
        // Title Label
        CGFloat titleHeight = [UtilManager heightForText:_currentInterestDetail.title havingWidth:firstSeparatorView.frame.size.width andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE]];
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, firstSeparatorView.frame.origin.y + [UtilManager height:15.], firstSeparatorView.frame.size.width, titleHeight)];
        [_titleLabel setAttributedText:[Utility convertHtmlToPlainText: _currentInterestDetail.title] withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:TITLE_FONT_KERN withLineBreak:NO];
        CGRect rect = [_titleLabel.attributedText boundingRectWithSize:CGSizeMake(_titleLabel.frame.size.width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        [_titleLabel setFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y, _titleLabel.frame.size.width, rect.size.height)];
        [_titleLabel setNumberOfLines:0];
        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
        [_titleLabel setBackgroundColor:TEST_BG_ONE];
        [self.whiteView addSubview:_titleLabel];
        
        
        // Separator Second
        UIView *secondSeparatorView;
        secondSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + [UtilManager height:10], WIDTH - 2 * pinImageView.frame.origin.x, 1)];
        [secondSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [self.whiteView addSubview:secondSeparatorView];
        
        
        // Address Label
        CGFloat addressHeight = [UtilManager heightForText:_currentInterestDetail.address havingWidth:secondSeparatorView.frame.size.width andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE]];
        _addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, secondSeparatorView.frame.origin.y + [UtilManager height:15.], secondSeparatorView.frame.size.width, addressHeight)];
        [_addressLabel setAttributedText:[Utility convertHtmlToPlainText: _currentInterestDetail.address] withLineHeight:CONTACT_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:SEMIBOLD_FONT size:CONTACT_FONT_SIZE] andColor:GRAY_DARK_TEXT_COLOR andKern:CONTACT_FONT_KERN withLineBreak:NO];
        CGRect addressRect = [_addressLabel.attributedText boundingRectWithSize:CGSizeMake(_addressLabel.frame.size.width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        [_addressLabel setFrame:CGRectMake(_addressLabel.frame.origin.x, _addressLabel.frame.origin.y, _addressLabel.frame.size.width, addressRect.size.height)];
        [_addressLabel setNumberOfLines:0];
        [_addressLabel setAdjustsFontSizeToFitWidth:YES];
        [_addressLabel setBackgroundColor:TEST_BG_TWO];
        [self.whiteView addSubview:_addressLabel];
        
        
        
        
        
        
        _dateLabel= [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _addressLabel.frame.origin.y + _addressLabel.frame.size.height + [UtilManager height:5], firstSeparatorView.frame.size.width, [UtilManager height:16])];
        //[_addressLabel setText:@"7:30 pm, Tuesday 25th Febraury"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:DATE_FORMATTER];
        //[_dateLabel setText:[dateFormatter stringFromDate:_currentEvent.startDate]];
        [_dateLabel setTextColor:GRAY_TEXT_HOME_COLOR];
        [_dateLabel setFont:[UIFont fontWithName:REGULAR_FONT size:10]];
        [_dateLabel setNumberOfLines:0];
        [_dateLabel setAdjustsFontSizeToFitWidth:YES];
        [_dateLabel setBackgroundColor:TEST_BG_ONE];
        
        id timeArray = [_currentInterestDetail.timetables valueForKey:@"opening"];
        if (timeArray != [NSNull null]) {
            NSArray *timeTables = [_currentInterestDetail.timetables valueForKey:@"opening"];
            NSMutableString *timeTable = [[NSMutableString alloc] initWithString:@""];
            
            int count = 0;
            for(NSDictionary *hoursDictionary in timeTables){
                if(count == 2 || count == 4 || count == 6){
                    [timeTable appendFormat:@"\n%@",[hoursDictionary valueForKey:@"hours"]];
                }else{
                    if([timeTable isEqualToString:@""])
                        [timeTable appendFormat:@"%@",[hoursDictionary valueForKey:@"hours"]];
                    else
                        [timeTable appendFormat:@", %@",[hoursDictionary valueForKey:@"hours"]];
                }
                count ++;
            }
            
            CGFloat timetableHeight = [UtilManager heightForText:timeTable havingWidth:_dateLabel.frame.size.width andFont:_dateLabel.font];
            [_dateLabel setFrame:CGRectMake(_dateLabel.frame.origin.x, _dateLabel.frame.origin.y, _dateLabel.frame.size.width, timetableHeight)];
            [_dateLabel setText:timeTable];
            
            [self.whiteView addSubview:_dateLabel];
        }
        
        UIView *thirdSeparatorView;
        
        thirdSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _dateLabel.frame.origin.y + _dateLabel.frame.size.height + [UtilManager height:15], WIDTH - 2 * pinImageView.frame.origin.x, 1)];
        [thirdSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [self.whiteView addSubview:thirdSeparatorView];
        
        UIImage *getDirectionsImage = [UIImage imageNamed:@"btn-view-route"];
        
        UIButton *getDirectionsButton;
        if(_isFirstLoad)
        {
            getDirectionsButton =[[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:34] , thirdSeparatorView.frame.origin.y + [UtilManager height:20], [UtilManager width:140], [UtilManager height:40])];
            [getDirectionsButton setImage:getDirectionsImage forState:UIControlStateNormal];
            [getDirectionsButton addTarget:self action:@selector(getDirectionsAction) forControlEvents:UIControlEventTouchUpInside];
            [self.whiteView addSubview:getDirectionsButton];
        }
        
        _originY = getDirectionsButton.frame.origin.y + getDirectionsButton.frame.size.height + [UtilManager height:15];
        
        UIImage *moreInfoImage = [UIImage imageNamed:@"btn-more-info"];
        
        UIButton *moreInfoButton;
        
        moreInfoButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:192], getDirectionsButton.frame.origin.y, [UtilManager width:142], [UtilManager height:40])];
        [moreInfoButton setImage:moreInfoImage forState:UIControlStateNormal];
        [moreInfoButton addTarget:self action:@selector(moreInfoAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.whiteView addSubview:moreInfoButton];
        
        _originY = moreInfoButton.frame.origin.y + moreInfoButton.frame.size.height + [UtilManager height:15];
        
        [self.whiteView addSubview:moreInfoButton];
        
    } else if(!_isRedeemPush && !_isFirstLoad){
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],_currentInterestDetail.mediaURL]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        [_rewardImageView setImageWithURLRequest:imageRequest placeholderImage:nil success:nil failure:nil];
        [_distanceLabel setText:@""];
        [_addressLabel setText:_currentInterestDetail.address];
        [_dateLabel setText:@""];
    }
    
    
    if(!_isRedeemPush && _isFirstLoad)
    {
        _redeemUserView = [self redeemUserViewWithFrame:CGRectMake(0., _originY, scrollView.frame.size.width, [UtilManager height:434])];
    }
    
    if(_isRedeemPush)
        [self.whiteView addSubview:_redeemUserView];
    else if(!_isRedeemPush && !_isFirstLoad)
        [_redeemUserView removeFromSuperview];
    
    if(!_isRedeemPush && _isFirstLoad)
    {
        _fourSeparatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20.], _originY, WIDTH - 2 * [UtilManager width:20.], 1)];
        [_fourSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [self.whiteView addSubview:_fourSeparatorView];
    }
    else
        [_fourSeparatorView setFrame:CGRectMake([UtilManager width:20.], _originY, WIDTH - 2 * [UtilManager width:20.], 1)];
    
    
    
    
    NSString *summaryString;
    summaryString = [Utility convertHtmlToPlainText: _currentInterestDetail.summary];
   CGFloat summaryHeight = [UtilManager heightForText:summaryString havingWidth:_fourSeparatorView.frame.size.width andFont:[UIFont fontWithName:LIGHT_FONT size:16]];
    if(!_isRedeemPush && _isFirstLoad){
        self.summaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(_fourSeparatorView.frame.origin.x,_fourSeparatorView.frame.origin.y + [UtilManager height:20] , _fourSeparatorView.frame.size.width, summaryHeight)];
    }else{
        [self.summaryLabel setFrame:CGRectMake(_fourSeparatorView.frame.origin.x,_fourSeparatorView.frame.origin.y + [UtilManager height:20] , _fourSeparatorView.frame.size.width, summaryHeight)];
    }
    
    
    [self.summaryLabel setTextAlignment:NSTextAlignmentLeft];
    [self.summaryLabel setAttributedText:summaryString withLineHeight:BODY_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:BODY_FONT_SIZE] andColor:GRAY_DARK_TEXT_COLOR andKern:BODY_FONT_KERN withLineBreak:NO];
    CGRect rect = [self.summaryLabel.attributedText boundingRectWithSize:CGSizeMake(self.summaryLabel.frame.size.width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    [self.summaryLabel setFrame:CGRectMake(self.summaryLabel.frame.origin.x, self.summaryLabel.frame.origin.y, self.summaryLabel.frame.size.width, rect.size.height)];
    [self.summaryLabel setNumberOfLines:0];
    [self.summaryLabel setBackgroundColor:TEST_BG_ONE];
    [self.whiteView addSubview:self.summaryLabel];
    
    
    
    
    
    
    if(!_isRedeemPush && _isFirstLoad)
    {
        [self addMap];
    } else {
        [mapView setFrame:CGRectMake(0., self.summaryLabel.frame.origin.y + self.summaryLabel.frame.size.height + [UtilManager height:30], WIDTH, [UtilManager height:180])];
    }
    
    if(!_isRedeemPush && _isFirstLoad){
        _footerView = [self rewardOnMapViewWithOriginY:(mapView.frame.origin.y + mapView.frame.size.height)];
        _footerView.frame = CGRectMake(0, self.view.frame.size.height - _footerView.frame.size.height, _footerView.frame.size.width, _footerView.frame.size.height);
        [self.view addSubview:_footerView];
    } else {
        _footerView.frame = CGRectMake(0, self.view.frame.size.height - _footerView.frame.size.height, _footerView.frame.size.width, _footerView.frame.size.height);
    }
    
    self.whiteView.frame = CGRectMake(self.whiteView.frame.origin.x, self.whiteView.frame.origin.y, self.whiteView.frame.size.width, mapView.frame.origin.y + mapView.frame.size.height);
    [scrollView setContentSize:CGSizeMake(WIDTH, self.whiteView.frame.origin.y + self.whiteView.frame.size.height + _footerView.frame.size.height)];
    
    [scrollView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:scrollView];
    
    
    [self setupHeaderForSection:CMSectionTypeTour];
    
    _isFirstLoad = FALSE;
}



- (UIView *)redeemUserViewWithFrame:(CGRect)frame{
    UIView *redeemView = [[UIView alloc] initWithFrame:frame];
    [redeemView setBackgroundColor:[UIColor orangeButtonColor]];
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:35], [UtilManager width:35], WIDTH - [UtilManager width:70], [UtilManager height:20])];
    [infoLabel setText:NSLocalizedString(@"Please show this screen to redeem your reward", nil)];
    [infoLabel setTextColor:[UIColor whiteColor]];
    [infoLabel setFont:[UIFont fontWithName:REGULAR_FONT size:14]];
    [infoLabel setAdjustsFontSizeToFitWidth:YES];
    
    [redeemView addSubview:infoLabel];
    
    UIImageView *profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.,infoLabel.frame.origin.y + infoLabel.frame.size.height + [UtilManager height:20], [UtilManager height:150], [UtilManager height:130])];
    [profileImageView setCenter:CGPointMake(redeemView.center.x, profileImageView.center.y)];
    [[profileImageView layer] setMasksToBounds:YES];
    [[profileImageView layer] setCornerRadius:profileImageView.frame.size.width/2];
    [profileImageView setContentMode:UIViewContentModeScaleAspectFit];
    [profileImageView setImage:[UIImage imageNamed:@"temp-profile-photo"]];
    
    [redeemView addSubview:profileImageView];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., profileImageView.frame.origin.y + profileImageView.frame.size.height + [UtilManager height:20], WIDTH, [UtilManager height:45])];
    [nameLabel setTextAlignment:NSTextAlignmentCenter];
    [nameLabel setFont:[UIFont fontWithName:REGULAR_FONT size:30]];
    [nameLabel setTextColor:[UIColor whiteColor ]];
    [nameLabel setText:[NSString stringWithFormat:@"%@ %@",[[[SessionManager shared] user] firstName],[[[SessionManager shared] user] lastName]]];
    [nameLabel setAdjustsFontSizeToFitWidth:YES];
    
    [redeemView addSubview:nameLabel];
    
    UILabel *memberLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., nameLabel.frame.origin.y + nameLabel.frame.size.height + [UtilManager height:8], WIDTH, [UtilManager height:20])];
    [memberLabel setTextAlignment:NSTextAlignmentCenter];
    [memberLabel setText:NSLocalizedString(@"Membership Number", nil)];
    [memberLabel setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    [memberLabel setAdjustsFontSizeToFitWidth:YES];
    [memberLabel setTextColor:[UIColor whiteColor]];
    
    [redeemView addSubview:memberLabel];
    
    UILabel *numberMemberLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., memberLabel.frame.origin.y + memberLabel.frame.size.height + [UtilManager height:8], WIDTH, [UtilManager height:20])];
    [numberMemberLabel setTextAlignment:NSTextAlignmentCenter];
    [numberMemberLabel setText:NSLocalizedString(@"#0123456789", nil)];
    [numberMemberLabel setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    [numberMemberLabel setAdjustsFontSizeToFitWidth:YES];
    [numberMemberLabel setTextColor:[UIColor whiteColor]];
    
    [redeemView addSubview:numberMemberLabel];
    
    _confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(0., numberMemberLabel.frame.origin.y + numberMemberLabel.frame.size.height + [UtilManager height:25], [UtilManager width:240], [UtilManager height:64])];
    [_confirmButton setCenter:CGPointMake(redeemView.center.x, _confirmButton.center.y)];
    [_confirmButton setBackgroundImage:[UIImage imageNamed:@"btn-reward-accept"] forState:UIControlStateNormal];
    [_confirmButton addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    [redeemView addSubview:_confirmButton];
    
    return redeemView;
}



- (UIView *)rewardOnMapViewWithOriginY:(CGFloat)originY{
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., originY, WIDTH, [UtilManager height:145])];
    [footerView setBackgroundColor:[UIColor footerColor]];
    
    UILabel *newsLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:20], WIDTH - [UtilManager width:6], [UtilManager height:20])];
    [newsLabel setAttributedText:NSLocalizedString(@"Want to see local tours on a map?", nil) withLineHeight:16 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:16] andColor:[UIColor footerTextAcentcolour] andKern:-0.6 withLineBreak:NO];
    [footerView addSubview:newsLabel];
    
    UILabel *updateLabel = [[UILabel alloc] initWithFrame:CGRectMake(newsLabel.frame.origin.x, newsLabel.frame.origin.y + newsLabel.frame.size.height + [UtilManager height:5], newsLabel.frame.size.width, newsLabel.frame.size.height)];
    [updateLabel setAttributedText:NSLocalizedString(@"Click the button below", nil) withLineHeight:16 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:16] andColor:[UIColor HeaderTextColour] andKern:-0.6 withLineBreak:NO];
    [footerView addSubview:updateLabel];

    UIImage *orangeButtonImage = [UIImage imageNamed:@"btn-toursdetail-map"];
    
    UIButton *preferencesButton = [[UIButton alloc] initWithFrame:CGRectMake(newsLabel.frame.origin.x, updateLabel.frame.origin.y + updateLabel.frame.size.height + [UtilManager height:15], orangeButtonImage.size.width, orangeButtonImage.size.height)];
    [preferencesButton setImage:orangeButtonImage forState:UIControlStateNormal];
    [preferencesButton addTarget:self action:@selector(rewardsMapAction) forControlEvents:UIControlEventTouchUpInside];
    [preferencesButton setTitle:NSLocalizedString(@"View trails on map", nil) forState:UIControlStateNormal];
    
    [footerView addSubview:preferencesButton];
    
    return footerView;
}

-(void)moreInfoAction:(id)sender{
    if(_currentInterestDetail.interestURL && _currentInterestDetail.interestURL.length > 0){
        WebViewController *webViewController = [[WebViewController alloc] init];
        [webViewController setUrlString:_currentInterestDetail.interestURL];
        [webViewController setTitleBar:_currentInterestDetail.title];
        [self.navigationController pushViewController:webViewController animated:YES];
    }
}

- (void)rewardsMapAction{
    NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"mapViewController",VIEWCONTROLLER_NOTIFICATION,
                                     [NSNumber numberWithInt:2],@"firstParam",nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
}

- (void)getDirectionsAction{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)confirmAction{
    
}

- (void)backAction{
    
}

#pragma mark UITableViewDelegate Methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float y = scrollView.contentOffset.y;
    CGRect imageFrame = self.rewardImageView.frame;
    imageFrame.origin.y = y/2;
    self.rewardImageView.frame = imageFrame;
}

#pragma mark WebServiceManagerDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    if(webServiceType == WebServiceTypeRewardsDetail){
        
    } else if(webServiceType == WebServiceTypeRewardsDetail){
        
    }else if(webServiceType == WebServiceTypeRedeemRewardsDetail){
        [_confirmButton setEnabled:YES];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    if(webServiceType == WebServiceTypeRewardsDetail){
        
    } else if(webServiceType == WebServiceTypeRewardsDetail){
        
    } else if(webServiceType == WebServiceTypeRedeemRewardsDetail){
        [_confirmButton setEnabled:YES];
    }
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
}

@end
