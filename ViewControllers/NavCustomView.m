//
//  NavCustomView.m
//  loqiva
//
//  Created by Manuel Manzanera on 1/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "NavCustomView.h"

#import "AppConstants.h"
#import "UtilManager.h"
#import "UIImageEffects.h"
#import "UILabel+Extra.h"
#import "BlurView.h"
#import "AppContext.h"
#import <Quartzcore/Quartzcore.h>
#import "CustomNotification.h"
#import "loqiva-Swift.h"


@implementation NavCustomView

@synthesize containerView;



- (id)initWithFrame:(CGRect)frame gradienAndUIBlurEffectStyle:(UIBlurEffectStyle)blurEffectStyle: (BOOL)applyGradient{
    self = [super initWithFrame:frame];
    if(self){
        
        containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT + [UtilManager height:20])];
        
//        _blurView = [[UIView alloc] initWithFrame:self.frame];
//        
//        
//        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:blurEffectStyle];
//        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//        [blurEffectView setFrame:self.bounds];
//        _blurView = blurEffectView;
        
        if (applyGradient) {
            
            UIColor * topColor = GRADIENT_DARK;
            UIColor * bottomColor = [UIColor clearColor];
            _cagradient = [CAGradientLayer layer];
            _cagradient.frame = self.frame;
            _cagradient.colors = [NSArray arrayWithObjects:(id)topColor.CGColor, (id)bottomColor.CGColor, nil];
            _cagradient.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:1.0f], nil];
            [self.layer addSublayer:_cagradient];
        }
        
        
//        [containerView addSubview:_blurView];
        
        
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(NAV_ICON_ORIGIN_X, NAV_ICON_ORIGIN_Y, NAV_ICON_WIDTH, NAV_ICON_HEIGHT)];
        [_icon setBackgroundColor:[UIColor clearColor]];
        [_icon setContentMode:UIViewContentModeScaleAspectFit];
        [_icon setImage:[UIImage imageNamed:@"icon-myhub"]];
        [containerView addSubview:_icon];
        
        NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
        NSString *appName = infoPlistDict[@"AppName"];
        if (appName.length == 0) {
            appName = @"Loqiva";
        }
        
        NSString *hubName = infoPlistDict[@"HubName"];
        if (hubName.length == 0) {
            hubName = @"My Hub";
        }
      
        _slideButton = [[UIImageView alloc] initWithFrame:CGRectMake(NAV_SLIDE_ORIGIN_X, NAV_SLIDE_ORIGIN_Y, NAV_SLIDE_WIDTH, NAV_SLIDE_HEIGHT)];
        [_slideButton setImage:[UIImage imageNamed:@"btn-slider"]];
        
        UIButton *slide = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40.], [UtilManager width:40])];
        [slide setCenter:_slideButton.center];
        [slide addTarget:self action:@selector(slideAction) forControlEvents:UIControlEventTouchUpInside];
        [containerView addSubview:_slideButton];
        [containerView addSubview:slide];
        
        
        _notificationButtonImage = [[UIImageView alloc] initWithFrame:CGRectMake(NAV_NOTIFICATION_ORIGIN_X, NAV_NOTIFICATION_ORIGIN_Y, NAV_NOTIFICATION_WIDTH, NAV_NOTIFICATION_HEIGHT)];
        
        [_notificationButtonImage setImage:[UIImage imageNamed:@"btn-notification"]];
        _notificationLabel = [[UILabel alloc] initWithFrame:CGRectMake(_notificationButtonImage.frame.origin.x + _notificationButtonImage.frame.size.width - [UtilManager width:7], _notificationButtonImage.frame.origin.y - [UtilManager width:17], [UtilManager width:20], [UtilManager width:20])];
        
        _redBadge= [UIView new];
        [_redBadge setBackgroundColor:[UtilManager colorwithHexString:@"B0021B" alpha:1.]];
        
        _notificationButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40.], [UtilManager width:40])];
        [_notificationButton setCenter:_notificationButtonImage.center];
        [_notificationButton addTarget:self action:@selector(notificationAction) forControlEvents:UIControlEventTouchUpInside];
        [containerView addSubview:_notificationButtonImage];
        [containerView addSubview:_redBadge];
        [containerView addSubview:_notificationLabel];
        
        
        _backButtonImage = [[UIImageView alloc] initWithFrame:CGRectMake(_notificationButtonImage.frame.origin.x, [UtilManager height:36], [UtilManager width:12], [UtilManager height:22])];
        [_backButtonImage setImage:[UIImage imageNamed:@"btn-back"]];
        
        _backButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40.], [UtilManager width:40])];
        [_backButton setCenter:_backButtonImage.center];
        [_backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [containerView addSubview:_backButtonImage];
        [containerView addSubview:_backButton];
        
        [containerView sendSubviewToBack:_backButton];
        [_backButton setHidden:YES];
        [_backButtonImage setHidden:YES];
        
        CGFloat boundsWidth = containerView.bounds.size.width;
        CGFloat iconLabelXPos = _icon.frame.origin.x + _icon.frame.size.width + [UtilManager width:10];
        CGFloat iconLabelWidth = boundsWidth -(iconLabelXPos + (boundsWidth - _backButton.frame.origin.x + 10));
        _iconLabel = [[UILabel alloc] initWithFrame:CGRectMake(iconLabelXPos, _icon.frame.origin.y, iconLabelWidth, [UtilManager height:42])];
        [_iconLabel setFont:[UIFont fontWithName:LIGHT_FONT size:NAV_CUSTOM_VIEW_FONT_SIZE]];
        [_iconLabel setTextColor:[UIColor whiteColor]];
        [_iconLabel setTextAlignment:NSTextAlignmentLeft];
        _iconLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [_iconLabel setText:hubName];
        [_iconLabel setCenter:CGPointMake(_iconLabel.center.x, _icon.center.y)];
        
        [containerView addSubview:_iconLabel];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, containerView.frame.size.height - [UtilManager height:21], WIDTH,[UtilManager height:1])];
        [_separatorView setBackgroundColor:HEADER_SEPARATOR_COLOR];
        [containerView addSubview:_separatorView];
        
        [self addSubview:containerView];
        
        [self updatedNotificationsBadge];
        // commented out this code. It creates multiple message. Not required here. -- wasiq
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDidUpdateBeaconNotification:) name:DID_UPDATE_BEACON_NOTIFICATION object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:@"updateTheBadgeIcon" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            [self updatedNotificationsBadge];
        }];
    }
    
    return self;
}

-(void)updatedNotificationsBadge {
    
    NSUInteger totalNotification = [AppContext sharedInstance].notifications.count + [CustomNotification getCustomNotifications].count;
    
    if(totalNotification > 0){
        NSString * txt1 ;
        if(totalNotification >99){
            txt1 = @"99+";
        }else{
            txt1 = [NSString stringWithFormat:@"%lu",(unsigned long)totalNotification];
        }
        
        

        
        [_notificationLabel setAttributedText:txt1 withLineHeight:14 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:REGULAR_FONT size:10] andColor:[UIColor whiteColor] andKern:-0.33 withLineBreak:NO];
        [_notificationLabel sizeToFit];
        CGSize stringsize1 = [txt1 sizeWithFont:[UIFont systemFontOfSize:12]];
        CGRect labelFrame = _notificationLabel.frame;
        labelFrame.size=stringsize1;
        
        CGFloat padding = totalNotification >9 ? stringsize1.width/3 : stringsize1.width*1.1;
        
        if(totalNotification == 1)
            padding = stringsize1.width*1.5;
        
        if(totalNotification>99){
            padding = stringsize1.width/10;
        }
        
        
        
        labelFrame.size.width += padding;
        
        _redBadge.frame=labelFrame;
        _notificationLabel.frame = labelFrame;
        _redBadge.layer.cornerRadius = (labelFrame.size.height)*.5;
        _redBadge.layer.masksToBounds = YES;
        
        CGPoint notificationImgCenter = _notificationButtonImage.center;
        notificationImgCenter.y = [UtilManager height:35];
        notificationImgCenter.x = _notificationButtonImage.center.x + (_notificationButtonImage.frame.size.width*.6) - (labelFrame.size.width/10);
        _redBadge.center =notificationImgCenter;
        _notificationLabel.center = notificationImgCenter;
        
        [_notificationButtonImage setHidden:NO];
        [_redBadge setHidden:NO];
        [_notificationLabel setHidden:NO];
        
    } else{
        [_redBadge setHidden:YES];
        [_notificationLabel setHidden:YES];
        
    }
    
    _notificationButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40.], [UtilManager width:40])];
    [_notificationButton setCenter:_notificationButtonImage.center];
    [_notificationButton addTarget:self action:@selector(notificationAction) forControlEvents:UIControlEventTouchUpInside];
    //[containerView addSubview:_notificationButtonImage];
    
    [containerView addSubview:_notificationButton];
}

-(void)onDidUpdateBeaconNotification:(NSNotification*)note{
    NSMutableArray *array = [[AppContext sharedInstance] notifications];
    if (!array) {
        array = [@[] mutableCopy];
    }
    [array addObjectsFromArray:[CustomNotification getCustomNotifications]];
    [[AppContext sharedInstance] setNotifications:array];
    [self updatedNotificationsBadge];
}

//Sticky header effect formula
-(void)updateScroll:(CGPoint)scrollOffset andScrollContentSize:(CGSize)scrollContensize{
    float y =  -scrollOffset.y - (self.frame.size.height)*.3f;
    CGRect imageFrame = self.frame;
    imageFrame.origin.y= y/2;
    self.frame = imageFrame;
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    static CGFloat previousOffset;
    CGRect rect = self.frame;
    rect.origin.y += previousOffset - scrollView.contentOffset.y;
    previousOffset = scrollView.contentOffset.y;
    self.frame = rect;
}

- (void)slideAction{
    [[NSNotificationCenter defaultCenter] postNotificationName:SLIDE_NOTIFICATION object:self.parentController];
}

- (void)notificationAction{
    [[NSNotificationCenter defaultCenter] postNotificationName:ACTION_NOTIFICATION object:self.parentController];
}

- (void)backAction{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:BACK_NOTIFICATION object:nil];
}

- (void)setupNotificationButton:(BOOL)isWhite{
    
}

- (void)changeBackColor {
    _backButtonImage.image = [[UIImage imageNamed:@"btn-back"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _backButtonImage.tintColor = [UIColor darkGrayColor];
}

- (void)setBackOption{
    [_backButtonImage setHidden:NO];
    [_backButton setHidden:NO];
    
    CGRect baclBtnImageFrm = _backButtonImage.frame;
    baclBtnImageFrm.origin.x -= 30;
    CGRect baclBtnFrm = baclBtnImageFrm;
    _backButtonImage.frame = baclBtnImageFrm;
    baclBtnFrm.size = CGSizeMake(50, 50);
    _backButton.frame = baclBtnFrm;
    _backButton.center=_backButtonImage.center;
    [containerView bringSubviewToFront:_backButton];
    
}

@end
