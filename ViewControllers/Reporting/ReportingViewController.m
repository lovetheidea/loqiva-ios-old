//
//  ReportingViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ReportingViewController.h"
#import "AFNetworkReachabilityManager.h"
#import "WebServiceManager.h"
#import "User.h"
#import "NavCustomView.h"
#import "RegularExpressionsManager.h"
#import <AVFoundation/AVFoundation.h>
#import "LocationManager.h"
#import "UIColor+Helper.h"
#import "AppConstants.h"
#import "AppDelegate.h"
#import "loqiva-Swift.h"

@interface ReportingViewController ()<UIImagePickerControllerDelegate, UITextFieldDelegate, UIActionSheetDelegate, WebServiceManagerDelegate, UIScrollViewDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource,UINavigationControllerDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) NSString *reportId;
@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) UIButton *typeTextBtn;
@property (nonatomic, strong) UIView *backGRPicker;
@property (nonatomic, strong) UITextView *momentTextView;
@property (nonatomic, strong) UIView *whiteView;
@property (nonatomic, strong) UITextField *nameTextField;
@property (nonatomic, strong) UITextField *mailTextField;
@property (nonatomic, strong) UITextField *phoneTextField;
@property (nonatomic, strong) UIImagePickerController *picker;
@property (nonatomic, strong) UIPickerView *pickerIssue;
@property (nonatomic, strong) UIButton *uploadButton;
@property (nonatomic, strong) UIButton *submitButton;
@property (nonatomic, assign) BOOL isMenuOpen;
@property (nonatomic, strong) UIImageView *rewardImageView;
@property (nonatomic, strong) UIButton *acceptPickerButton;
@property (nonatomic, strong) NSArray *types;


@end

@implementation ReportingViewController

@synthesize scrollView, picker;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _types = [[NSArray alloc] initWithObjects:@"Fly Tipping", @"Vandalism", @"Road Issues", @"Safety Hazards", nil];
    _isMenuOpen = FALSE;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboards)];
    
    [self.view addGestureRecognizer:tap];
    
    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [WebServiceManager generateReportKey:self];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)configureView{
    
    User *currentUser = [[SessionManager shared] user];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    if (!_backGRPicker) {
        _backGRPicker = [[UIView alloc] initWithFrame:self.view.frame];
    }
    
    _backGRPicker.backgroundColor=[UIColor blackColor];
    _backGRPicker.userInteractionEnabled = NO;
    _backGRPicker.alpha=.5;
    
    if (!scrollView) {
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0., -20., WIDTH, HEIGHT + 20)];
    }
    
    [scrollView setBackgroundColor:[UIColor clearColor]];
    [scrollView setDelegate:self];
    
    self.rewardImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:290])];
    [self.rewardImageView setImage:[UIImage imageNamed:@"temp-reporting-image"]];
    [scrollView addSubview:self.rewardImageView];
    
    if (!self.whiteView) {
        self.whiteView = [[UIView alloc] init];
    }
    
    self.whiteView.frame = CGRectMake(0, _rewardImageView.frame.size.height, self.view.frame.size.width, 100);
    [scrollView addSubview:self.whiteView];

    [self.whiteView setBackgroundColor:[UIColor whiteColor]];
    
    if (!_pickerIssue) {
        _pickerIssue = [[UIPickerView alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - 216, self.view.frame.size.width, 216)];
    }
    
    [_pickerIssue setDataSource:self];
    [_pickerIssue setDelegate:self];
    
    [_pickerIssue setUserInteractionEnabled:YES];
    
    if (!_acceptPickerButton) {
        _acceptPickerButton = [[UIButton alloc] initWithFrame:CGRectMake(_pickerIssue.frame.size.width - 80., _pickerIssue.frame.origin.y + 10, 80., 24.)];
    }
    
    [_acceptPickerButton setTitleColor:BLACK_TEXT_COLOR forState:UIControlStateNormal];
    [_acceptPickerButton setTitle:NSLocalizedString(@"Accept", nil) forState:UIControlStateNormal];
    
    [_acceptPickerButton addTarget:self action:@selector(acceptPickerAction) forControlEvents:UIControlEventTouchDown];
    
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:20], WIDTH - [UtilManager width:20] * 2, [UtilManager height:150])];
    [infoLabel setAttributedText:NSLocalizedString(@"Help us by reporting issues and letting us know of problems in your area", nil) withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:TITLE_FONT_KERN withLineBreak:NO];
    [infoLabel setNumberOfLines:0];
    [self.whiteView addSubview:infoLabel];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(infoLabel.frame.origin.x, infoLabel.frame.origin.y + infoLabel.frame.size.height + [UtilManager height:10.], infoLabel.frame.size.width, 1)];
    [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
    [self.whiteView addSubview:separatorView];
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(separatorView.frame.origin.x, separatorView.frame.origin.y + [UtilManager height:8], separatorView.frame.size.width, [UtilManager height:90])];

    [descriptionLabel setAttributedText:NSLocalizedString(@"Simply upload a photo to our database where we will be able to review the information and act accordingly", nil) withLineHeight:BODY_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:BODY_FONT_SIZE] andColor:GRAY_TEXT_COLOR andKern:BODY_FONT_KERN withLineBreak:NO];
    [descriptionLabel setNumberOfLines:0];
    [self.whiteView addSubview:descriptionLabel];
    
    UIView *separatorViewSecond = [[UIView alloc] initWithFrame:CGRectMake(infoLabel.frame.origin.x, descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height + [UtilManager height:12.], infoLabel.frame.size.width, 1)];
    [separatorViewSecond setBackgroundColor:GRAY_SEPARATOR_COLOR];
    [self.whiteView addSubview:separatorViewSecond];
    
    UIImage *buttonImage = [UIImage imageNamed:@"btn-orange-short"];
    
    CGFloat inset = buttonImage.size.height  /2;
    
    UIEdgeInsets insets = UIEdgeInsetsMake(inset,inset+10, inset, inset+10);
    UIImage *stretchableImage = [buttonImage resizableImageWithCapInsets:insets];
    _uploadButton = [[UIButton alloc] initWithFrame:CGRectMake(0., separatorViewSecond.frame.origin.y + separatorViewSecond.frame.size.height + [UtilManager height:20], buttonImage.size.width , buttonImage.size.height * 0.8)];
    
    [_uploadButton setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    [_uploadButton setTitle:NSLocalizedString(@"Upload a Photo", nil) forState:UIControlStateNormal];
    [[_uploadButton titleLabel] setFont:[UIFont fontWithName:SEMIBOLD_FONT size:BUTTON_FONT_SIZE]];
    
    [[_uploadButton titleLabel] setAdjustsFontSizeToFitWidth:YES];
    [_uploadButton setCenter:CGPointMake(scrollView.center.x, _uploadButton.center.y)];
    [_uploadButton addTarget:self action:@selector(uploadPictureAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.whiteView addSubview:_uploadButton];
    [scrollView setContentSize:CGSizeMake(WIDTH, HEIGHT)];
    
    _picImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:15], _uploadButton.frame.origin.y,[UtilManager width:335], [UtilManager height:175])];
    [_picImageView setCenter:CGPointMake(self.view.center.x, _picImageView.center.y)];
    [_picImageView setContentMode:UIViewContentModeScaleAspectFit];
    [_picImageView setImage:[UIImage imageNamed:@"temp-upload-picture"]];
    [_picImageView setUserInteractionEnabled:YES];
    
    UIView *blueView = [[UIView alloc] initWithFrame:CGRectMake(_picImageView.frame.size.width - [UtilManager width:52], 0, [UtilManager width:52], [UtilManager width:52])];
    [blueView setBackgroundColor:[UtilManager colorwithHexString:@"8EFFFF" alpha:0.75]];
    
    UIImageView *closeImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn-close"]];
    [blueView addSubview:closeImageView];
    [closeImageView setCenter:CGPointMake(blueView.frame.size.width/2, blueView.frame.size.width/2)];
    
    [_picImageView addSubview:blueView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPictureAction)];
    
    [_picImageView addGestureRecognizer:tapGesture];
    [_picImageView setHidden:YES];
    
    [self.whiteView addSubview:_picImageView];
    
    _typeTextBtn = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:15], _picImageView.frame.origin.y + _picImageView.frame.size.height + [UtilManager height:15], [UtilManager width:330], UITEXTFIELD_HEIGHT)];
    [_typeTextBtn setBackgroundColor:[UIColor clearColor]];
    [_typeTextBtn setCenter:CGPointMake(self.view.center.x,_typeTextBtn.center.y)];
    [_typeTextBtn setBackgroundImage:[UIImage imageNamed:@"image-textfield-grey"] forState:UIControlStateNormal];
    
   [_typeTextBtn setTitleColor:GRAY_TEXT_COLOR forState:UIControlStateNormal];
    [_typeTextBtn.titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:16]];
    
    [_typeTextBtn setTitle:@"Type of issue" forState:UIControlStateNormal];

    
    [_typeTextBtn addTarget:self action:@selector(presentIssuePickerView) forControlEvents:UIControlEventTouchUpInside];
    _typeTextBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _typeTextBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);

    [_typeTextBtn setHidden:YES];
    
    [self.whiteView addSubview:_typeTextBtn];
    
    
    
    
    _nameTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:15], _typeTextBtn.frame.origin.y + _typeTextBtn.frame.size.height + [UtilManager height:8], _typeTextBtn.frame.size.width, UITEXTFIELD_HEIGHT)];
    [_nameTextField setBackgroundColor:[UIColor clearColor]];
    [_nameTextField setCenter:CGPointMake(self.view.center.x,_nameTextField.center.y)];
    [_nameTextField setBackground:[UIImage imageNamed:@"image-textfield-grey"]];
    [_nameTextField setBorderStyle:UITextBorderStyleNone];
    [_nameTextField setTextColor:GRAY_TEXT_COLOR];
    [_nameTextField setFont:[UIFont fontWithName:LIGHT_FONT size:16]];
    [_nameTextField setTextAlignment:NSTextAlignmentLeft];
    [_nameTextField setReturnKeyType:UIReturnKeyNext];
    
    _nameTextField.tag = 0;
    if(currentUser)
        [_nameTextField setText:currentUser.firstName];
    else {
        _nameTextField.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Your name", nil)
                                        attributes:@{
                                                     NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                     NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                     }
         ];
    }
    
    [_nameTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_nameTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_nameTextField setReturnKeyType:UIReturnKeyNext];
    [_nameTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_nameTextField setDelegate:self];
    
    UIView *leftNameTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:15],[UtilManager height:26])];
    leftNameTextFieldView.backgroundColor = [UIColor clearColor];
    [_nameTextField setLeftView:leftNameTextFieldView];
    [_nameTextField setHidden:YES];
    [self.whiteView addSubview:_nameTextField];
    
    _momentTextView = [[UITextView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _nameTextField.frame.origin.y + _nameTextField.frame.size.height + [UtilManager height:8], [UtilManager width:330.], [UtilManager height:150])];
    
    UIImageView *imageMomentView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:330.], [UtilManager height:150.])];
    [imageMomentView setImage:[UIImage imageNamed:@"image-textview-grey"]];
    [imageMomentView setCenter:_momentTextView.center];
    
    [_momentTextView setText:NSLocalizedString(@"Describe the issue", nil)];
    [_momentTextView setBackgroundColor:[UIColor clearColor]];
    [_momentTextView setFont:[UIFont fontWithName:LIGHT_FONT size:16]];
    [_momentTextView setTextColor:GRAY_TEXT_COLOR];
    [_momentTextView setHidden:YES];
    [_momentTextView setDelegate:self];

    
    [_momentTextView setReturnKeyType:UIReturnKeyNext];
    _momentTextView.tag = 1;
    [self.whiteView addSubview:imageMomentView];
    [self.whiteView addSubview:_momentTextView];
    
    _mailTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:15], _momentTextView.frame.origin.y + _momentTextView.frame.size.height + [UtilManager height:8], _typeTextBtn.frame.size.width, UITEXTFIELD_HEIGHT)];
    [_mailTextField setBackgroundColor:[UIColor clearColor]];
    [_mailTextField setCenter:CGPointMake(self.view.center.x,_mailTextField.center.y)];
    [_mailTextField setBackground:[UIImage imageNamed:@"image-textfield-grey"]];
    _mailTextField.tag = 2;
    [_mailTextField setBorderStyle:UITextBorderStyleNone];
    [_mailTextField setTextColor:[UIColor grayColor]];
    [_mailTextField setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [_mailTextField setTextAlignment:NSTextAlignmentLeft];
    [_mailTextField setReturnKeyType:UIReturnKeyNext];
    if(currentUser) {
        [_mailTextField setText:currentUser.mail];
    } else {
        _mailTextField.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email", nil)
                                        attributes:@{
                                                     NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                     NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                     }
         ];
    }
    
    [_mailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_mailTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    
    [_mailTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_mailTextField setDelegate:self];
    UIView *leftMailTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:15],[UtilManager height:26])];
    leftMailTextFieldView.backgroundColor = [UIColor clearColor];
    [_mailTextField setLeftView:leftMailTextFieldView];
    [_mailTextField setHidden:YES];
    [self.whiteView addSubview:_mailTextField];
    
    _phoneTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:15], _mailTextField.frame.origin.y + _mailTextField.frame.size.height + [UtilManager height:8], _typeTextBtn.frame.size.width, UITEXTFIELD_HEIGHT)];
    [_phoneTextField setBackgroundColor:[UIColor clearColor]];
    [_phoneTextField setCenter:CGPointMake(self.view.center.x,_phoneTextField.center.y)];
    [_phoneTextField setBackground:[UIImage imageNamed:@"image-textfield-grey"]];
    [_phoneTextField setBorderStyle:UITextBorderStyleNone];
    [_phoneTextField setTextColor:GRAY_TEXT_COLOR];
    [_phoneTextField setFont:[UIFont fontWithName:LIGHT_FONT size:16]];
    [_phoneTextField setTextAlignment:NSTextAlignmentLeft];
    [_mailTextField setReturnKeyType:UIReturnKeyNext];
    _phoneTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Phone", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                 NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                 }];
    
    _phoneTextField.tag = 3;
    [_phoneTextField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [_phoneTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    
    [_phoneTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_phoneTextField setDelegate:self];
    
    UIView *leftPhoneTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:15],[UtilManager height:26])];
    leftPhoneTextFieldView.backgroundColor = [UIColor clearColor];
    [_phoneTextField setLeftView:leftPhoneTextFieldView];
    
    [_phoneTextField setReturnKeyType:UIReturnKeyDone];
    [_phoneTextField setHidden:YES];
    [self.whiteView addSubview:_phoneTextField];
    
    if (currentUser != nil) {
        _phoneTextField.text = currentUser.phoneNumber;
    }

    _submitButton = [[UIButton alloc] initWithFrame:CGRectMake(0., _phoneTextField.frame.origin.y + _phoneTextField.frame.size.height + [UtilManager height:20], buttonImage.size.width * 0.8, buttonImage.size.height * 0.8)];
    [_submitButton setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    [_submitButton setTitle:NSLocalizedString(@"Submit", nil) forState:UIControlStateNormal];
    [[_submitButton titleLabel] setFont:[UIFont fontWithName:SEMIBOLD_FONT size:BUTTON_FONT_SIZE]];
    [_submitButton setCenter:CGPointMake(scrollView.center.x, _submitButton.center .y)];
    [_submitButton addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.whiteView addSubview:_submitButton];
    
    self.whiteView.frame = CGRectMake(self.whiteView.frame.origin.x, self.whiteView.frame.origin.y, self.whiteView.frame.size.width, _submitButton.frame.origin.y + _submitButton.frame.size.height);
    
    [self.view addSubview:scrollView];
    [self setupHeaderForSection:CMSectionTypeReporting];
    
    
    [self.view addSubview:_backGRPicker];
    [self.view addSubview:_pickerIssue];
    [self.view addSubview:_acceptPickerButton];
    
    _pickerIssue.hidden = YES;
    _acceptPickerButton.hidden = YES;
    _backGRPicker.hidden = YES;
}



- (void)uploadPictureAction{
    if(![[SessionManager shared] user]){
        
        __weak ReportingViewController * weakSelf = self;
        self.tempUserViewController = [[TemporaryUserViewController alloc] initWithDismissBlock:^{
            
            [weakSelf.tempUserViewController.view removeFromSuperview];
            
        } andCallToSignForFree:^{
            [weakSelf.tempUserViewController.view removeFromSuperview];
            [weakSelf presentSignForFreeController];
            
        }];
        
        [self.view addSubview:self.tempUserViewController.view];
        
        return;
    }
    
    if (![AppDelegate showAlertIfDisconnected]) {
        [_picImageView setUserInteractionEnabled:FALSE];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select an option.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:nil, nil];
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Take a picture", nil)];
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Select from gallery", nil)];
        
        [actionSheet showInView:self.view];
    }
}

- (void)uploadAction{
    
    if(!_isMenuOpen){
        _isMenuOpen = TRUE;
        
        [UIView animateWithDuration:0.8 animations:^{
            [scrollView setContentSize:CGSizeMake(WIDTH, self.whiteView.frame.origin.y + self.whiteView.frame.size.height + [UtilManager height:40])];
            [_uploadButton setHidden:TRUE];
            [_picImageView setHidden:FALSE];
            [_nameTextField setHidden:FALSE];
            [_mailTextField setHidden:FALSE];
            [_phoneTextField setHidden:FALSE];
            [_typeTextBtn setHidden:FALSE];
            [_momentTextView setHidden:FALSE];
            
        } completion:^(BOOL finished){
           
        }];
    } else {
         _isMenuOpen = FALSE;
        
        [UIView animateWithDuration:0.8 animations:^{
            [scrollView setContentSize:CGSizeMake(WIDTH, HEIGHT)];
            [_uploadButton setHidden:FALSE];
            [_picImageView setHidden:TRUE];
            [_nameTextField setHidden:TRUE];
            [_mailTextField setHidden:TRUE];
            [_phoneTextField setHidden:TRUE];
            [_typeTextBtn setHidden:TRUE];
            [_momentTextView setHidden:TRUE];
            
            
        } completion:^(BOOL finished){
            
        }];
    }
}

- (void)submitAction{

    if([self checkFields]){
        if (![AppDelegate showAlertIfDisconnected]) {
            [WebServiceManager postReportImage:_picImageView.image andReportId:_reportId andDelegate:self];
            [_submitButton setEnabled:FALSE];
        } 
    }
}

- (NSDictionary *)reportingDictionary{
    
    NSNumber *latitude = [NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.latitude];
    NSNumber *longitude = [NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.longitude];
    
    if (latitude == nil) {
        latitude = @(0);
    }
    
    if (longitude == nil) {
        longitude = @(0);
    }
    
    NSDictionary *parameters = @{@"reportid": _reportId,
                                @"describephoto": _momentTextView.text,
                                @"type": _typeTextBtn.titleLabel.text,
                                @"latitude":latitude,
                                @"longitude":longitude,
                                @"description":_momentTextView.text,
                                @"name":_nameTextField.text,
                                @"email":_mailTextField.text,
                                @"phone":_phoneTextField.text
                                };
    
    return parameters;
}

- (BOOL)checkFields{
    
    if(_typeTextBtn.titleLabel.text.length == 0){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Type of issue cannot be empty", nil)];
        return FALSE;
    }
    if(_typeTextBtn.titleLabel.text.length < 4){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Type of issue cannot be less that 4 letters", nil)];
        return FALSE;
    }
    if([_typeTextBtn.titleLabel.text isEqualToString:@"Type of issue"]){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Type of issue cannot be empty", nil)];
        return FALSE;
    }
    
    if(_nameTextField.text.length == 0){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Your Name cannot be empty", nil)];
        return FALSE;
    }
    if(_nameTextField.text.length < 4){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Your Name cannot be less that 4 letters", nil)];
        return FALSE;
    }
    if([_nameTextField.text isEqualToString:@"Your Name"]){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Your Name must be edit", nil)];
        return FALSE;
    }
    
    if(_mailTextField.text.length == 0){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Email cannot be empty", nil)];
        return FALSE;
    }
    if(![RegularExpressionsManager checkMail:_mailTextField.text]){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please, insert a valid email", nil)];
        return FALSE;
    }
    

    
    return YES;
}

- (void)hideKeyboards
{
    
    [_mailTextField resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_momentTextView resignFirstResponder];
    [_nameTextField resignFirstResponder];
}

- (void)presentIssuePickerView{
    
    [self hideKeyboards];
    
    _pickerIssue.hidden = NO;
    _acceptPickerButton.hidden = NO;
    _backGRPicker.hidden = NO;

    [_backGRPicker removeFromSuperview];
    [self.view addSubview:_backGRPicker];
    [_pickerIssue removeFromSuperview];
    [self.view addSubview:_pickerIssue];
    [_acceptPickerButton removeFromSuperview];
    [self.view addSubview:_acceptPickerButton];

}
-(void)dismmissPickerIssue {

    _pickerIssue.hidden = YES;
    _backGRPicker.hidden = YES;
    _acceptPickerButton.hidden = YES;
}
- (void)acceptPickerAction
{
    if([_pickerIssue selectedRowInComponent:0] == 0)
     
        [_typeTextBtn setTitle:[_types objectAtIndex:0] forState:UIControlStateNormal];
    else
        [_typeTextBtn setTitle:[_types objectAtIndex:[_pickerIssue selectedRowInComponent:0]] forState:UIControlStateNormal];
    
    
    NSLog(@"VFVFVF ::2 %@",[_types objectAtIndex:[_pickerIssue selectedRowInComponent:0]]);
    
    [self dismmissPickerIssue];
    

}

-(void)keyboardWillShow {
    [self dismmissPickerIssue];
    [UIView animateWithDuration:0.3f animations:^ {
        self.view.frame = CGRectMake(0, -160, WIDTH, self.view.frame.size.height);
    }];
}

-(void)keyboardWillHide {
    [UIView animateWithDuration:0.3f animations:^ {
        
    }];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.view.frame = CGRectMake(0, 0, WIDTH, self.view.frame.size.height);
    } completion:^(BOOL finished){
        [scrollView setContentOffset:CGPointMake(0, 450) animated:NO];
    }];
}

#pragma mark UIScrollViewDelegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float y = scrollView.contentOffset.y;
    CGRect imageFrame = self.rewardImageView.frame;
    imageFrame.origin.y = y/2;
    self.rewardImageView.frame = imageFrame;
}

#pragma mark UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    [_picImageView setImage:image];
    [_picImageView setContentMode:UIViewContentModeScaleAspectFill];
    [[_picImageView layer] setMasksToBounds:YES];
    
    [self.picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
        if(!_isMenuOpen)
            [self uploadAction];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0){
        NSLog(@"Cancel Action Sheet Did Push");
        [_picImageView setUserInteractionEnabled:TRUE];
    }
    else
    {
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        switch (authStatus) {
            case AVAuthorizationStatusAuthorized:{
                if(buttonIndex == 1)
                {
                   
                    
                    
                    self.picker = [[UIImagePickerController alloc] init];
                    [self.picker setAllowsEditing:YES];
                    [self.picker setDelegate:self];
                    [self.picker setSourceType:UIImagePickerControllerSourceTypeCamera];
                    
                    [self.navigationController presentViewController:self.picker animated:YES completion:^{
                        [_picImageView setUserInteractionEnabled:TRUE];
                    }];
                }
                else if(buttonIndex == 2)
                {
                    self.picker = [[UIImagePickerController alloc] init];
                    [self.picker setAllowsEditing:YES];
                    [self.picker setDelegate:self];
                    [self.picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                    [self.navigationController presentViewController:self.picker animated:YES completion:^{
                        [_picImageView setUserInteractionEnabled:TRUE];
                    }];
                    
                    
                }
            }
                break;
            case AVAuthorizationStatusNotDetermined:{
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if(granted){
                            // UI updates as needed
                            if(buttonIndex == 1)
                            {
                                self.picker = [[UIImagePickerController alloc] init];
                                [self.picker setAllowsEditing:YES];
                                [self.picker setDelegate:self];
                                [self.picker setSourceType:UIImagePickerControllerSourceTypeCamera];
                                
                                [self.navigationController presentViewController:self.picker animated:YES completion:^{
                                    [_picImageView setUserInteractionEnabled:TRUE];
                                }];
                            }
                            else if(buttonIndex == 2)
                            {
                                self.picker = [[UIImagePickerController alloc] init];
                                [self.picker setAllowsEditing:YES];
                                [self.picker setDelegate:self];
                                [self.picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                [self.navigationController presentViewController:self.picker animated:YES completion:^{
                                    [_picImageView setUserInteractionEnabled:TRUE];
                                }];
                            }
                        }
                        else {
                            // UI updates as needed
                            NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                        }
                    });
                    
                }];
            }
                break;
            case AVAuthorizationStatusDenied:{
                [self presentSettingAlert];
            }
                break;
            case AVAuthorizationStatusRestricted:{
                [self presentSettingAlert];
            }
                break;
            default:
                break;
        }
    }
}

- (void)presentSettingAlert{
    [UtilManager presentAlertWithMessage:NSLocalizedString(@"You can enable access in Privacy Settings.", nil) andTitle:NSLocalizedString(@"This app does not have access to your camera.", nil)];
}

#pragma mark WebServiceDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    [_submitButton setEnabled:TRUE];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    if(webServiceType == WebServiceTypeGeneratingReport){
        _reportId = (NSString *)object;
        NSLog(@"Received report Id");
        
    } else if (webServiceType == WebServiceTypeImageReport){
        
        [WebServiceManager postReportFields:[self reportingDictionary] andReportId:_reportId andDelegate:self];

    }else{
        [_submitButton setEnabled:TRUE];
        
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Thank you, your report has been submitted", nil) andTitle:NSLocalizedString(@"Report Sent", nil)];
        [self uploadAction];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    [_submitButton setEnabled:TRUE];
}

#pragma mark UITextViewDelegate Methods

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if([textView.text isEqualToString:NSLocalizedString(@"Describe the issue", nil)])
        [textView setText:@""];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"])
    {
        if(_momentTextView.text.length == 0)
            [_momentTextView setText:NSLocalizedString(@"Describe the issue", nil)];
        [_momentTextView resignFirstResponder];
        [_mailTextField becomeFirstResponder];
        
        return FALSE;
    }
    
    return TRUE;
}


#pragma mark UITextFieldDelegate MEthods

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO;
    
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    //hide picker
    [self dismmissPickerIssue];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
 
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength;
    
    newLength = [textField.text length] + [string length] - range.length;
    
    if([textField isEqual:_mailTextField])
        return (newLength > 30) ? NO : YES;
    if([textField isEqual:_phoneTextField])
        return (newLength > 12) ? NO : YES;
    if([textField isEqual:_nameTextField])
        return (newLength > 24) ? NO : YES;
    return YES;
}

#pragma mark UIPickerDataSource

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _types.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [_types objectAtIndex:row];
}

-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UIView *pickerCustomView = (id)view;
    UILabel *pickerViewLabel;
    UIImageView *pickerImageView;
    
    if (!pickerCustomView) {
        pickerCustomView= [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,
                                                                   [pickerView rowSizeForComponent:component].width - 10.0f, [pickerView rowSizeForComponent:component].height)];
        pickerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 35.0f, 35.0f)];
        pickerViewLabel= [[UILabel alloc] initWithFrame:CGRectMake(37.0f, -5.0f,
                                                                   [pickerView rowSizeForComponent:component].width - 10.0f, [pickerView rowSizeForComponent:component].height)];
        // the values for x and y are specific for my example
        [pickerCustomView addSubview:pickerImageView];
        [pickerCustomView addSubview:pickerViewLabel];
    }
    
    
    pickerCustomView.backgroundColor = GRAY_PIKCER_COLOR;
    UIView * separatorline = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    separatorline.backgroundColor = [UIColor whiteColor];
    pickerView.backgroundColor = GRAY_PIKCER_COLOR;
    [pickerView addSubview:separatorline];
    
    pickerViewLabel.text = _types[row]; // where therapyTypes[row] is a specific example from my code
    pickerViewLabel.font = [UIFont fontWithName:REGULAR_FONT size:20];
    
    return pickerCustomView;
}

@end
