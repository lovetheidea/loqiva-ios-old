//
//  FunctionsTableViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"


@class MenuContainerNavController;

@protocol FunctionsTableViewDelegate;

@interface FunctionsTableViewController : ParentViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *functionTableView;
@property (nonatomic, assign) id<FunctionsTableViewDelegate>delegate;
- (void)backToStartApplication;
@end


@protocol FunctionsTableViewDelegate <NSObject>

- (void)presentFunctionViewController:(UIViewController *)viewController;
@end
