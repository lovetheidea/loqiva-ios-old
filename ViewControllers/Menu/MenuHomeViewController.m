//
//  MenuHomeViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "MenuHomeViewController.h"
#import "MenuContainerNavController.h"
#import "UIImageView+AFNetworking.h"
#import "HomeViewController.h"
#import <ATLocationBeacon/ATLocationBeacon.h>
#import <ATConnectionHttp/ATConnectionHttp.h>
#import "UtilManager.h"
#import "AppDelegate.h"
#import "WebServiceManager.h"
#import "AppContext.h"
#import "BeaconData.h"
#import "CustomNotification.h"
#import "BlueCatsSDK/BlueCatsSDK.h"
#import <CoreLocation/CoreLocation.h>
#import "GeoLocationMessage.h"
#import "LocationManager.h"
#import "loqiva-Swift.h"

@interface MenuHomeViewController () <ATRangeDelegate,ATBeaconBleLocationStatusDelegate, WebServiceManagerDelegate,BCBeaconManagerDelegate,BCEventManagerDelegate,CLLocationManagerDelegate>{
    ATBeaconManager *beaconManager;
}

@property (strong, nonatomic) BCBeaconManager *BCbeaconManager;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *controlLocation;

@end

@implementation MenuHomeViewController

@synthesize functionTableViewController, containerViewController, container;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isNotified = FALSE;

    MenuHomeViewController * __weak weakSelf = self;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
            // Stop normal location updates and start significant location change updates for battery efficiency.
            [weakSelf.locationManager stopUpdatingLocation];
            [weakSelf.locationManager startMonitoringSignificantLocationChanges];
        }
        else {
            NSLog(@"Significant location change monitoring is not available.");
        }
    }];
    
    // Stop the significant location change service, if available,
    // and start the standard location service.
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillEnterForegroundNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
            // Stop significant location updates and start normal location updates again since the app is in the forefront.
            [weakSelf.locationManager stopMonitoringSignificantLocationChanges];
            [weakSelf.locationManager startUpdatingLocation];
        }
        else {
            NSLog(@"Significant location change monitoring is not available.");
        }
        // Reset the icon badge number to zero.
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }];
    
    if(!isBLUECAT){
        beaconManager = [ATBeaconManager sharedInstance];
        // Register Range Delegate protocol to your view
        [beaconManager registerAdtagRangeDelegate:self];
        [[ATBeaconManager sharedInstance] registerAdtagBeaconBleLocationDelegate:self];
    // Do any additional setup after loading the view, typically from a nib.
    } else {
        _BCbeaconManager = [[BCBeaconManager alloc] initWithDelegate:self queue:nil];
        [[BCEventManager sharedManager] setDelegate:self];
    }
    
    HomeViewController *homeViewcontroller = [[HomeViewController alloc] init];
    containerViewController = [[MenuContainerNavController alloc] initWithRootViewController:homeViewcontroller];
    
    container = [SideMenuContainerViewController containerWithCenterViewController:containerViewController leftMenuViewController:nil rightMenuViewController:nil];
    [container setPanMode:SideMenuPanModeCenterViewController];
    [containerViewController setContainerDelegate:self];
    
    [self.view addSubview:container.view];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appActive) name:@"APP_ACTIVE" object:nil];
}

- (void)viewDidUnload {
    self.locationManager.delegate = nil;
    self.locationManager = nil;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"APP_ACTIVE" object:nil];
    for (UIViewController *controller in self.containerViewController.viewControllers) {
        [controller removeFromParentViewController];
        [controller.view removeFromSuperview];
    }
    
    self.locationManager.delegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    _isNotified = FALSE;
}

- (void)appActive{
    _isNotified = FALSE;
}

#pragma mark ContainerViewControllerDelegate Methods

- (void)slideActionDidSelected
{
    if(container.menuState == SideMenuStateRightMenuOpen)
    {
        [container setMenuState:SideMenuStateClosed completion:^{
            
        }];
    }
    else
    {
        [container setMenuState:SideMenuStateRightMenuOpen completion:^{
        }];
    }
}

#pragma mark 

-(void)checkBleStatus:(CBCentralManagerState)bleStatus
       locationStatus:(CLAuthorizationStatus)locationStatus{
    
    if(locationStatus==kCLAuthorizationStatusDenied || locationStatus == kCLAuthorizationStatusRestricted){
        [UtilManager presentAlertWithMessage:@"Please activate your bluetooth in settings"];
    }else if (bleStatus== CBCentralManagerStateUnauthorized || bleStatus == CBCentralManagerStatePoweredOff){
        [UtilManager presentAlertWithMessage:@"Please activate your bluetooth in settings"];
        
    }else if(bleStatus == CBCentralManagerStateUnsupported){
        [UtilManager presentAlertWithMessage:@"This device not support Beacons"];
        
    }else{
        NSLog(@"Perfect");
    }
}

// OLD API METHOD

-(void)vpn:(NSArray *)_beacons
        beaconContents:(NSArray *)beaconContents
     informationStatus:(ATRangeInformationStatus )informationStatus
            feedStatus:(ATRangeFeedStatus)feedStatus
                region:(CLRegion *)region{
    
    if(_beacons.count>0){
        //[UtilManager presentAlertWithMessage:[NSString stringWithFormat:@"%@",_beacons]];
        NSLog(@"%@",_beacons);
    }
    
    if(beaconContents.count > 0){
        NSLog(@"%@",beaconContents);
    }
    
}

- (void)didRangeBeacons:(NSArray *)_beacons beaconContents:(NSArray *)beaconContents informationStatus:(ATRangeInformationStatus)informationStatus feedStatus:(ATRangeFeedStatus)feedStatus region:(CLRegion *)region{
    switch (feedStatus) {
            case ATRangeFeedStatusBackendSuccess:
            break;
            
        default:
            break;
    }
    
    if(beaconContents.count > 0){
        for(ATBeaconContent *beaconContent in beaconContents){
            if(!_isNotified){
                //[[AppDelegate sharedDelegate] createNotification:beaconContent];
                [WebServiceManager getBeaconRewardWithBeaconDictionary:[self beaconDictionary:beaconContent] andDelegate:self];
                _isNotified = TRUE;
            }
            
        }
    }
    
}

- (NSDictionary *)beaconDictionary:(ATBeaconContent *)beaconContent{
    
    NSDictionary *Localisation = [[[beaconContent.poi.newCategories valueForKey:@"Localisation"] valueForKey:@"fields"] valueForKey:@"Localisation"];
    NSArray *beaconLocation = [Localisation valueForKey:@"location"];
    
    NSNumber *latitude;
    NSNumber *longitude;
    if(beaconLocation.count == 2){
        latitude = [[NSNumber alloc] initWithFloat:[[beaconLocation objectAtIndex:0] floatValue]];
        longitude = [[NSNumber alloc] initWithFloat:[[beaconLocation objectAtIndex:1] floatValue]];
    } else {
        latitude = [[NSNumber alloc] initWithFloat:0];
        longitude = [[NSNumber alloc] initWithFloat:0];
    }
    NSString *pointUniqueId = beaconContent.access.id;
    
    NSDictionary *beaconDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[[SessionManager shared] user].userToken,@"userid",pointUniqueId,@"pointuniqueid",beaconContent.uuid,@"beaconuuid",beaconContent.major,@"major",beaconContent.minor,@"minor",latitude,@"latitude",longitude,@"longitude",@"CT",@"vandortype", nil];
    
    return beaconDictionary;
}

- (NSDictionary *)BCbeaconDictionary:(BCBeacon *)beacon{
    
    NSNumber *latitude;
    NSNumber *longitude;
    
    if([[LocationManager sharedInstance] currentLocation]){
        
        double lat = [[[LocationManager sharedInstance] currentLocation] coordinate].latitude;
        double longi = [[[LocationManager sharedInstance] currentLocation] coordinate].longitude;
        
        latitude = [NSNumber numberWithDouble:lat];
        longitude = [NSNumber numberWithDouble:longi];
    } else {
        latitude = [NSNumber numberWithInt:0];
        longitude = [NSNumber numberWithInt:0];
    }

    
    NSDictionary *beaconDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[[SessionManager shared] user].userToken,@"userid",beacon.proximityUUIDString, @"beaconuuid",beacon.beaconID,@"beaconid",beacon.major,@"major",beacon.minor,@"minor",@"BT",@"vandortype",latitude,@"latitude",longitude,@"longitude", nil];

    return beaconDictionary;
}


#pragma marks WebServiceManagerDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    _isNotified = FALSE;
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    _isNotified = FALSE;
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    NSDictionary *resultset = (NSDictionary *)object;
    
    
    if(resultset.allKeys.count <4){
        NSLog(@"Resultset was empty");
    } else {
        
        //Whe "save" the notification when the server response
        
        CustomNotification *customNotification = [[CustomNotification alloc] init];
        [customNotification setTitle:[resultset valueForKey:@"title"]];
        [customNotification setSummary:[resultset  valueForKey:@"description"]];
        [customNotification setNotificationId:[resultset valueForKey:@"id"]];
        [customNotification setNotificationDate:[NSDate date]];
        [customNotification setIsRead:[NSNumber numberWithBool:FALSE]];
        [customNotification setCustomNotificationType:[NSNumber numberWithInt:CustomNotificationTypeBeacon]];
        
        [CustomNotification saveCustomNotification:customNotification];
        
       
        // Un-Commented out following code as we have saved the data on SaveCustomNotification -- wasiq
        NSMutableArray *notificationsArray = [[AppContext sharedInstance] notifications];
        [notificationsArray addObject:customNotification];
        [[AppContext sharedInstance] setNotifications:notificationsArray];
        [[NSNotificationCenter defaultCenter] postNotificationName:DID_UPDATE_BEACON_NOTIFICATION object:nil];
        // upto here
            
        [self launchLocalNotification:resultset];
    }
}

- (void)launchLocalNotification:(NSDictionary *)resultset {
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    localNotification.fireDate = [NSDate date];
    localNotification.alertAction = [resultset valueForKey:@"title"];
    localNotification.alertBody = [resultset valueForKey:@"description"];
    localNotification.userInfo = resultset;
    
    localNotification.soundName = @"alarm.wav";
    //[localNotification setAlertLaunchImage:[UIImage imageNamed:@""]];
    
    localNotification.applicationIconBadgeNumber = 0;
    
    [localNotification setUserInfo:resultset];
    
    BeaconData *beaconData = [[BeaconData alloc] init];
    [beaconData setBodyNotification:[resultset valueForKey:@"description"]];
    [beaconData setTitleNotification:[resultset valueForKey:@"title"]];
    [beaconData setIcon:[resultset valueForKey:@"icon"]];
    [beaconData setHeading:[resultset valueForKey:@"heading"]];
    [beaconData setRewardId:[resultset valueForKey:@"id"]];
        //NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],beaconData.icon]]
    //                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
    //                                          timeoutInterval:60];
    
    //[beaconImage setImageWithURLRequest:imageRequest placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
       
        //[beaconData setImageData:UIImageJPEGRepresentation(image, 0.8)];
        [BeaconData saveBeacon:beaconData];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    //} failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
    //    NSLog(@"%@",error.description);
    //}];
    BCTrigger *trigger = [[BCTrigger alloc] init];
    
    [trigger setEventName:beaconData.titleNotification];
    [trigger setEventIdentifier:beaconData.rewardId];
    [trigger setRepeatCount:1];
    [trigger setValidFrom:[NSDate date]];
    [trigger setExpiresAt:[NSDate dateWithTimeIntervalSinceNow:60*60*24*1]];
    
    //This is to monitoring the BEACON
    [[BCEventManager sharedManager] monitorEventWithTrigger:trigger];
    
}

#pragma mark - BCBeaconManagerDelegate classes
-(void)beaconManager:(BCBeaconManager *)monitor didRangeBeacons:(NSArray<BCBeacon *> *)beacons {
    //    This delegate method is going to show how you can add items to an array by getting the detail on your beacons by hand - showing the difference from EvenFilters.
    // Note you have to manually check the site, and the category - so you're searching through each beacon ranged to do this - eventfilters get rid of the need to do this.
    
    if (beacons.count > 0){
        for (BCBeacon *currentBeacon in beacons) {
            NSLog(@"BEACONID %@",currentBeacon.beaconID);
            NSLog(@"BEACONID %@",currentBeacon.beaconID);
            if(!_isNotified){
                //[[AppDelegate sharedDelegate] createNotification:beaconContent];
                [WebServiceManager getBeaconRewardWithBeaconDictionary:[self BCbeaconDictionary:currentBeacon] andDelegate:self];
                _isNotified = TRUE;
            }
            if (currentBeacon.site){ // We need to check the site because we can see all beacons within the beacon team - especially if beacons and site aren't in private mode.
                if (currentBeacon.categories.count > 0){
                    NSArray<BCCategory *> *myCATegories = currentBeacon.categories;
                    for (BCCategory *currentCategory in myCATegories){
                        NSLog(@"%@",currentCategory.name);
                        if(!_isNotified){
                            //[[AppDelegate sharedDelegate] createNotification:beaconContent];
                            [WebServiceManager getBeaconRewardWithBeaconDictionary:[self BCbeaconDictionary:currentBeacon] andDelegate:self];
                            _isNotified = TRUE;
                        }
                        if ([currentCategory.name isEqualToString:@"Attendee"]){
                                NSLog(@"Added %@", currentBeacon.name);
                            
                        }
                    }
                }
            }
        }
    }
}

- (void)beaconManager:(BCBeaconManager *)beaconManager didEnterSite:(BCSite *)site{
    NSLog(@"%@",site);
}
- (void)beaconManager:(BCBeaconManager *)beaconManager didExitSite:(BCSite *)site{

}

- (void)beaconManager:(BCBeaconManager *)beaconManager didDetermineState:(BCSiteState)state forSite:(BCSite *)site{
    NSLog(@"%@",site);
}

- (void)beaconManager:(BCBeaconManager *)beaconManager didRangeBlueCatsBeacons:(NSArray<BCBeacon *> *)blueCatsBeacons{
    NSLog(@"%@",blueCatsBeacons);
}

- (void)beaconManager:(BCBeaconManager *)monitor didRangeNewbornBeacons:(NSArray<BCBeacon *> *)newbornBeacons{
    NSLog(@"%@",newbornBeacons);
}

- (void)beaconManager:(BCBeaconManager *)monitor didRangeIBeacons:(NSArray<BCBeacon *> *)iBeacons{
    NSLog(@"%@",iBeacons);
}

- (void)beaconManager:(BCBeaconManager *)monitor didRangeEddystoneBeacons:(NSArray<BCBeacon *> *)eddystoneBeacons{

}

- (void)beaconManager:(BCBeaconManager *)monitor didEnterBeacons:(NSArray <BCBeacon *> *)beacons{
    NSLog(@"%@",beacons);
    if(beacons.count > 0){
        NSLog(@"Enter in Beacon Region");
    }
}

- (void)beaconManager:(BCBeaconManager *)monitor didExitBeacons:(NSArray <BCBeacon *> *)beacons{
    NSLog(@"%@",beacons);
    if(beacons.count > 0){
        NSLog(@"Exit in Beacon Region");
    }
}

- (void)beaconManager:(BCBeaconManager *)beaconManager didDetermineState:(BCBeaconState)state forBeacon:(BCBeacon *)beacon{
    NSLog(@"%@",beacon);
}

- (void)beaconManager:(BCBeaconManager *)monitor didDiscoverEddystoneURL:(NSURL *)eddystoneURL{
    NSLog(@"%@",eddystoneURL);
}

-(void)eventManager:(BCEventManager *)eventManager triggeredEvent:(BCTriggeredEvent *)triggeredEvent
{
    //I think is the method to launch an action in background
    // The Coonecthings SDK launch the notifications buts BlueCats not
    
    if(triggeredEvent.filteredMicroLocation.beacons.count > 0){
        BCBeacon *saveBeacon = [triggeredEvent.filteredMicroLocation.beacons objectAtIndex:0];
        NSLog(@"%@",saveBeacon);
    }
    
    BeaconData *beaconData = [BeaconData getBeaconData];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    localNotification.fireDate = [NSDate date];
    localNotification.alertAction = beaconData.titleNotification;
    localNotification.alertBody = beaconData.bodyNotification;
    //localNotification.userInfo = resultset;
    
    localNotification.soundName = @"alarm.wav";
    //[localNotification setAlertLaunchImage:[UIImage imageNamed:@""]];
    
    localNotification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;

    //[[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    //We can delete the Beacon Notification after Launch
    [eventManager removeMonitoredEvent:triggeredEvent.event.eventIdentifier];
    
    NSLog(@"%@",triggeredEvent.event);
}

//- (void)loadRegionsWithLocation:(CLLocation *)userLocation{
//    
//    if([[AppContext sharedInstance] geoLocationMessage]){
//    
//        GeoLocationMessage *geoLocationMessage = [[AppContext sharedInstance] geoLocationMessage];
//        
//        if ([CLLocationManager isMonitoringAvailableForClass:[CLCircularRegion class]]) {
//            // Create a new region based on the center of the map view.
//            CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(geoLocationMessage.userlatitude.floatValue, geoLocationMessage.userlongitude.floatValue);
//            
//            CLCircularRegion *newRegion = [[CLCircularRegion alloc] initWithCenter:coord
//                                                                            radius:1000.0
//                                                                        identifier:geoLocationMessage.welcomeMsg];
//            
//            newRegion.notifyOnEntry = YES;
//            newRegion.notifyOnExit = YES;
//            // Start monitoring the newly created region.
//            
//            BOOL doesItContainMyPoint = [newRegion containsCoordinate:CLLocationCoordinate2DMake(geoLocationMessage.userlatitude.floatValue, geoLocationMessage.userlongitude.floatValue)];
//            
//            if(!doesItContainMyPoint){
//                NSLog(@"Problema con %@",geoLocationMessage.msgtitle);
//            }
//            [self.locationManager startMonitoringForRegion:newRegion];
//        }
//        else {
//            NSLog(@"Region monitoring is not available.");
//        }
//    }
//}

//#pragma mark - CLLocationManagerDelegate
//
//- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
//    
//    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusAuthorizedAlways) {
//        [self.locationManager startUpdatingLocation];
//    }
//}
//
//- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
//    NSLog(@"didFailWithError: %@", error);
//}
//
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
//    
//    if(!_controlLocation || [_controlLocation distanceFromLocation:newLocation] > 50){
//        for (CLRegion *monitored in [self.locationManager monitoredRegions])
//            [self.locationManager stopMonitoringForRegion:monitored];
//        _controlLocation = newLocation;
//        [self loadRegionsWithLocation:newLocation];
//    }
//}
//
//// The device entered a monitored region.
//- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region  {
//    NSString *event = [NSString stringWithFormat:@"%@", region.identifier];
//    NSLog(@"%@",event);
//    GeoLocationMessage *geolocationMessage = [[AppContext sharedInstance] geoLocationMessage];
//    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//    
//    localNotification.fireDate = [NSDate date];
//    localNotification.alertAction = geolocationMessage.msgtitle;
//    localNotification.alertBody = geolocationMessage.welcomeMsg;
//    
//    localNotification.soundName = @"alarm.wav";
//    //[localNotification setAlertLaunchImage:[UIImage imageNamed:@""]];
//    localNotification.applicationIconBadgeNumber = 0;
//    
//    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//}
//
//// The device exited a monitored region.
//- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
//    NSString *event = [NSString stringWithFormat:@"%@", region.identifier];
//    NSLog(@"%@",event);
//    GeoLocationMessage *geolocationMessage = [[AppContext sharedInstance] geoLocationMessage];
//    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//    
//    localNotification.fireDate = [NSDate date];
//    localNotification.alertAction = geolocationMessage.msgtitle;
//    localNotification.alertBody = geolocationMessage.goodbyeMsg;
//    
//    localNotification.soundName = @"alarm.wav";
//    //[localNotification setAlertLaunchImage:[UIImage imageNamed:@""]];
//    localNotification.applicationIconBadgeNumber = 0;
//    
//    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//    
//}
//
//// A monitoring error occurred for a region.
//- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
//    NSString *event = [NSString stringWithFormat:@"%@", region.identifier];
//    NSLog(@"%s %@", __PRETTY_FUNCTION__, event);
//    NSLog(@"%@",error);
//    
//    //[self updateWithEvent:event];
//}
//
//- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region{
//    NSString *event = [NSString stringWithFormat:@"%@", region.identifier];
//    NSLog(@"%s %@", __PRETTY_FUNCTION__, event);
//}

@end
