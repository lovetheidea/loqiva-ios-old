//
//  UIViewController+SideMenuAdditions.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "UIViewController+SideMenuAdditions.h"
#import "SideMenuContainerViewController.h"
#import "loqiva-Swift.h"

@implementation UIViewController (SideMenuAdditions)

@dynamic menuContainerViewController;

- (SideMenuContainerViewController *)menuContainerViewController
{
    id containerView = self;
    while (![containerView isKindOfClass:[SideMenuContainerViewController class]] && containerView)
    {
        if ([containerView respondsToSelector:@selector(parentViewController)])
            containerView = [containerView parentViewController];
        if ([containerView respondsToSelector:@selector(splitViewController)] && !containerView)
            containerView = [containerView splitViewController];
    }
    return containerView;
}

@end
