//
//  MenuHomeViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FunctionsTableViewController.h"
#import "MenuContainerNavController.h"
#import "SideMenuContainerViewController.h"

#import "ParentViewController.h"

@interface MenuHomeViewController : ParentViewController <MenuContainerNavControllerDelegate>

@property (nonatomic, strong) SideMenuContainerViewController *container;
@property (nonatomic, strong) MenuContainerNavController *containerViewController;
@property (nonatomic, strong) FunctionsTableViewController *functionTableViewController;

@property (nonatomic, assign) BOOL isNotified;

@end
