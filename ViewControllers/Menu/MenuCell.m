//
//  MenuCell.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "MenuCell.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@implementation MenuCell

@synthesize nameLabel, icon;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        [self setBackgroundColor:[UIColor orangeAppColor]];
        [[self contentView] setBackgroundColor:[UIColor orangeAppColor]];
        
        icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:25], 0., [UtilManager width:35], [UtilManager height:33])];
        [icon setBackgroundColor:[UIColor clearColor]];
        [icon setContentMode:UIViewContentModeScaleAspectFit];
        [icon setCenter:CGPointMake(icon.center.x, self.contentView.center.y)];
        [[self contentView] addSubview:icon];
        
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(icon.frame.origin.x + icon.frame.size.width + [UtilManager width:24], 0., [UtilManager width:258], 44)];
        [nameLabel setTextAlignment:NSTextAlignmentLeft];
        [nameLabel setTextColor:[UIColor whiteColor]];
        [nameLabel setFont:[UIFont fontWithName:LIGHT_FONT size:26]];
        [nameLabel setCenter:CGPointMake(nameLabel.center.x, icon.center.y)];
        
        [[self contentView] addSubview:nameLabel];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:FALSE animated:FALSE];
}

@end
