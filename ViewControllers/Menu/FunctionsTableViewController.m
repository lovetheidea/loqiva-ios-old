//
//  FunctionsTableViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "FunctionsTableViewController.h"

#import "SideMenuContainerViewController.h"
#import "UIViewController+SideMenuAdditions.h"
#import "ScheduleMonthViewController.h"
#import "WelcomeViewController.h"

#import "UtilManager.h"
#import "AppConstants.h"
#import "MenuCell.h"
#import "AppDelegate.h"

#import "MenuContainerNavController.h"
#import "UIColor+Helper.h"
#import "HomeViewController.h"
#import "MapViewController.h"
#import "RewardsViewController.h"
#import "ScheduleViewController.h"
#import "InformationViewController.h"
#import "ReportingViewController.h"
#import "PaymentsViewController.h"
#import "TransportationViewController.h"
#import "SurveysViewController.h"
#import "ProfileViewController.h"
#import "SignUpViewController.h"
#import "SignNavController.h"

#import "AppContext.h"
#import "User.h"
#import "UserDefaultManager.h"

#import "UILabel+Extra.h"
#import "loqiva-Swift.h"

@interface FunctionsTableViewController ()

@property (nonatomic, strong) NSArray *tableData;

@end

@implementation FunctionsTableViewController

@synthesize functionTableView, delegate;

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    NSString *appName = infoPlistDict[@"AppName"];
    if (appName.length == 0) {
        appName = @"Loqiva";
    }

    self.tableData = infoPlistDict[@"ApplicationMenuItems"];
    
    [self configureView];
}

- (void)configureView
{
    [self.view setBackgroundColor:[UIColor orangeAppColor]];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20.], [UtilManager height:29], WIDTH, [UtilManager height:32])];
    [nameLabel setTextColor:[UIColor whiteColor]];
    
    User *currentUser = [[SessionManager shared] user];
    if(currentUser)
        [nameLabel setText:[NSString stringWithFormat:@"Hi %@",currentUser.firstName]];
    else
        [nameLabel setText:NSLocalizedString(@"Welcome", nil)];
    [nameLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21.]];
    [self.view addSubview:nameLabel];
    
    UIImage *closeImage = [UIImage imageNamed:@"btn-close"];
    
    CGFloat closePadd = 15.0f;
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(NAV_CLOSE_ORIGIN_X, NAV_CLOSE_ORIGIN_Y-(closePadd/2),NAV_CLOSE_WIDTH+closePadd, NAV_CLOSE_HEIGHT+closePadd)];
    [backButton setImage:closeImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    backButton.contentEdgeInsets = UIEdgeInsetsMake(-closePadd, -closePadd, -closePadd, -closePadd);
    [self.view addSubview:backButton];

    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., NAV_BAR_HEIGHT - [UtilManager height:2], WIDTH, [UtilManager width:1])];
    [separatorView setBackgroundColor:HEADER_SEPARATOR_COLOR];
    [self.view addSubview:separatorView];
    
    functionTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., separatorView.frame.origin.y + separatorView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - (separatorView.frame.origin.y + separatorView.frame.size.height))];
    [functionTableView setBackgroundColor:[UIColor orangeAppColor]];
    [functionTableView setBounces:FALSE];
    [functionTableView setDelegate:self];
    [functionTableView setDataSource:self];
    [functionTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [functionTableView setScrollEnabled:YES];
    [functionTableView setBounces:YES];
    [functionTableView registerClass:[MenuCell class] forCellReuseIdentifier:@"cell"];
    [functionTableView setBounces:FALSE];
    [self.view addSubview:functionTableView];
    
    UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissAction)];
    [swipeRecognizer setDirection:UISwipeGestureRecognizerDirectionUp];
    [self.view addGestureRecognizer:swipeRecognizer];
}

#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = self.tableData[indexPath.row];
    NSString *title = dict[@"Title"];
    NSString *iconName = dict[@"Icon"];
    
    if ([title containsString:@"Log In"] && [[SessionManager shared] isLoggedIn]) {
        title = @"Log Out";
    }
    
    MenuCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [[tableViewCell nameLabel] setAttributedText:title withLineHeight:41 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:30] andColor:[UIColor whiteColor] andKern:0 withLineBreak:NO];
    [[tableViewCell icon] setImage:[UIImage imageNamed:iconName]];
    [tableViewCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return tableViewCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableData.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, 80)];
    [headerView setBackgroundColor:[UIColor orangeAppColor]];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [UtilManager height:20];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [UtilManager height:50];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dict = self.tableData[indexPath.row];
    NSString *controllerName = dict[@"Controller"];
    if ([controllerName isEqualToString:@"Hub"]) {
        [self presentHomeViewController];
    }
    else if ([controllerName isEqualToString:@"Maps"]) {
        [self presentMapViewController];
    }
    else if ([controllerName isEqualToString:@"Rewards"]) {
        [self presentRewardsViewController];
    }
    else if ([controllerName isEqualToString:@"Schedule"]) {
        [self presentScheduleViewController];
    }
    else if ([controllerName isEqualToString:@"Information"]) {
        [self presentInformationViewController];
    }
    else if ([controllerName isEqualToString:@"Report"]) {
        [self presentReportingViewController];
    }
    else if ([controllerName isEqualToString:@"Transportation"]) {
        [self presentTransportationViewController];
    }
    else if ([controllerName isEqualToString:@"Profile"]) {
        [self presentProfileViewController];
    }
    else if ([controllerName isEqualToString:@"Survey"]) {
        [self presentSurveysViewController];
    }
    else if ([controllerName isEqualToString:@"Purchase"]) {
        [self presentPaymentsViewController];
    }
    else if ([controllerName isEqualToString:@"LogIn"]) {
        [self logout];
    }
}

- (void)presentHomeViewController{
    HomeViewController *homeViewController = [[HomeViewController alloc] init];
    [self presentFunctionViewController:homeViewController];
}

- (void)presentMapViewController{
    MapViewController *mapViewController = [[MapViewController alloc] init];
    [mapViewController setMapSelectType:MapSelectTypeNone];
    [self presentFunctionViewController:mapViewController];
}

- (void)presentRewardsViewController{
    RewardsViewController *rewardsViewController = [[RewardsViewController alloc] init];
    [self presentFunctionViewController:rewardsViewController];
}

- (void)presentRewardsDetailViewController:(NSNotification *)notification{
    
}

- (void)presentScheduleViewController{
    ScheduleMonthViewController *scheduleViewController = [[ScheduleMonthViewController alloc] init];
    
    [self presentFunctionViewController:scheduleViewController];
}

- (void)presentInformationViewController{
    InformationViewController *informationViewController = [[InformationViewController alloc] init];
    [self presentFunctionViewController:informationViewController];
}

- (void)presentReportingViewController{
 
    ReportingViewController *reportingViewController = [[ReportingViewController alloc] init];
    [self presentFunctionViewController:reportingViewController];
 
}

- (void)presentPaymentsViewController{
  
    PaymentsViewController *paymentsViewController = [[PaymentsViewController alloc] init];
    [self presentFunctionViewController:paymentsViewController];
  
}

- (void)presentTransportationViewController{
  
    TransportationViewController *transportationViewController = [[TransportationViewController alloc] init];
    [self presentFunctionViewController:transportationViewController];

}

- (void)presentSurveysViewController{
   
    if ([[SessionManager shared] isLoggedIn] == false) {
        
        __weak FunctionsTableViewController * weakSelf = self;
        self.tempUserViewController = [[TemporaryUserViewController alloc] initWithDismissBlock:^{
            
            [weakSelf.tempUserViewController.view removeFromSuperview];
            
        } andCallToSignForFree:^{
            [weakSelf.tempUserViewController.view removeFromSuperview];
            [weakSelf presentSignForFreeController];
            
        }];
        
        [self.view addSubview:self.tempUserViewController.view];
        
        return;
    }
    
    SurveysViewController *surveysViewController = [[SurveysViewController alloc] init];
    [self presentFunctionViewController:surveysViewController];
  
}

- (void)presentProfileViewController{
    
    if([[SessionManager shared] isLoggedIn] == false){
        
        __weak FunctionsTableViewController * weakSelf = self;
        self.tempUserViewController = [[TemporaryUserViewController alloc] initWithDismissBlock:^{
            
            [weakSelf.tempUserViewController.view removeFromSuperview];
            
        } andCallToSignForFree:^{
            [weakSelf.tempUserViewController.view removeFromSuperview];
            [weakSelf presentSignForFreeController];
            
        }];
        
        [self.view addSubview:self.tempUserViewController.view];
        
        return;
    }
    
    ProfileViewController *profileViewController = [[ProfileViewController alloc] init];
    [self presentFunctionViewController:profileViewController];
}


- (void)presentFunctionViewController:(UIViewController *)viewController{
    
    if([delegate respondsToSelector:@selector(presentFunctionViewController:)])
        [delegate presentFunctionViewController:viewController];
    
    [self dismissAction];
}

- (void)dismissAction {
    
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame = CGRectMake(0, -self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        self.view.alpha = 0;
        [self dismissViewControllerAnimated:NO completion:^{
            AppDelegate *cont = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [cont.windowImageView setImage: nil];
        }];
    }];
}

- (void)backToStartApplication {
    WelcomeViewController *viewController = [[WelcomeViewController alloc] init];
    SignNavController *customNavigationController = [[SignNavController alloc] initWithRootViewController:viewController];
    
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame = CGRectMake(0, -self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        self.view.alpha = 0;
        
        [[[AppDelegate sharedDelegate] window] setRootViewController:customNavigationController];
        [self dismissViewControllerAnimated:NO completion:^{
            AppDelegate *cont = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [cont.windowImageView setImage: nil];
        }];
    }];
}

@end
