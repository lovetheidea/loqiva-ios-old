//
//  MenuContainerNavController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "MenuContainerNavController.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "UtilManager.h"
#import "RegisterUserViewController.h"
#import "SignNavController.h"
#import "AppDelegate.h"
#import "WebViewController.h"
#import "UIImage+ImageEffects.h"
#import "WebServiceManager.h"
#import "MapViewController.h"
#import "HomeViewController.h"
#import "NotificationViewController.h"
#import "FunctionsTableViewController.h"
#import "RewardsViewController.h"
#import "RewardsDetailViewController.h"
#import "ScheduleViewController.h"
#import "ScheduleMonthViewController.h"
#import "ScheduleDetailViewController.h"
#import "InformationViewController.h"
#import "InformationDetailViewController.h"
#import "ReportingViewController.h"
#import "PaymentsViewController.h"
#import "TransportationViewController.h"
#import "SurveysViewController.h"
#import "SurveysDetailViewController.h"
#import "ProfileViewController.h"
#import "WebViewController.h"
#import "AttractionViewController.h"
#import "NotificationViewController.h"

#import "BeaconData.h"
#import "loqiva-Swift.h"

@interface MenuContainerNavController ()<FunctionsTableViewDelegate>

@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, assign) CGFloat spaceBar;
@property(nonatomic, strong) TemporaryUserViewController *tempUserViewController;

@end

@implementation MenuContainerNavController

@synthesize containerView;
@synthesize slideButton, backButton, titleLabel, icon;
@synthesize separatorView, spaceBar, blurView;
@synthesize containerDelegate, notificationButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationBar] setHidden:YES];
    [self setDelegate:self];
    
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.interactivePopGestureRecognizer.enabled = YES;
    }
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:80])];
    
    blurView = [[UIView alloc] initWithFrame:containerView.frame];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    [blurEffectView setFrame:blurView.bounds];
    blurView = blurEffectView;
    
    [containerView addSubview:blurView];
    
    icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:18], [UtilManager height:30], [UtilManager width:30.], [UtilManager height:30])];
    [icon setBackgroundColor:[UIColor clearColor]];
    [icon setContentMode:UIViewContentModeScaleAspectFit];
    [containerView addSubview:icon];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(icon.frame.origin.x + icon.frame.size.width + [UtilManager width:5], icon.frame.origin.y, [UtilManager width:180.], icon.frame.size.height)];
    [titleLabel setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setTextAlignment:NSTextAlignmentLeft];
    
    [containerView addSubview:titleLabel];
    
    slideButton  = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:35], [UtilManager height:40], [UtilManager width:28], [UtilManager height:20])];
    [slideButton setImage:[UIImage imageNamed:@"btn-slider"] forState:UIControlStateNormal];
    [slideButton addTarget:self action:@selector(slideAction) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:slideButton];
    
    notificationButton = [[UIButton alloc] initWithFrame:CGRectMake(slideButton.frame.origin.x - [UtilManager width:30.] - [UtilManager width:4.], [UtilManager height:30], [UtilManager width:30.],[UtilManager height:40])];
    [notificationButton setImage:[UIImage imageNamed:@"btn-notification"] forState:UIControlStateNormal];
    [notificationButton addTarget:self action:@selector(notificationAction) forControlEvents:UIControlEventTouchUpInside];
   
    
    backButton = [[UIButton alloc] initWithFrame:CGRectMake(notificationButton.frame.origin.x,notificationButton.frame.origin.y, [UtilManager width:12], [UtilManager height:22])];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [backButton setHidden:YES];
    [containerView addSubview:backButton];
    
    [containerView addSubview:notificationButton];
    
    separatorView = [[UIView alloc] initWithFrame:CGRectMake(slideButton.frame.origin.x + slideButton.frame.size.width, slideButton.frame.origin.y, 1., slideButton.frame.size.height)];
    [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
    spaceBar = self.view.frame.size.width - separatorView.frame.origin.x - separatorView.frame.size.width;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backAction) name:BACK_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(slideAction:) name:SLIDE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationAction:) name:ACTION_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentNotificationViewController:) name:VIEWCONTROLLER_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentWebViewController:) name:WEBVIEW_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter ] addObserver:self selector:@selector(presentNotificationViewController:) name:REWARD_DETAIL_NOTIFICATION object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BACK_NOTIFICATION  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SLIDE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ACTION_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:VIEWCONTROLLER_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:WEBVIEW_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:REWARD_DETAIL_NOTIFICATION object:nil];
}

- (UIImage *)renderImageFromView:(UIView *)view withRect:(CGRect)frame {
    UIGraphicsBeginImageContextWithOptions(frame.size, YES, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    
    UIImage *renderedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return renderedImage;
}

- (void)slideAction:(NSNotification *)object
{
    [containerView setHidden:YES];
    FunctionsTableViewController *functionsTableViewcontroller = [[FunctionsTableViewController alloc] init];
    [functionsTableViewcontroller setDelegate:self];
    functionsTableViewcontroller.view.alpha = 0;
    
    UIViewController *controller = [object object];
    [self pushController:functionsTableViewcontroller fromController:controller];
}

- (void)pushController:(UIViewController *)toController fromController:(UIViewController *)fromController {
    UIImage *image = [self renderImageFromView:[fromController view] withRect:[fromController.view frame]];
    
    AppDelegate *cont = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [cont.windowImageView setImage:image];
    [cont.window sendSubviewToBack:cont.windowImageView];
    
    [cont.window.rootViewController presentViewController:toController animated:NO completion:^{
        toController.view.frame = CGRectMake(0, -toController.view.frame.size.height, toController.view.frame.size.width, toController.view.frame.size.height);
        toController.view.alpha = 1;
        
        [UIView animateWithDuration:0.25 animations:^{
            toController.view.frame = CGRectMake(0, 0, toController.view.frame.size.width, toController.view.frame.size.height);
        }];
    }];
}

- (void)notificationAction:(NSNotification *)object {
    
    NotificationViewController *notificationViewController = [[NotificationViewController alloc] init];
    
    UIViewController *controller = [object object];
    [self pushController:notificationViewController fromController:controller];
}

- (void)backAction
{
    [self popViewControllerAnimated:YES];
}

- (void)presentSignForFreeController {
    RegisterUserViewController *viewController = [[RegisterUserViewController alloc] init];
    SignNavController *customNavigationController = [[SignNavController alloc] initWithRootViewController:viewController];
    customNavigationController.comesFromApp=YES;
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:customNavigationController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:customNavigationController];
     }];
    
}

- (void)presentNotificationViewController:(NSNotification *)notification {
    
    if ([AppDelegate showAlertIfDisconnected]) {
        return;
    }
    
    NSDictionary *notifDict = (NSDictionary *)notification.object;
    
    NSString *viewControllerString = [notifDict valueForKey:VIEWCONTROLLER_NOTIFICATION];
    if([viewControllerString isEqualToString:@"rewardsDetailViewController"]){
        
        if([[SessionManager shared] isLoggedIn] == false){
            
            __weak MenuContainerNavController * weakSelf = self;
            self.tempUserViewController = [[TemporaryUserViewController alloc] initWithDismissBlock:^{
                
                [weakSelf.tempUserViewController.view removeFromSuperview];
                
            } andCallToSignForFree:^{
                [weakSelf.tempUserViewController.view removeFromSuperview];
                [weakSelf presentSignForFreeController];
                
            }];
            
            [self.view addSubview:self.tempUserViewController.view];
            
            return;
        }
        
        RewardsDetailViewController *rewardsDetailViewController;
        if ([[self topViewControllerFind] isKindOfClass:[RewardsDetailViewController class]]) {
            rewardsDetailViewController = [self topViewControllerFind];
        }
        else {
            rewardsDetailViewController = [[RewardsDetailViewController alloc] init];
        }
        
        [rewardsDetailViewController setIsFromMenu:YES];
        
        Reward *currentReward = [Reward getRewardWithTitle:[notifDict valueForKey:@"rewardtitle"]];
        if(!currentReward) {
            currentReward = [Reward getRewardWithMapItemPoint:[notifDict valueForKey:@"rewardtitle"]];
            
            if (!currentReward) {
                [UtilManager presentAlertWithMessage:@"Oops something has gone wrong, please try again" andTitle:@"Error"];
                return;
            }
        }
        
        [rewardsDetailViewController setCurrentReward:currentReward];
        UIViewController *controller = [self topViewControllerFind];
        if ([controller respondsToSelector:@selector(dismissAction)]) {
            [controller performSelector:@selector(dismissAction)];
        }
        
        [self pushViewController:rewardsDetailViewController animated:YES];
    } else if([viewControllerString isEqualToString:@"scheduleDetailViewcontroller"]) {
        
        ScheduleDetailViewController *scheduleDetailViewController;
        if ([[self topViewControllerFind] isKindOfClass:[ScheduleDetailViewController class]]) {
            scheduleDetailViewController = [self topViewControllerFind];
        }
        else {
            scheduleDetailViewController = [[ScheduleDetailViewController alloc] init];
        }
        
        [scheduleDetailViewController setIsFromMenu:YES];
        UIViewController *controller = [self topViewControllerFind];
        if ([controller respondsToSelector:@selector(dismissAction)]) {
            [controller performSelector:@selector(dismissAction)];
        }
        
        [self pushViewController:scheduleDetailViewController animated:YES];
    }
    else if ([viewControllerString isEqualToString:@"attractionDetailViewcontroller"]){
        AttractionViewController *atractionViewController;
        if([notifDict valueForKey:@"attractionId"]){
            atractionViewController=[[AttractionViewController alloc] initWithCurrentInterestDetail:nil andInterestDetailID:[notifDict valueForKey:@"attractionId"]];
            
        }else{
            atractionViewController=[[AttractionViewController alloc] initWithCurrentInterestDetail:[[AppContext sharedInstance] selectedInterestDetail] andInterestDetailID:nil];
        }
        
        UIViewController *controller = [self topViewControllerFind];
        if ([controller respondsToSelector:@selector(dismissAction)]) {
            [controller performSelector:@selector(dismissAction)];
        }
        
        
        //Apply delay in order to show view content before the animation
        double delayInSeconds = 0.4;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self pushViewController:atractionViewController animated:YES];
        });
    }
    else if([viewControllerString isEqualToString:@"mapViewController"]){
        MapViewController *mapViewController;
        if ([[self topViewControllerFind] isKindOfClass:[MapViewController class]]) {
            mapViewController = [self topViewControllerFind];
        }
        else {
            mapViewController = [[MapViewController alloc] init];
        }
        
        if([[notifDict valueForKey:@"firstParam"] isEqual:[NSNumber numberWithInt:0]]) {
            [mapViewController setMapSelectType:MapSelectTypeRewards];
        }
        else if([[notifDict valueForKey:@"firstParam"] isEqual:[NSNumber numberWithInt:1]]) {
            [mapViewController setMapSelectType:MapSelectTypeFriends];
        }
        else if([[notifDict valueForKey:@"firstParam"] isEqual:[NSNumber numberWithInt:2]]) {
            [mapViewController setMapSelectType:MapSelectTypeTours];
        }
        
        else if([[notifDict valueForKey:@"firstParam"] isEqual:[NSNumber numberWithInt:3]]) {
            [mapViewController setMapSelectType:MapSelectTypeInfo];
        }
        
        NSString *mapId = [notifDict objectForKey:@"mapId"];
        if (mapId.length) {
            [mapViewController loadMapId:mapId];
        }
        
        UIViewController *controller = [self topViewControllerFind];
        if ([controller respondsToSelector:@selector(dismissAction)]) {
            [controller performSelector:@selector(dismissAction)];
        }

        [self pushViewController:mapViewController animated:YES];
    }
    else if ([viewControllerString isEqualToString:@"webViewcontroller"]){
        
        WebViewController *webViewController = [[WebViewController alloc] init];
        NSString *url = [notifDict objectForKey:@"firstParam"];
        NSString *type = [notifDict objectForKey:@"type"];
        [webViewController setUrlString:url];
        [webViewController setTitleBar:type];
        [webViewController setIsAlternative:YES];
        [webViewController setIsBackAvailable:NO];
        
        UIViewController *controller = [self topViewControllerFind];
        if ([controller respondsToSelector:@selector(dismissAction)]) {
            [controller performSelector:@selector(dismissAction)];
        }
        
        [self pushViewController:webViewController animated:YES];
    }
    
    else {
        BeaconData *lastBeaconData = [BeaconData getBeaconData];
        Reward *currentReward = [Reward getRewardWithMapItemPoint:[NSString stringWithFormat:@"%d",lastBeaconData.rewardId.intValue]];
        
        RewardsDetailViewController *rewardsDetailViewController;
        if ([[self topViewControllerFind] isKindOfClass:[RewardsDetailViewController class]]) {
            rewardsDetailViewController = [self topViewControllerFind];
        }
        else {
            rewardsDetailViewController = [[RewardsDetailViewController alloc] init];
        }
        [rewardsDetailViewController setIsFromMenu:TRUE];
        [rewardsDetailViewController setCurrentReward:currentReward];
        
        UIViewController *controller = [self topViewControllerFind];
        if ([controller respondsToSelector:@selector(dismissAction)]) {
            [controller performSelector:@selector(dismissAction)];
        }
        
        [self pushViewController:rewardsDetailViewController animated:FALSE];
    }
}

- (UIViewController*)topViewControllerFind {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)viewController {
    if ([viewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)viewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([viewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navContObj = (UINavigationController*)viewController;
        return [self topViewControllerWithRootViewController:navContObj.visibleViewController];
    } else if (viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed) {
        UIViewController* presentedViewController = viewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    }
    else {
        for (UIView *view in [viewController.view subviews])
        {
            id subViewController = [view nextResponder];
            if ( subViewController && [subViewController isKindOfClass:[UIViewController class]])
            {
                if ([(UIViewController *)subViewController presentedViewController]  && ![subViewController presentedViewController].isBeingDismissed) {
                    return [self topViewControllerWithRootViewController:[(UIViewController *)subViewController presentedViewController]];
                }
            }
        }
        return viewController;
    }
}

- (void)presentWebViewController:(NSNotification *)notification{
    NSDictionary *notifDict = (NSDictionary *)notification.object;
    
    WebViewController *webViewController = [[WebViewController alloc] init];
    [webViewController setUrlString:[notifDict valueForKey:@"url"]];
    [webViewController setTitleBar:[notifDict valueForKey:@"title"]];
    [webViewController setIsBackAvailable:TRUE];
    [self pushViewController:webViewController animated:YES];
    
}

#pragma mark UINavigationControllerDelegate Methods

//Mitch Controllers added here will make the status bar black
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    BOOL shouldBeDefault = NO;
    if ([viewController isKindOfClass:[WebViewController class]]) {
        shouldBeDefault = [(WebViewController *)viewController isAlternative];
    }
    
    if([viewController isKindOfClass:[SurveysViewController class]] || [viewController isKindOfClass:[SurveysDetailViewController class]] || [viewController isKindOfClass:[InformationViewController class]] || [viewController isKindOfClass:[InformationDetailViewController class]] || [viewController isKindOfClass:[ProfileViewController class]] || [viewController isKindOfClass:[PaymentsViewController class]] || [viewController isKindOfClass:[TransportationViewController class]] || [viewController isKindOfClass:[RewardsViewController class]] || [viewController isKindOfClass:[ScheduleMonthViewController class]] || shouldBeDefault)
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        if(self.viewControllers.count >= 2) {
            [backButton setHidden:FALSE];
        }
        else {
            [backButton setHidden:YES];
        }
        
    }
    else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    
}

#pragma mark FunctionsTableViewController Delegate MEthods

- (void)presentFunctionViewController:(UIViewController *)viewController{
    NSArray *controllers = [NSArray arrayWithObject:viewController];
    [self setViewControllers:controllers];
}

@end
