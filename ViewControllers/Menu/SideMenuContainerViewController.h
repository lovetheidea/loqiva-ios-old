//
//  SideMenuContainerViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideMenuShadow.h"

extern NSString * const SideMenuStateNotificationEvent;

typedef enum {
    SideMenuPanModeNone = 0,
    SideMenuPanModeCenterViewController = 1 << 0,
    SideMenuPanModeSideMenu = 1 << 1,
    SideMenuPanModeDefault = SideMenuPanModeCenterViewController | SideMenuPanModeSideMenu
} SideMenuPanMode;

typedef enum {
    SideMenuStateClosed,
    SideMenuStateLeftMenuOpen,
    SideMenuStateRightMenuOpen
} SideMenuState;

typedef enum {
    SideMenuStateEventMenuWillOpen,
    SideMenuStateEventMenuDidOpen,
    SideMenuStateEventMenuWillClose,
    SideMenuStateEventMenuDidClose
} SideMenuStateEvent;

@interface SideMenuContainerViewController : UIViewController <UIGestureRecognizerDelegate>

+ (SideMenuContainerViewController *)containerWithCenterViewController:(id)centerViewController
                                                  leftMenuViewController:(id)leftMenuViewController
                                                 rightMenuViewController:(id)rightMenuViewController;

@property (nonatomic, strong) id centerViewController;
@property (nonatomic, strong) UIViewController *leftMenuViewController;
@property (nonatomic, strong) UIViewController *rightMenuViewController;

@property (nonatomic, assign) SideMenuState menuState;
@property (nonatomic, assign) SideMenuPanMode panMode;

@property (nonatomic, assign) CGFloat menuAnimationDefaultDuration;
@property (nonatomic, assign) CGFloat menuAnimationMaxDuration;

@property (nonatomic, assign) CGFloat menuWidth;
@property (nonatomic, assign) CGFloat leftMenuWidth;
@property (nonatomic, assign) CGFloat rightMenuWidth;

@property (nonatomic, strong) SideMenuShadow *shadow;

@property (nonatomic, assign) BOOL menuSlideAnimationEnabled;
@property (nonatomic, assign) CGFloat menuSlideAnimationFactor;

- (void)toggleLeftSideMenuCompletion:(void (^)(void))completion;
- (void)toggleRightSideMenuCompletion:(void (^)(void))completion;
- (void)setMenuState:(SideMenuState)menuState completion:(void (^)(void))completion;
- (void)setMenuWidth:(CGFloat)menuWidth animated:(BOOL)animated;
- (void)setLeftMenuWidth:(CGFloat)leftMenuWidth animated:(BOOL)animated;
- (void)setRightMenuWidth:(CGFloat)rightMenuWidth animated:(BOOL)animated;

- (UIPanGestureRecognizer *)panGestureRecognizer;

@end
