//
//  MenuContainerNavController.h
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuContainerNavControllerDelegate;

@interface MenuContainerNavController : UINavigationController <UINavigationControllerDelegate>

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIButton *slideButton;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UIView *blurView;
@property (nonatomic, strong) UIButton *notificationButton;

@property (nonatomic, weak) id<MenuContainerNavControllerDelegate>containerDelegate;

@end

@protocol MenuContainerNavControllerDelegate <NSObject>

- (void)slideActionDidSelected;

@end
