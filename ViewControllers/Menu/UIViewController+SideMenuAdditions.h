//
//  UIViewController+SideMenuAdditions.h
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SideMenuContainerViewController;

@interface UIViewController (SideMenuAdditions)

@property(nonatomic,readonly,retain) SideMenuContainerViewController *menuContainerViewController;

@end
