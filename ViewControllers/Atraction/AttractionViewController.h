//
//  AtractionViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 25/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "POIDetailViewController.h"
#import "InterestDetail.h"

@interface AttractionViewController : POIDetailViewController

@property (nonatomic, strong) InterestDetail *currentInterestDetail;
@property (nonatomic, strong) NSNumber *interestDetailId;

-(id)initWithCurrentInterestDetail:(InterestDetail*)currentInterestDetail andInterestDetailID:(NSNumber*)interestDetailId;

// private
- (void)configureView;
- (void)addAnnotations;

@end
