//
//  ParentViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavCustomView.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "AppContext.h"
#import "UILabel+Extra.h"
#import "TemporaryUserViewController.h"
typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;


enum {
    CMSectionTypeService = 1,
    CMSectionTypeInfoDetail = 2,
    CMSectionTypeRewards = 3,
    CMSectionTypeSchedule = 4,
    CMSectionTypeProfile = 5,
    CMSectionTypeRewardsDetail = 6,
    CMSectionTypeReporting = 7,
    CMSectionTypeAttracction = 8,
    CMSectionTypeWebView = 9,
    CMSectionTypeScheduleMonth = 10,
    CMSectionTypeTour = 11,
    CMSectionTypeInformation = 12,
    CMSectionTypeTransportation = 13,
    CMSectionTypePayments = 14,
    CMSectionTypeWebViewAlternative
};
typedef NSUInteger CMSectionType;

@interface ParentViewController : UIViewController
@property(nonatomic,strong) NavCustomView * headerNav;
@property(nonatomic, strong) TemporaryUserViewController * tempUserViewController;
@property (nonatomic, strong) NSString *titleBar;
-(void)setupHeaderForSection:(CMSectionType)section;
- (void)presentSignForFreeController;
@end
