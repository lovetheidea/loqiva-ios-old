//
//  SplashViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "SplashViewController.h"
#import "WelcomeViewController.h"
#import "SignNavController.h"
#import "MenuHomeViewController.h"
#import "InterestsViewController.h"
#import "AFNetworkReachabilityManager.h"
#import "WebServiceManager.h"
#import "UserDefaultManager.h"
#import "ResetPasswordViewController.h"

#import "LocationManager.h"
#import "loqiva-Swift.h"
#import "AppDelegate.h"

@interface SplashViewController()<WebServiceManagerDelegate>

@property (nonatomic, strong) NSString *userToken;

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *splashImageView  = [[UIImageView alloc] initWithFrame:self.view.frame];
    UIImage *image = [UIImage imageNamed:[self splashImageNameForOrientation]];
    [splashImageView setImage:image];
    [self.view addSubview:splashImageView];
    
    // TODO this should not be triggered via a timer, but rather the delegate callbacks from the location manager
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(loading) userInfo:nil repeats:NO];
}

- (void)viewDidAppear:(BOOL)animated{
    locationManager = [[CLLocationManager alloc] init];
    [locationManager requestAlwaysAuthorization];
    
    [[NSUserDefaults standardUserDefaults] setFloat:locationManager.location.coordinate.latitude forKey:@"USERLAT"];
    [[NSUserDefaults standardUserDefaults] setFloat:locationManager.location.coordinate.longitude forKey:@"USERLNG"];

    
    NSLog(@"wLatitude %lf wLongitude %lf", [[NSUserDefaults standardUserDefaults] floatForKey:@"USERLAT"], [[NSUserDefaults standardUserDefaults] floatForKey:@"USERLNG"]);
}

- (NSString *)splashImageNameForOrientation {
    CGSize viewSize = self.view.bounds.size;
    NSString* viewOrientation = @"Portrait";
    
    NSArray* imagesDict = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"UILaunchImages"];
    for (NSDictionary* dict in imagesDict) {
        CGSize imageSize = CGSizeFromString(dict[@"UILaunchImageSize"]);
        if (CGSizeEqualToSize(imageSize, viewSize) && [viewOrientation isEqualToString:dict[@"UILaunchImageOrientation"]])
        return dict[@"UILaunchImageName"];
    }
    return nil;
}

- (void)loading{
    if(![[LocationManager sharedInstance] currentLocation])
    {
        [WebServiceManager getHomePageResourcesWithLatitude:[NSNumber numberWithDouble:0.] andLongitude:[NSNumber numberWithDouble:0.] andDelegate:nil];
        [WebServiceManager getEventsWithLatitude:[NSNumber numberWithDouble:0.] andLongitude:[NSNumber numberWithDouble:0.] andDelegate:nil];
    }
    else{
        
        [WebServiceManager getHomePageResourcesWithLatitude:[NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.latitude]  andLongitude:[NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.longitude] andDelegate:nil];
        [WebServiceManager getEventsWithLatitude:[NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.latitude] andLongitude:[NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.longitude] andDelegate:nil];
    }
    
    
    [WebServiceManager getInformationDetails:nil];
    [WebServiceManager getPaymentTypes:nil];
    [WebServiceManager getPaymentTransports:nil];
    
    [self requestRewards];
    [self requestAttractions];
    [self requestServices];
    [self requestTours];
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
#warning DEVELOPER METHODS
    
    // TODO why are we logging in when the app launches? The user should already be logged in. If the server was to invalidate the session we would simply log the user out and ask them to sign back in.
    
    if([[SessionManager shared] isLoggedIn]) {
        [self loginIfNeeded];
    } else {
        [self presentSignViewController];
    }
}

- (void)presentSignViewController
{
    WelcomeViewController *viewController = [[WelcomeViewController alloc] init];
    SignNavController *customNavigationController = [[SignNavController alloc] initWithRootViewController:viewController];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:customNavigationController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:customNavigationController];
     }];
    
}

- (void)presentDeveloperViewController{
    ResetPasswordViewController *viewController = [[ResetPasswordViewController alloc] init];
    SignNavController *customNavigationController = [[SignNavController alloc] initWithRootViewController:viewController];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:customNavigationController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:customNavigationController];
     }];
    
}

- (void)presentHomeViewController{
    
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(presentHome) userInfo:nil repeats:NO];
}

- (void)presentHome{
    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:homeViewController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
     }];
}

#pragma mark WebServiceManagerDelegate Methods

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    if (webServiceType == WebServiceTypeHomePage){
        if([[SessionManager shared] isLoggedIn])
            [self presentHomeViewController];
        else
            [self presentSignViewController];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    [self presentSignViewController];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    [self presentSignViewController];
}

@end
