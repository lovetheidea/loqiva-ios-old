//
//  PaymentCell.h
//  loqiva
//
//  Created by Manuel Manzanera on 5/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *arrow;
@property (nonatomic, strong) UIView *separatorView;

@end
