//
//  PaymentsViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "PaymentsViewController.h"
#import "NavCustomView.h"
#import "PaymentCell.h"
#import "PaymentType.h"
#import "AppContext.h"
#import "WebViewcontroller.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface PaymentsViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *paymentTypes;
@property (nonatomic, strong) UITableView *paymentsTableView;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, assign) BOOL isPaymentOpen;
@property (nonatomic, assign) BOOL isPaymentSelected;
@property (nonatomic, strong) UIButton *paymentButton;
@property (nonatomic, strong) PaymentType *paymentTypeSelected;

@end

@implementation PaymentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isPaymentOpen = FALSE;
    _isPaymentSelected = FALSE;
    _paymentTypes = [[[AppContext sharedInstance] paymentTypes] mutableCopy];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    
    [self.view addSubview:[self headerTitleView:NSLocalizedString(@"Use the app to make instant, easy payments on the go", nil)]];
    
    CGFloat descriptionSize = [UtilManager heightForText:NSLocalizedString(@"Select your chosen option from the drop down menu below to start that a payment process.", nil) havingWidth:(WIDTH - [UtilManager width:40.]) andFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], NAV_BAR_HEIGHT + [UtilManager height:170], WIDTH - [UtilManager width:40.], descriptionSize)];
    [descriptionLabel setTextAlignment:NSTextAlignmentLeft];
    [descriptionLabel setTextColor:[UIColor darkGrayColor]];
    [descriptionLabel setText:NSLocalizedString(@"Select your chosen option from the drop down menu below to start that a payment process.", nil)];
    [descriptionLabel setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [descriptionLabel setNumberOfLines:0];
    [descriptionLabel setAdjustsFontSizeToFitWidth:YES];
    
    [self.view addSubview:descriptionLabel];
    
    _separatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20], descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height + [UtilManager height:20], WIDTH - [UtilManager width:40], 1)];
    [_separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
    [self.view addSubview:_separatorView];
    
    _paymentsTableView = [[UITableView alloc] initWithFrame:CGRectMake(_separatorView.frame.origin.x, _separatorView.frame.origin.y + [UtilManager height:20], _separatorView.frame.size.width, [UtilManager height:50]) style:UITableViewStylePlain];
    [_paymentsTableView setDelegate:self];
    [_paymentsTableView setDataSource:self];
    [_paymentsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_paymentsTableView registerClass:[PaymentCell class] forCellReuseIdentifier:@"cell"];
    [_paymentsTableView setBackgroundColor:[UIColor whiteColor]];
    [[_paymentsTableView layer] setCornerRadius:16];
    [[_paymentsTableView layer] setBorderColor:GRAY_SEPARATOR_COLOR.CGColor];
    [[_paymentsTableView layer] setBorderWidth:1];
    [_paymentsTableView setBounces:FALSE];
    
    [self.view addSubview:_paymentsTableView];
    
    _paymentButton = [[UIButton alloc] initWithFrame:CGRectMake(0., _paymentsTableView.frame.origin.y + _paymentsTableView.frame.size.height + [UtilManager height:40], [UtilManager width:140], [UtilManager height:40])];
    [_paymentButton setImage:[UIImage imageNamed:@"btn-payment-unselected"] forState:UIControlStateNormal];
    [_paymentButton setCenter:CGPointMake(self.view.center.x, _paymentButton.center.y)];
    [_paymentButton setEnabled:FALSE];
    [_paymentButton addTarget:self action:@selector(paymentAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_paymentButton];
    
    [self setupHeaderForSection:CMSectionTypePayments];
}

- (UIView *)headerTitleView:(NSString *)title{
    UIView *headerTitleView = [[UIView alloc] initWithFrame:CGRectMake(0., NAV_BAR_HEIGHT+ [UtilManager height:20], WIDTH, [UtilManager height:140])];
    [headerTitleView setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], 0, headerTitleView.frame.size.width - 2 * [UtilManager width:20], [UtilManager height:130])];
    [titleLabel setNumberOfLines:0];
    [titleLabel setText:title];
    [titleLabel setTextAlignment:NSTextAlignmentLeft];
    [titleLabel setTextColor:[UIColor orangeButtonColor]];
    [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:34]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    [headerTitleView addSubview:titleLabel];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:139], WIDTH - [UtilManager width:40], 1)];
    [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
    [headerTitleView addSubview:separatorView];
    
    return headerTitleView;
}

- (UIView *)navigationView{
    NavCustomView *containerView = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT)];
    [[containerView icon] setImage:[UIImage imageNamed:@"icon-payments-orange"]];
    [[containerView iconLabel] setTextColor:[UIColor orangeButtonColor]];
    [[containerView iconLabel] setText:NSLocalizedString(@"Payments", nil)];
    [[containerView slideButton]setImage:[UIImage imageNamed:@"btn-slider-orange"]];
    [[containerView  notificationButtonImage] setImage:[UIImage imageNamed:@"btn-notification-orange"]];
    [[containerView separatorView] setBackgroundColor:[UIColor orangeButtonColor]];
    [[containerView separatorView] setAlpha:0.8];
    containerView.parentController = self;
    [containerView setBackgroundColor:[UIColor whiteColor]];
    
    return containerView;
}


- (void)paymentAction{
    WebViewController *webViewcontroller = [[WebViewController alloc] init];
    [webViewcontroller setUrlString:_paymentTypeSelected.paymentURL];
    [webViewcontroller setTitleBar:_paymentTypeSelected.paymentTypeTitle];
    [webViewcontroller setIsBackAvailable:TRUE];
    [self.navigationController pushViewController:webViewcontroller animated:YES];
}

#pragma mark UITableViewDataSource Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(!_isPaymentOpen)
        return 1;
    else
        return _paymentTypes.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return [UtilManager height:0];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [UtilManager height:50];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PaymentCell *cell = [_paymentsTableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    if(!_isPaymentOpen){
        if(_paymentTypeSelected)
            [[cell titleLabel] setText:_paymentTypeSelected.paymentTypeTitle];
        else{
            [[cell titleLabel] setText:NSLocalizedString(@"What do you want to pay?", nil)];
            //PaymentType *paymentType = [_paymentTypes objectAtIndex:indexPath.row];
            //[[cell titleLabel] setText:paymentType.paymentTypeTitle];
        }
        [[cell separatorView] setHidden:YES];
        [[cell arrow] setHidden:FALSE];
    }
    else{
        PaymentType *paymentType = [_paymentTypes objectAtIndex:indexPath.row];
        [[cell titleLabel] setText:paymentType.paymentTypeTitle];
        [[cell separatorView] setHidden:FALSE];
        [[cell arrow] setHidden:YES];
    }
    return cell;
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(![[SessionManager shared] user]){
        
        __weak PaymentsViewController * weakSelf = self;
        self.tempUserViewController = [[TemporaryUserViewController alloc] initWithDismissBlock:^{
            
            [weakSelf.tempUserViewController.view removeFromSuperview];
            
        } andCallToSignForFree:^{
            [weakSelf.tempUserViewController.view removeFromSuperview];
            [weakSelf presentSignForFreeController];
            
        }];
        
        [self.view addSubview:self.tempUserViewController.view];
        
        return;
    }
    
    PaymentType *paymentType = [_paymentTypes objectAtIndex:indexPath.row];
    _paymentTypeSelected = paymentType;
    if(!_isPaymentOpen)
    {
        _isPaymentOpen = TRUE;
        [_paymentsTableView setFrame:CGRectMake(_separatorView.frame.origin.x, _separatorView.frame.origin.y + [UtilManager height:20], _separatorView.frame.size.width, [UtilManager height:50] * _paymentTypes.count + 1)];
        ;
    } else {
        _isPaymentOpen = FALSE;
        [_paymentsTableView setFrame:CGRectMake(_separatorView.frame.origin.x, _separatorView.frame.origin.y + [UtilManager height:20], _separatorView.frame.size.width, [UtilManager height:50])];
        ;
        
        if(!_isPaymentSelected){
            _isPaymentSelected   = TRUE;
            [_paymentButton setEnabled:YES];
            [_paymentButton setImage:[UIImage imageNamed:@"btn-payment"] forState:UIControlStateNormal];
        }
    }
                      
    [_paymentButton setFrame:CGRectMake(_paymentButton.frame.origin.x, _paymentsTableView.frame.origin.y + _paymentsTableView.frame.size.height + [UtilManager height:40], _paymentButton.frame.size.width, _paymentButton.frame.size.height)];
    [_paymentsTableView reloadData];
}



@end
