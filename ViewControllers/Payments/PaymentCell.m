//
//  PaymentCell.m
//  loqiva
//
//  Created by Manuel Manzanera on 5/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "PaymentCell.h"

#import "AppConstants.h"
#import "AppContext.h"
#import "UtilManager.h"
#import "loqiva-Swift.h"

@implementation PaymentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:10], [UtilManager width:300], [UtilManager height:30])];
        [_titleLabel setTextColor:[UIColor grayColor]];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setText:@""];
        [_titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
        
        [self.contentView addSubview:_titleLabel];
        
        UIImage *arrowImage = [UIImage imageNamed:@"image-arrow-down"];
        _arrow = [[UIImageView alloc] initWithImage:arrowImage];
        [_arrow setCenter:CGPointMake(_titleLabel.frame.size.width, _titleLabel.center.y)];
        [self.contentView addSubview:_arrow];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., [UtilManager width:49], self.contentView.frame.size.width, 1)];
        [_separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [self.contentView addSubview:_separatorView];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
