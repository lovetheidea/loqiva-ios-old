//
//  WebViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 29/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "WebViewController.h"
#import "NavCustomView.h"
#import "MRProgress.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface WebViewController ()<UIWebViewDelegate>

@property (nonatomic, strong) UIWebView *webView;

@end

@implementation WebViewController

@synthesize title;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0.,NAV_BAR_HEIGHT - 5, WIDTH,HEIGHT - NAV_BAR_HEIGHT + 5)];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]]];
    [_webView setOpaque:FALSE];
    [_webView setContentMode:UIViewContentModeScaleAspectFill];
    [_webView setScalesPageToFit:YES];
    [[_webView scrollView] setMaximumZoomScale:4];
    [_webView setDelegate:self];
  
    [self.view addSubview:_webView];
    
    MRProgressOverlayView *progres = [MRProgressOverlayView showOverlayAddedTo:TOPWINDOW animated:YES];
    [progres setTintColor:[UIColor orangeAppColor]];
    [progres setTitleLabelText:NSLocalizedString(@"loading", nil)];

    if (self.isAlternative) {
        [self setupHeaderForSection:CMSectionTypeWebViewAlternative];
        [[self.headerNav iconLabel] setAttributedText:self.titleBar withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:23] andColor:[UIColor darkGrayColor] andKern:-0.71 withLineBreak:NO];
    }
    else {
        [self setupHeaderForSection:CMSectionTypeWebView];
        [[self.headerNav iconLabel] setAttributedText:self.titleBar withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:23] andColor:[UIColor whiteColor] andKern:-0.71 withLineBreak:NO];
    }

    [self.headerNav iconLabel].lineBreakMode = NSLineBreakByTruncatingTail;
}

#pragma mark UIWebViewDelegate Methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if(![[request.URL absoluteString]hasPrefix:@"http://"])
    {
        
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [MRProgressOverlayView dismissOverlayForView:TOPWINDOW animated:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:( NSError *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [MRProgressOverlayView dismissOverlayForView:TOPWINDOW animated:YES];
     NSLog(@"%@",webView.request.URL.absoluteString);
}

@end
