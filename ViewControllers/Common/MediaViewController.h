//
//  MediaViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 30/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ParentViewController.h"

@interface MediaViewController : ParentViewController{
    MPMoviePlayerController *player;
}

@property (nonatomic, strong) NSString *mediaString;
@property (nonatomic, strong) MPMoviePlayerController *player;

@end
