//
//  WebViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 29/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ParentViewController.h"

@interface WebViewController : ParentViewController

@property (nonatomic, assign) BOOL isAlternative;
@property (nonatomic, strong) NSString *urlString;

@property (nonatomic, assign) BOOL isBackAvailable;

@end
