//
//  MediaViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 30/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "MediaViewController.h"
#import "loqiva-Swift.h"

@interface MediaViewController ()<MPMediaPickerControllerDelegate>

@end

@implementation MediaViewController

@synthesize player;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:_mediaString ofType:@"mp4"]];
    
    
    MPMoviePlayerController *mp = [[MPMoviePlayerController alloc] initWithContentURL:url];
    
    mp.controlStyle = MPMovieControlStyleFullscreen;
    mp.repeatMode = MPMovieRepeatModeNone;
    mp.view.frame = CGRectMake(0.0, 0.0, self.view.bounds.size.width, self.view.bounds.size.height);
    self.player = mp;
    [[self player] setMovieSourceType:MPMovieSourceTypeFile];
    [[self player] setControlStyle:MPMovieControlStyleFullscreen];
    [self.view addSubview:self.player.view];
    [self.player prepareToPlay];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(movieFinishedCallback:)
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doneButtonClick:)
                                                 name:MPMoviePlayerWillExitFullscreenNotification
                                               object:nil];
    
    self.player.backgroundView.hidden = YES;
    
    [self.player play];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) movieFinishedCallback:(NSNotification*) notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)doneButtonClick:(NSNotification*)aNotification{
}

#pragma mark MPMediaPickerControllerDelegate

- (void)mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection
{
    
}

- (void)mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker
{
    
}

@end
