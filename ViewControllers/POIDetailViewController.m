//
//  POIDetailViewController.m
//  loqiva
//
//  Created by Matt Harding on 01/08/2018.
//  Copyright © 2018 inup. All rights reserved.
//

#import "POIDetailViewController.h"
#import "CustomAnnotation.h"
#import "CustomAnnotation.h"
#import "CustomPinAnnotationView.h"

@interface POIDetailViewController ()
@end

@implementation POIDetailViewController

-(void)addMap {
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0., _summaryLabel.frame.origin.y + _summaryLabel.frame.size.height + [UtilManager height:30], WIDTH, [UtilManager height:180])];
    [self.mapView setMapType:MKMapTypeStandard];
    [self.mapView setDelegate:self];
    [self.mapView setShowsCompass:FALSE];
    [self.mapView setShowsTraffic:FALSE];
    [self.mapView setShowsBuildings:YES];
    [self.mapView setShowsUserLocation:YES];
    [self.mapView setShowsPointsOfInterest:YES];
    
    if (self.whiteView) {
        [self.whiteView addSubview:self.mapView];
    }
    else {
        [self.scrollView addSubview:self.mapView];
    }
}

#pragma mark MKMapView Delegate Methods

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView {
    [self zoomToFitMapAnnotations];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation NS_AVAILABLE(10_9, 4_0) {
    if (!self.userIsLocated) {
        [self zoomToFitMapAnnotations];
    }
}

-(void)zoomToFitMapAnnotations
{
    if([self.mapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    NSArray *array = [self.mapView annotations];
    
    for(CustomAnnotation* annotation in array)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
        
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.5; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.5; // Add a little extra space on the sides
    
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *identifier = @"pinAnnotation";
    if ([annotation isKindOfClass:[CustomAnnotation  class]])
    {
        CustomPinAnnotationView *annotationView = (CustomPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if(!annotationView)
        {
            annotationView = [[CustomPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        }
        
        annotationView.enabled = YES;
        [annotationView setHidden:NO];
        [annotationView setDraggable:NO];
        [annotationView setSelected:YES];
        [annotationView setExclusiveTouch:NO];
        [annotationView setMultipleTouchEnabled:NO];
        
        return annotationView;
    }else{
        return [self.mapView viewForAnnotation:self.mapView.userLocation];
    }
}

#pragma mark User Location

-(void) createLocationManager {
    
    self.manager = [[CLLocationManager alloc] init];
    self.manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    self.manager.distanceFilter = kCLDistanceFilterNone;
    [self.manager startUpdatingLocation];
    [self.manager requestWhenInUseAuthorization];
}

@end
