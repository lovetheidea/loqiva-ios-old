//
//  ScheduleMonthViewController.m
//  loqiva
//  Template for a month view
//
//  Created by juan on 29/11/2016.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ScheduleMonthViewController.h"
#import "ScheduleDetailViewController.h"
#import "WebServiceManager.h"
#import "LocationManager.h"
#import "ScheduleWeekStripeView.h"
#import "ScheduleMonthView.h"
#import "ScheduleCell.h"
#import "LocationManager.h"
#import "UILabel+Extra.h"
#import "NSDate+Extra.h"
#import "NSString+HTML.h"
#import "Event.h"
#import "TemporaryUserViewController.h"
#import "Utility.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface ScheduleMonthViewController ()<WebServiceManagerDelegate, UITableViewDataSource, UITableViewDelegate>


@property (nonatomic) NSDate * dateSelected;
@property (nonatomic, strong) JTCalendarDayView *todaysDayView;
@property (nonatomic, strong) UITableView *scheduleTableView;
@property (nonatomic, strong) NSArray *dayEvents;
@property (nonatomic, strong) UILabel *eventLabel;
@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) ScheduleWeekStripeView * weekstripe;

@property (nonatomic) BOOL isFirst;

-(void)updateCurrentDayLabelWithDate:(NSDate*)date;

@end

@implementation ScheduleMonthViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    _isFirst = YES;
    
    [WebServiceManager getEventsWithLatitude:[NSNumber numberWithDouble:[LocationManager sharedInstance].currentLocation.coordinate.latitude] andLongitude:[NSNumber numberWithDouble:[LocationManager sharedInstance].currentLocation.coordinate.longitude] andDelegate:self];
}

-(UIView*)currentDayViewStripe {
    
    if(!_currentDaySelectedLbl && !_currentDayView){
        
        _currentDayView = [[UIView alloc] initWithFrame:CGRectMake(0, MONT_CALENDAR_HEIGHT + NAV_BAR_HEIGHT, WIDTH, [UtilManager width:50])];
        
        UIView *topSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _currentDayView.frame.size.width, 0.5)];
        [topSeparator setBackgroundColor:[UIColor lightGrayColor]];
        [_currentDayView addSubview:topSeparator];
        
        UIView *bottomSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, _currentDayView.frame.size.height - 0.5, _currentDayView.frame.size.width, 0.5)];
        [bottomSeparator setBackgroundColor:[UIColor lightGrayColor]];
        [_currentDayView addSubview:bottomSeparator];
        
        _currentDaySelectedLbl = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:15], -1, WIDTH, _currentDayView.frame.size.height)];
        [_currentDaySelectedLbl setAdjustsFontSizeToFitWidth:NO];
        [_currentDayView setBackgroundColor:GRAY_BACKGROUND_COLOR];
        [_currentDayView addSubview:_currentDaySelectedLbl];
        
        
    }
    
    return _currentDayView;
}

-(void)updateCurrentDayLabelWithDate:(NSDate *)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_FORMATTER_FULL];
    NSString *dateString = [dateFormatter stringFromDate:date];
    [_currentDaySelectedLbl setAttributedText:[Utility convertHtmlToPlainText: dateString] withLineHeight:CONTACT_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:SEMIBOLD_FONT size:CONTACT_FONT_SIZE] andColor:GRAY_DARK_TEXT_COLOR andKern:CONTACT_FONT_KERN withLineBreak:NO];
}




- (void)configureView {
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_FORMATTER];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    _selectedDate = [NSDate date];
    _dayEvents = [Event eventInDate:[dateFormatter dateFromString:dateString]];
    
    //Current day stripe
    [self.view addSubview:[self currentDayViewStripe]];
    
    //Month calendar
    if(!_calendarContentView){
        _calendarContentView = [[JTVerticalCalendarView alloc] initWithFrame:CGRectMake(0, NAV_BAR_HEIGHT, WIDTH, MONT_CALENDAR_HEIGHT)];
        _calendarMenuView.contentRatio = .75;
        _calendarManager.settings.weekDayFormat = JTCalendarWeekDayFormatSingle;
        [_calendarManager setMenuView:_calendarMenuView];
        [_calendarManager setContentView:_calendarContentView];
        [_calendarManager setDate:[NSDate date]];
        
        [_calendarContentView setScrollsToTop:NO];
        
        [self.view addSubview:_calendarContentView];
    }
    
    //Week stripe
    if(!_weekstripe){
        _weekstripe = [[ScheduleWeekStripeView alloc] initWithFrame:CGRectMake(0, NAV_BAR_HEIGHT, WIDTH, 20)];
        [self.view addSubview:_weekstripe];
    }
    

    
    //Table view
    if(!_scheduleTableView){
        _scheduleTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., _currentDayView.frame.origin.y + _currentDayView.frame.size.height, WIDTH, [UtilManager height:288]) style:UITableViewStylePlain];
        
        [_scheduleTableView setDelegate:self];
        [_scheduleTableView setDataSource:self];
        [_scheduleTableView registerClass:[ScheduleCell class] forCellReuseIdentifier:@"scheduleCell"];
        [_scheduleTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"scheduleInfoCell"];
        [_scheduleTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"firstCell"];
        [_scheduleTableView setBounces:NO];
        [_scheduleTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
        [self.view addSubview:_scheduleTableView];
    }
    
    
    [self setupHeaderForSection:CMSectionTypeScheduleMonth];
    
    //Current month view
    [self updateCurrentDayLabelWithDate:[NSDate date]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    [self configureView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(_dayEvents.count > 0)
    {
        return _dayEvents.count;
    }else{
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if(_dayEvents.count > 0)
    {
        ScheduleCell *cell = [_scheduleTableView dequeueReusableCellWithIdentifier:@"scheduleCell"];
        
        Event *currentEvent = [_dayEvents objectAtIndex:indexPath.row];
        
        [cell.titleLabel setAttributedText:[Utility convertHtmlToPlainText: currentEvent.title] withLineHeight:CONTACT_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:REGULAR_FONT size:16] andColor:GRAY_DARK_TEXT_COLOR andKern:CONTACT_FONT_KERN withLineBreak:YES];
        
        [cell.titleLabel setBackgroundColor:TEST_BG_TWO];
        
        
        NSString *addressString = [currentEvent.address stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        [cell.subtitleLabel setAttributedText:[Utility convertHtmlToPlainText: addressString] withLineHeight:CONTACT_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:REGULAR_FONT size:CONTACT_FONT_SIZE] andColor:GRAY_TEXT_COLOR andKern:CONTACT_FONT_KERN withLineBreak:YES];
        
        [cell.subtitleLabel setBackgroundColor:TEST_BG_ONE];

        
        
        // Distance
        CLLocation *location = [[CLLocation alloc] initWithLatitude:currentEvent.latitude.doubleValue longitude:currentEvent.longitude.doubleValue];
        [cell.distanceLabel setAttributedText:[[NSString stringWithFormat:@"%.2f", [LocationManager getDistanceFrom:location]] distanceString] withLineHeight:CONTACT_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:REGULAR_FONT size:10] andColor:GRAY_TEXT_COLOR andKern:CONTACT_FONT_KERN withLineBreak:NO];
        
        [cell.distanceLabel setBackgroundColor:TEST_BG_TWO];

        
        // Time
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm a"];
        NSString *dateString = [dateFormatter stringFromDate:currentEvent.startDate];
        NSArray* words = [dateString componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        dateString = [words componentsJoinedByString:@""];
        [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        [cell.hourLabel setAttributedText:dateString withLineHeight:CONTACT_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:SEMIBOLD_FONT size:14] andColor:GRAY_DARK_TEXT_COLOR andKern:CONTACT_FONT_KERN withLineBreak:NO];
        
        [cell.hourLabel setBackgroundColor:TEST_BG_ONE];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    else {
        
        UITableViewCell *cell = [_scheduleTableView dequeueReusableCellWithIdentifier:@"scheduleInfoCell"];
        
        [cell.textLabel setAttributedText:NSLocalizedString(@"Sorry, there are no events scheduled for today", nil) withLineHeight:34 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:30] andColor:[UIColor EventMessageNoEventsColour] andKern:TITLE_FONT_KERN withLineBreak:NO];
        [cell.textLabel setBackgroundColor:[UIColor whiteColor]];
        [cell.textLabel setNumberOfLines:0];
        [cell.textLabel setFrame:cell.contentView.frame];
        [cell.textLabel setBackgroundColor:TEST_BG_ONE];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0)
    {
        if(_dayEvents.count)
        {
            [_scheduleTableView deselectRowAtIndexPath:indexPath animated:YES];
            
            ScheduleDetailViewController *scheduleDetailViewController = [[ScheduleDetailViewController alloc] init];
            Event *currentEvent = [_dayEvents objectAtIndex:indexPath.row];
            [scheduleDetailViewController setCurrentEvent:currentEvent];

            [self.navigationController pushViewController:scheduleDetailViewController animated:YES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(_dayEvents.count>0)
    return [UtilManager height:75.];
    else
    return [UtilManager height:260.];
    
}



#pragma mark - CalendarManager delegate
- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar{
    [self updateCurrentDayLabelWithDate:[calendar date]];
}
- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar{
    [self updateCurrentDayLabelWithDate:[calendar date]];
}
// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    dayView.hidden = NO;
    BOOL isToday = NO;

    
    // Other month
    if([dayView isFromAnotherMonth]){
        dayView.hidden = YES;
    }
    // Today
    else if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        
        isToday =YES;
        
        // Selected date background colour
        dayView.circleView.backgroundColor = [UIColor grayColor] ;
        

        dayView.textLabel.textColor = [UIColor whiteColor];
        _todaysDayView = dayView;
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor EventDateSelectorBGColour];
        
        dayView.textLabel.textColor = [UIColor whiteColor];
        return;
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
    
    }
    
    if([self haveEventForDay:dayView.date]){
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    else{
       dayView.textLabel.textColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.00];
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView {
    
    if(![[SessionManager shared] user]){
        
        __weak ScheduleMonthViewController * weakSelf = self;
        self.tempUserViewController = [[TemporaryUserViewController alloc] initWithDismissBlock:^{
            [weakSelf.tempUserViewController.view removeFromSuperview];
            
            
        } andCallToSignForFree:^{
            [weakSelf.tempUserViewController.view removeFromSuperview];
            [weakSelf presentSignForFreeController];
            
        }];
        
        [self.view addSubview:self.tempUserViewController.view];
        
        return;
    }
    
    _dateSelected = dayView.date;
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                    } completion:nil];
    
    
    // Don't change page in week mode because block the selection of days in first and last weeks of the month
    if(_calendarManager.settings.weekModeEnabled){
        return;
    }
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
    
    dayView.textLabel.textColor = [UIColor whiteColor];
    dayView.circleView.backgroundColor = [UIColor EventDateSelectorBGColour];
    
    _dayEvents = [Event eventInDate:dayView.date];
    
    [_scheduleTableView reloadData];
    
    [self updateCurrentDayLabelWithDate:dayView.date];
}

#pragma mark - Views customization



- (void)calendar:(JTCalendarManager *)calendar prepareMenuItemView:(UILabel *)menuItemView date:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"MMMM yyyy";
        
        dateFormatter.locale = _calendarManager.dateHelper.calendar.locale;
        dateFormatter.timeZone = _calendarManager.dateHelper.calendar.timeZone;
    }
    
    menuItemView.text = [dateFormatter stringFromDate:date];
}



- (UIView<JTCalendarDay> *)calendarBuildDayView:(JTCalendarManager *)calendar
{
    JTCalendarDayView *view = [JTCalendarDayView new];
    
    view.textLabel.font = [UIFont fontWithName:REGULAR_FONT size:13];
    CGRect textFrame = view.textLabel.frame;
    textFrame.origin.y =-5;
    view.textLabel.frame=textFrame;
    
    view.circleRatio = 1;
    
    
    return view;
}


#pragma mark WebServiceMethods Delegate

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    if (webServiceType == WebServiceTypeEvents) {
        _dayEvents = [[[AppContext sharedInstance] events] copy];
        [self.calendarManager reload];
        [self.scheduleTableView reloadData];
        [self calendar:nil didTouchDayView:self.todaysDayView];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
}



#pragma mark - Fake data



- (BOOL)haveEventForDay:(NSDate *)date
{

    
    if([Event eventInDate:date].count>0){
        return YES;
    }
    
    
    
    return NO;
    
}


@end
