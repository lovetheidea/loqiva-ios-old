//
//  ScheduleWeekStripeView.m
//  loqiva
//
//  Created by juan on 29/11/2016.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ScheduleWeekStripeView.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "loqiva-Swift.h"

@implementation ScheduleWeekStripeView

-(id)initWithFrame:(CGRect)frame{
    if(self=[super initWithFrame:frame]){
        
        [self setBackgroundColor:[UIColor colorWithRed:0.961 green:0.961 blue:0.961 alpha:1.00]];
        
        NSArray * daysText = [[NSArray alloc] initWithObjects:@"M",@"T",@"W",@"T",@"F",@"S",@"S", nil];
        
        CGFloat cellsize = frame.size.width/7;
        
        float posX = 0;
        
        for (NSString * dayText in daysText) {
            
            UILabel * dayLabl  = [[UILabel alloc] initWithFrame:CGRectMake(posX, 0, cellsize, frame.size.height)];
            [dayLabl setTextAlignment:NSTextAlignmentCenter];
            [dayLabl setText:dayText];
            [dayLabl setTextColor:[UIColor blackColor]];
            [dayLabl setFont:[UIFont fontWithName:SEMIBOLD_FONT size:10]];
            [dayLabl setAdjustsFontSizeToFitWidth:NO];
            
            posX += cellsize;
            
            [self addSubview:dayLabl];
        }
        
        UIView *separatorLine = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height -1, WIDTH, 1)];
        separatorLine.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.80 alpha:1.00];
        [self addSubview:separatorLine];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
