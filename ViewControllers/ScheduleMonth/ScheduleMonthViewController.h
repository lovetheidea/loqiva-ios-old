//
//  ScheduleMonthViewController.h
//  loqiva
//
//  Created by juan on 29/11/2016.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JTCalendar/JTCalendar.h>


#import "ParentViewController.h"
@interface ScheduleMonthViewController : ParentViewController<JTCalendarDelegate>

@property (strong, nonatomic) JTVerticalCalendarView *calendarContentView;
@property (strong, nonatomic) JTCalendarMenuView *calendarMenuView;

@property (strong, nonatomic) JTCalendarManager *calendarManager;

@property(strong, nonatomic) UILabel * currentDaySelectedLbl;
@property(strong, nonatomic) UIView * currentDayView;

@end
