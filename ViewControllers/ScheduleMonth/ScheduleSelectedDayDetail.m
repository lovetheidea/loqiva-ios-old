//
//  ScheduleSelectedDayDetail.m
//  loqiva
//
//  Created by juan on 29/11/2016.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ScheduleSelectedDayDetail.h"
#import "loqiva-Swift.h"

@implementation ScheduleSelectedDayDetail

-(id)initWithFrame:(CGRect)frame{
    if(self=[super initWithFrame:frame]){
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        
        [self addSubview:_tableView];
        
    }
    return self;
}





@end
