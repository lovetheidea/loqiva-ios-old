//
//  NotificationCell.m
//  loqiva
//
//  Created by Manuel Manzanera on 16/11/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "NotificationCell.h"

#import "UtilManager.h"
#import "AppContext.h"
#import "AppConstants.h"

#import "loqiva-Swift.h"

@implementation NotificationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        [self setBackgroundColor:[UIColor clearColor]];
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 16, WIDTH - 2 * [UtilManager width:20], [UtilManager height:33])];
        [_titleLabel setNumberOfLines:2];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
        [_titleLabel setTextColor:[UIColor whiteColor]];
        
        [self.contentView addSubview:_titleLabel];
        
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height + 8, 0, [UtilManager height:20])];
        [_dateLabel setTextAlignment:NSTextAlignmentLeft];
        [_dateLabel setTextColor:[UIColor whiteColor]];
        [_dateLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:10]];
        [self.contentView addSubview:_dateLabel];
        
        _descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, self.dateLabel.frame.origin.y + self.dateLabel.frame.size.height + 12, 0, 100)];
        [_descriptionLabel setTextColor:[UIColor whiteColor]];
        [_descriptionLabel setTextAlignment:NSTextAlignmentLeft];
        [_descriptionLabel setFont:[UIFont fontWithName:REGULAR_FONT size:14]];
        [_descriptionLabel setNumberOfLines:3];
        [self.contentView addSubview:_descriptionLabel];
        
        _deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, 0, 40, 40)];
        [_deleteButton setBackgroundImage:[UIImage imageNamed:@"deleteIcon"] forState:UIControlStateNormal];
        [_deleteButton addTarget:self action:@selector(deleteAction) forControlEvents:UIControlEventTouchUpInside];
        [[_deleteButton titleLabel] setFont:[UIFont fontWithName:REGULAR_FONT size:13]];
        
        [self.contentView addSubview:_deleteButton];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, 0, _descriptionLabel.frame.size.width, 1)];
        [_separatorView setBackgroundColor:HEADER_SEPARATOR_COLOR];
        [self.contentView addSubview:_separatorView];

//        [_titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
//        [_dateLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
//        [_descriptionLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
//        [_deleteButton setTranslatesAutoresizingMaskIntoConstraints:NO];
//        [_separatorView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
//        NSDictionary *views = NSDictionaryOfVariableBindings(_titleLabel, _dateLabel, _descriptionLabel, _deleteButton, _separatorView);
//        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-16-[_titleLabel]-8-[_dateLabel]-12-[_descriptionLabel]-16-[_deleteButton(46)]-50-[_separatorView(1)]" options:0 metrics:nil views:views]];
//        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_titleLabel]-20-|" options:0 metrics:nil views:views]];
//        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_dateLabel]-20-|" options:0 metrics:nil views:views]];
//        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_descriptionLabel]-20-|" options:0 metrics:nil views:views]];
//        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_deleteButton(46)]|" options:0 metrics:nil views:views]];
//        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_separatorView]|" options:0 metrics:nil views:views]];
    }
    return self;
}

-(void)relayoutBtns:(NSArray *)titles {
    self.titlesArray = titles;
    
    [self removeButtons];
    
    NSString *title = self.titleLabel.text;
    NSString *desc = self.descriptionLabel.text;
    
    NSInteger height = 0;
    NSInteger descHeight = 0;
    
    if (desc.length > 90) {
        descHeight = 60;
    }
    else if (desc.length > 45) {
        descHeight = 40;
    }
    else {
        descHeight = 20;
    }
    
    if (title.length > 32) {
        height = 70;
    }
    else {
        height = 40;
    }

    self.titleLabel.frame = CGRectMake(20, 16, WIDTH - 2 * [UtilManager width:20], [UtilManager height:height]);
    self.dateLabel.frame = CGRectMake(_titleLabel.frame.origin.x, self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height + 8, WIDTH - 2 * [UtilManager width:20], [UtilManager height:20]);
    self.descriptionLabel.frame = CGRectMake(_titleLabel.frame.origin.x, self.dateLabel.frame.origin.y + self.dateLabel.frame.size.height + 12, WIDTH - 2 * [UtilManager width:20], descHeight);
    self.deleteButton.frame = CGRectMake(_titleLabel.frame.origin.x, self.descriptionLabel.frame.origin.y + self.descriptionLabel.frame.size.height + 20, 46, 46);
    
    self.titleLabel.numberOfLines = 2;
    
    
    UIButton *lastButton = _deleteButton;
    NSInteger xPosition = 20;
    
    if (_deleteButton.frame.origin.y == 0) {
        return;
    }
    
    for (NSInteger i = 0; i < titles.count; i++) {
        UIButton *viewButton = [[UIButton alloc] initWithFrame:CGRectMake(xPosition, lastButton.frame.origin.y, _deleteButton.frame.size.width, _deleteButton.frame.size.height)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xPosition, lastButton.frame.origin.y + 45, _deleteButton.frame.size.width, 30)];
        [label setNumberOfLines:2];
        [label setTextColor:[UIColor whiteColor]];
        [label setFont:[UIFont fontWithName:SEMIBOLD_FONT size:9]];
        [label setTextAlignment:NSTextAlignmentCenter];
    
        UIImage *buttonImage = [UIImage new];
        NSString *title = titles[i];
        if ([title containsString:@"Survey"]) {
            buttonImage = [UIImage imageNamed:@"surveyIcon"];
            label.text = @"View Survey";
        }
        else if ([title containsString:@"Document"]) {
            buttonImage = [UIImage imageNamed:@"documentIcon"];
            label.text = @"Read Document";
        }
        else if ([title containsString:@"Map"]) {
            buttonImage = [UIImage imageNamed:@"mapIcon"];
            label.text = @"View \nMap Item";
        }
        else if ([title containsString:@"Weblink"] || [title containsString:@"Webpage"]) {
            buttonImage = [UIImage imageNamed:@"webpageIcon"];
            label.text = @"Visit Webpage";
        }
        else if ([title containsString:@"Reward"]) {
            buttonImage = [UIImage imageNamed:@"rewardIcon"];
            label.text = @"View Reward";
        }
        
        [viewButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
        [viewButton addTarget:self action:@selector(ticketAction:) forControlEvents:UIControlEventTouchUpInside];
        [viewButton setTag:100 + i];
        [label setTag:1000 + i];
        [self.contentView addSubview:label];
        [self.contentView addSubview:viewButton];
        
        xPosition = lastButton.frame.size.width + xPosition + 13;
    }
    
    [_deleteButton setTranslatesAutoresizingMaskIntoConstraints:YES];
    _deleteButton.frame = CGRectMake(xPosition, _deleteButton.frame.origin.y, _deleteButton.frame.size.width, _deleteButton.frame.size.height);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xPosition - 3, lastButton.frame.origin.y + 45, _deleteButton.frame.size.width + 6, 30)];
    [label setNumberOfLines:2];
    [label setTextColor:[UIColor whiteColor]];
    [label setFont:[UIFont fontWithName:SEMIBOLD_FONT size:9]];
    [label setTextAlignment:NSTextAlignmentCenter];
    label.text = @"Delete Notification";
    [label setTag:1004];
    [self.contentView addSubview:label];
    
    self.separatorView.frame = CGRectMake(_titleLabel.frame.origin.x, label.frame.origin.y + label.frame.size.height + 12, _descriptionLabel.frame.size.width, 1);
}

- (void)deleteAction {
    if([_delegate respondsToSelector:@selector(deleteActionWithIndex:)]) {
        [_delegate deleteActionWithIndex:self.currentIndex];
    }
}

- (void)ticketAction:(UIButton *)sender {
    if([_delegate respondsToSelector:@selector(ticketActionWithIndex:withTitle:)]) {
        NSInteger tag = sender.tag - 100;
        NSString *title = _titlesArray[tag];
        [_delegate ticketActionWithIndex:self.currentIndex withTitle:title];
    }
}

- (void)removeButtons {
    NSArray *array = @[@100, @101, @102, @103, @104, @1000, @1001, @1002, @1003, @1004];
    for (NSNumber *number in array) {
        UIView *view = [self.contentView viewWithTag:number.integerValue];
        if (view) {
            [view removeFromSuperview];
        }
    }
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self removeButtons];
}

@end
