//
//  NotificationViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 25/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "NotificationViewController.h"
#import "NSDate+Extra.h"
#import "NotificationCell.h"
#import "WebServiceManager.h"
#import "CustomNotification.h"
#import "Notification.h"
#import "UIColor+Helper.h"
#import "WebViewController.h"
#import "loqiva-Swift.h"

@interface NotificationViewController ()<UITableViewDelegate, UITableViewDataSource, NotificationCellDelegate, WebServiceManagerDelegate>

@property (nonatomic, strong) UITableView *notificationTableView;
@property (nonatomic, strong) NSMutableArray *notifications;
@property (nonatomic, strong) UILabel *numberOfNotificationsLabel;
@property (nonatomic, strong) NSMutableArray *customNotifications;

-(void)updateNotificationHeaderLabel;


@end

@implementation NotificationViewController

static int HeaderViewHeigth = 61;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _notifications = [[NSMutableArray alloc] initWithArray:[[AppContext sharedInstance] notifications]];
    self.customNotifications = [[CustomNotification getCustomNotifications] mutableCopy];
    [_notifications addObjectsFromArray:self.customNotifications];
    [self updateNotificationHeaderLabel];
    
    
    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [WebServiceManager getNotificationsDelegate:self];
    [_notificationTableView reloadData];
}

- (void)configureView {
    [self.view setBackgroundColor:[UIColor orangeAppColor]];
    
    _notificationTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., NAV_BAR_HEIGHT + HeaderViewHeigth + 1, WIDTH, self.view.frame.size.height - NAV_BAR_HEIGHT - HeaderViewHeigth) style:UITableViewStylePlain];
    
    [_notificationTableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [_notificationTableView setBackgroundColor:[UIColor clearColor]];
    [_notificationTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_notificationTableView registerClass:[NotificationCell class] forCellReuseIdentifier:@"cell"];
    [_notificationTableView setDelegate:self];
    [_notificationTableView setDataSource:self];
    
    [self.view addSubview:_notificationTableView];
    [self.view addSubview:[self navigationView]];
    [self.view addSubview:[self headerView]];
    
}

-(void)updateNotificationHeaderLabel{
    
    [_numberOfNotificationsLabel setTextAlignment:NSTextAlignmentLeft];
    
    if(_notifications.count == 0)
    [_numberOfNotificationsLabel setAttributedText:[NSString stringWithFormat:@"You have 0 notifications"] withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:26] andColor:[UIColor whiteColor] andKern:-0.5 withLineBreak:NO];
    else if (_notifications.count == 1)
    [_numberOfNotificationsLabel setAttributedText:[NSString stringWithFormat:@"You have 1 notification"] withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:26] andColor:[UIColor whiteColor] andKern:-0.5 withLineBreak:NO];
    else
    [_numberOfNotificationsLabel setAttributedText:[NSString stringWithFormat:@"You have %lu notifications",(unsigned long)_notifications.count] withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:26] andColor:[UIColor whiteColor] andKern:-0.5 withLineBreak:NO];
    
}
- (UIView *)navigationView{
    
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT)];
    
    [containerView setBackgroundColor:[UIColor orangeAppColor]];
    
    UIImageView *_icon = [[UIImageView alloc] initWithFrame:CGRectMake(NAV_ICON_ORIGIN_X, NAV_ICON_ORIGIN_Y, NAV_ICON_WIDTH, NAV_ICON_HEIGHT)];
    [_icon setBackgroundColor:[UIColor clearColor]];
    [_icon setContentMode:UIViewContentModeCenter];
    [_icon setImage:[UIImage imageNamed:@"icon-notification"]];
    [containerView addSubview:_icon];
    
    UILabel *notificationLabel = [[UILabel alloc] initWithFrame:CGRectMake(_icon.frame.origin.x + _icon.frame.size.width - [UtilManager width:14], _icon.frame.origin.y - [UtilManager width:14], [UtilManager width:14], [UtilManager width:14])];
    [[notificationLabel layer] setCornerRadius:notificationLabel.frame.size.width/2];
    [notificationLabel setBackgroundColor:[UtilManager colorwithHexString:@"B0021B" alpha:1.]];
    [notificationLabel setAttributedText:@"1" withLineHeight:14 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:REGULAR_FONT size:10] andColor:[UIColor whiteColor] andKern:-0.33 withLineBreak:NO];
    
    //[containerView addSubview:notificationLabel];
    
    UILabel *_iconLabel = [[UILabel alloc] initWithFrame:CGRectMake(_icon.frame.origin.x + _icon.frame.size.width + [UtilManager width:18], _icon.frame.origin.y, [UtilManager width:180.], [UtilManager height:30])];
    [_iconLabel setCenter:CGPointMake(_iconLabel.center.x, _icon.center.y)];
    [_iconLabel setFont:[UIFont fontWithName:LIGHT_FONT size:20]];
    [_iconLabel setTextColor:[UIColor whiteColor]];
    [_iconLabel setTextAlignment:NSTextAlignmentLeft];
    [_iconLabel setText:NSLocalizedString(@"Notifications", nil)];
    
    [containerView addSubview:_iconLabel];
    
    UIImage *closeImage = [UIImage imageNamed:@"btn-close"];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(NAV_CLOSE_ORIGIN_X, NAV_CLOSE_ORIGIN_Y,NAV_CLOSE_WIDTH, NAV_CLOSE_HEIGHT)];
    [backButton setImage:closeImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchDown];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., NAV_BAR_HEIGHT - 1, WIDTH, 1)];
    [separatorView setBackgroundColor:HEADER_SEPARATOR_COLOR];
    [containerView addSubview:separatorView];
    
    [containerView addSubview:backButton];
    
    return containerView;
}

- (UIView *)headerView{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., NAV_BAR_HEIGHT, WIDTH, HeaderViewHeigth)];
    [headerView setBackgroundColor:[UIColor orangeAppColor]];
    
    _numberOfNotificationsLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20],[UtilManager width:10], WIDTH - [UtilManager width:40], [UtilManager height:50])];

    [self updateNotificationHeaderLabel];
    
    [headerView addSubview:_numberOfNotificationsLabel];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(_numberOfNotificationsLabel.frame.origin.x, HeaderViewHeigth - 1, WIDTH - 2 * _numberOfNotificationsLabel.frame.origin.x, 1)];
    [separatorView setBackgroundColor:HEADER_SEPARATOR_COLOR];
    [headerView addSubview:separatorView];
    
    return headerView;
}

- (void)slideAction{
    [[NSNotificationCenter defaultCenter] postNotificationName:SLIDE_NOTIFICATION object:self];
}


- (void)dismissAction{
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame = CGRectMake(0, -self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        self.view.alpha = 0;
        [self dismissViewControllerAnimated:NO completion:^{
            AppDelegate *cont = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [cont.windowImageView setImage: nil];
        }];
    }];
}

#pragma mark UITableViewDelegate Methods

#pragma mark UITableViewDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _notifications.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = 250;
    
    id object = [_notifications objectAtIndex:indexPath.section];
    if ([object isKindOfClass:[CustomNotification class]]) {
        CustomNotification *customNotification = [_notifications objectAtIndex:indexPath.section];
        NSString *title = customNotification.title;
        NSString *desc = customNotification.summary;
        NSInteger addedOnHeight = 0;
        
        if (desc.length > 90) {
            addedOnHeight += 50;
        }
        else if (desc.length > 45) {
            addedOnHeight += 25;
        }
        
        if (title.length > 32) {
            addedOnHeight += 30;
        }
        
        return [UtilManager height:height + addedOnHeight];
    }
    
    Notification *customNotification = [_notifications objectAtIndex:indexPath.section];
    NSString *title = customNotification.notificationHeader;
    NSString *desc = customNotification.notificationBody;
    NSInteger addedOnHeight = 0;
    
    if (desc.length > 90) {
        addedOnHeight += 50;
    }
    else if (desc.length > 45) {
        addedOnHeight += 25;
    }
    
    if (title.length > 32) {
        addedOnHeight += 30;
    }
    
    return [UtilManager height:height + addedOnHeight];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NotificationCell *notificationCell = [_notificationTableView dequeueReusableCellWithIdentifier:@"cell"];
    
    [notificationCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [notificationCell setCurrentIndex:indexPath.section];
    [notificationCell setDelegate:self];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_FORMATTER_NOTIFICATION];
    
    NSMutableArray *array = [@[] mutableCopy];
    
    id object = [_notifications objectAtIndex:indexPath.section];
    if ([object isKindOfClass:[CustomNotification class]]) {
        CustomNotification *customNotification = [_notifications objectAtIndex:indexPath.section];
        
        NSMutableAttributedString *attributedString;
        attributedString = [[NSMutableAttributedString alloc] initWithString:customNotification.title];
        [attributedString addAttribute:NSKernAttributeName value:@0.8 range:NSMakeRange(0, customNotification.title.length)];
        [[notificationCell titleLabel] setAttributedText:attributedString];
        
        [[notificationCell descriptionLabel] setText:customNotification.summary];
        [[notificationCell dateLabel] setText:[dateFormatter stringFromDate:customNotification.notificationDate]];
        
        if(customNotification.customNotificationType.intValue == CustomNotificationTypeGeoAlert){
            if(customNotification.url && customNotification.url.length > 0)
            {
                [array addObject:@"Visit Webpage"];
            }
        }
        
        if(customNotification.notificationId && customNotification.notificationId.length > 0) {
            [array addObject:@"View Reward"];
        }
    }
    else {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:DATE_FORMATTER_NOTIFICATION];
        
        Notification *customNotification = [_notifications objectAtIndex:indexPath.section];
        
        NSMutableAttributedString *attributedString;
        attributedString = [[NSMutableAttributedString alloc] initWithString:customNotification.notificationHeader];
        [attributedString addAttribute:NSKernAttributeName value:@0.3 range:NSMakeRange(0, customNotification.notificationHeader.length)];
        [[notificationCell titleLabel] setAttributedText:attributedString];
        [[notificationCell descriptionLabel] setText:customNotification.notificationBody];
        [[notificationCell dateLabel] setText:customNotification.date];

        if (customNotification.surveyId.length) {
            [array addObject:@"View Survey"];
        }
        if (customNotification.documentId.length) {
            [array addObject:@"View Document"];
        }
        if (customNotification.mapId.length) {
            [array addObject:@"View Map Item"];
        }
        if (customNotification.hyperLink.length) {
            [array addObject:@"View Weblink"];
        }
    }

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [notificationCell relayoutBtns:array];
    });
    
    [notificationCell relayoutBtns:array];
        
    return notificationCell;
}

#pragma mark NotificationCellDelegate Methods

- (void)ticketActionWithIndex:(NSInteger)index withTitle:(NSString *)title {
    id note = [_notifications objectAtIndex:index];
    if ([note isKindOfClass:[CustomNotification class]]) {
        CustomNotification *customNotification = [_notifications objectAtIndex:index];
        [customNotification setIsRead:[NSNumber numberWithBool:TRUE]];
        
        if(customNotification.customNotificationType.intValue == CustomNotificationTypeBeacon) {
            NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"rewardsDetailViewController",VIEWCONTROLLER_NOTIFICATION,
                                             [NSNumber numberWithBool:TRUE],@"firstParam",customNotification.notificationId,@"rewardtitle",nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
        }
        else if (customNotification.customNotificationType.intValue == CustomNotificationTypeGeoAlert){
            if ([title isEqualToString:@"View Reward"]) {
                NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"rewardsDetailViewController",VIEWCONTROLLER_NOTIFICATION,
                                                 [NSNumber numberWithBool:TRUE],@"firstParam",customNotification.notificationId,@"rewardtitle",nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
            }
            else if(customNotification.url && customNotification.url > 0) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customNotification.url]];
            }
        }
    }
    
    else {
        Notification *customNotification = [_notifications objectAtIndex:index];
        if ([title isEqualToString:@"View Document"]) {
            NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"webViewcontroller", VIEWCONTROLLER_NOTIFICATION,
                                             customNotification.documentId, @"firstParam", @"Document", @"type", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
        }
        else if ([title isEqualToString:@"View Map Item"]) {
            NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"attractionDetailViewcontroller", VIEWCONTROLLER_NOTIFICATION,
                                             [NSNumber numberWithBool:TRUE],@"firstParam", customNotification.mapId, @"attractionId", nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
        }
        else if ([title isEqualToString:@"View Weblink"]) {            
            NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"webViewcontroller", VIEWCONTROLLER_NOTIFICATION,
                                             customNotification.hyperLink, @"firstParam", @"Hyperlink", @"type", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
        }
        else if ([title isEqualToString:@"View Survey"]) {
            NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"webViewcontroller", VIEWCONTROLLER_NOTIFICATION,
                                             customNotification.surveyId, @"firstParam", @"Survey", @"type", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
        }
        else {
            NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"rewardsDetailViewController",VIEWCONTROLLER_NOTIFICATION, [NSNumber numberWithBool:TRUE],@"firstParam",customNotification.identifier, @"rewardtitle",nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
        }
    }
}

- (void)deleteActionWithIndex:(NSInteger)index{
    
    id note = [_notifications objectAtIndex:index];
    if (![note isKindOfClass:[CustomNotification class]]) {
        Notification *notification = [_notifications objectAtIndex:index];
        [WebServiceManager deleteNotification:notification.identifier];
        NSMutableArray *array = [[AppContext sharedInstance] notifications];
        [array removeObject:note];
        [[AppContext sharedInstance] setNotifications:array];
    }
    else {
        CustomNotification *notification = (CustomNotification *)note;
        [CustomNotification deleteCustomNotification:notification];
    }
    
    [_notifications removeObjectAtIndex:index];
    
    [self updateNotificationHeaderLabel];
    [_notificationTableView reloadData];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object
{
    if (_notifications.count == ([[[AppContext sharedInstance] notifications] count] + [[CustomNotification getCustomNotifications] count])) {
        return;
    }
    
    _notifications = [[NSMutableArray alloc] initWithArray:[[AppContext sharedInstance] notifications]];
    [_notifications addObjectsFromArray:[CustomNotification getCustomNotifications]];
    [_notificationTableView reloadData];
    [self updateNotificationHeaderLabel];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType
{
    NSLog(@"Failed");
}

@end
