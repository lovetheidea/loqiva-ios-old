
#import "JCNotificationBannerView.h"
#import "BeaconData.h"
#import "UIImageView+AFNetworking.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "AppContext.h"
#import "GeoLocationMessage.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

const CGFloat kJCNotificationBannerViewOutlineWidth = 2.0;
const CGFloat kJCNotificationBannerViewMarginX = 60.0;
const CGFloat kJCNotificationBannerViewMarginY = 20.0;

@interface JCNotificationBannerView () {
  BOOL isPresented;
  NSObject* isPresentedMutex;
}

- (void) handleSingleTap:(UIGestureRecognizer*)gestureRecognizer;

@end

@implementation JCNotificationBannerView

@synthesize notificationBanner;
@synthesize iconImageView;
@synthesize titleLabel;
@synthesize messageLabel;

- (id) initWithNotification:(JCNotificationBanner*)notification {
  self = [super init];
  if (self) {
    isPresentedMutex = [NSObject new];

    self.backgroundColor = [UIColor clearColor];
      
    UIView *backgroundView = [[UIView alloc] initWithFrame:self.bounds];
    [backgroundView setBackgroundColor:[UIColor blackColor]];
    [backgroundView setAlpha:0.6];
      
    [self addSubview:backgroundView];
      

    self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:15], [UtilManager height:30], [UtilManager width:40], [UtilManager height:40])];
    
      NSURLRequest *imageRequest;
      
      if([[AppContext sharedInstance] geoLocationMessage]){
          GeoLocationMessage *geoLocationMessage = [[AppContext sharedInstance] geoLocationMessage];
          
          imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],geoLocationMessage.icon]]
                                          cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                      timeoutInterval:2];
          
          __weak UIImageView *downloadImageView = self.iconImageView;
          
          [self.iconImageView setImageWithURLRequest:imageRequest placeholderImage:[UtilManager imageWithColor:GRAY_TEXT_COLOR] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
              //set notification image
              [downloadImageView setImage:image];
              [downloadImageView setContentMode:UIViewContentModeScaleAspectFill];
              [[downloadImageView layer] setMasksToBounds:YES];
              [[downloadImageView layer] setCornerRadius:downloadImageView.frame.size.width/2];
              [[downloadImageView layer] setBorderWidth:1];
              [[downloadImageView layer] setBorderColor:[UIColor whiteColor].CGColor];
          } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
              UIImage *image = [UIImage imageNamed:@"AppIcon"];
              //UIImage *image = [UIImage imageNamed:@"Icon-60"];
              [downloadImageView setImage:image];
              [downloadImageView setContentMode:UIViewContentModeScaleAspectFill];
              [[downloadImageView layer] setMasksToBounds:YES];
              [[downloadImageView layer] setCornerRadius:downloadImageView.frame.size.width/2];
              [[downloadImageView layer] setBorderWidth:1];
              [[downloadImageView layer] setBorderColor:[UIColor whiteColor].CGColor];
          }];
      
      } else {
          BeaconData *beaconData = [BeaconData getBeaconData];
          
          if (beaconData) {
              NSString *imageIcon = [beaconData.icon copy];
              [BeaconData saveBeacon:nil];
              
              imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],imageIcon]]
                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                          timeoutInterval:2];
              
              __weak UIImageView *downloadImageView = self.iconImageView;
              
              [self.iconImageView setImageWithURLRequest:imageRequest placeholderImage:[UtilManager imageWithColor:GRAY_TEXT_COLOR] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                  
                  [downloadImageView setImage:image];
                  [downloadImageView setContentMode:UIViewContentModeScaleAspectFill];
                  [[downloadImageView layer] setMasksToBounds:YES];
                  [[downloadImageView layer] setCornerRadius:downloadImageView.frame.size.width/2];
                  [[downloadImageView layer] setBorderWidth:1];
                  [[downloadImageView layer] setBorderColor:[UIColor whiteColor].CGColor];
              } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                  UIImage *image = [UIImage imageNamed:@"AppIcon"];
                  //UIImage *image = [UIImage imageNamed:@"Icon-60"];
                  [downloadImageView setImage:image];
                  [downloadImageView setContentMode:UIViewContentModeScaleAspectFill];
                  [[downloadImageView layer] setMasksToBounds:YES];
                  [[downloadImageView layer] setCornerRadius:downloadImageView.frame.size.width/2];
                  [[downloadImageView layer] setBorderWidth:1];
                  [[downloadImageView layer] setBorderColor:[UIColor whiteColor].CGColor];
              }];
          }
          else {
              UIImage *image = [UIImage imageNamed:@"AppIcon"];
              //UIImage *image = [UIImage imageNamed:@"Icon-60"];
              [self.iconImageView setImage:image];
              [self.iconImageView setContentMode:UIViewContentModeScaleAspectFill];
              [[self.iconImageView layer] setMasksToBounds:YES];
              [[self.iconImageView layer] setCornerRadius:self.iconImageView.frame.size.width/2];
              [[self.iconImageView layer] setBorderWidth:1];
              [[self.iconImageView layer] setBorderColor:[UIColor whiteColor].CGColor];
          }
      }
      
    [self addSubview:self.iconImageView];
      
    self.titleLabel = [UILabel new];
    self.titleLabel.font = [UIFont fontWithName:SEMIBOLD_FONT size:12];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.titleLabel];
    self.messageLabel = [UILabel new];
    self.messageLabel.font = [UIFont fontWithName:REGULAR_FONT size:10];
    self.messageLabel.textColor = [UIColor whiteColor];
    self.messageLabel.backgroundColor = [UIColor clearColor];
    self.messageLabel.numberOfLines = 1;
    [self addSubview:self.messageLabel];

    UITapGestureRecognizer* tapRecognizer;
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                            action:@selector(handleSingleTap:)];
    [self addGestureRecognizer:tapRecognizer];

    self.notificationBanner = notification;
      
      _separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., NAV_BAR_HEIGHT, WIDTH, 1)];
      [_separatorView setBackgroundColor:[UIColor whiteColor]];
      [self addSubview:_separatorView];
  }
  return self;
}

- (void) drawRect:(CGRect)rect {
  CGRect bounds = self.bounds;

  CGFloat lineWidth = kJCNotificationBannerViewOutlineWidth;
  CGFloat radius = 10;
  CGFloat height = bounds.size.height;
  CGFloat width = bounds.size.width;

  CGContextRef context = UIGraphicsGetCurrentContext();

  CGContextSetAllowsAntialiasing(context, true);
  CGContextSetShouldAntialias(context, true);

  CGMutablePathRef outlinePath = CGPathCreateMutable();

  CGPathMoveToPoint(outlinePath, NULL, lineWidth, 0);
  CGPathAddLineToPoint(outlinePath, NULL, lineWidth, height - radius - lineWidth);
  CGPathAddArc(outlinePath, NULL, radius + lineWidth, height - radius - lineWidth, radius, -M_PI, M_PI_2, 1);
  CGPathAddLineToPoint(outlinePath, NULL, width - radius - lineWidth, height - lineWidth);
  CGPathAddArc(outlinePath, NULL, width - radius - lineWidth, height - radius - lineWidth, radius, M_PI_2, 0, 1);
  CGPathAddLineToPoint(outlinePath, NULL, width - lineWidth, 0);

  CGContextSetRGBFillColor(context, 0, 0, 0, 0.9);
  CGContextAddPath(context, outlinePath);
  CGContextFillPath(context);

  CGContextAddPath(context, outlinePath);
  CGContextSetRGBFillColor(context, 0, 0, 0, 1);
  CGContextSetLineWidth(context, lineWidth);
  CGContextDrawPath(context, kCGPathStroke);

  CGPathRelease(outlinePath);
}

- (void) layoutSubviews {
  if (!(self.frame.size.width > 0)) { return; }
    
  BOOL hasTitle = notificationBanner ? (notificationBanner.title.length > 0) : NO;

  CGFloat borderY = kJCNotificationBannerViewOutlineWidth + kJCNotificationBannerViewMarginY;
  CGFloat borderX = kJCNotificationBannerViewOutlineWidth + kJCNotificationBannerViewMarginX;
  CGFloat currentX = borderX;
  CGFloat currentY = borderY;
  //CGFloat contentWidth = self.frame.size.width - (borderX * 2.0);

  currentY += 2.0;
  if (hasTitle) {
    self.titleLabel.frame = CGRectMake(currentX -5, currentY , WIDTH - self.iconImageView.frame.origin.x * 3 - self.iconImageView.frame.size.width, 20.0);
    currentY += 18.0;
  }
  self.messageLabel.frame = CGRectMake(currentX -5, currentY, self.titleLabel.frame.size.width - 10, [UtilManager height:15]);
    self.messageLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
 // [self.messageLabel sizeToFit];
  //CGRect messageFrame = self.messageLabel.frame;
  //CGFloat spillY = (currentY + messageFrame.size.height + kJCNotificationBannerViewMarginY) - self.frame.size.height;
  //if (spillY > 0.0) {
  //  messageFrame.size.height -= spillY;
  //  self.messageLabel.frame = messageFrame;
  //}
}

- (void) setNotificationBanner:(JCNotificationBanner*)notification {
  notificationBanner = notification;

  self.titleLabel.text = notification.title;
  self.messageLabel.text = notification.message;
}

- (void) handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
  if (notificationBanner && notificationBanner.tapHandler) {
    notificationBanner.tapHandler();
      
  }
}

- (BOOL) getCurrentPresentingStateAndAtomicallySetPresentingState:(BOOL)state {
  @synchronized(isPresentedMutex) {
    BOOL originalState = isPresented;
    isPresented = state;
    return originalState;
  }
}

@end
