//
//  NotificationCell.h
//  loqiva
//
//  Created by Manuel Manzanera on 16/11/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NotificationCellDelegate <NSObject>

- (void)ticketActionWithIndex:(NSInteger)index withTitle:(NSString *)title;
- (void)deleteActionWithIndex:(NSInteger)index;


@end

@interface NotificationCell : UITableViewCell

-(void)relayoutBtns:(NSArray *)titles;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, strong) NSArray *titlesArray;
@property (nonatomic, assign) id<NotificationCellDelegate>delegate;

@end
