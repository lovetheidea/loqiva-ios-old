//
//  NavCustomView.h
//  loqiva
//
//  Created by Manuel Manzanera on 1/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    BlurEffectTypeLight,
    BlurEffectTypeDark,
    BlurEffectTypeNone
} BlurEffectType;

@interface NavCustomView : UIView <UIScrollViewDelegate>

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *iconLabel;
@property (nonatomic, strong) UIView * redBadge;
@property (nonatomic, strong) UIImageView *slideButton;
@property (nonatomic, strong) UIImageView *notificationButtonImage;
@property (nonatomic, strong) UIButton *notificationButton;
@property (nonatomic, strong) UILabel *notificationLabel;
@property (nonatomic, strong) UIImageView *backButtonImage;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIView *blurView;
@property (nonatomic, strong) UIView *separatorView;
@property(nonatomic, strong) CAGradientLayer * cagradient;
@property (nonatomic, weak) UIViewController *parentController;


- (id)initWithFrame:(CGRect)frame gradienAndUIBlurEffectStyle:(UIBlurEffectStyle)blurEffectStyle:(BOOL)applyGradient;

- (void)updateScroll:(CGPoint)scrollOffset andScrollContentSize:(CGSize)scrollContensize;
- (void)changeBackColor;
- (void)addGradientLayer;
- (void)setBackOption;

@end
