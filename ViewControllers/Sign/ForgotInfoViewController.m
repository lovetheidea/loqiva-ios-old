//
//  ForgotInfoViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 10/6/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ForgotInfoViewController.h"
#import "WebServiceManager.h"
#import "MenuHomeViewController.h"
#import "AppDelegate.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface ForgotInfoViewController()<WebServiceManagerDelegate>

@end

@implementation ForgotInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
//#ifdef LOQIVA
    [self.view setBackgroundColor:[UIColor orangeAppColor]];
//#elif OLDSEAPORT
//    [self.view setBackgroundColor:[UIColor whiteColor]];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image-blurred-bg"]];
//    [self.view addSubview:imageView];
//#endif
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20.], [UtilManager height:178], [UtilManager width:340], [UtilManager height:178])];
    [infoLabel setCenter:CGPointMake(self.view.center.x, infoLabel.center.y)];
    [infoLabel setTextAlignment:NSTextAlignmentCenter];
    [infoLabel setNumberOfLines:0];
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    [infoLabel setAttributedText:NSLocalizedString(@"We have sent you an\nemail with instructions\non how to reset your\npassword", nil) withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:WHITE_TEXT_COLOR andKern:TITLE_FONT_KERN withLineBreak:NO];
    [infoLabel setAdjustsFontSizeToFitWidth:YES];
    
    [self.view addSubview:infoLabel];
    
    UILabel *skipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:546], WIDTH, [UtilManager height:36])];
    [skipLabel setAttributedText:NSLocalizedString(@"Skip directly to the app", nil) withLineHeight:22 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:SEMIBOLD_FONT size:16] andColor:[UIColor whiteColor] andKern:-0.5 withLineBreak:NO];
    [skipLabel setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(skipAction)];
    [skipLabel addGestureRecognizer:tapGesture];
    
    [self.view addSubview:skipLabel];
    UILabel *signInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:595], infoLabel.frame.size.width, [UtilManager height:40])];
    [signInfoLabel setCenter:CGPointMake(self.view.center.x, signInfoLabel.center.y)];
    [signInfoLabel setTextAlignment:NSTextAlignmentCenter];
    [signInfoLabel setText:NSLocalizedString(@"Signing up to an account will create\n a personal app experience tailored to you", nil)];
    [signInfoLabel setTextColor:[UIColor whiteColor]];
    [signInfoLabel setNumberOfLines:0];
    [signInfoLabel setFont:[UIFont fontWithName:LIGHT_FONT size:12]];
    
    [self.view addSubview:signInfoLabel];
}

- (void)skipAction{
    if (![AppDelegate showAlertIfDisconnected]) {
        [WebServiceManager createTempUser:self];
    }
}

- (void)presentHomeViewController{
    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:homeViewController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
     }];
}

#pragma mark WebServiceDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    if (webServiceType == WebServiceTypeTempUser){
        [self presentHomeViewController];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
}

@end
