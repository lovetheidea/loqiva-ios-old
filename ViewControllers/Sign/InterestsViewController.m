//
//  InterestsViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "InterestsViewController.h"
#import "WebServiceManager.h"
#import "AFNetworkReachabilityManager.h"
#import "AppDelegate.h"
#import "InterestView.h"
#import "TagView.h"
#import "Tag.h"
#import "UIColor+Helper.h"
#import "MenuHomeViewController.h"
#import "loqiva-Swift.h"

@interface InterestsViewController() <InterestViewDelegate, WebServiceManagerDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation InterestsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [WebServiceManager getInterests:self];
    
    [self configureView];
}

- (void)configureView {
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.scrollView];
    [self.view setBackgroundColor:[UIColor orangeAppColor]];
}

- (void)interestView{
    NSInteger heightOfView = [InterestView heightOfView] + 50;
    InterestView *interestView = [[InterestView alloc] initWithFrame:CGRectMake(0., [UtilManager height:80], self.view.frame.size.width, heightOfView)];
    interestView.backgroundColor = [UIColor clearColor];
    [interestView setDelegate:self];
    
    [self.scrollView addSubview:interestView];
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, heightOfView)];
}

- (void)useAction{
    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:homeViewController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
     }];
}

#pragma mark InterestViewDelegate Methods

- (void)actionDidPush{
    [self useAction];
}

#pragma mark WebServiceDelegate Manager

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    [self interestView];
}


@end
