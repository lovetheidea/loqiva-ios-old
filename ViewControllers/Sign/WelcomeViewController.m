//
//  WelcomeViewController.m
//  loqiva
//
//  Created by juan Jimenez on 08/12/2016.
//  Copyright © 2016 inup. All rights reserved.
//

#import "WelcomeViewController.h"
#import "UtilManager.h"
#import "WebServiceManager.h"
#import "MenuHomeViewController.h"
#import "AppDelegate.h"
#import "SignUpViewController.h"
#import "RegisterUserViewController.h"
#import "UIColor+Helper.h"
#import "WebViewController.h"
#import "OnboardingViewController.h"
#import "TOWebViewController/TOWebViewController.h"
#import "loqiva-Swift.h"

@import SafariServices;

@interface WelcomeViewController () <WebServiceManagerDelegate, SFSafariViewControllerDelegate>

@property (nonatomic, retain) UIImageView *splashImageView;

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureView];
    
    self.splashImageView  = [[UIImageView alloc] initWithFrame:self.view.frame];
    [self.splashImageView setImage:[UIImage imageNamed:@"splashimage"]];
    [self.view addSubview:self.splashImageView];

    //if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstLaunch"]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self updateFirstLoad];
        });
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLaunch"];
    //}
}

- (void)showTermsController {
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    NSString *url = infoPlistDict[@"T&CLink"];
    
    TOWebViewController *webViewController = [[TOWebViewController alloc] initWithURL:[NSURL URLWithString:url]];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
    navigationController.navigationBar.barTintColor = [UIColor orangeAppColor];
    navigationController.toolbar.barTintColor = [UIColor orangeAppColor];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)showPrivacyController {
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    NSString *url = infoPlistDict[@"PrivacyLink"];
    
    TOWebViewController *webViewController = [[TOWebViewController alloc] initWithURL:[NSURL URLWithString:url]];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
    navigationController.navigationBar.barTintColor = [UIColor orangeAppColor];
    navigationController.toolbar.barTintColor = [UIColor orangeAppColor];
    [self presentViewController:navigationController animated:YES completion:nil];
    
}

- (void)updateFirstLoad {
    OnboardingViewController *controller = [[OnboardingViewController alloc] initWithNibName:@"OnboardingViewController" bundle:nil];
    controller.view.frame = self.view.frame;
    [self presentViewController:controller animated:YES completion:^{
        [UIView animateWithDuration:0 animations:^{
            self.splashImageView.alpha = 0;
        } completion:^(BOOL finished) {
            [self.splashImageView removeFromSuperview];
        }];
    }];
}

- (void)configureView{
    [self.view setBackgroundColor:[UIColor orangeAppColor]];
    
    //Info label
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20.], [UtilManager height:100 ], [UtilManager width:340], [UtilManager height:250])];
    [infoLabel setCenter:CGPointMake(self.view.center.x, infoLabel.center.y)];
    [infoLabel setTextAlignment:NSTextAlignmentCenter];
    [infoLabel setNumberOfLines:0];
    [infoLabel setAttributedText:NSLocalizedString(@"You’re only seconds\naway from receiving\ncontent and benefits\ntailored for you.", nil) withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:[UIColor whiteColor] andKern:TITLE_FONT_KERN withLineBreak:NO];
    [self.view addSubview:infoLabel];
    

    //Sign up btn
    UIImage *signupImage = [UIImage imageNamed:@"btn-signup-free"];
    UIButton *signUpButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:70],infoLabel.frame.origin.y + infoLabel.frame.size.height + [UtilManager height:10], [UtilManager width:240], [UtilManager height:50])];
    
    [signUpButton setBackgroundImage:signupImage forState:UIControlStateNormal];
    [signUpButton addTarget:self action:@selector(signUpAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:signUpButton];
    
    //Already account label
    UILabel *alreadyAccount = [[UILabel alloc] initWithFrame:CGRectMake(0, signUpButton.frame.origin.y + signUpButton.frame.size.height + [UtilManager height:30], WIDTH, [UtilManager height:24])];
    [alreadyAccount setAttributedText:NSLocalizedString(@"Already have an account?", nil) withLineHeight:20 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:16] andColor:[UIColor whiteColor] andKern:-0.47 withLineBreak:NO];
    [self.view addSubview:alreadyAccount];
    

    
    //Login btn
    UILabel *loginLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, alreadyAccount.frame.origin.y + alreadyAccount.frame.size.height , WIDTH, [UtilManager height:24])];
    [loginLabel setAttributedText:NSLocalizedString(@"Log in here", nil) withLineHeight:20 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:SEMIBOLD_FONT size:16] andColor:[UIColor whiteColor] andKern:-0.47 withLineBreak:NO];
    [loginLabel setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapLogin = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginAction)];
    [loginLabel addGestureRecognizer:tapLogin];
    [self.view addSubview:loginLabel];
    

    //Skip label
    UILabel *skipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:546], WIDTH, [UtilManager height:26])];
    [skipLabel setAttributedText:NSLocalizedString(@"Preview the app", nil) withLineHeight:22 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:SEMIBOLD_FONT size:16] andColor:[UIColor whiteColor] andKern:-0.5 withLineBreak:NO];
    [skipLabel setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(skipAction)];
    [skipLabel addGestureRecognizer:tapGesture];
    
    [self.view addSubview:skipLabel];
    

    
    UILabel *signInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:580], infoLabel.frame.size.width, [UtilManager height:40])];
    [signInfoLabel setCenter:CGPointMake(self.view.center.x - 46, signInfoLabel.center.y)];
    [signInfoLabel setTextAlignment:NSTextAlignmentCenter];
    signInfoLabel.userInteractionEnabled = YES;
    [signInfoLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showTermsController)]];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"View our T&C's" attributes:nil];
    NSRange linkRange = NSMakeRange(9, 5);
    
    NSDictionary *linkAttributes = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:0.05 green:0.4 blue:0.65 alpha:1.0],
                                      NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle) };
    [attributedString setAttributes:linkAttributes range:linkRange];
    
    // Assign attributedText to UILabel
    signInfoLabel.attributedText = attributedString;
    [signInfoLabel setTextColor:[UIColor whiteColor]];
    [signInfoLabel setNumberOfLines:0];
    [signInfoLabel setFont:[UIFont fontWithName:LIGHT_FONT size:12]];
    
    [self.view addSubview:signInfoLabel];
    
    UILabel *privacyInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:580], infoLabel.frame.size.width, [UtilManager height:40])];
    [privacyInfoLabel setCenter:CGPointMake(self.view.center.x  + 46, privacyInfoLabel.center.y)];
    [privacyInfoLabel setTextAlignment:NSTextAlignmentCenter];
    privacyInfoLabel.userInteractionEnabled = YES;
    [privacyInfoLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPrivacyController)]];
    NSMutableAttributedString *attributedPrivacyString = [[NSMutableAttributedString alloc] initWithString:@"and Privacy Policy" attributes:nil];
    NSRange privacyLinkRange = NSMakeRange(4, 14);
    
    NSDictionary *privacyLinkAttributes = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:0.05 green:0.4 blue:0.65 alpha:1.0],
                                      NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle) };
    [attributedPrivacyString setAttributes:privacyLinkAttributes range:privacyLinkRange];
    
    // Assign attributedText to UILabel
    privacyInfoLabel.attributedText = attributedPrivacyString;
    [privacyInfoLabel setTextColor:[UIColor whiteColor]];
    [privacyInfoLabel setNumberOfLines:0];
    [privacyInfoLabel setFont:[UIFont fontWithName:LIGHT_FONT size:12]];
    
    [self.view addSubview:privacyInfoLabel];
    
}

-(void)loginAction{
    if (![AppDelegate showAlertIfDisconnected]) {
        SignUpViewController *detailViewcontroller = [[SignUpViewController alloc] init];
        [self.navigationController pushViewController:detailViewcontroller animated:YES];
    }
    
}

-(void)signUpAction{
    if (![AppDelegate showAlertIfDisconnected]) {
        RegisterUserViewController *detailViewcontroller = [[RegisterUserViewController alloc] init];
        [self.navigationController pushViewController:detailViewcontroller animated:YES];
    }
}

- (void)skipAction{
    if (![AppDelegate showAlertIfDisconnected]) {
        [WebServiceManager createTempUser:self];
    }
}

- (void)presentHomeViewController{
    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:homeViewController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
     }];
}
#pragma mark WebServiceDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    if (webServiceType == WebServiceTypeTempUser){
        [self presentHomeViewController];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
}

@end
