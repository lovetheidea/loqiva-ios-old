//
//  SignNavController.m
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "SignNavController.h"
#import "WelcomeViewController.h"
#import "SignUpViewController.h"
#import "RegisterUserViewController.h"
#import "InterestsViewController.h"
#import "ForgotViewController.h"
#import "ForgotInfoViewController.h"
#import "ResetPasswordViewController.h"
#import "MenuHomeViewController.h"
#import "AppDelegate.h"
#import "UIColor+Helper.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "loqiva-Swift.h"

#ifdef LOQIVA

#define TITLE_APP @"Welcome to Loqiva"

#elif OLDSEAPORT

#define TITLE_APP @"Welcome to OldSeaPort"

#elif MYCHIPPY

#define TITLE_APP @"Welcome to MyChippy"

#endif

@interface SignNavController ()<UINavigationControllerDelegate, SignNavViewDelegate>

@end

@implementation SignNavController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(_comesFromApp){
          NSLog(@"GTGTGTGTGTGTGTG");
        [[_sigNavView backButton] setHidden:NO];
        [[_sigNavView backButtonImage] setHidden:NO];
    }
    
    [self setNavigationBarHidden:YES];
    
    [self setDelegate:self];
    
    _sigNavView = [[SignNavView alloc] initWithFrame:CGRectMake(0., 0., WIDTH,NAV_BAR_HEIGHT)];
    [_sigNavView setDelegate:self];
    [self.view addSubview:_sigNavView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentResetPassword) name:@"ResetPassWord" object:nil];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UINavigationControllerDelegate Methods

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    NSString *appName = infoPlistDict[@"AppName"];
    if (appName.length == 0) {
        appName = @"Loqiva";
    }
    
    if(navigationController.viewControllers.count >1 || _comesFromApp){
        [[_sigNavView backButton] setHidden:NO];
        [[_sigNavView backButtonImage] setHidden:NO];
        [_sigNavView setBackgroundColor:[UIColor orangeAppColor]];
    }else{
        
      
        [[_sigNavView backButton] setHidden:YES];
        [[_sigNavView backButtonImage] setHidden:YES];
    }
    if([viewController isKindOfClass:[WelcomeViewController class]]){
        
         [[_sigNavView titleLabel] setAttributedText:[NSString stringWithFormat:@"Welcome to %@", appName] withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:23] andColor:[UIColor whiteColor] andKern:-0.71 withLineBreak:NO];
    }
    else if([viewController isKindOfClass:[SignUpViewController class]])
        [[_sigNavView titleLabel] setAttributedText:[NSString stringWithFormat:@"Welcome to %@", appName] withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:23] andColor:[UIColor whiteColor] andKern:-0.71 withLineBreak:NO];
    else if([viewController isKindOfClass:[RegisterUserViewController class]])
    {
        [[_sigNavView titleLabel] setAttributedText:NSLocalizedString(@"Your details", nil) withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:23] andColor:[UIColor whiteColor] andKern:-0.71 withLineBreak:NO];

    }
    else if([viewController isKindOfClass:[InterestsViewController class]]){
        [[_sigNavView titleLabel] setAttributedText:NSLocalizedString(@"Your interests", nil) withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:23] andColor:[UIColor whiteColor] andKern:-0.71 withLineBreak:NO];
     
    }else if([viewController isKindOfClass:[ForgotViewController class]] || [viewController isKindOfClass:[ForgotInfoViewController class]]){
        [[_sigNavView titleLabel] setAttributedText:NSLocalizedString(@"Forgot your password?", nil) withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:23] andColor:[UIColor whiteColor] andKern:-0.71 withLineBreak:NO];
//        [[_sigNavView backButton] setHidden:NO];
//        [[_sigNavView backButtonImage] setHidden:NO];
    } else if([viewController isKindOfClass:[ResetPasswordViewController class]]){
        [[_sigNavView titleLabel] setAttributedText:NSLocalizedString(@"Reset your password", nil) withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:23] andColor:[UIColor whiteColor] andKern:-0.71 withLineBreak:NO];
//        [[_sigNavView backButton] setHidden:NO];
//        [[_sigNavView backButtonImage] setHidden:NO];
    }
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
}
- (void)presentHomeViewController{
    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:homeViewController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
     }];
}

#pragma mark SignNavViewDelegate Methods

- (void)backAction{
    
    if(_comesFromApp){
        
        [self presentHomeViewController];
        return;
    }
    
    if(self.navigationController.viewControllers.count>1){
        [self presentViewController:self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] animated:YES completion:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self popViewControllerAnimated:YES];
            }); 
        }];
        
    }else{
       [self popViewControllerAnimated:YES];
    }
}

- (void)presentResetPassword{
    if(![self.visibleViewController isKindOfClass:[ResetPasswordViewController class]]){
        ResetPasswordViewController *resetPasswordViewController = [[ResetPasswordViewController alloc] init];
        [self pushViewController:resetPasswordViewController animated:YES];
    }
}


@end
