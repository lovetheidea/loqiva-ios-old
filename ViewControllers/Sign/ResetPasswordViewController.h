//
//  ResetPasswordViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 10/6/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ParentViewController.h"

@interface ResetPasswordViewController : ParentViewController

@end
