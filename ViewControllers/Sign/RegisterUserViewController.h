//
//  RegisterUserViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ParentViewController.h"

@interface RegisterUserViewController : ParentViewController
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITextField *firstNameTextField;
@property (nonatomic, strong) UITextField *lastNameTextField;
@property (nonatomic, strong) UITextField *postcodeTextField;
@property (nonatomic, strong) UITextField *mailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UITextField *telephoneTextField;
@property (nonatomic, strong) UIButton *saveButton;
@property (nonatomic, strong) UIButton *profilePictureButton;
@property (nonatomic, strong) UIButton *ageBtn;
@property (nonatomic, strong) UIButton *titleBtn;
@property (nonatomic, strong) UIImagePickerController *picker;
@property (nonatomic, strong) UIImage *userImage;
@property(nonatomic) CGSize totalScrollSize;
@property(nonatomic) UIImageView *imageHeader;
//Picker
@property (nonatomic, strong) UIPickerView *pickerIssue;
@property (nonatomic, strong) UIButton *acceptPickerButton;
@property (nonatomic, strong) UIView *backGRPicker;
@property(nonatomic) NSMutableArray *titlesPiker;
@property(nonatomic) NSMutableArray *agesPicker;
@property(nonatomic) Boolean isAgePicker;
@property(nonatomic) NSString * ageSelected;
@property(nonatomic) NSString * titleSelected;
@end
