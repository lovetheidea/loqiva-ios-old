//
//  RegisterUserViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "RegisterUserViewController.h"
#import "InterestsViewController.h"
#import "WebServiceManager.h"
#import "AFNetworkReachabilityManager.h"
#import "User.h"
#import "NSString+Encryption.h"
#import "RegularExpressionsManager.h"
#import "UserDefaultManager.h"
#import "ErrorManager.h"
#import "UIColor+Helper.h"
#import "UIImage+Crop.h"
#import "AppDelegate.h"
#import "loqiva-Swift.h"

@interface RegisterUserViewController ()<UITextFieldDelegate, UIScrollViewDelegate, WebServiceManagerDelegate,UIImagePickerControllerDelegate, UIActionSheetDelegate,UINavigationControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource>


@end

@implementation RegisterUserViewController

@synthesize scrollView,firstNameTextField,lastNameTextField,postcodeTextField,mailTextField,passwordTextField,telephoneTextField;

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    _isAgePicker = NO;
    
    _agesPicker = [NSMutableArray new];
    _titlesPiker = [NSMutableArray new];
    
    [WebServiceManager getAgeGroup:self];
    [WebServiceManager getTitleGroup:self];
    
    
    _totalScrollSize = CGSizeZero;
}


-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    
//#ifdef LOQIVA
    [self.view setBackgroundColor:[UIColor orangeAppColor]];
//#elif OLDSEAPORT
//    [self.view setBackgroundColor:[UIColor whiteColor]];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image-blurred-bg"]];
//    [self.view addSubview:imageView];
//#endif
    
    
    
//    UIImage * img = [UIImage imageNamed:@"image-blurred-bg"];
//    img=[img crop:CGRectMake(0, 0, WIDTH, 80)];
//    _imageHeader = [[UIImageView alloc] initWithImage:img];
//    _imageHeader.frame = CGRectMake(0, 0, WIDTH, 80);
//    _imageHeader.contentMode = UIViewContentModeTop;
    
    
    
    //Picker
    _backGRPicker = [[UIView alloc] initWithFrame:self.view.frame];
    _backGRPicker.backgroundColor=[UIColor blackColor];
    _backGRPicker.userInteractionEnabled = NO;
    _backGRPicker.alpha=.5;
    
    _pickerIssue = [[UIPickerView alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - 216, self.view.frame.size.width, 216)];
    [_pickerIssue setDataSource:self];
    [_pickerIssue setDelegate:self];
    
    [_pickerIssue setUserInteractionEnabled:YES];
    
    _acceptPickerButton = [[UIButton alloc] initWithFrame:CGRectMake(_pickerIssue.frame.size.width - 80., _pickerIssue.frame.origin.y + 10, 80., 24.)];
    [_acceptPickerButton setTitleColor:BLACK_TEXT_COLOR forState:UIControlStateNormal];
    [_acceptPickerButton setTitle:NSLocalizedString(@"Accept", nil) forState:UIControlStateNormal];
    
    
    [_acceptPickerButton addTarget:self action:@selector(acceptPickerAction) forControlEvents:UIControlEventTouchDown];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.,0.,WIDTH , HEIGHT)];
    [scrollView setBackgroundColor:[UIColor clearColor]];
    

    
    _profilePictureButton = [[UIButton alloc] initWithFrame:CGRectMake(0, [UtilManager height:85], [UtilManager width:150], [UtilManager width:150])];
    [_profilePictureButton setCenter:CGPointMake(self.view.center.x, _profilePictureButton.center.y)];
    [[_profilePictureButton layer] setCornerRadius:_profilePictureButton.frame.size.width/2];
    [_profilePictureButton setImage:[UIImage imageNamed:@"btn-profile-photo"] forState:UIControlStateNormal];
    [_profilePictureButton addTarget:self action:@selector(uploadPicture) forControlEvents:UIControlEventTouchUpInside];
    
    [scrollView addSubview:_profilePictureButton];
    
    _totalScrollSize.height += (_profilePictureButton.frame.size.height + _profilePictureButton.frame.origin.y);
    
    
    UIImageView *firstNameTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., _profilePictureButton.frame.origin.y + _profilePictureButton.frame.size.height + [UtilManager height:20], [UtilManager width:330], [UtilManager height:50])];
    [firstNameTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
    [firstNameTextFieldBacgroundView setCenter:CGPointMake(self.view.center.x, firstNameTextFieldBacgroundView.center.y)];
    [scrollView addSubview:firstNameTextFieldBacgroundView];
    
    firstNameTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:15], _profilePictureButton.frame.origin.y + _profilePictureButton.frame.size.height + [UtilManager height:40], [UtilManager width:330], UITEXTFIELD_HEIGHT)];
    [firstNameTextField setBackgroundColor:[UIColor clearColor]];
    [firstNameTextField setCenter:CGPointMake(self.view.center.x,firstNameTextFieldBacgroundView.center.y)];
    [firstNameTextField setBorderStyle:UITextBorderStyleNone];
    [firstNameTextField setTextColor:GRAY_TEXT_COLOR];
    [firstNameTextField setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [firstNameTextField setTextAlignment:NSTextAlignmentCenter];
    firstNameTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"First Name", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                 NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                 }
     ];
    [firstNameTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [firstNameTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [firstNameTextField setReturnKeyType:UIReturnKeyNext];
    [firstNameTextField setLeftViewMode:UITextFieldViewModeAlways];
    [firstNameTextField setTextColor:GRAY_TEXT_COLOR];
    [firstNameTextField setDelegate:self];
    UIView *leftFirstNameTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
    leftFirstNameTextFieldView.backgroundColor = [UIColor clearColor];
    [firstNameTextField setLeftView:leftFirstNameTextFieldView];
    [scrollView addSubview:firstNameTextField];
    
    UIImageView *lastNameTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:330], [UtilManager height:50])];
    [lastNameTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
    [lastNameTextFieldBacgroundView setCenter:CGPointMake(self.view.center.x, firstNameTextFieldBacgroundView.frame.origin.y + firstNameTextFieldBacgroundView.frame.size.height + [UtilManager height:12] + firstNameTextFieldBacgroundView.frame.size.height/2)];
    [scrollView addSubview:lastNameTextFieldBacgroundView];
    
    lastNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(firstNameTextField.frame.origin.x, firstNameTextField.frame.origin.y + firstNameTextField.frame.size.height + [UtilManager height:12], firstNameTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
//    [lastNameTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [lastNameTextField setBackgroundColor:[UIColor clearColor]];
    [lastNameTextField setCenter:CGPointMake(self.view.center.x,lastNameTextFieldBacgroundView.center.y)];
    [firstNameTextField setReturnKeyType:UIReturnKeyNext];
    [lastNameTextField setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [lastNameTextField setTextAlignment:NSTextAlignmentCenter];
    [lastNameTextField setTextColor:GRAY_TEXT_COLOR];
    lastNameTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Last Name", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                 NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                 }
     ];
    [lastNameTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [lastNameTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [lastNameTextField setReturnKeyType:UIReturnKeyNext];
    [lastNameTextField setLeftViewMode:UITextFieldViewModeAlways];
    [lastNameTextField setDelegate:self];
    UIView *leftLastNameTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
    leftLastNameTextFieldView.backgroundColor = [UIColor clearColor];
    [lastNameTextField setLeftView:leftLastNameTextFieldView];
    [scrollView addSubview:lastNameTextField];
    
    
    
    //Age selector
    _ageBtn = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:15], lastNameTextField.frame.origin.y + lastNameTextField.frame.size.height + [UtilManager height:8], firstNameTextField.frame.size.width, [UtilManager height:50])];
    _ageBtn.tag=3;
    [_ageBtn setBackgroundColor:[UIColor clearColor]];
    [_ageBtn setCenter:CGPointMake(self.view.center.x,_ageBtn.center.y)];
    [_ageBtn setBackgroundImage:[UIImage imageNamed:@"image-textfield"] forState:UIControlStateNormal];
    
    [_ageBtn setTitleColor:GRAY_TEXT_COLOR forState:UIControlStateNormal];
    [_ageBtn.titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:16]];
    [_ageBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    _ageBtn.enabled = NO;
    
    [_ageBtn setTitle:@"Age" forState:UIControlStateNormal];
    
    
    [_ageBtn addTarget:self action:@selector(presentIssuePickerView:) forControlEvents:UIControlEventTouchUpInside];
    _ageBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    UIImageView *arrowAgeImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn-arrow-down"]];
    arrowAgeImg.frame = CGRectMake(_ageBtn.frame.origin.x+_ageBtn.frame.size.width -[UtilManager width:40], _ageBtn.frame.origin.y,[UtilManager width:20], [UtilManager width:15]);
    arrowAgeImg.center = CGPointMake(arrowAgeImg.center.x, _ageBtn.center.y);
    arrowAgeImg.userInteractionEnabled = NO;

    [scrollView addSubview:_ageBtn];
    [scrollView addSubview:arrowAgeImg];
    
    //Title selector

    _titleBtn = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:15], _ageBtn.frame.origin.y + _ageBtn.frame.size.height + [UtilManager height:12], firstNameTextField.frame.size.width, [UtilManager height:50])];
    [_titleBtn setBackgroundColor:[UIColor clearColor]];
    [_titleBtn setCenter:CGPointMake(self.ageBtn.center.x,_titleBtn.center.y)];
    [_titleBtn setBackgroundImage:[UIImage imageNamed:@"image-textfield"] forState:UIControlStateNormal];
    [_titleBtn setTitleColor:GRAY_TEXT_COLOR forState:UIControlStateNormal];
    [_titleBtn.titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:16]];
    [_titleBtn setTitle:@"Title" forState:UIControlStateNormal];
    _titleBtn.enabled = NO;
    [_titleBtn addTarget:self action:@selector(presentIssuePickerView:) forControlEvents:UIControlEventTouchUpInside];
    _titleBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    
    UIImageView *arrowTitleImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn-arrow-down"]];
    arrowTitleImg.frame = CGRectMake(_titleBtn.frame.origin.x+_titleBtn.frame.size.width -[UtilManager width:40], _ageBtn.frame.origin.y,[UtilManager width:20], [UtilManager width:15]);
    arrowTitleImg.center = CGPointMake(arrowAgeImg.center.x, _titleBtn.center.y);
    arrowTitleImg.userInteractionEnabled = NO;
    
    
    [scrollView addSubview:_titleBtn];
    [scrollView addSubview:arrowTitleImg];
    
    
    UIImageView *postCodeTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:330], [UtilManager height:50])];
    [postCodeTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
    [postCodeTextFieldBacgroundView setCenter:CGPointMake(self.view.center.x, _titleBtn.frame.origin.y + _titleBtn.frame.size.height + [UtilManager height:12] + firstNameTextFieldBacgroundView.frame.size.height/2)];
    [scrollView addSubview:postCodeTextFieldBacgroundView];
    
    postcodeTextField = [[UITextField alloc] initWithFrame:CGRectMake(firstNameTextField.frame.origin.x, _titleBtn.frame.origin.y + _titleBtn.frame.size.height + [UtilManager height:12], firstNameTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
    [postcodeTextField setBorderStyle:UITextBorderStyleNone];
    [postcodeTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [postcodeTextField setCenter:CGPointMake(self.view.center.x,postCodeTextFieldBacgroundView.center.y)];
    [postcodeTextField setTextAlignment:NSTextAlignmentCenter];
    [postcodeTextField setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [postcodeTextField setTextColor:GRAY_TEXT_COLOR];
    postcodeTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Post Code", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                 NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                 }
     ];
    [postcodeTextField setReturnKeyType:UIReturnKeyNext];
    [postcodeTextField setLeftViewMode:UITextFieldViewModeAlways];
    [postcodeTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [postcodeTextField setDelegate:self];
    UIView *postcodeTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
    [postcodeTextField setLeftView:postcodeTextFieldView];
    [scrollView addSubview:postcodeTextField];
    
    UIImageView *phoneTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:330], [UtilManager height:50])];
    [phoneTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
    [phoneTextFieldBacgroundView setCenter:CGPointMake(self.view.center.x, postcodeTextField.frame.origin.y + postcodeTextField.frame.size.height + [UtilManager height:10] + firstNameTextFieldBacgroundView.frame.size.height/2)];
    [scrollView addSubview:phoneTextFieldBacgroundView];
    
    telephoneTextField = [[UITextField alloc] initWithFrame:CGRectMake(firstNameTextField.frame.origin.x, postcodeTextField.frame.origin.y + postcodeTextField.frame.size.height + [UtilManager height:12], firstNameTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
    [telephoneTextField setCenter:CGPointMake(self.view.center.x, phoneTextFieldBacgroundView.center.y)];
    [telephoneTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [telephoneTextField setBorderStyle:UITextBorderStyleNone];
    [telephoneTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [telephoneTextField setTextAlignment:NSTextAlignmentCenter];
    [telephoneTextField setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [telephoneTextField setTextColor:GRAY_TEXT_COLOR];
    telephoneTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Telephone Number", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                 NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                 }
     ];
    [telephoneTextField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [telephoneTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [telephoneTextField setReturnKeyType:UIReturnKeyNext];
    [telephoneTextField setLeftViewMode:UITextFieldViewModeAlways];
    [telephoneTextField setDelegate:self];
    UIView *phoneTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
    [telephoneTextField setLeftView:phoneTextFieldView];
    [scrollView addSubview:telephoneTextField];
    
    UIImageView *mailTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:330], [UtilManager height:50])];
    [mailTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
    [mailTextFieldBacgroundView setCenter:CGPointMake(self.view.center.x, telephoneTextField.frame.origin.y + telephoneTextField.frame.size.height + [UtilManager height:10] + firstNameTextFieldBacgroundView.frame.size.height/2)];
    [scrollView addSubview:mailTextFieldBacgroundView];
    
    mailTextField = [[UITextField alloc] initWithFrame:CGRectMake(firstNameTextField.frame.origin.x, telephoneTextField.frame.origin.y + telephoneTextField.frame.size.height + [UtilManager height:12], firstNameTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
    [mailTextField setCenter:CGPointMake(self.view.center.x, mailTextFieldBacgroundView.center.y)];
    [mailTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [mailTextField setBorderStyle:UITextBorderStyleNone];
    [mailTextField setTextAlignment:NSTextAlignmentCenter];
    [mailTextField setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [mailTextField setTextColor:GRAY_TEXT_COLOR];
    mailTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                 NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                 }
     ];
    [mailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [mailTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [mailTextField setReturnKeyType:UIReturnKeyNext];
    [mailTextField setLeftViewMode:UITextFieldViewModeAlways];
    [mailTextField setDelegate:self];
    UIView *mailTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
    [mailTextField setLeftView:mailTextFieldView];
    [scrollView addSubview:mailTextField];
    
    UIImageView *passwordFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:330], [UtilManager height:50])];
    [passwordFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
    [passwordFieldBacgroundView setCenter:CGPointMake(self.view.center.x, mailTextField.frame.origin.y + mailTextField.frame.size.height + [UtilManager height:10] + firstNameTextFieldBacgroundView.frame.size.height/2)];
    [scrollView addSubview:passwordFieldBacgroundView];
    
    
    
    passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(firstNameTextField.frame.origin.x, mailTextField.frame.origin.y + mailTextField.frame.size.height + [UtilManager height:12], firstNameTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
    [passwordTextField setBackgroundColor:[UIColor clearColor]];
    [passwordTextField setTextAlignment:NSTextAlignmentCenter];
    [passwordTextField setCenter:CGPointMake(passwordTextField.center.x, passwordFieldBacgroundView.center.y)];
    [passwordTextField setBorderStyle:UITextBorderStyleNone];
    [passwordTextField setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [passwordTextField setTextColor:GRAY_TEXT_COLOR];
    passwordTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Password", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                 NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                 }
     ];
    [passwordTextField setSecureTextEntry:TRUE];
    [passwordTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [passwordTextField setReturnKeyType:UIReturnKeyDone];
    [passwordTextField setLeftViewMode:UITextFieldViewModeAlways];
    [passwordTextField setRightViewMode:UITextFieldViewModeWhileEditing];
    //[passwordTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [passwordTextField setDelegate:self];
    UIView *passwordFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
    [passwordTextField setLeftView:passwordFieldView];
    
    
    
    
    UIView *passwordTextViewContainer = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:-55],0,[UtilManager width:45],UITEXTFIELD_HEIGHT)];
    
    UIButton *showPasswordFieldView = [[UIButton alloc] initWithFrame:CGRectMake(0.,0,[UtilManager width:25],[UtilManager height:15])];
    [showPasswordFieldView setCenter:CGPointMake(showPasswordFieldView.center.x, passwordTextViewContainer.center.y)];
    [showPasswordFieldView setBackgroundColor:[UIColor clearColor]];
    [showPasswordFieldView setImage:[UIImage imageNamed:@"btn-show-password"] forState:UIControlStateNormal];
    [showPasswordFieldView addTarget:self action:@selector(showPassword) forControlEvents:UIControlEventTouchUpInside];
    [passwordTextViewContainer addSubview:showPasswordFieldView];
    [passwordTextField setRightView:passwordTextViewContainer];
    
    [scrollView addSubview:passwordTextField];
    
    
    
    UIImage *buttonShortImage = [UIImage imageNamed:@"btn-orange"];
    _saveButton = [[UIButton alloc] initWithFrame:CGRectMake(0., passwordTextField.frame.origin.y + passwordTextField.frame.size.height + [UtilManager height:20], buttonShortImage.size.width, buttonShortImage.size.height)];
    [_saveButton setCenter:CGPointMake(self.view.center.x, _saveButton.center.y)];
    [_saveButton setBackgroundImage:buttonShortImage forState:UIControlStateNormal];
    [_saveButton addTarget:self action:@selector(saveAction) forControlEvents:UIControlEventTouchUpInside];
    [_saveButton setTitle:NSLocalizedString(@"Save Details", nil) forState:UIControlStateNormal];
    [[_saveButton titleLabel] setFont:[UIFont fontWithName:SEMIBOLD_FONT size:BUTTON_FONT_SIZE]];
    [scrollView addSubview:_saveButton];
    
    [scrollView setContentSize:CGSizeMake(WIDTH, _saveButton.frame.origin.y + _saveButton.frame.size.height + [UtilManager height:30])];
    [scrollView setScrollEnabled:YES];
    [scrollView setDelegate:self];
    
    [self.view addSubview:scrollView];
    [self.view addSubview:_imageHeader];
}

- (void)showPassword {
    if(passwordTextField.isSecureTextEntry) {
        [passwordTextField setSecureTextEntry:FALSE];
    }
    else {
        [passwordTextField setSecureTextEntry:TRUE];
    }
}

- (NSDictionary *)userDictionary {
    NSDictionary *userDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[firstNameTextField.text urlencode],@"firstname",
                                    [lastNameTextField.text urlencode],@"lastname",
                                    mailTextField.text,@"email",
                                    _titleSelected,@"title",
                                    _ageSelected,@"age",
                                    [postcodeTextField.text urlencode],@"postcode",
                                    passwordTextField.text,@"password",
                                    telephoneTextField.text,@"mobile", nil];
    
    return userDictionary;
}

- (void)presentIssuePickerView:(id)sender{
    
    UIButton * senderBtn = (UIButton*)sender;
    
    _isAgePicker = (senderBtn.tag == 3);
    

    [self hideKeyboards];
    
    _pickerIssue.hidden = NO;
    _acceptPickerButton.hidden = NO;
    _backGRPicker.hidden = NO;
    
    [_backGRPicker removeFromSuperview];
    [self.view addSubview:_backGRPicker];
    [_pickerIssue removeFromSuperview];
    [self.view addSubview:_pickerIssue];
    [_acceptPickerButton removeFromSuperview];
    [self.view addSubview:_acceptPickerButton];
    [_pickerIssue reloadAllComponents];
    
    
}
-(void)dismmissPickerIssue {
    
    _pickerIssue.hidden = YES;
    _backGRPicker.hidden = YES;
    _acceptPickerButton.hidden = YES;
}

- (void)acceptPickerAction
{
    [self dismmissPickerIssue];
}

- (void)saveAction {
    if (![AppDelegate showAlertIfDisconnected]) {
        if([self checkFields]){
            [_saveButton setEnabled:FALSE];
            User *user = [self generateUserFromForm];
            [self registerNewUser:user profileImage:_userImage];
            [self hideKeyboards];
        }
    }
}

-(User *) generateUserFromForm {
    User *newUser = [[User alloc] init];
    [newUser setFirstName:firstNameTextField.text];
    [newUser setLastName:lastNameTextField.text];
    [newUser setPostcode:postcodeTextField.text];
    [newUser setMail:mailTextField.text];
    [newUser setPassword:passwordTextField.text];
    [newUser setPhoneNumber:telephoneTextField.text];
    [newUser setAge:_ageSelected];
    [newUser setTitle:_titleSelected];
    
    return newUser;
}

- (BOOL)checkFields {
    
    if(firstNameTextField.text.length == 0){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please enter your first name", nil)];
        return FALSE;
    }
    
    if(firstNameTextField.text.length < 3){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"First name cannot be less that 3 letters", nil)];
        return FALSE;
    }
    
    if(lastNameTextField.text.length == 0){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please enter your last name", nil)];
        return FALSE;
    }
    if(!_ageSelected){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please select your age", nil)];
        return FALSE;
    }
    
    if(!_titleSelected){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please select your title", nil)];
        return FALSE;
    }
    
    if(firstNameTextField.text.length < 2){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Last name cannot be less that 2 letters", nil)];
        return FALSE;
    }

    if(postcodeTextField.text.length == 0){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please enter your Zip Code", nil)];
        return FALSE;
    }
    
    if(postcodeTextField.text.length < 5){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Zip Code cannot be less that 5 letters", nil)];
        return FALSE;
    }
    
    if(mailTextField.text.length == 0){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please enter an email address", nil)];
        return FALSE;
    }
    
    if(![RegularExpressionsManager checkMail:mailTextField.text]){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please enter a valid email address", nil)];
        return FALSE;
    }
    
    if(passwordTextField.text.length == 0){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please enter a password", nil)];
        return FALSE;
    }
    
    if(passwordTextField.text.length < 6 ){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Passwords need to be a minimum of 6 characters. No special characters are allowed.", nil)];
        return FALSE;
    }
    
    if (![self validatePhone:telephoneTextField.text]) {
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please enter a valid phone number", nil)];
        return FALSE;
    }
    
    if(!_userImage){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please upload a profile picture", nil)];
        return FALSE;
    }
    
    return TRUE;
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^[0-9]{8,12}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:[phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""]];
}

- (void)hideKeyboards{
    [firstNameTextField resignFirstResponder];
    [lastNameTextField resignFirstResponder];
    [mailTextField resignFirstResponder];
    [postcodeTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    [telephoneTextField resignFirstResponder];
}

- (void)uploadPicture{
    [_profilePictureButton setUserInteractionEnabled:FALSE];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select an option.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Take a picture", nil)];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Select from gallery", nil)];
    
    [actionSheet showInView:self.view];
}

#pragma mark UIActionSheet Delegate Methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0){
        NSLog(@"Cancel Action Sheet Did Push");
        [_profilePictureButton setUserInteractionEnabled:TRUE];
    }
    else
    {
        if(buttonIndex == 1)
        {
            _picker = [[UIImagePickerController alloc] init];
            [_picker setAllowsEditing:YES];
            [_picker setDelegate:self];
            [_picker setSourceType:UIImagePickerControllerSourceTypeCamera];
            
            [self presentViewController:_picker animated:YES completion:^{
                [_profilePictureButton setUserInteractionEnabled:TRUE];
            }];
        }
        else if(buttonIndex == 2)
        {
            _picker = [[UIImagePickerController alloc] init];
            [_picker setAllowsEditing:YES];
            [_picker setDelegate:self];
            [_picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            [self presentViewController:_picker animated:YES completion:^{
                [_profilePictureButton setUserInteractionEnabled:TRUE];
            }];
            
            
        }
    }
}



#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if([textField isEqual:passwordTextField])
        [passwordTextField setSecureTextEntry:YES];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:passwordTextField]) {
        [passwordTextField resignFirstResponder];
    }
    if([textField isEqual:firstNameTextField]) {
        [lastNameTextField becomeFirstResponder];
    }
    if([textField isEqual:lastNameTextField]) {
        [postcodeTextField becomeFirstResponder];
    }
    if([textField isEqual:postcodeTextField]) {
        [telephoneTextField becomeFirstResponder];
    }
    if([textField isEqual:telephoneTextField]) {
        [mailTextField becomeFirstResponder];
    }
    if([textField isEqual:mailTextField]) {
        [passwordTextField becomeFirstResponder];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self dismmissPickerIssue];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == passwordTextField) {
        [scrollView setContentOffset:CGPointMake(0., -20.) animated:YES];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength;
    
    newLength = [textField.text length] + [string length] - range.length;
    
    if([textField isEqual:firstNameTextField])
        return (newLength > 30) ? NO : YES;
    if([textField isEqual:lastNameTextField])
        return (newLength > 30) ? NO : YES;
    if([textField isEqual:passwordTextField])
        return (newLength > 25) ? NO : YES;
    if([textField isEqual:mailTextField])
        return (newLength > 100) ? NO : YES;
    if([textField isEqual:postcodeTextField])
        return (newLength > 10) ? NO : YES;
    return YES;
}

#pragma mark WebServiceDelegateMethods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    NSString *response = (NSString *)object;
    [UtilManager presentAlertWithMessage:response];

    [_saveButton setEnabled:TRUE];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    [_saveButton setEnabled:TRUE];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    [_saveButton setEnabled:TRUE];
    
    if (webServiceType == WebServiceTypeTitle){
        NSLog(@"GTGTGT :: %@",[object class]);
        NSArray * titles = (NSArray*)object;
        _titlesPiker = [titles mutableCopy];
        
        _titleBtn.enabled=YES;
    }
    
    if (webServiceType == WebServiceTypeAgeGroupUser){
        NSLog(@"GTGTGT :: %@",object);
        NSArray * ages = (NSArray*)object;
        _agesPicker = [ages mutableCopy];
        _ageBtn.enabled=YES;
    }
}

#pragma mark pickerviewdelegate mthods

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(_isAgePicker){
        _ageSelected = _agesPicker[row];
        NSLog(@"Age:: %@",_agesPicker[row]);
        [_ageBtn setTitle:_agesPicker[row] forState:UIControlStateNormal];
        
    }else{
        _titleSelected = _titlesPiker[row];
        NSLog(@"title:: %@",_titlesPiker[row]);
         [_titleBtn setTitle:_titlesPiker[row] forState:UIControlStateNormal];
        
    }
    
}
#pragma mark UIPickerDataSource

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(_isAgePicker) {
        return _agesPicker.count;
    }else{
        return _titlesPiker.count;
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(_isAgePicker) {
        return [_agesPicker objectAtIndex:row];
    }else{
        return [_titlesPiker objectAtIndex:row];
    }
    
}

-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UIView *pickerCustomView = (id)view;
    UILabel *pickerViewLabel;
    UIImageView *pickerImageView;
    
    if (!pickerCustomView) {
        pickerCustomView= [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,
                                                                   [pickerView rowSizeForComponent:component].width - 10.0f, [pickerView rowSizeForComponent:component].height)];
        pickerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 35.0f, 35.0f)];
        pickerViewLabel= [[UILabel alloc] initWithFrame:CGRectMake(37.0f, -5.0f,
                                                                   [pickerView rowSizeForComponent:component].width - 10.0f, [pickerView rowSizeForComponent:component].height)];
        // the values for x and y are specific for my example
        [pickerCustomView addSubview:pickerImageView];
        [pickerCustomView addSubview:pickerViewLabel];
    }
    
    
    pickerCustomView.backgroundColor = GRAY_PIKCER_COLOR;
    UIView * separatorline = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    separatorline.backgroundColor = [UIColor whiteColor];
    pickerView.backgroundColor = GRAY_PIKCER_COLOR;
    [pickerView addSubview:separatorline];
    
    if(_isAgePicker){
       pickerViewLabel.text = _agesPicker[row];
    }else{
        pickerViewLabel.text = _titlesPiker[row];
    }
    
    // where therapyTypes[row] is a specific example from my code
    pickerViewLabel.font = [UIFont fontWithName:REGULAR_FONT size:20];
    
    return pickerCustomView;
}
- (void)keyboardWasHide:(NSNotification *)notification
{
    if(!_picker.view.hidden){
        
        scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
        [self.scrollView setContentOffset:bottomOffset animated:YES];
        
    }
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    int kbheight = MIN(keyboardSize.height,keyboardSize.width);
   // int width = MAX(keyboardSize.height,keyboardSize.width);
    
    
    if(!_picker.view.hidden){
        
        scrollView.contentInset = UIEdgeInsetsMake(0, 0, kbheight, 0);
        
    }
    //your other code here..........
}

#pragma mark UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    _userImage = [info objectForKey:UIImagePickerControllerEditedImage];
    [_profilePictureButton setImage:_userImage forState:UIControlStateNormal];
    [[_profilePictureButton layer] setMasksToBounds:YES];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
        [_profilePictureButton setUserInteractionEnabled:TRUE];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        [_profilePictureButton setUserInteractionEnabled:TRUE];
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    }];
}


@end
