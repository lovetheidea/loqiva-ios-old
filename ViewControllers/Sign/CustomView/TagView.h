//
//  TagView.h
//  loqiva
//
//  Created by Manuel Manzanera on 22/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TagViewDelegate;

typedef NS_ENUM(NSUInteger, TagsControlMode) {
    TagsControlModeEdit,
    TagsControlModeList,
};

@interface TagView : UIScrollView

@property (nonatomic, strong) NSMutableArray *tags;
@property (nonatomic, strong) UIColor *tagsBackgroundColor;
@property (nonatomic, strong) UIColor *tagsTextColor;
@property (nonatomic, strong) UIColor *tagsDeleteButtonColor;
@property (nonatomic, strong) NSString *tagPlaceholder;
@property (nonatomic) TagsControlMode mode;
@property (nonatomic) NSInteger widthOfView;

@property (assign, nonatomic) id<TagViewDelegate> tapDelegate;

- (id)initWithFrame:(CGRect)frame andTags:(NSArray *)tags withTagsControlMode:(TagsControlMode)mode;

- (void)addTag:(NSString *)tag;
- (void)reloadTagSubviews;
- (NSInteger)width;

@end

@protocol TagViewDelegate <NSObject>

- (void)tagView:(TagView *)tagView tagAtIndex:(NSInteger)index;

@end
