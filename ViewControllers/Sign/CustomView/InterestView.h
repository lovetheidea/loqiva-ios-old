//
//  InterestView.h
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InterestViewDelegate;

@interface InterestView : UIView

@property (nonatomic, assign) id<InterestViewDelegate>delegate;
@property (nonatomic, strong) UIButton *useAppButton;

+ (NSInteger)heightOfView;

@end

@protocol InterestViewDelegate <NSObject>

- (void)actionDidPush;

@end
