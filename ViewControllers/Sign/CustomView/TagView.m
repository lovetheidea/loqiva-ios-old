//
//  TagView.m
//  loqiva
//
//  Created by Manuel Manzanera on 22/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "TagView.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "Tag.h"
#import "Interest.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface TagView () <UITextFieldDelegate, UIGestureRecognizerDelegate>

@end

@implementation TagView {
    UITextField                 *tagInputField_;
    NSMutableArray              *tagSubviews_;
}

@synthesize tapDelegate;

- (instancetype)init {
    self = [super init];
    
    if (self != nil) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self != nil) {
        [self commonInit];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame andTags:(NSArray *)tags withTagsControlMode:(TagsControlMode)mode {
    self = [super initWithFrame:frame];
    
    if (self != nil) {
        [self commonInit];
        [self setTags:[[NSMutableArray alloc]initWithArray:tags]];
        [self setMode:mode];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self != nil) {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit {
    _tags = [NSMutableArray array];
    
    self.layer.cornerRadius = 0;
    [self setShowsHorizontalScrollIndicator:FALSE];
    [self setScrollEnabled:FALSE];
    
    tagSubviews_ = [NSMutableArray array];
    
    tagInputField_ = [[UITextField alloc] initWithFrame:self.frame];
    tagInputField_.layer.cornerRadius = RADIUS;
    tagInputField_.layer.borderColor = [UIColor lightGrayColor].CGColor;
    tagInputField_.delegate = self;
    tagInputField_.font = [UIFont fontWithName:REGULAR_FONT size:14];
    tagInputField_.autocorrectionType = UITextAutocorrectionTypeNo;
}

#pragma mark - layout stuff

- (void)layoutSubviews {
    [super layoutSubviews];
    CGSize contentSize = self.contentSize;
    CGRect frame = CGRectMake(0, 0, [UtilManager width:120], self.frame.size.height);
    CGRect tempViewFrame;
    NSInteger tagIndex = 0;
    for (UIView *view in tagSubviews_) {
        tempViewFrame = view.frame;
        NSInteger index = [tagSubviews_ indexOfObject:view];
        if (index != 0) {
            UIView *prevView = tagSubviews_[index - 1];
            tempViewFrame.origin.x = prevView.frame.origin.x + prevView.frame.size.width + [UtilManager width:5];
        } else {
            tempViewFrame.origin.x = [UtilManager width:20];
        }
        tempViewFrame.origin.y = frame.origin.y;
        view.frame = tempViewFrame;
        
        if (_mode == TagsControlModeList) {
            view.tag = tagIndex;
            
            UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gestureAction:)];
            [tapRecognizer setNumberOfTapsRequired:1];
            [tapRecognizer setDelegate:self];
            [view setUserInteractionEnabled:YES];
            [view addGestureRecognizer:tapRecognizer];
        }
        
        tagIndex++;
    }
    
    if (_mode == TagsControlModeEdit) {
        frame = tagInputField_.frame;
        frame.size.height = self.frame.size.height;
        frame.origin.y = 0;
        
        if (tagSubviews_.count == 0) {
            frame.origin.x = [UtilManager width:20];
        } else {
            UIView *view = tagSubviews_.lastObject;
            frame.origin.x = view.frame.origin.x + view.frame.size.width + [UtilManager width:10];
        }
        
        if (self.frame.size.width - tagInputField_.frame.origin.x > [UtilManager width:120]) {
            frame.size.width = self.frame.size.width - frame.origin.x - 12;
        } else {
            frame.size.width = [UtilManager width:120];
        }
        tagInputField_.frame = frame;
    } else {
        UIView *lastTag = tagSubviews_.lastObject;
        if (lastTag != nil) {
            frame = lastTag.frame;
        } else {
            frame.origin.x = 7;
        }
    }
    
    contentSize.width = frame.origin.x + frame.size.width;
    contentSize.height = self.frame.size.height;
    
    self.contentSize = contentSize;
    
    tagInputField_.placeholder = (_tagPlaceholder == nil) ? @"" : _tagPlaceholder;
}

- (void)addTag:(NSString *)tag {
    for (NSString *oldTag in _tags) {
        if ([oldTag isEqualToString:tag]) {
            return;
        }
    }
    
    [_tags addObject:tag];
    [self reloadTagSubviews];
    
    CGSize contentSize = self.contentSize;
    CGPoint offset = self.contentOffset;
    
    if (contentSize.width > self.frame.size.width) {
        if (_mode == TagsControlModeEdit) {
            offset.x = tagInputField_.frame.origin.x + tagInputField_.frame.size.width - self.frame.size.width;
        } else {
            UIView *lastTag = tagSubviews_.lastObject;
            offset.x = lastTag.frame.origin.x + lastTag.frame.size.width - self.frame.size.width;
        }
    } else {
        offset.x = 0;
    }
    
    self.contentOffset = offset;
}

- (NSInteger)width {
    return self.widthOfView;
}

- (void)reloadTagSubviews {
    
    for (UIView *view in tagSubviews_) {
        [view removeFromSuperview];
    }
    
    [tagSubviews_ removeAllObjects];
    
    UIColor *tagBackgrounColor = _tagsBackgroundColor != nil ? _tagsBackgroundColor : [UIColor orangeButtonColor];
    UIColor *tagDeleteButtonColor = _tagsDeleteButtonColor != nil ? _tagsDeleteButtonColor : [UIColor whiteColor];
    
    for (Interest *tag in _tags) {
        
        NSString *tagCustomName = [NSString stringWithFormat:@"  %@",tag.name];
        
        float width = [tagCustomName boundingRectWithSize:CGSizeMake(3000, tagInputField_.frame.size.height)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]}
                                        context:nil].size.width;
        
        UIView *tagView = [[UIView alloc] initWithFrame:CGRectMake(tagInputField_.frame.origin.x, tagInputField_.frame.origin.y, tagInputField_.frame.size.width + [UtilManager width:20], tagInputField_.frame.size.height)];
        CGRect tagFrame = tagView.frame;
        tagView.layer.cornerRadius = RADIUS;
        tagFrame.origin.y = tagInputField_.frame.origin.y;
        tagView.backgroundColor = tagBackgrounColor;
        
        UILabel *tagLabel = [[UILabel alloc] init];
        CGRect labelFrame = tagLabel.frame;
        tagLabel.font = [UIFont fontWithName:REGULAR_FONT size:11];
        labelFrame.size.width = width;
        labelFrame.size.height = tagInputField_.frame.size.height;
        tagLabel.text = tagCustomName;
        tagLabel.textAlignment = NSTextAlignmentCenter;
        tagLabel.clipsToBounds = YES;
        tagLabel.layer.cornerRadius = RADIUS;
        
        UIButton *deleteTagButton = [[UIButton alloc] initWithFrame:tagInputField_.frame];
        CGRect buttonFrame = deleteTagButton.frame;
        [deleteTagButton.titleLabel setFont:tagInputField_.font];
        [deleteTagButton addTarget:self action:@selector(deleteTagButton:) forControlEvents:UIControlEventTouchUpInside];
        buttonFrame.size.width = 35;
        buttonFrame.size.height = 26;
        [deleteTagButton setTag:tagSubviews_.count];
        [deleteTagButton setTitleColor:tagDeleteButtonColor forState:UIControlStateNormal];
        buttonFrame.origin.y = 4;
        buttonFrame.origin.x = labelFrame.size.width - 2;
            
        deleteTagButton.frame = buttonFrame;
        tagFrame.size.width = labelFrame.size.width + buttonFrame.size.width;
        [tagView addSubview:deleteTagButton];
        labelFrame.origin.x = 3;
        
        if([tag.status isEqualToString:@"Off"])
        {
            tagLabel.textColor = [UIColor whiteColor];
            tagView.backgroundColor = [UIColor InterestTagBGColour];
            [deleteTagButton setTitle:@"" forState:UIControlStateNormal];
            [deleteTagButton setImage:[UIImage imageNamed:@"image-tag-unselected"] forState:UIControlStateNormal];
        }
        else
        {
            tagLabel.textColor = [UIColor InterestTagNameSelected];
            tagView.backgroundColor = [UIColor whiteColor];
            [deleteTagButton setTitle:@"" forState:UIControlStateNormal];
            [deleteTagButton setImage:[UIImage imageNamed:@"icon-tag-selected"] forState:UIControlStateNormal];
        }
        
        [tagView addSubview:tagLabel];
        labelFrame.origin.y = 0;
        UIView *lastView = tagSubviews_.lastObject;
        
        if (lastView != nil) {
            tagFrame.origin.x = lastView.frame.origin.x + lastView.frame.size.width + 4;
        }
        
        tagLabel.frame = labelFrame;
        tagView.frame = tagFrame;
        [tagSubviews_ addObject:tagView];
        [self addSubview:tagView];
        
        if (tagView.frame.origin.x + tagView.frame.size.width + 30 > self.widthOfView) {
            self.widthOfView = tagView.frame.origin.x + tagView.frame.size.width + 30;
        }
    }
    
    
    if (_mode == TagsControlModeEdit) {
        if (tagInputField_.superview == nil) {
            [self addSubview:tagInputField_];
        }
        CGRect frame = tagInputField_.frame;
        if (tagSubviews_.count == 0) {
            frame.origin.x = [UtilManager width:20];
        } else {
            UIView *view = tagSubviews_.lastObject;
            frame.origin.x = view.frame.origin.x + view.frame.size.width + [UtilManager width:10];
        }
        tagInputField_.frame = frame;
        
    } else {
        if (tagInputField_.superview != nil) {
            [tagInputField_ removeFromSuperview];
        }
    }
    
    [self setNeedsLayout];
}

#pragma mark - buttons handlers

- (void)deleteTagButton:(UIButton *)sender {
    UIView *view = sender.superview;
    NSInteger index = [tagSubviews_ indexOfObject:view];
    [tapDelegate tagView:self tagAtIndex:index];
}

- (void)tagButtonPressed:(id)sender {
    UIButton *button = sender;
    
    tagInputField_.text = @"";
    [self addTag:button.titleLabel.text];
}

#pragma mark - textfield stuff

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.text.length > 0) {
        NSString *tag = textField.text;
        textField.text = @"";
        [self addTag:tag];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *resultingString;
    NSString *text = textField.text;
    
    
    if (string.length == 1 && [string rangeOfCharacterFromSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]].location != NSNotFound) {
        return NO;
    } else {
        if (!text || [text isEqualToString:@""]) {
            resultingString = string;
        } else {
            if (range.location + range.length > text.length) {
                range.length = text.length - range.location;
            }
            
            resultingString = [textField.text stringByReplacingCharactersInRange:range
                                                                      withString:string];
        }
        
        NSArray *components = [resultingString componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]];
        
        if (components.count > 2) {
            for (NSString *component in components) {
                if (component.length > 0 && [component rangeOfCharacterFromSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]].location == NSNotFound) {
                    [self addTag:component];
                    break;
                }
            }
            
            return NO;
        }
        
        return YES;
    }
}

#pragma mark - other

- (void)setMode:(TagsControlMode)mode {
    _mode = mode;
}

- (void)setTags:(NSMutableArray *)tags {
    _tags = tags;
}

- (void)setPlaceholder:(NSString *)tagPlaceholder {
    _tagPlaceholder = tagPlaceholder;
}

- (void)gestureAction:(id)sender {
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
    [tapDelegate tagView:self tagAtIndex:tapRecognizer.view.tag];
    
}


@end
