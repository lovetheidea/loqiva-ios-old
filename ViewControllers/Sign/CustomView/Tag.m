//
//  Tag.m
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "Tag.h"
#import "loqiva-Swift.h"

@implementation Tag

+ (NSMutableArray *)getFirstTags{
    Tag *freeFlightTag = [[Tag alloc] init];
    [freeFlightTag setTitle:NSLocalizedString(@"Climbing", nil)];
    [freeFlightTag setIsSelected:FALSE];
    
    Tag *aplineTag = [[Tag alloc] init];
    [aplineTag setTitle:NSLocalizedString(@"Food", nil)];
    [aplineTag setIsSelected:FALSE];
    
    Tag *mountainBikeTag = [[Tag alloc] init];
    [mountainBikeTag setTitle:NSLocalizedString(@"History", nil)];
    [mountainBikeTag setIsSelected:FALSE];
    
    Tag *healthTag = [[Tag alloc] init];
    [healthTag setTitle:NSLocalizedString(@"Health", nil)];
    [healthTag setIsSelected:FALSE];
    
    NSMutableArray *tags = [[NSMutableArray alloc] initWithArray:@[freeFlightTag, aplineTag, mountainBikeTag,healthTag]];
    
    return tags;
}

+ (NSMutableArray *)getSecondsTags{
    Tag *freeFlightTag = [[Tag alloc] init];
    [freeFlightTag setTitle:NSLocalizedString(@"Freeflight", nil)];
    [freeFlightTag setIsSelected:FALSE];
    
    Tag *aplineTag = [[Tag alloc] init];
    [aplineTag setTitle:NSLocalizedString(@"Apline", nil)];
    [aplineTag setIsSelected:FALSE];
    
    Tag *mountainBikeTag = [[Tag alloc] init];
    [mountainBikeTag setTitle:NSLocalizedString(@"Mountain Bike", nil)];
    [mountainBikeTag setIsSelected:FALSE];
    
    NSMutableArray *tags = [[NSMutableArray alloc] initWithArray:@[freeFlightTag, aplineTag, mountainBikeTag]];
    
    return tags;
}

+ (NSMutableArray *)getThirdsTags{
    
    Tag *freeFlightTag = [[Tag alloc] init];
    [freeFlightTag setTitle:NSLocalizedString(@"Film", nil)];
    [freeFlightTag setIsSelected:FALSE];
    
    Tag *aplineTag = [[Tag alloc] init];
    [aplineTag setTitle:NSLocalizedString(@"Hiking", nil)];
    [aplineTag setIsSelected:FALSE];
    
    Tag *mountainBikeTag = [[Tag alloc] init];
    [mountainBikeTag setTitle:NSLocalizedString(@"Theater", nil)];
    [mountainBikeTag setIsSelected:FALSE];
    
    NSMutableArray *tags = [[NSMutableArray alloc] initWithArray:@[freeFlightTag, aplineTag, mountainBikeTag]];
    
    return tags;
}

+ (NSMutableArray *)getFoursTags{
    
    Tag *freeFlightTag = [[Tag alloc] init];
    [freeFlightTag setTitle:NSLocalizedString(@"Cycling", nil)];
    [freeFlightTag setIsSelected:FALSE];
    
    Tag *aplineTag = [[Tag alloc] init];
    [aplineTag setTitle:NSLocalizedString(@"Freestyle", nil)];
    [aplineTag setIsSelected:FALSE];
    
    Tag *mountainBikeTag = [[Tag alloc] init];
    [mountainBikeTag setTitle:NSLocalizedString(@"Outdoors", nil)];
    [mountainBikeTag setIsSelected:FALSE];
    
    NSMutableArray *tags = [[NSMutableArray alloc] initWithArray:@[freeFlightTag, aplineTag, mountainBikeTag]];
    
    return tags;
}

+ (NSMutableArray *)getFiveTags{
    Tag *freeFlightTag = [[Tag alloc] init];
    [freeFlightTag setTitle:NSLocalizedString(@"Endurance", nil)];
    [freeFlightTag setIsSelected:FALSE];
    
    Tag *aplineTag = [[Tag alloc] init];
    [aplineTag setTitle:NSLocalizedString(@"Music", nil)];
    [aplineTag setIsSelected:FALSE];
    
    Tag *mountainBikeTag = [[Tag alloc] init];
    [mountainBikeTag setTitle:NSLocalizedString(@"Freeflight", nil)];
    [mountainBikeTag setIsSelected:FALSE];
    
    NSMutableArray *tags = [[NSMutableArray alloc] initWithArray:@[freeFlightTag, aplineTag, mountainBikeTag]];
    
    return tags;
}

@end
