//
//  SignNavView.m
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "SignNavView.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@implementation SignNavView

@synthesize delegate, titleLabel;

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
#ifdef LOQIVA
        [self setBackgroundColor:[UIColor orangeAppColor]];
#elif OLDSEAPORT
       [self setBackgroundColor:[UIColor clearColor]];
#endif
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:28], [UtilManager width:310], [UtilManager height:42])];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setNumberOfLines:0];
        [titleLabel setTextAlignment:NSTextAlignmentLeft];
        [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:20]];
        
        [self addSubview:titleLabel];
        
        _backButtonImage = [[UIImageView alloc] initWithFrame:CGRectMake(NAV_SLIDE_ORIGIN_X, [UtilManager height:40], [UtilManager width:12], [UtilManager height:22])];
        [_backButtonImage setImage:[UIImage imageNamed:@"btn-back"]];
        [_backButtonImage setCenter:CGPointMake(_backButtonImage.center.x, titleLabel.center.y)];
        
        _backButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40.], [UtilManager width:40])];
        [_backButton setCenter:_backButtonImage.center];
        [_backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_backButtonImage];
        [self addSubview:_backButton];
        
        [_backButton setHidden:YES];
        [_backButtonImage setHidden:YES];
        
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., self.frame.size.height - 2, WIDTH, [UtilManager height:2])];
        [separatorView setBackgroundColor:HEADER_SEPARATOR_COLOR];
        [self addSubview:separatorView];
    }
    
    return self;
}

- (void)backAction{
    if([delegate respondsToSelector:@selector(backAction)])
        [delegate backAction];
}

@end
