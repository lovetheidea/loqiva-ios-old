//
//  InterestView.m
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "InterestView.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "TagView.h"
#import "Tag.h"
#import "Interest.h"
#import "AppContext.h"
#import "UILabel+Extra.h"
#import "UIColor+Helper.h"
#import "WebServiceManager.h"
#import "loqiva-Swift.h"

@interface InterestView ()<TagViewDelegate, WebServiceManagerDelegate>

@property (nonatomic, strong) NSMutableArray *countOfItems;

@end

@implementation InterestView

@synthesize delegate;

+ (NSInteger)heightOfView {
    NSArray *interests = [[AppContext sharedInstance] interests];
    NSInteger sections = [interests count] / 3;
    if ([interests count] % 3 != 0) {
        sections += 1;
    }
    
    NSInteger height = sections * 40 + 280;
    return height;
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor orangeAppColor]];
        
        UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20.], [UtilManager height:40], [UtilManager width:290], [UtilManager height:120])];
        [infoLabel setCenter:CGPointMake(self.frame.size.width / 2, infoLabel.center.y)];
        
        
        [infoLabel setAttributedText: NSLocalizedString(@"Tap a few things you like to enrich your app experience", nil) withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:WHITE_TEXT_COLOR andKern:TITLE_FONT_KERN withLineBreak:NO];
        

        [infoLabel setNumberOfLines:0];
        [infoLabel setBackgroundColor:[UIColor clearColor]];
        [infoLabel setAdjustsFontSizeToFitWidth:YES];
        
        [self addSubview:infoLabel];
        
        self.countOfItems = [@[] mutableCopy];
        
        NSArray *interests = [[AppContext sharedInstance] interests];
        for (Interest *interest in interests) {
            if ([interest.status isEqualToString:@"On"]) {
                [self.countOfItems addObject:interest];
            }
        }
        
        NSInteger sections = [interests count] / 3;
        if ([interests count] % 3 != 0) {
            sections += 1;
        }
        
        
        UIScrollView *tagScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0., infoLabel.frame.origin.y + infoLabel.frame.size.height + [UtilManager height:30.], self.frame.size.width, [UtilManager height:50.] * sections)];
        
        CGFloat widthScrollView = self.frame.size.width;
        
        for(int i = 0; i < sections; i++){
            NSInteger xPosition = [UtilManager width:0];
            NSInteger yPosition = ([UtilManager height:40] * i) + ([UtilManager height:10.] * i);
            
            TagView *tagView = [[TagView alloc] initWithFrame:CGRectMake(xPosition, yPosition, self.frame.size.width, [UtilManager height:40])];
            NSMutableArray *secondTags = [[NSMutableArray alloc] init];
            NSInteger startPosition = 3 * i;
            [secondTags addObject:[interests objectAtIndex:startPosition]];
            if (startPosition + 1 < [interests count]) {
                [secondTags addObject:[interests objectAtIndex:startPosition + 1]];
            }
            
            if (startPosition + 2 < [interests count]) {
                [secondTags addObject:[interests objectAtIndex:startPosition + 2]];
            }
            
            tagView.tags = [secondTags mutableCopy];
            [tagView reloadTagSubviews];
            [tagView setTagsBackgroundColor:[UIColor orangeButtonColor]];
            [tagView setTagsTextColor:[UIColor whiteColor]];
            [tagView setTagsDeleteButtonColor:[UIColor whiteColor]];
            tagView.mode = TagsControlModeList;
            [tagView setTapDelegate:self];
            tagView.frame = CGRectMake(tagView.frame.origin.x, tagView.frame.origin.y, [tagView widthOfView], tagView.frame.size.height);
            
            [tagScrollView addSubview:tagView];
            
            if ([tagView widthOfView] > widthScrollView) {
                widthScrollView = [tagView widthOfView];
            }
        }
        
        [tagScrollView setContentSize:CGSizeMake(widthScrollView, [UtilManager height:50] * sections)];
        [self addSubview:tagScrollView];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20.], tagScrollView.frame.origin.y + tagScrollView.frame.size.height + [UtilManager height:0], WIDTH - 2 * [UtilManager width:20.], 1)];
        [lineView setBackgroundColor:[UIColor whiteColor]];
        [lineView setAlpha:0.8];
        [self addSubview:lineView];
        
        _useAppButton = [[UIButton alloc] initWithFrame:CGRectMake(0.,lineView.frame.origin.y + [UtilManager height:20], [UtilManager width:240.], [UtilManager width:50])];
        [_useAppButton setCenter:CGPointMake(self.center.x, _useAppButton.center.y)];
        [_useAppButton setBackgroundImage:[UIImage imageNamed:@"btn-orange"] forState:UIControlStateNormal];
        [_useAppButton setTitle:NSLocalizedString(@"Use the app", nil) forState:UIControlStateNormal];
        [[_useAppButton titleLabel] setFont:[UIFont fontWithName:REGULAR_FONT size:BUTTON_FONT_SIZE]];
        [_useAppButton addTarget:self action:@selector(useAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_useAppButton];
    }
    
    return self;
}

- (void)useAction {
    if (self.countOfItems.count < 5) {
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please add 5 items you are interested in", nil)];
    }
    else {
        if([delegate respondsToSelector:@selector(actionDidPush)]) {
            [delegate actionDidPush];
        }
    }
}

#pragma mark TapViewDelegate Methods

- (void)tagView:(TagView *)tagView tagAtIndex:(NSInteger)index{
    
    for(NSInteger i = 0; i<=tagView.tags.count ; i++){
        if(index == i){
            Interest *pushTag = [tagView.tags objectAtIndex:index];
            
            if([pushTag.status isEqualToString:@"Off"])
            {
                [WebServiceManager saveInterestWithId:pushTag.interestId andStatus:[NSNumber numberWithInt:1] andDelegate:self];
                [pushTag setStatus:@"On"];
                [self.countOfItems addObject:pushTag];
            }
            else
            {
                [WebServiceManager saveInterestWithId:pushTag.interestId andStatus:[NSNumber numberWithInt:0] andDelegate:self];
                [pushTag setStatus:@"Off"];
                [self.countOfItems removeObject:pushTag];
            }
            
            
            [tagView.tags replaceObjectAtIndex:index withObject:pushTag];
        }
    }
    
    [tagView reloadTagSubviews];
}

#pragma mark WebServiceDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{

}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
}

@end
