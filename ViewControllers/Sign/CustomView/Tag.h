//
//  Tag.h
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tag : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) BOOL isSelected;

+ (NSMutableArray *)getFirstTags;
+ (NSMutableArray *)getSecondsTags;
+ (NSMutableArray *)getThirdsTags;
+ (NSMutableArray *)getFoursTags;
+ (NSMutableArray *)getFiveTags;


@end
