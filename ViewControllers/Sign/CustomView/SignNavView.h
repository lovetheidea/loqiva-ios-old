//
//  SignNavView.h
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SignNavViewDelegate;

@interface SignNavView : UIView

@property (nonatomic, assign) id<SignNavViewDelegate>delegate;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIImageView *backButtonImage;

@end

@protocol  SignNavViewDelegate <NSObject>

- (void)backAction;

@end