//
//  ForgotViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 10/6/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ForgotViewController.h"
#import "ForgotInfoViewController.h"
#import "WebServiceManager.h"
#import "MenuHomeViewController.h"
#import "AppDelegate.h"
#import "RegularExpressionsManager.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface ForgotViewController ()<UITextFieldDelegate, WebServiceManagerDelegate>

@property (nonatomic, strong) UITextField *mailTextField;

@end

@implementation ForgotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
//#ifdef LOQIVA
    [self.view setBackgroundColor:[UIColor orangeAppColor]];
//#elif OLDSEAPORT
//    [self.view setBackgroundColor:[UIColor whiteColor]];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image-blurred-bg"]];
//    [self.view addSubview:imageView];
//#endif
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20.], [UtilManager height:120], [UtilManager width:340], [UtilManager height:128])];
    [infoLabel setCenter:CGPointMake(self.view.center.x, infoLabel.center.y)];
    [infoLabel setTextAlignment:NSTextAlignmentCenter];
    [infoLabel setNumberOfLines:0];
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    [infoLabel setAttributedText:NSLocalizedString(@"To reset your\npassword, please enter\nyour email address", nil) withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:WHITE_TEXT_COLOR andKern:TITLE_FONT_KERN withLineBreak:NO];
    [infoLabel setAdjustsFontSizeToFitWidth:YES];
    
    [self.view addSubview:infoLabel];
    
    UIImageView *mailTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., [UtilManager height:272], [UtilManager width:330], [UtilManager height:50])];
    [mailTextFieldBacgroundView setCenter:CGPointMake(self.view.center.x, mailTextFieldBacgroundView.center.y)];
    [mailTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
    [self.view addSubview:mailTextFieldBacgroundView];
    
    _mailTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:55.], [UtilManager height:252], [UtilManager width:265], UITEXTFIELD_HEIGHT)];
    [_mailTextField setCenter:CGPointMake(self.view.center.x, mailTextFieldBacgroundView.center.y)];
    [_mailTextField setBorderStyle:UITextBorderStyleNone];
    [_mailTextField setTextAlignment:NSTextAlignmentCenter];
    [_mailTextField setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [_mailTextField setTextColor:GRAY_TEXT_COLOR];
    _mailTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email address", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                 NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                 }
     ];
    [_mailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_mailTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_mailTextField setReturnKeyType:UIReturnKeyDone];
    [_mailTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_mailTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_mailTextField setDelegate:self];
    UIView *mailTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
    [_mailTextField setLeftView:mailTextFieldView];
    [self.view addSubview:_mailTextField];
    
    UIImage *resetImage = [UIImage imageNamed:@"btn-reset"];
    
    UIButton *resetButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:70], [UtilManager height:346], [UtilManager width:240], [UtilManager height:50])];
    [resetButton setCenter:CGPointMake(self.view.center.x, resetButton.center.y)];
    [resetButton setBackgroundImage:resetImage forState:UIControlStateNormal];
    [resetButton addTarget:self action:@selector(resetAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:resetButton];
    
    UILabel *skipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:546], WIDTH, [UtilManager height:36])];
    [skipLabel setAttributedText:NSLocalizedString(@"Skip directly to the app", nil) withLineHeight:22 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:SEMIBOLD_FONT size:16] andColor:[UIColor whiteColor] andKern:-0.5 withLineBreak:NO];
    [skipLabel setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(skipAction)];
    [skipLabel addGestureRecognizer:tapGesture];
    
    [self.view addSubview:skipLabel];
    UILabel *signInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:595], infoLabel.frame.size.width, [UtilManager height:40])];
    [signInfoLabel setCenter:CGPointMake(self.view.center.x, signInfoLabel.center.y)];
    [signInfoLabel setTextAlignment:NSTextAlignmentCenter];
    [signInfoLabel setText:NSLocalizedString(@"Signing up to an account will create\n a personal app experience tailored to you", nil)];
    [signInfoLabel setTextColor:[UIColor whiteColor]];
    [signInfoLabel setNumberOfLines:0];
    [signInfoLabel setFont:[UIFont fontWithName:LIGHT_FONT size:12]];
    
    [self.view addSubview:signInfoLabel];
}

- (void)resetAction{
    if (![AppDelegate showAlertIfDisconnected]) {
        [WebServiceManager forgottenPassWord:_mailTextField.text andDelegate:self];
    }
}

- (void)skipAction{
    if (![AppDelegate showAlertIfDisconnected]) {
        [WebServiceManager createTempUser:self];
    }
}

- (void)presentHomeViewController{
    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:homeViewController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
     }];
}

- (BOOL)checkFields{
    if(_mailTextField.text.length == 0){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Email cannot be empty", nil)];
        return FALSE;
    }
    
    if(![RegularExpressionsManager checkMail:_mailTextField.text]){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please, insert a valid email", nil)];
        return FALSE;
    }
    return TRUE;
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength;
    
    newLength = [textField.text length] + [string length] - range.length;
    if([textField isEqual:_mailTextField])
        return (newLength > 100) ? NO : YES;
    return YES;
}


#pragma mark WebServiceDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{

}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    if (webServiceType == WebServiceTypeTempUser){
        [self presentHomeViewController];
    } else {
        
        NSError *jsonError;
        NSDictionary *responseForgottemn = [NSJSONSerialization JSONObjectWithData:object options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if([[responseForgottemn objectForKey:@"errorcode"] integerValue] == 104){
            [UtilManager presentAlertWithMessage:[responseForgottemn objectForKey:@"errormsg"]];
        }else{
            ForgotInfoViewController *forgotInfoViewcontroller = [[ForgotInfoViewController alloc] init];
            [self.navigationController pushViewController:forgotInfoViewcontroller animated:YES];
        }
        

    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
}

@end
