//
//  SignNavController.h
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignNavView.h"

@interface SignNavController : UINavigationController

@property (nonatomic, strong) SignNavView *sigNavView;
@property(nonatomic) BOOL comesFromApp;

@end
