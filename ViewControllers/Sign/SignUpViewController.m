//
//  SignUpViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "SignUpViewController.h"
#import "RegisterUserViewController.h"
#import "InterestsViewController.h"
#import "MenuHomeViewController.h"
#import "ForgotViewController.h"
#import "AFNetworkReachabilityManager.h"
#import "RegularExpressionsManager.h"
#import "WebServiceManager.h"
#import "AppDelegate.h"
#import "AppContext.h"
#import "UserDefaultManager.h"
#import "UILabel+Extra.h"
#import "ErrorManager.h"
#import "LocationManager.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface SignUpViewController ()<WebServiceManagerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UITextField *mailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) NSString *usertoken;

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
}

- (void)configureView{
//#ifdef LOQIVA
    [self.view setBackgroundColor:[UIColor orangeAppColor]];
//#elif OLDSEAPORT
//    [self.view setBackgroundColor:[UIColor whiteColor]];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image-blurred-bg"]];
//    [self.view addSubview:imageView];
//#endif
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20.], [UtilManager height:93], [UtilManager width:340], [UtilManager height:108])];
    [infoLabel setCenter:CGPointMake(self.view.center.x, infoLabel.center.y)];
    [infoLabel setTextAlignment:NSTextAlignmentCenter];
    [infoLabel setNumberOfLines:0];
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    [infoLabel setAdjustsFontSizeToFitWidth:YES];
    [infoLabel setAttributedText:NSLocalizedString(@"Please enter your\n details", nil) withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:[UIColor whiteColor] andKern:TITLE_FONT_KERN withLineBreak:NO];
    
    [self.view addSubview:infoLabel];
    
    UIImageView *mailTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., [UtilManager height:226], [UtilManager width:330], [UtilManager height:50])];
    [mailTextFieldBacgroundView setCenter:CGPointMake(self.view.center.x, mailTextFieldBacgroundView.center.y)];
    [mailTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
    [self.view addSubview:mailTextFieldBacgroundView];
    
    _mailTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:55.], infoLabel.frame.origin.y + infoLabel.frame.size.height + [UtilManager height:12], [UtilManager width:265], UITEXTFIELD_HEIGHT)];
    [_mailTextField setCenter:CGPointMake(self.view.center.x, mailTextFieldBacgroundView.center.y)];
    [_mailTextField setBorderStyle:UITextBorderStyleNone];
    [_mailTextField setTextAlignment:NSTextAlignmentCenter];
    [_mailTextField setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [_mailTextField setTextColor:GRAY_TEXT_COLOR];
    _mailTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email address", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                 NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                 }
     ];
    [_mailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_mailTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_mailTextField setReturnKeyType:UIReturnKeyNext];
    [_mailTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_mailTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_mailTextField setDelegate:self];
    UIView *mailTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
    [_mailTextField setLeftView:mailTextFieldView];
    [self.view addSubview:_mailTextField];
    
    UIImageView *passwordFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:330  ], [UtilManager height:50])];
    [passwordFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
    [passwordFieldBacgroundView setCenter:CGPointMake(self.view.center.x, mailTextFieldBacgroundView.frame.origin.y + mailTextFieldBacgroundView.frame.size.height + [UtilManager height:12] + mailTextFieldBacgroundView.frame.size.height/2)];
    [self.view addSubview:passwordFieldBacgroundView];
    
    _passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(_mailTextField.frame.origin.x, _mailTextField.frame.origin.y + _mailTextField.frame.size.height + [UtilManager height:12], _mailTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
    [_passwordTextField setBackgroundColor:[UIColor clearColor]];
    [_passwordTextField setTextAlignment:NSTextAlignmentCenter];
    [_passwordTextField setCenter:CGPointMake(_passwordTextField.center.x, passwordFieldBacgroundView.center.y)];
    [_passwordTextField setBorderStyle:UITextBorderStyleNone];
    [_passwordTextField setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [_passwordTextField setTextColor:GRAY_TEXT_COLOR];
    _passwordTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Password", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                 NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                 }
     ];
    [_passwordTextField setSecureTextEntry:TRUE];
    [_passwordTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_passwordTextField setReturnKeyType:UIReturnKeyDone];
    [_passwordTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_passwordTextField setRightViewMode:UITextFieldViewModeWhileEditing];
    //[passwordTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_passwordTextField setDelegate:self];
    UIView *passwordFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
    [_passwordTextField setLeftView:passwordFieldView];
    
    UIView *passwordTextViewContainer = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:-55],0,[UtilManager width:45],UITEXTFIELD_HEIGHT)];
    
    UIButton *showPasswordFieldView = [[UIButton alloc] initWithFrame:CGRectMake(0.,0,[UtilManager width:25],[UtilManager height:15])];
    [showPasswordFieldView setCenter:CGPointMake(showPasswordFieldView.center.x, passwordTextViewContainer.center.y)];
    [showPasswordFieldView setBackgroundColor:[UIColor clearColor]];
    [showPasswordFieldView setImage:[UIImage imageNamed:@"btn-show-password"] forState:UIControlStateNormal];
    [showPasswordFieldView addTarget:self action:@selector(showPassword) forControlEvents:UIControlEventTouchUpInside];
    [passwordTextViewContainer addSubview:showPasswordFieldView];
    [_passwordTextField setRightView:passwordTextViewContainer];
    
    [self.view addSubview:_passwordTextField];
    
    UIImage *loginImage = [UIImage imageNamed:@"btn-login"];
    
    UIButton *loginButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:70], [UtilManager height:355], [UtilManager width:240], [UtilManager height:50])];
    [loginButton setCenter:CGPointMake(self.view.center.x, loginButton.center.y)];
    [loginButton setBackgroundImage:loginImage forState:UIControlStateNormal];
    [loginButton addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:loginButton];
    
    UILabel *forgottenLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:420], WIDTH, [UtilManager height:24])];
    [forgottenLabel setCenter:CGPointMake(self.view.center.x, forgottenLabel.center.y)];
    [forgottenLabel setAttributedText:NSLocalizedString(@"Forgotten Password?", nil) withLineHeight:20 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:SEMIBOLD_FONT size:15] andColor:[UIColor whiteColor] andKern:-0.47 withLineBreak:NO];
    [forgottenLabel setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapGestureRecoggnizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(forgottenAction)];
    [forgottenLabel addGestureRecognizer:tapGestureRecoggnizer];
    
    [self.view addSubview:forgottenLabel];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, [UtilManager height:410], [UtilManager width:315], 1)];
    [separatorView setBackgroundColor:[UIColor whiteColor]];
    [separatorView setCenter:CGPointMake(self.view.center.x, separatorView.center.y)];
    
    //[self.view addSubview:separatorView];
    
    UIImage *signUpImage = [UIImage imageNamed:@"btn-signup-new"];
    
    UIButton *signUpButton = [[UIButton alloc] initWithFrame:CGRectMake(0., [UtilManager height:483], [UtilManager width:240], [UtilManager height:50])];
    [signUpButton setCenter:CGPointMake(self.view.center.x, signUpButton.center.y)];
    [signUpButton setBackgroundImage:signUpImage forState:UIControlStateNormal];
    [signUpButton addTarget:self action:@selector(mailAction) forControlEvents:UIControlEventTouchUpInside];
    
    //[self.view addSubview:signUpButton];
    
    UILabel *signUpLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:518], WIDTH, [UtilManager height:24])];
    [signUpLabel setCenter:CGPointMake(self.view.center.x, signUpLabel.center.y)];
    [signUpLabel setAttributedText:NSLocalizedString(@"Sign up here", nil) withLineHeight:20 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:SEMIBOLD_FONT size:16] andColor:[UIColor whiteColor] andKern:-0.47 withLineBreak:NO];
    [signUpLabel setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapSignGestureRecoggnizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mailAction)];
    [signUpLabel addGestureRecognizer:tapSignGestureRecoggnizer];
    [self.view addSubview:signUpLabel];
    
    UILabel *skipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:546], WIDTH, [UtilManager height:26])];
    [skipLabel setAttributedText:NSLocalizedString(@"Preview the app", nil) withLineHeight:22 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:SEMIBOLD_FONT size:16] andColor:[UIColor whiteColor] andKern:-0.5 withLineBreak:NO];
    [skipLabel setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(skipAction)];
    [skipLabel addGestureRecognizer:tapGesture];
    [self.view addSubview:skipLabel];
    
    UILabel *signInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:595], infoLabel.frame.size.width, [UtilManager height:40])];
    [signInfoLabel setCenter:CGPointMake(self.view.center.x, signInfoLabel.center.y)];
    [signInfoLabel setTextAlignment:NSTextAlignmentCenter];
    [signInfoLabel setText:NSLocalizedString(@"You’re only seconds away from receiving content and benefits tailored for you. \n Sign up now for free.", nil)];
    [signInfoLabel setTextColor:[UIColor whiteColor]];
    [signInfoLabel setNumberOfLines:0];
    [signInfoLabel setFont:[UIFont fontWithName:LIGHT_FONT size:12]];
    
    [self.view addSubview:signInfoLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)checkFields{
    if(_mailTextField.text.length == 0){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please enter a valid email address", nil)];
        return FALSE;
    }
    
    if(![RegularExpressionsManager checkMail:_mailTextField.text]){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Please enter a valid email address", nil)];
        return FALSE;
    }
    
    if(_passwordTextField.text.length == 0){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Password cannot be empty", nil)];
        return FALSE;
    }
    
    return TRUE;
}

- (void)loginAction {
    
    NSString *username = _mailTextField.text;
    NSString *password = _passwordTextField.text;
    
    [_passwordTextField resignFirstResponder];
    [_mailTextField resignFirstResponder];
    if([self checkFields]){
        if (![AppDelegate showAlertIfDisconnected]) {
            [self loginWithUsername:username password: password];
        }
    }
}

- (void)mailAction{
    if (![AppDelegate showAlertIfDisconnected]) {
        RegisterUserViewController *viewController = [[RegisterUserViewController alloc] init];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void)skipAction{
    if (![AppDelegate showAlertIfDisconnected]) {
        [WebServiceManager createTempUser:self];
    }
}

- (void)forgottenAction{
    if (![AppDelegate showAlertIfDisconnected]) {
        ForgotViewController *forgotViewController = [[ForgotViewController alloc] init];
        [self.navigationController pushViewController:forgotViewController animated:YES];
    }
}

- (void)showPassword{
    if(_passwordTextField.isSecureTextEntry)
        [_passwordTextField setSecureTextEntry:FALSE];
    else
        [_passwordTextField setSecureTextEntry:TRUE];
}

- (void)presentHomeViewController{
    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:homeViewController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
     }];
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if([textField isEqual:_passwordTextField])
        [_passwordTextField setSecureTextEntry:YES];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:_passwordTextField])
        [textField resignFirstResponder];
    if([textField isEqual:_mailTextField])
        [_passwordTextField becomeFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength;
    
    newLength = [textField.text length] + [string length] - range.length;
    
    if([textField isEqual:_passwordTextField])
    {
        return (newLength > 25) ? NO : YES;
    }
    if([textField isEqual:_mailTextField])
        return (newLength > 100) ? NO : YES;
    return YES;
}


#pragma mark WebServiceDelegateMethods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object
{
    
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    if (webServiceType == WebServiceTypeTempUser){
        [self presentHomeViewController];
    }
}

@end
