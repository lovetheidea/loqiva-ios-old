//
//  SignUpViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ParentViewController.h"

@interface SignUpViewController : ParentViewController
- (void)presentHomeViewController;
@end
