//
//  ResetPasswordViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 10/6/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "WebServiceManager.h"
#import "SignUpViewController.h"
#import "SignNavController.h"
#import "AppDelegate.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface ResetPasswordViewController ()<UITextFieldDelegate, WebServiceManagerDelegate>

@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UITextField *repeatPasswordTextField;

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
#ifdef LOQIVA
    [self.view setBackgroundColor:[UIColor orangeAppColor]];
#elif OLDSEAPORT
    [self.view setBackgroundColor:[UIColor whiteColor]];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image-blurred-bg"]];
    [self.view addSubview:imageView];
#endif
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20.], [UtilManager height:93], [UtilManager width:340], [UtilManager height:108])];
    [infoLabel setCenter:CGPointMake(self.view.center.x, infoLabel.center.y)];
    [infoLabel setTextAlignment:NSTextAlignmentCenter];
    [infoLabel setNumberOfLines:0];
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    [infoLabel setAdjustsFontSizeToFitWidth:YES];
    [infoLabel setAttributedText:NSLocalizedString(@"Reset your\n password", nil) withLineHeight:26 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:LIGHT_FONT size:26] andColor:[UIColor whiteColor] andKern:-0.5 withLineBreak:NO];
    
    [self.view addSubview:infoLabel];
    
    UIImageView *mailTextFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., [UtilManager height:226], [UtilManager width:335], [UtilManager height:50])];
    [mailTextFieldBacgroundView setCenter:CGPointMake(self.view.center.x, mailTextFieldBacgroundView.center.y)];
    [mailTextFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
    [self.view addSubview:mailTextFieldBacgroundView];
    
    _passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:55.], infoLabel.frame.origin.y + infoLabel.frame.size.height + [UtilManager height:12], [UtilManager width:265], UITEXTFIELD_HEIGHT)];
    [_passwordTextField setCenter:CGPointMake(self.view.center.x, mailTextFieldBacgroundView.center.y)];
    [_passwordTextField setBorderStyle:UITextBorderStyleNone];
    [_passwordTextField setTextAlignment:NSTextAlignmentCenter];
    [_passwordTextField setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [_passwordTextField setTextColor:GRAY_TEXT_COLOR];
    _passwordTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"New password", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                 NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                 }
     ];
    [_passwordTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_passwordTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_passwordTextField setReturnKeyType:UIReturnKeyDone];
    [_passwordTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_passwordTextField setSecureTextEntry:TRUE];
    [_passwordTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_passwordTextField setDelegate:self];
    UIView *mailTextFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
    [_passwordTextField setLeftView:mailTextFieldView];
    
    UIView *passwordTextViewContainer = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:-55],0,[UtilManager width:45],UITEXTFIELD_HEIGHT)];
    
    UIButton *showPasswordFieldView = [[UIButton alloc] initWithFrame:CGRectMake(0.,0,[UtilManager width:25],[UtilManager height:15])];
    [showPasswordFieldView setCenter:CGPointMake(showPasswordFieldView.center.x, passwordTextViewContainer.center.y)];
    [showPasswordFieldView setBackgroundColor:[UIColor clearColor]];
    [showPasswordFieldView setImage:[UIImage imageNamed:@"btn-show-password"] forState:UIControlStateNormal];
    [showPasswordFieldView addTarget:self action:@selector(showPassword) forControlEvents:UIControlEventTouchUpInside];
    [passwordTextViewContainer addSubview:showPasswordFieldView];
    [_passwordTextField setRightView:passwordTextViewContainer];
    
    [self.view addSubview:_passwordTextField];
    
    UIImageView *repetpasswordFieldBacgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:335], [UtilManager height:50])];
    [repetpasswordFieldBacgroundView setImage:[UIImage imageNamed:@"image-textfield"]];
    [repetpasswordFieldBacgroundView setCenter:CGPointMake(self.view.center.x, mailTextFieldBacgroundView.frame.origin.y + mailTextFieldBacgroundView.frame.size.height + [UtilManager height:12] + mailTextFieldBacgroundView.frame.size.height/2)];
    [self.view addSubview:repetpasswordFieldBacgroundView];
    
    _repeatPasswordTextField = [[UITextField alloc] initWithFrame:CGRectMake(_passwordTextField.frame.origin.x, _passwordTextField.frame.origin.y + _passwordTextField.frame.size.height + [UtilManager height:12], _passwordTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
    [_repeatPasswordTextField setBackgroundColor:[UIColor clearColor]];
    [_repeatPasswordTextField setTextAlignment:NSTextAlignmentCenter];
    [_repeatPasswordTextField setCenter:CGPointMake(_repeatPasswordTextField.center.x, repetpasswordFieldBacgroundView.center.y)];
    [_repeatPasswordTextField setBorderStyle:UITextBorderStyleNone];
    [_repeatPasswordTextField setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [_repeatPasswordTextField setTextColor:GRAY_TEXT_COLOR];
    _repeatPasswordTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Confirm password", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_TEXT_COLOR,
                                                 NSFontAttributeName : [UIFont fontWithName:LIGHT_FONT size:15.0]
                                                 }
     ];
    [_repeatPasswordTextField setSecureTextEntry:TRUE];
    [_repeatPasswordTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_repeatPasswordTextField setReturnKeyType:UIReturnKeyDone];
    [_repeatPasswordTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_repeatPasswordTextField setRightViewMode:UITextFieldViewModeWhileEditing];
    [_repeatPasswordTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    //[passwordTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_repeatPasswordTextField setDelegate:self];
    
    UIView *passwordFieldView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],0,[UtilManager width:7],[UtilManager height:26])];
    [_repeatPasswordTextField setLeftView:passwordFieldView];
    
    UIView *repeatPasswordTextViewContainer = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:-55],0,[UtilManager width:45],UITEXTFIELD_HEIGHT)];
    
    UIButton *showRepeatPasswordFieldView = [[UIButton alloc] initWithFrame:CGRectMake(0.,0,[UtilManager width:25],[UtilManager height:21])];
    [showRepeatPasswordFieldView setCenter:CGPointMake(showPasswordFieldView.center.x, passwordTextViewContainer.center.y)];
    [showRepeatPasswordFieldView setBackgroundColor:[UIColor clearColor]];
    [showRepeatPasswordFieldView setImage:[UIImage imageNamed:@"btn-show-password"] forState:UIControlStateNormal];
    [showRepeatPasswordFieldView addTarget:self action:@selector(showPassword) forControlEvents:UIControlEventTouchUpInside];
    [repeatPasswordTextViewContainer addSubview:showRepeatPasswordFieldView];
    [_repeatPasswordTextField setRightView:repeatPasswordTextViewContainer];
    
    [self.view addSubview:_repeatPasswordTextField];
    
    UIImage *resetImage = [UIImage imageNamed:@"btn-reset"];
    
    UIButton *resetButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:70], [UtilManager height:346], [UtilManager width:240], [UtilManager height:50])];
    [resetButton setCenter:CGPointMake(self.view.center.x, resetButton.center.y)];
    [resetButton setBackgroundImage:resetImage forState:UIControlStateNormal];
    [resetButton addTarget:self action:@selector(resetAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:resetButton];
    
    UILabel *skipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:546], WIDTH, [UtilManager height:36])];
    [skipLabel setAttributedText:NSLocalizedString(@"Skip directly to the app", nil) withLineHeight:22 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:SEMIBOLD_FONT size:16] andColor:[UIColor whiteColor] andKern:-0.5 withLineBreak:NO];
    [skipLabel setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(skipAction)];
    [skipLabel addGestureRecognizer:tapGesture];
    
    [self.view addSubview:skipLabel];
    UILabel *signInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:595], infoLabel.frame.size.width, [UtilManager height:40])];
    [signInfoLabel setCenter:CGPointMake(self.view.center.x, signInfoLabel.center.y)];
    [signInfoLabel setTextAlignment:NSTextAlignmentCenter];
    [signInfoLabel setText:NSLocalizedString(@"Signing up to an account will create\n a personal app experience tailored to you", nil)];
    [signInfoLabel setTextColor:[UIColor whiteColor]];
    [signInfoLabel setNumberOfLines:0];
    [signInfoLabel setFont:[UIFont fontWithName:LIGHT_FONT size:12]];
    
    [self.view addSubview:signInfoLabel];
}

- (void)skipAction{
    if (![AppDelegate showAlertIfDisconnected]) {
        [WebServiceManager createTempUser:self];
    }
}

- (void)resetAction{
    [WebServiceManager resetPassword:_passwordTextField.text andConfirmPassword:_repeatPasswordTextField.text andDelegate:self];
}

- (void)showPassword{
    if(_passwordTextField.isSecureTextEntry)
        [_passwordTextField setSecureTextEntry:FALSE];
    else
        [_passwordTextField setSecureTextEntry:TRUE];
}

- (void)showRepeatPassword{
    if(_repeatPasswordTextField.isSecureTextEntry)
        [_repeatPasswordTextField setSecureTextEntry:FALSE];
    else
        [_repeatPasswordTextField setSecureTextEntry:TRUE];
}

- (BOOL)checkFields{
    if(_passwordTextField.text != _repeatPasswordTextField.text){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"New password and Confirm password must be equal", nil)];
    }
    
    return true;
}

- (void)presentSignViewController
{
    SignUpViewController *viewController = [[SignUpViewController alloc] init];
    SignNavController *customNavigationController = [[SignNavController alloc] initWithRootViewController:viewController];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:customNavigationController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:customNavigationController];
     }];
    
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if([textField isEqual:_passwordTextField])
        [_passwordTextField setSecureTextEntry:YES];
    if([textField isEqual:_repeatPasswordTextField])
        [_repeatPasswordTextField setSecureTextEntry:YES];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:_passwordTextField] || [textField isEqual:_repeatPasswordTextField])
        [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength;
    
    newLength = [textField.text length] + [string length] - range.length;
    
    if([textField isEqual:_passwordTextField] || [textField isEqual:_repeatPasswordTextField])
    {
        return (newLength > 25) ? NO : YES;
    }
  
    return YES;
}


#pragma mark WebServiceManager Delegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
