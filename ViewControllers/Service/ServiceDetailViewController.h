//
//  ServiceDetailViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 26/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "POIDetailViewController.h"
#import "InterestDetail.h"
#import "Event.h"

@interface ServiceDetailViewController : POIDetailViewController

@property (nonatomic, strong) InterestDetail *currentInterestDetail;

@end
