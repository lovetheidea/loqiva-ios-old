//
//  ScheduleMonthView.m
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ScheduleMonthView.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "NSDate+Extra.h"
#import "loqiva-Swift.h"

@implementation ScheduleMonthView

@synthesize delegate;

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., frame.size.width, [UtilManager height:55])];
        [backgroundView setBackgroundColor:[UIColor blackColor]];
        [backgroundView setAlpha:0.75];
        
        [self addSubview:backgroundView];
        
        _dateLabel = [[UILabel alloc] initWithFrame:backgroundView.frame];
        [_dateLabel setTextColor:[UIColor whiteColor]];
        [_dateLabel setText:@"May 2016"];
        [_dateLabel setTextAlignment:NSTextAlignmentCenter];
        [_dateLabel setFont:[UIFont fontWithName:LIGHT_FONT size:16]];
        
        [self addSubview:_dateLabel];
        
        UIImage *nextImage = [UIImage imageNamed:@"btn-next-schedule"];
        UIImage *backImage = [UIImage imageNamed:@"btn-back-schedule"];
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:20], 0, backImage.size.width, backImage.size.height)];
        [backButton setCenter:CGPointMake(backButton.center.x, _dateLabel.center.y)];
        [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [backButton setImage:backImage forState:UIControlStateNormal];
        
        [self addSubview:backButton];
        
        UIButton *nextButton  =[[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:40] - nextImage.size.width, backButton.frame.origin.y, nextImage.size.width, nextImage.size.height)];
        [nextButton setImage:nextImage forState:UIControlStateNormal];
        [nextButton addTarget:self action:@selector(nextAction) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:nextButton];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(testTap)];
        [self addGestureRecognizer:tapGesture];
    }
    
    return self;
}

- (void)testTap{
    NSLog(@"Tap");
}

- (void)backAction{
    if([delegate respondsToSelector:@selector(backMonthDidPush)])
        [delegate backMonthDidPush];
}

- (void)nextAction{
    if([delegate respondsToSelector:@selector(nextMonthDidPush)])
        [delegate nextMonthDidPush];
}

- (void)setSelectedDate:(NSDate *)selectedDate{
    
    NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
    [yearFormatter setDateFormat:@"yyyy"];
    NSString *year = [yearFormatter stringFromDate:selectedDate];
    
    [_dateLabel setText:[NSString stringWithFormat:@"%@ %@",[selectedDate currentMonth],year]];
}

@end
