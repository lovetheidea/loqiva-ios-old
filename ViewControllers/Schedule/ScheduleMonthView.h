//
//  ScheduleMonthView.h
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ScheduleMonthViewDelegate;

@interface ScheduleMonthView : UIView

@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, assign) id<ScheduleMonthViewDelegate>delegate;

@property (nonatomic, strong) NSDate *selectedDate;

@end

@protocol ScheduleMonthViewDelegate <NSObject>

- (void)nextMonthDidPush;
- (void)backMonthDidPush;

@end
