//
//  ScheduleDetailViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 26/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ScheduleDetailViewController.h"
#import "WebServiceManager.h"
#import "LocationManager.h"
#import <MapKit/Mapkit.h>
#import "NavCustomView.h"
#import "AppContext.h"
#import "UIImageView+AFNetworking.h"
#import "MapViewcontroller.h"
#import "CustomAnnotation.h"
#import "CustomPinAnnotationView.h"
#import "CalloutMapAnnotation.h"
#import "CalloutMapAnnotationView.h"
#import "CalloutContentMapView.h"
#import "WebViewController.h"
#import "NSDate+Extra.h"
#import "Utility.h"
#import "NSString+HTML.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface ScheduleDetailViewController ()<CLLocationManagerDelegate, MKMapViewDelegate, MKAnnotation, WebServiceManagerDelegate,EKEventEditViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) CLLocationManager *manager;
@property (nonatomic, assign) BOOL isRedeemPush;
@property (nonatomic, assign) BOOL isFirstLoad;

@property (nonatomic, strong) UIView *navCustomView;

@property (nonatomic, strong) UIView *redeemUserView;
@property (nonatomic, strong) UIView *fourSeparatorView;
@property (nonatomic, strong) UILabel *summaryLabel;
@property (nonatomic, strong) UIView *footerView;

@property (nonatomic, assign) CGFloat originY;
@property (nonatomic, assign) BOOL userIsLocated;

@property (nonatomic, strong) UILabel *distanceLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *dateLabel;

@property (nonatomic, strong) CalloutMapAnnotation *calloutAnnotation;
@property (nonatomic, strong) CalloutMapAnnotationView *calloutMapAnnotationView;
@property (nonatomic, strong) CalloutContentMapView *calloutcontentMapView;
@property (nonatomic, strong) MKAnnotationView *selectedAnnotationView;

@end

@implementation ScheduleDetailViewController

@synthesize scrollView, mapView, coordinate, manager;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isRedeemPush = FALSE;
    _isFirstLoad = TRUE;
    _userIsLocated = FALSE;
    
    manager = [[CLLocationManager alloc] init];
    manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    manager.distanceFilter = kCLDistanceFilterNone;
    [manager startUpdatingLocation];
    [manager requestWhenInUseAuthorization];
    
    [self configureView];
    [self addAnnotations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureView{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    if(!_isRedeemPush && _isFirstLoad){
        
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0., -20., WIDTH, HEIGHT + 20)];
        
        UIImageView *rewardImageView;
        
        rewardImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,WIDTH,[UtilManager height:290])];
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],_currentEvent.mediaURL]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        [rewardImageView setImageWithURLRequest:imageRequest placeholderImage:[UtilManager imageWithColor:GRAY_TEXT_HOME_COLOR] success:nil failure:nil];
        [scrollView addSubview:rewardImageView];
        [scrollView addSubview:_navCustomView];
        
        UIImage *pinImage;
        pinImage = [UIImage imageNamed:@"icon-pin-small"];

        UIImageView *pinImageView;
        pinImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:20.], rewardImageView.frame.origin.y + rewardImageView.frame.size.height + [UtilManager height:15], pinImage.size.width, pinImage.size.height)];
        [pinImageView setImage:pinImage];
        [scrollView addSubview:pinImageView];
        
        _distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x + pinImageView.frame.size.width + [UtilManager width:10.], pinImageView.frame.origin.y, [UtilManager width:180.], [UtilManager height:20])];
        [_distanceLabel setTextColor:[UIColor DistanceLabelTextColor]];
        [_distanceLabel setCenter:CGPointMake(_distanceLabel.center.x, pinImageView.center.y)];
        [_distanceLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:10]];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:_currentEvent.latitude.doubleValue longitude:_currentEvent.longitude.doubleValue];
        [_distanceLabel setText:[[NSString stringWithFormat:@"%.0f", [LocationManager getDistanceFrom:location]] distanceString]];
        [_distanceLabel setAdjustsFontSizeToFitWidth:YES];
        
        [scrollView addSubview:_distanceLabel];
        
        UIView *firstSeparatorView;
        
        firstSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _distanceLabel.frame.origin.y + _distanceLabel.frame.size.height + [UtilManager height:15], WIDTH - 2 * pinImageView.frame.origin.x, 1)];
        [firstSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        
        [scrollView addSubview:firstSeparatorView];
        
        
        
        
        // Title Label
        CGFloat titleHeight = [UtilManager heightForText:_currentEvent.title havingWidth:firstSeparatorView.frame.size.width andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE]];
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, firstSeparatorView.frame.origin.y + [UtilManager height:15.], firstSeparatorView.frame.size.width, titleHeight)];
        [_titleLabel setAttributedText:[Utility convertHtmlToPlainText: _currentEvent.title] withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:TITLE_FONT_KERN withLineBreak:NO];
        CGRect rect = [_titleLabel.attributedText boundingRectWithSize:CGSizeMake(_titleLabel.frame.size.width, _titleLabel.frame.size.height) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        [_titleLabel setFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y, _titleLabel.frame.size.width, rect.size.height)];
        [_titleLabel setNumberOfLines:0];
        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
        [_titleLabel setBackgroundColor:TEST_BG_ONE];
        
        [scrollView addSubview:_titleLabel];
        
        
        
//        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, firstSeparatorView.frame.origin.y + [UtilManager height:10.], [UtilManager width:335], [UtilManager height:80])];
//        [_titleLabel setAttributedText:_currentEvent.title withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:[UIColor orangeButtonColor] andKern:TITLE_FONT_KERN];
//        [_titleLabel setNumberOfLines:0];
//        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
//        [scrollView addSubview:_titleLabel];
        
        
        
        
        
        
        
        
        
        UIView *secondSeparatorView;
        secondSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + [UtilManager height:10], WIDTH - 2 * pinImageView.frame.origin.x, 1)];
        [secondSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [scrollView addSubview:secondSeparatorView];
        
        
        
        
        
        
        // Date Label
        _dateLabel= [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, secondSeparatorView.frame.origin.y + [UtilManager height:15.], secondSeparatorView.frame.size.width, [UtilManager height:18])];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:DATE_FORMATTER_PRETTY];
        
        [_dateLabel setAttributedText:[dateFormatter stringFromDate:_currentEvent.startDate] withLineHeight:CONTACT_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:SEMIBOLD_FONT size:14] andColor:GRAY_DARK_TEXT_COLOR andKern:CONTACT_FONT_KERN withLineBreak:NO];
        [_dateLabel setNumberOfLines:0];
        [_dateLabel setAdjustsFontSizeToFitWidth:YES];
        [_dateLabel setBackgroundColor:TEST_BG_TWO];
        
        [scrollView addSubview:_dateLabel];
        
        
        
        // Address Label
        CGFloat addressHeight = [UtilManager heightForText:_currentEvent.address havingWidth:firstSeparatorView.frame.size.width andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE]];
        
        _addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _dateLabel.frame.origin.y + [UtilManager height:20], secondSeparatorView.frame.size.width, addressHeight)];
        
        [_addressLabel setAttributedText:[Utility convertHtmlToPlainText: _currentEvent.address] withLineHeight:CONTACT_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:CONTACT_FONT_SIZE] andColor:GRAY_TEXT_COLOR andKern:CONTACT_FONT_KERN withLineBreak:NO];
        
        CGRect addressRect = [_addressLabel.attributedText boundingRectWithSize:CGSizeMake(_addressLabel.frame.size.width, _addressLabel.frame.size.height) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        
        [_addressLabel setFrame:CGRectMake(_addressLabel.frame.origin.x, _addressLabel.frame.origin.y, _addressLabel.frame.size.width, addressRect.size.height)];
        [_addressLabel setNumberOfLines:0];
        [_addressLabel setAdjustsFontSizeToFitWidth:YES];
        [_addressLabel setBackgroundColor:TEST_BG_ONE];
        
        [scrollView addSubview:_addressLabel];
        
        
        
//        _addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _dateLabel.frame.origin.y + _dateLabel.frame.size.height + [UtilManager height:5], firstSeparatorView.frame.size.width, [UtilManager height:32])];
//        CGFloat addressHeight = [UtilManager heightForText:_currentEvent.address havingWidth:_addressLabel.frame.size.width andFont:[UIFont fontWithName:REGULAR_FONT size:13]];
//        [_addressLabel setFrame:CGRectMake(_addressLabel.frame.origin.x, _addressLabel.frame.origin.y, _addressLabel.frame.size.width, addressHeight)];
//        [_addressLabel setText:_currentEvent.address];
//        
//        
//        [_addressLabel setTextColor:[UIColor grayColor]];
//        [_addressLabel setFont:[UIFont fontWithName:REGULAR_FONT size:12]];
//        [_addressLabel setNumberOfLines:0];
//        [_addressLabel setAdjustsFontSizeToFitWidth:YES];
//        [_addressLabel setBackgroundColor:TEST_BG_ONE];
//        
//        [scrollView addSubview:_addressLabel];
        
        
        
        
        // Calendar Button
        UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(_addressLabel.frame.origin.x, _addressLabel.frame.origin.y + _addressLabel.frame.size.height + [UtilManager height:10], [UtilManager width:110], [UtilManager height:16])];
        [addButton setImage:[UIImage imageNamed:@"btn-add-calendar"] forState:UIControlStateNormal];
        [addButton addTarget:self action:@selector(addCalendar) forControlEvents:UIControlEventTouchUpInside];
        [addButton setBackgroundColor:TEST_BG_ONE];
        
        [scrollView addSubview:addButton];
        
        
        
        
        
        UIView *thirdSeparatorView;
        
        thirdSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, addButton.frame.origin.y + addButton.frame.size.height + [UtilManager height:15], WIDTH - 2 * pinImageView.frame.origin.x, 1)];
        [thirdSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [scrollView addSubview:thirdSeparatorView];
        
        UIImage *redeemRewardImage = [UIImage imageNamed:@"btn-buy-tickets"];
        
        UIButton *redeemRewardButton;
        
        redeemRewardButton = [[UIButton alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, thirdSeparatorView.frame.origin.y + [UtilManager height:20], redeemRewardImage.size.width * 0.85, redeemRewardImage.size.height * 0.85)];
        [redeemRewardButton setImage:redeemRewardImage forState:UIControlStateNormal];
        [redeemRewardButton addTarget:self action:@selector(redeemRewardAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [scrollView addSubview:redeemRewardButton];
        
        UIImage *getDirectionsImage = [UIImage imageNamed:@"btn-get-directions"];
        
        UIButton *getDirectionsButton;
        if(!_isRedeemPush && _isFirstLoad)
        {
            getDirectionsButton =[[UIButton alloc] initWithFrame:CGRectMake(firstSeparatorView.frame.origin.x+firstSeparatorView.frame.size.width - getDirectionsImage.size.width , redeemRewardButton.frame.origin.y, getDirectionsImage.size.width* 0.85, getDirectionsImage.size.height* 0.85)];
            [getDirectionsButton setImage:getDirectionsImage forState:UIControlStateNormal];
            [getDirectionsButton addTarget:self action:@selector(goToDirection) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:getDirectionsButton];
        }
        
        _originY = getDirectionsButton.frame.origin.y + getDirectionsButton.frame.size.height + [UtilManager height:15];
    } else {
        
    
    }
    
    
    if(!_isRedeemPush && _isFirstLoad)
    {
        _redeemUserView = [self redeemUserViewWithFrame:CGRectMake(0., _originY, scrollView.frame.size.width, [UtilManager height:434])];
    }
    
    if(_isRedeemPush)
        [scrollView addSubview:_redeemUserView];
    else if(!_isRedeemPush && !_isFirstLoad)
        [_redeemUserView removeFromSuperview];
    
    
    if(!_isRedeemPush && _isFirstLoad)
    {
        _fourSeparatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20.], _originY + 3, WIDTH - 2 * [UtilManager width:20.], 1)];
        [_fourSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [scrollView addSubview:_fourSeparatorView];
    }
    else
        [_fourSeparatorView setFrame:CGRectMake([UtilManager width:20.], _originY, WIDTH - 2 * [UtilManager width:20.], 1)];
    
    
    
    
    
    
    
    
    NSString *summaryString;
    summaryString = [Utility convertHtmlToPlainText: _currentEvent.summary];
    CGFloat summaryHeight = [UtilManager heightForText:summaryString havingWidth:_fourSeparatorView.frame.size.width andFont:[UIFont fontWithName:LIGHT_FONT size:16]];
    if(!_isRedeemPush && _isFirstLoad){
        _summaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(_fourSeparatorView.frame.origin.x,_fourSeparatorView.frame.origin.y + [UtilManager height:20] , _fourSeparatorView.frame.size.width, summaryHeight)];
    }else{
        [_summaryLabel setFrame:CGRectMake(_fourSeparatorView.frame.origin.x,_fourSeparatorView.frame.origin.y + [UtilManager height:20] , _fourSeparatorView.frame.size.width, summaryHeight)];
    }
    
    
    
    [_summaryLabel setTextAlignment:NSTextAlignmentLeft];
    [_summaryLabel setAttributedText:summaryString withLineHeight:BODY_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:BODY_FONT_SIZE] andColor:GRAY_DARK_TEXT_COLOR andKern:BODY_FONT_KERN withLineBreak:NO];
    CGRect rect = [_summaryLabel.attributedText boundingRectWithSize:CGSizeMake(_summaryLabel.frame.size.width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    [_summaryLabel setFrame:CGRectMake(_summaryLabel.frame.origin.x, _summaryLabel.frame.origin.y, _summaryLabel.frame.size.width, rect.size.height)];
    [_summaryLabel setNumberOfLines:0];
    [_summaryLabel setBackgroundColor:TEST_BG_ONE];
    [scrollView addSubview:_summaryLabel];

    
    
    
    
    
    
    
    
    
    if(!_isRedeemPush && _isFirstLoad)
    {
        mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0., _summaryLabel.frame.origin.y + _summaryLabel.frame.size.height + [UtilManager height:30], WIDTH, [UtilManager height:180])];
        [mapView setMapType:MKMapTypeStandard];
        [mapView setDelegate:self];
        [mapView setShowsCompass:FALSE];
        [mapView setShowsTraffic:FALSE];
        [mapView setShowsBuildings:YES];
        [mapView setShowsUserLocation:YES];
        [mapView setShowsPointsOfInterest:YES];
        
        [scrollView addSubview:mapView];
    } else {
        [mapView setFrame:CGRectMake(0., _summaryLabel.frame.origin.y + _summaryLabel.frame.size.height + [UtilManager height:30], WIDTH, [UtilManager height:180])];
    }
    
    if(!_isRedeemPush && _isFirstLoad){
        _footerView = [self rewardOnMapViewWithOriginY:(mapView.frame.origin.y + mapView.frame.size.height)];
        [scrollView addSubview:_footerView];
    } else{
        [_footerView setFrame:CGRectMake(_footerView.frame.origin.x, mapView.frame.origin.y + mapView.frame.size.height, _footerView.frame.size.width, _footerView.frame.size.height)];
    }
    
    [scrollView setContentSize:CGSizeMake(WIDTH, _footerView.frame.origin.y + _footerView.frame.size.height)];
    [self.view addSubview:scrollView];
    
    [self setupHeaderForSection:CMSectionTypeSchedule];
    
    [self.headerNav setBackOption];
    
    _isFirstLoad = FALSE;
}

- (UIView *)redeemUserViewWithFrame:(CGRect)frame{
    UIView *redeemView = [[UIView alloc] initWithFrame:frame];
    [redeemView setBackgroundColor:[UIColor orangeButtonColor]];
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:35], [UtilManager width:35], WIDTH - [UtilManager width:70], [UtilManager height:20])];
    [infoLabel setText:NSLocalizedString(@"Please show this screen to redeem your reward", nil)];
    [infoLabel setTextColor:[UIColor whiteColor]];
    [infoLabel setFont:[UIFont fontWithName:REGULAR_FONT size:20]];
    [infoLabel setAdjustsFontSizeToFitWidth:YES];
    
    [redeemView addSubview:infoLabel];
    
    UIImageView *profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.,infoLabel.frame.origin.y + infoLabel.frame.size.height + [UtilManager height:20], [UtilManager height:150], [UtilManager height:130])];
    [profileImageView setCenter:CGPointMake(redeemView.center.x, profileImageView.center.y)];
    [[profileImageView layer] setMasksToBounds:YES];
    [[profileImageView layer] setCornerRadius:profileImageView.frame.size.width/2];
    [profileImageView setContentMode:UIViewContentModeScaleAspectFit];
    [profileImageView setImage:[UIImage imageNamed:@"temp-profile-photo"]];
    
    [redeemView addSubview:profileImageView];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., profileImageView.frame.origin.y + profileImageView.frame.size.height + [UtilManager height:20], WIDTH, [UtilManager height:30])];
    [nameLabel setTextAlignment:NSTextAlignmentCenter];
    [nameLabel setFont:[UIFont fontWithName:BOLD_FONT size:25]];
    [nameLabel setTextColor:[UIColor whiteColor ]];
    [nameLabel setText:[NSString stringWithFormat:@"%@ %@",[[[SessionManager shared] user] firstName],[[[SessionManager shared] user] lastName]]];
    [nameLabel setAdjustsFontSizeToFitWidth:YES];
    
    [redeemView addSubview:nameLabel];
    
    UILabel *memberLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., nameLabel.frame.origin.y + nameLabel.frame.size.height + [UtilManager height:8], WIDTH, [UtilManager height:20])];
    [memberLabel setTextAlignment:NSTextAlignmentCenter];
    [memberLabel setText:NSLocalizedString(@"Membership Number", nil)];
    [memberLabel setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    [memberLabel setAdjustsFontSizeToFitWidth:YES];
    [memberLabel setTextColor:[UIColor whiteColor]];
    
    [redeemView addSubview:memberLabel];
    
    UILabel *numberMemberLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., memberLabel.frame.origin.y + memberLabel.frame.size.height + [UtilManager height:8], WIDTH, [UtilManager height:20])];
    [numberMemberLabel setTextAlignment:NSTextAlignmentCenter];
    [numberMemberLabel setText:NSLocalizedString(@"#0123456789", nil)];
    [numberMemberLabel setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    [numberMemberLabel setAdjustsFontSizeToFitWidth:YES];
    [numberMemberLabel setTextColor:[UIColor whiteColor]];
    
    [redeemView addSubview:numberMemberLabel];
    
    UIButton *confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(0., numberMemberLabel.frame.origin.y + numberMemberLabel.frame.size.height + [UtilManager height:25], [UtilManager width:240], [UtilManager height:64])];
    [confirmButton setCenter:CGPointMake(redeemView.center.x, confirmButton.center.y)];
    [confirmButton setImage:[UIImage imageNamed:@"btn-reward-accept"] forState:UIControlStateNormal];
    
    [redeemView addSubview:confirmButton];
    
    return redeemView;
}



- (void)addAnnotations{
    CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(_currentEvent.latitude.doubleValue, _currentEvent.longitude.doubleValue) title:_currentEvent.title summary:_currentEvent.summary subtitle:@"" pin:@"image-pin-info"];
    [annotation setAccessibilityLabel:_currentEvent.title];
    [annotation setType:@"Attraction"];
    [annotation setImage:@""];
    [annotation setInterestId:[NSString stringWithFormat:@"%d",_currentEvent.eventId.intValue]];
    
    [mapView addAnnotations:@[annotation]];
}

- (UIView *)rewardOnMapViewWithOriginY:(CGFloat)originY{
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., originY, WIDTH, [UtilManager height:145])];
    [footerView setBackgroundColor:[UIColor footerColor]];
    
    UILabel *newsLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:20], WIDTH - [UtilManager width:6], [UtilManager height:20])];
    [newsLabel setAttributedText:NSLocalizedString(@"Want to see more events local to you?", nil) withLineHeight:16 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:16] andColor:[UIColor footerTextAcentcolour] andKern:-0.6 withLineBreak:NO];
    [footerView addSubview:newsLabel];
    
    UILabel *updateLabel = [[UILabel alloc] initWithFrame:CGRectMake(newsLabel.frame.origin.x, newsLabel.frame.origin.y + newsLabel.frame.size.height + [UtilManager height:5], newsLabel.frame.size.width, newsLabel.frame.size.height)];
    [updateLabel setAttributedText:NSLocalizedString(@"Click the button below", nil) withLineHeight:16 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:16] andColor:[UIColor HeaderTextColour] andKern:-0.6 withLineBreak:NO];
    [footerView addSubview:updateLabel];
    
    UIImage *orangeButtonImage = [UIImage imageNamed:@"btn-see-today-events"];
    
    UIButton *preferencesButton = [[UIButton alloc] initWithFrame:CGRectMake(newsLabel.frame.origin.x, updateLabel.frame.origin.y + updateLabel.frame.size.height + [UtilManager height:15], orangeButtonImage.size.width, orangeButtonImage.size.height)];
    [preferencesButton setImage:orangeButtonImage forState:UIControlStateNormal];
    [preferencesButton addTarget:self action:@selector(rewardsAction) forControlEvents:UIControlEventTouchUpInside];
    //[preferencesButton setTitle:NSLocalizedString(@"Update My Preferences", nil) forState:UIControlStateNormal];
    
    [footerView addSubview:preferencesButton];
    
    return footerView;
}

- (void)redeemRewardAction:(id)sender{
    if(_currentEvent.ticketsurl && _currentEvent.ticketsurl.length > 0){
        WebViewController *webViewController = [[WebViewController alloc] init];
        [webViewController setUrlString:_currentEvent.ticketsurl];
        [webViewController setTitleBar:_currentEvent.title];
        [self.navigationController pushViewController:webViewController animated:YES];
    }
}

- (void)rewardsAction{
    
}

- (void)goToDirection{
    MapViewController *mapViewController = [[MapViewController alloc] init];
    [mapViewController setCurrentEvent:_currentEvent];
    [mapViewController setMapSelectType:MapSelectTypeInfo];
    [self.navigationController pushViewController:mapViewController animated:YES];
}

- (void)addCalendar{
    EKEventEditViewController *addController = [[EKEventEditViewController alloc] init];
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if(granted){
            addController.eventStore = eventStore;
            addController.editViewDelegate = self;
            addController.event.title = _currentEvent.title;
            
            NSDate *date = [[NSDate alloc] init];
            addController.event.startDate = date;
            
            // set the start date to the current date/time and the event duration to two hours
            NSDate *startDate = _currentEvent.startDate;
            addController.event.startDate = startDate;
            addController.event.endDate = [startDate dateByAddingTimeInterval:3600];
            addController.event.location = _currentEvent.address;
            
            [self presentViewController:addController animated:YES completion:nil];
        }
        else
            [UtilManager presentAlertWithMessage:error.description];
    }];
    
    
}

- (void)backAction{
    NSLog(@"Back Action");
}

#pragma mark EKEventViewController

- (void)eventEditViewController:(EKEventEditViewController *)controller
          didCompleteWithAction:(EKEventEditViewAction)action
{
    
    [self dismissViewControllerAnimated:YES completion:^
     {
         if (action != EKEventEditViewActionCanceled)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [UtilManager presentAlertWithMessage:NSLocalizedString(@"Your appointment has been saved", nil)];
                 //[self.navigationController popToRootViewControllerAnimated:YES];
             });
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [UtilManager presentAlertWithMessage:NSLocalizedString(@"Your appointment has not been saved.", nil)];
                 //[self.navigationController popToRootViewControllerAnimated:YES];
             });
         }
     }];
}

#pragma mark MKMapView Delegate Methods

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
    
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    
}

-(void)zoomToFitMapAnnotations:(MKMapView*)mapView
{
    if([self.mapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(CustomAnnotation* annotation in self.mapView.annotations)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.5; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.5; // Add a little extra space on the sides
    
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region animated:YES];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation NS_AVAILABLE(10_9, 4_0)
{
    if(!_userIsLocated)
    {
        _userIsLocated = true;
        
        MKCoordinateRegion region;
        MKCoordinateSpan span;
        
        span.latitudeDelta = 0.0103932;
        span.longitudeDelta = 0.0076580;
        
        region.center = CLLocationCoordinate2DMake(userLocation.location.coordinate.latitude,userLocation.location.coordinate.longitude);
        region.span = span;
        
        [self zoomToFitMapAnnotations:self.mapView];
        
        //[self.mapView setRegion:region animated:YES];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *identifier = @"pinAnnotation";
    if ([annotation isKindOfClass:[CustomAnnotation  class]])
    {
        CustomPinAnnotationView *annotationView = (CustomPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if(!annotationView)
        {
            annotationView = [[CustomPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        }
        
        annotationView.enabled = YES;
        [annotationView setHidden:NO];
        [annotationView setDraggable:NO];
        [annotationView setSelected:YES];
        [annotationView setExclusiveTouch:NO];
        [annotationView setMultipleTouchEnabled:NO];
        
        return annotationView;
    }else if(annotation == self.calloutAnnotation){
        _calloutMapAnnotationView = (CalloutMapAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"CalloutAnnotation"];
        
        if(!_calloutMapAnnotationView)
            _calloutMapAnnotationView = nil;
        
        _calloutMapAnnotationView = [[CalloutMapAnnotationView alloc] initWithAnnotation:annotation
                                                                         reuseIdentifier:@"CalloutAnnotation"];
        
        if(!_calloutcontentMapView)
            _calloutcontentMapView = [[CalloutContentMapView alloc] initWithFrame:CGRectMake(0., -[UtilManager height:5], [UtilManager width:250], [UtilManager height:100])];
        [_calloutcontentMapView setBackgroundColor:[UIColor whiteColor]];
        [[_calloutcontentMapView titleLabel] setText:self.calloutAnnotation.title];
        [[_calloutcontentMapView infoLabel] setText:self.calloutAnnotation.type];
        [[_calloutcontentMapView imageView] setBackgroundColor:GRAY_TEXT_COLOR];
        
        [_calloutMapAnnotationView.contentView addSubview:_calloutcontentMapView];
        _calloutMapAnnotationView.parentAnnotationView = self.selectedAnnotationView;
        _calloutMapAnnotationView.mapView = self.mapView;
        _calloutMapAnnotationView.calloutMapAnnotation = self.calloutAnnotation;
        //[_calloutMapAnnotationView setDelegate:self];
        
        return _calloutMapAnnotationView;
    }else{
        return [self.mapView viewForAnnotation:self.mapView.userLocation];
    }
}

#pragma mark WebServiceManagerDelegate

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    if(webServiceType == WebServiceTypeEventDetail){
    
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    if(webServiceType == WebServiceTypeEventDetail){
        _currentEvent = (Event *)object;
        
    }
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{

}

@end

