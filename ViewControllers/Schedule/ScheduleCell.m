//
//  ScheduleCell.m
//  loqiva
//
//  Created by Manuel Manzanera on 4/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ScheduleCell.h"

#import "UtilManager.h"
#import "AppConstants.h"
#import "loqiva-Swift.h"

@implementation ScheduleCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        _hourLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:15], [UtilManager height:18], [UtilManager width:75], [UtilManager height:20])];
        [_hourLabel setAdjustsFontSizeToFitWidth:YES];
        [self.contentView addSubview:_hourLabel];
        
        UIImage *pinImage;
        pinImage = [UIImage imageNamed:@"icon-pin-small"];
        UIImageView * iconPin = [[UIImageView alloc] initWithFrame:CGRectMake(_hourLabel.frame.origin.x, _hourLabel.frame.origin.y + _hourLabel.frame.size.height + [UtilManager height:3], pinImage.size.width, pinImage.size.height)];
        [iconPin setImage:pinImage];
        [self.contentView addSubview:iconPin];
        
        _distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(iconPin.frame.origin.x + iconPin.frame.size.width + [UtilManager width:3], iconPin.frame.origin.y - [UtilManager width:1], [UtilManager width:85.], iconPin.frame.size.height)];
        [_distanceLabel setAdjustsFontSizeToFitWidth:YES];
        
        [self.contentView addSubview:_distanceLabel];
        
        UIView *verticalSeparatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:100], 0, 1, [UtilManager height:75])];
        [verticalSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        
        [self.contentView addSubview:verticalSeparatorView];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(verticalSeparatorView.frame.origin.x + [UtilManager width:15], [UtilManager height:20.], [UtilManager width:250], [UtilManager height:20])];
        [self.contentView addSubview:_titleLabel];
        
        _subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y + _titleLabel.frame.size.height, [UtilManager width:250], [UtilManager height:20])];
        [self.contentView addSubview:_subtitleLabel];
        
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., [UtilManager height:74.], WIDTH, 1)];
        [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [self.contentView addSubview:separatorView];
    }
    
    return self;
}

@end
