//
//  ScheduleHeaderCell.m
//  loqiva
//
//  Created by Manuel Manzanera on 29/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ScheduleHeaderCell.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "ScheduleMonthView.h"
#import "DayPicker.h"

@implementation ScheduleHeaderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:290])];
        [headerView setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:headerView.frame];
        [backgroundImageView setImage:[UIImage imageNamed:@"temp-image-event"]];
        [backgroundImageView setContentMode:UIViewContentModeScaleAspectFill];
        [headerView addSubview:backgroundImageView];
        
        NSDateComponents *fourWeeks = [[NSDateComponents alloc] init];
        fourWeeks.day = 28 * 7;
        NSDate *endDate = [[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]dateByAddingComponents:fourWeeks toDate:[NSDate date] options:0];
        
        if(!dayPicker)
        {
            dayPicker = [[DayPicker alloc] initWithFrame:CGRectMake(0., backgroundImageView.frame.size.height, WIDTH, [UtilManager height:76])];
            [dayPicker addObserver:self forKeyPath:@"selectedDate" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
            [dayPicker setStartDate:_selectedDate endDate:endDate];
        }
        [dayPicker setBackgroundColor:[UIColor whiteColor]];
        dayPicker.selectedDateBackgroundImage = [UIImage imageNamed:@"image-day-selected"];
        [dayPicker setWeekdayFont:[UIFont fontWithName:LIGHT_FONT size:12]];
        [dayPicker setDateFont:[UIFont fontWithName:LIGHT_FONT size:14]];
        
        //[dayPicker setSelectedDateBackgroundColor:ORANGE_APP_COLOR];
        
        [headerView addSubview:dayPicker];
        
        UIView *dayLabelContainerView = [[UIView alloc] initWithFrame:CGRectMake(-1, dayPicker.frame.origin.y + dayPicker.frame.size.height, WIDTH + 2, [UtilManager height:50])];
        [[dayLabelContainerView layer] setBorderWidth:1];
        [[dayLabelContainerView layer] setBorderColor:GRAY_SEPARATOR_COLOR.CGColor];
        [dayLabelContainerView setBackgroundColor:[UIColor colorWithRed:242/255. green:242/255. blue:242/255. alpha:1]];
        [headerView addSubview:dayLabelContainerView];
        
        if(!dayLabel)
            dayLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], 0, WIDTH, dayLabelContainerView.frame.size.height)];
        [dayLabel setBackgroundColor:[UIColor clearColor]];
        [dayLabel setTextAlignment:NSTextAlignmentLeft];
        [dayLabel setTextColor:[UIColor blackColor]];
        [dayLabel setText:[NSDateFormatter localizedStringFromDate:_selectedDate dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle]];
        [dayLabel setFont:[UIFont fontWithName:REGULAR_FONT size:14]];
        
        [dayLabelContainerView addSubview:dayLabel];
        
        [headerView addSubview:[self navigationView]];
        
        UIView *darkBlurView = [[UIView alloc] initWithFrame:CGRectMake(0., NAV_BAR_HEIGHT + 1, WIDTH, headerView.frame.size.height - NAV_BAR_HEIGHT - 1)];
        //UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        //UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        //[blurEffectView setFrame:CGRectMake(0., darkBlurView.frame.origin.y, darkBlurView.frame.size.width, darkBlurView.frame.size.height)];
        //darkBlurView = blurEffectView;
        [darkBlurView setBackgroundColor:[UIColor blackColor]];
        [darkBlurView setAlpha:0.4];
        
        //[headerView addSubview:darkBlurView];
        
        UILabel *eventLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], darkBlurView.frame.origin.y + [UtilManager height:20], [UtilManager width:270], [UtilManager height:68])];
        [eventLabel setNumberOfLines:0];
        [eventLabel setText:@"There are 4 events in Aberdeen today!"];
        [eventLabel setTextColor:[UIColor whiteColor]];
        [eventLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:24]];
        [eventLabel setTextAlignment:NSTextAlignmentLeft];
        [eventLabel setAdjustsFontSizeToFitWidth:YES];
        //[headerView addSubview:eventLabel];
        
        if(!scheduleMonthView)
        {
            scheduleMonthView = [[ScheduleMonthView alloc] initWithFrame:CGRectMake(0, dayPicker.frame.origin.y - [UtilManager height:55], WIDTH, [UtilManager height:55.])];
            [scheduleMonthView setDelegate:self];
        }
        [headerView addSubview:scheduleMonthView];
        
        return headerView;
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
