//
//  ScheduleViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ScheduleViewController.h"
#import "ScheduleDetailViewController.h"
#import "LocationManager.h"
#import "WebServiceManager.h"
#import "DayPicker.h"
#import "NavCustomView.h"
#import "ScheduleMonthView.h"
#import "ScheduleCell.h"
#import "LocationManager.h"
#import "UILabel+Extra.h"
#import "NSDate+Extra.h"
#import "NSString+HTML.h"
#import "Event.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface ScheduleViewController ()<UITableViewDelegate, UITableViewDataSource, ScheduleMonthViewDelegate, WebServiceManagerDelegate>

@property (nonatomic, strong) UITableView *scheduleTableView;
@property (nonatomic, strong) DayPicker *dayPicker;
@property (nonatomic, strong) UILabel *dayLabel;
@property (nonatomic, strong) ScheduleMonthView *scheduleMonthView;
@property (nonatomic, strong) NSArray *dayEvents;
@property (nonatomic, strong) UILabel *eventLabel;
@property (nonatomic, strong) NSDate *selectedDate;

@end

@implementation ScheduleViewController

@synthesize scheduleTableView, dayPicker, dayLabel, scheduleMonthView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self refresh];
    [self configureView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self refresh];
}

- (void)dealloc {
    [dayPicker removeObserver:self forKeyPath:@"selectedDate"];
}

- (void)refresh {
    [WebServiceManager getEventsWithLatitude:[NSNumber numberWithDouble:[LocationManager sharedInstance].currentLocation.coordinate.latitude] andLongitude:[NSNumber numberWithDouble:[LocationManager sharedInstance].currentLocation.coordinate.longitude] andDelegate:self];
}

- (void)configureView {
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self chooseEvent];
    
    if (!scheduleTableView) {
        scheduleTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., -20., WIDTH, HEIGHT + 20) style:UITableViewStylePlain];
        [self.view addSubview:scheduleTableView];
    }
    [scheduleTableView setDelegate:self];
    [scheduleTableView setDataSource:self];
    [scheduleTableView registerClass:[ScheduleCell class] forCellReuseIdentifier:@"scheduleCell"];
    [scheduleTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"scheduleInfoCell"];
    [scheduleTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"firstCell"];
    [scheduleTableView setBounces:FALSE];
    [scheduleTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [scheduleTableView reloadData];
    
    [self setupHeaderForSection:CMSectionTypeSchedule];
}

- (void)chooseEvent {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_FORMATTER];
    
    _selectedDate = [NSDate date];
    
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    _dayEvents = [Event eventInDate:[dateFormatter dateFromString:dateString]];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 1)
    {
    if(_dayEvents.count)
    {
        [scheduleTableView deselectRowAtIndexPath:indexPath animated:YES];
    
        ScheduleDetailViewController *scheduleDetailViewController = [[ScheduleDetailViewController alloc] init];
        Event *currentEvent = [_dayEvents objectAtIndex:indexPath.row];
        [scheduleDetailViewController setCurrentEvent:currentEvent];
        [self.navigationController pushViewController:scheduleDetailViewController animated:YES];
    }
    }
}

#pragma mark UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            if(_dayEvents.count == 0)
                return 1;
            else
                return _dayEvents.count;
            break;
            
        default:
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0)
    {
        UITableViewCell *cell = [scheduleTableView dequeueReusableCellWithIdentifier:@"firstCell"];
        
        UIView *headerView = [self navigationViewAndHeaderView];
        [[cell contentView] addSubview:headerView];
        /**
        UIView *navigationView = [self navigationView];
        [[cell contentView] addSubview:navigationView];
         **/
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    } else {
    if(_dayEvents.count > 0)
    {
        ScheduleCell *cell = [scheduleTableView dequeueReusableCellWithIdentifier:@"scheduleCell"];
        
        Event *currentEvent = [_dayEvents objectAtIndex:indexPath.row];
        
        [[cell titleLabel] setText:currentEvent.title];
        NSString *addressString = [currentEvent.address stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        [[cell subtitleLabel] setText:addressString];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:currentEvent.latitude.doubleValue longitude:currentEvent.longitude.doubleValue];
        [[cell distanceLabel] setText:[[NSString stringWithFormat:@"%.2f", [LocationManager getDistanceFrom:location]] distanceString]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm a"];
        NSString *dateString = [dateFormatter stringFromDate:currentEvent.startDate];
        NSArray* words = [dateString componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        dateString = [words componentsJoinedByString:@""];
        [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
        [[cell hourLabel] setText:dateString];
        
        return cell;
    } else {
        
        UITableViewCell *cell = [scheduleTableView dequeueReusableCellWithIdentifier:@"scheduleInfoCell"];
        
        [cell.textLabel setBackgroundColor:[UIColor whiteColor]];
        [cell.textLabel setNumberOfLines:0];
        [cell.textLabel setText:NSLocalizedString(@"Sorry, there are no events scheduled for today", nil)];
        [cell.textLabel setTextColor:[UIColor EventMessageNoEventsColour]];
        [cell.textLabel setFont:[UIFont fontWithName:LIGHT_FONT size:32]];
        [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
        [cell.textLabel setFrame:cell.contentView.frame];
        
        return cell;
    }
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0)
        return [UtilManager height:235];
    else{
        if(_dayEvents.count>0)
            return [UtilManager height:68.];
        else
            return [UtilManager height:260.];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0)
        return [UtilManager height:235];
    else{
    
    if(_dayEvents.count>0)
        return [UtilManager height:68.];
    else
        return [UtilManager height:260.];
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 0;
            break;
        case 1:
            return [UtilManager height:175];
            break;
        case 2:
            return [UtilManager height:140];
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            //return [UtilManager height:415];
            return 0;
            break;
        case 1:
            return [UtilManager height:175];
            break;
        case 2:
            return [UtilManager height:140];
            break;
        default:
            return 0;
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 1:
            return [self scheduleView];
            break;
        case 2:
            return [self preferencesView];
            break;
        default:
            return nil;
            break;
    }
}

- (UIView *)navigationViewAndHeaderView{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:290])];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:headerView.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"temp-image-event"]];
    [backgroundImageView setContentMode:UIViewContentModeScaleAspectFill];
    [headerView addSubview:backgroundImageView];
    
    return headerView;
}

- (UIView *)scheduleView{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:175])];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    NSDateComponents *fourWeeks = [[NSDateComponents alloc] init];
    fourWeeks.day = 28 * 7;
    NSDate *endDate = [[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]dateByAddingComponents:fourWeeks toDate:[NSDate date] options:0];
    
    if(!dayPicker)
    {
        dayPicker = [[DayPicker alloc] initWithFrame:CGRectMake(0., [UtilManager height:55], WIDTH, [UtilManager height:76])];
        [dayPicker addObserver:self forKeyPath:@"selectedDate" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
        [dayPicker setStartDate:_selectedDate endDate:endDate];
    }
    [dayPicker setBackgroundColor:[UIColor whiteColor]];
    dayPicker.selectedDateBackgroundImage = [UIImage imageNamed:@"image-day-selected"];
    [dayPicker setWeekdayFont:[UIFont fontWithName:LIGHT_FONT size:12]];
    [dayPicker setDateFont:[UIFont fontWithName:LIGHT_FONT size:14]];
    
    //[dayPicker setSelectedDateBackgroundColor:[UIColor orangeAppColor]];
    
    [headerView addSubview:dayPicker];
    
    UIView *dayLabelContainerView = [[UIView alloc] initWithFrame:CGRectMake(-1, dayPicker.frame.origin.y + dayPicker.frame.size.height, WIDTH + 2, [UtilManager height:50])];
    [[dayLabelContainerView layer] setBorderWidth:1];
    [[dayLabelContainerView layer] setBorderColor:GRAY_SEPARATOR_COLOR.CGColor];
    [dayLabelContainerView setBackgroundColor:[UIColor colorWithRed:242/255. green:242/255. blue:242/255. alpha:1]];
    [headerView addSubview:dayLabelContainerView];
    
    if(!dayLabel)
        dayLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:16], 0, WIDTH, dayLabelContainerView.frame.size.height)];
    [dayLabel setBackgroundColor:[UIColor clearColor]];
    [dayLabel setAttributedText:[NSString stringWithFormat:@"%@%@ %@",[_selectedDate currentDay],[_selectedDate daySuffix],[_selectedDate currentMonth]] withLineHeight:20 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:REGULAR_FONT size:14] andColor:BLACK_TEXT_COLOR andKern:-0.47 withLineBreak:NO];
    
    [dayLabelContainerView addSubview:dayLabel];
    
    if(!scheduleMonthView)
    {
        scheduleMonthView = [[ScheduleMonthView alloc] initWithFrame:CGRectMake(0, dayPicker.frame.origin.y - [UtilManager height:55], WIDTH, [UtilManager height:55.])];
        [scheduleMonthView setDelegate:self];
        [scheduleMonthView setUserInteractionEnabled:YES];
    }
    [scheduleMonthView setSelectedDate:_selectedDate];
    [headerView addSubview:scheduleMonthView];
    
    [headerView setUserInteractionEnabled:YES];
    return headerView;
}



- (UIView *)preferencesView{
    UIView *preferencesView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:140])];
    
    [preferencesView setBackgroundColor:[UIColor headerBackgroundColor]];
    
    UILabel *newsLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:15.], preferencesView.frame.size.width - [UtilManager width:6], [UtilManager height:22])];
    [newsLabel setTextColor:[UIColor footerTextAcentcolour]];
    [newsLabel setTextAlignment:NSTextAlignmentLeft];
    [newsLabel setText:NSLocalizedString(@"Want to see your events on a map?", nil)];
    [newsLabel setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    
    //[preferencesView addSubview:newsLabel];
    
    UILabel *updateLabel = [[UILabel alloc] initWithFrame:CGRectMake(newsLabel.frame.origin.x, newsLabel.frame.origin.y + newsLabel.frame.size.height, newsLabel.frame.size.width, newsLabel.frame.size.height)];
    [updateLabel setTextAlignment:NSTextAlignmentLeft];
    [updateLabel setTextColor:[UIColor whiteColor]];
    [updateLabel setText:NSLocalizedString(@"Click the button below", nil)];
    [updateLabel setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    
    //[preferencesView addSubview:updateLabel];
    
    UIImage *orangeButtonImage = [UIImage imageNamed:@"btn-view-event-map"];
    
    UIButton *preferencesButton = [[UIButton alloc] initWithFrame:CGRectMake(newsLabel.frame.origin.x, updateLabel.frame.origin.y + updateLabel.frame.size.height + [UtilManager height:10], orangeButtonImage.size.width * 0.85, orangeButtonImage.size.height * 0.85)];
    [preferencesButton setImage:orangeButtonImage forState:UIControlStateNormal];
    [preferencesButton addTarget:self action:@selector(preferencesAction) forControlEvents:UIControlEventTouchUpInside];
    //[preferencesButton setTitle:NSLocalizedString(@"Update My Preferences", nil) forState:UIControlStateNormal];
    
    //[preferencesView addSubview:preferencesButton];
    
    return preferencesView;
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    NSDate *day = change[NSKeyValueChangeNewKey];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:DATE_FORMATTER];
    NSString *dateString = [df stringFromDate:[NSDate dateWithTimeInterval:3600*3 sinceDate:day]];
    _dayEvents = [Event eventInDate:[df dateFromString:dateString]];
    
    [df setDateFormat:@"YYYY-MM-dd"];
    
    if(![[df stringFromDate:_selectedDate] isEqualToString:[df stringFromDate:[NSDate dateWithTimeInterval:1 sinceDate:day]]])
    {
        _selectedDate = [NSDate dateWithTimeInterval:3600*3 sinceDate:day];
    }
    
    [scheduleTableView reloadData];
    [dayLabel setAttributedText:[NSString stringWithFormat:@"%@%@ %@",[_selectedDate currentDay],[_selectedDate daySuffix],[_selectedDate currentMonth]] withLineHeight:20 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:SEMIBOLD_FONT size:12] andColor:BLACK_TEXT_COLOR andKern:-0.47 withLineBreak:NO];
}

- (void)preferencesAction{
    
}

#pragma mark ScheduleMonthViewDelegate Methods

- (void)nextMonthDidPush{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:dayPicker.selectedDate options:0];
    [dayPicker setSelectedDate:newDate];
    _selectedDate = newDate;
    dateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:newDate];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSString *monthName = [[df monthSymbols] objectAtIndex:([dateComponents month]-1)];
    [[scheduleMonthView dateLabel] setAttributedText:[NSString stringWithFormat:@"%@ %ld",monthName,(long)[dateComponents year]] withLineHeight:20 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:REGULAR_FONT size:20] andColor:[UIColor whiteColor] andKern:-0.56 withLineBreak:NO];
    
    [scheduleTableView reloadData];
}

- (void)backMonthDidPush{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:-1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:dayPicker.selectedDate options:0];
    [dayPicker setSelectedDate:newDate];
    dateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:newDate];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSString *monthName = [[df monthSymbols] objectAtIndex:([dateComponents month] - 1)];
    [[scheduleMonthView dateLabel] setAttributedText:[NSString stringWithFormat:@"%@ %ld",monthName,(long)[dateComponents year]] withLineHeight:20 andTextAlignment:NSTextAlignmentCenter andFont:[UIFont fontWithName:REGULAR_FONT size:20] andColor:[UIColor whiteColor] andKern:-0.56 withLineBreak:NO];
    
    [df setDateFormat:DATE_FORMATTER];
    NSString *dateString = [df stringFromDate:newDate];
    _dayEvents = [Event eventInDate:[df dateFromString:dateString]];
    [scheduleTableView reloadData];
}

#pragma mark WebServiceMethods Delegate

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    if (webServiceType == WebServiceTypeEvents) {
        [self configureView];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{

}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
}

@end
