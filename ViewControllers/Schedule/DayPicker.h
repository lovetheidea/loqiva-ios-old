//
//  DayPicker.h
//  loqiva
//
//  Created by Manuel Manzanera on 29/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DayPicker : UIView

@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) NSArray *weekdayTitles;
@property (nonatomic, readonly) NSDate *startDate, *endDate;
@property (nonatomic, readonly) NSUInteger selectedWeekday;
@property (nonatomic, assign) UIEdgeInsets edgeInsets;
@property (nonatomic, strong) UIColor *weekdayTextColor;
@property (nonatomic, strong) UIFont *weekdayFont;
@property (nonatomic, strong) UIColor *selectedWeekdayTextColor;
@property (nonatomic, strong) UIColor *dateTextColor;
@property (nonatomic, strong) UIFont *dateFont;
@property (nonatomic, strong) UIColor *outOfRangeDateTextColor;
@property (nonatomic, strong) UIImage *selectedDateBackgroundImage;
@property (nonatomic, strong) UIColor *selectedDateBackgroundColor;
@property (nonatomic, assign) BOOL selectedDateBackgroundExtendsToTop;
@property (nonatomic, strong) UIColor *selectedDateTextColor;

- (void)setStartDate:(NSDate *)date endDate:(NSDate *)endDate;
+ (NSArray*)weekdayTitlesWithLocaleIdentifier:(NSString*)localeIdentifier length:(NSUInteger)length uppercase:(BOOL)uppercase;

@end
