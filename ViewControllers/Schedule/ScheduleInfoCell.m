//
//  ScheduleInfoCell.m
//  loqiva
//
//  Created by Manuel Manzanera on 24/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ScheduleInfoCell.h"
#import "loqiva-Swift.h"

@implementation ScheduleInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
