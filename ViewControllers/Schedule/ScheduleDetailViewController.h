//
//  ScheduleDetailViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 26/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "ParentViewController.h"

@interface ScheduleDetailViewController : ParentViewController

@property (nonatomic, assign) BOOL isFromMenu;
@property (nonatomic, strong) Event *currentEvent;
@property (nonatomic, strong) NSDate *selectedDate;

@end
