//
//  RewardsDetailViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 26/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "RewardsDetailViewController.h"
#import "AFNetworkReachabilityManager.h"
#import "MapViewController.h"
#import "WebServiceManager.h"
#import "UIImageView+AFNetworking.h"
#import "NavCustomView.h"
#import <MapKit/Mapkit.h>
#import "AppContext.h"
#import "LocationManager.h"
#import "UILabel+Extra.h"
#import "NSString+HTML.h"
#import "CustomAnnotation.h"
#import "CustomPinAnnotationView.h"
#import "Utility.h"
#import "UIColor+Helper.h"
#import "AppDelegate.h"
#import "loqiva-Swift.h"

@interface RewardsDetailViewController ()<CLLocationManagerDelegate, MKMapViewDelegate, MKAnnotation, WebServiceManagerDelegate>

@property (nonatomic, assign) BOOL isRedeemPush;
@property (nonatomic, assign) BOOL isFirstLoad;

@property (nonatomic, strong) UIView *navCustomView;

@property (nonatomic, strong) UIView *redeemUserView;
@property (nonatomic, strong) UIView *fourSeparatorView;
@property (nonatomic, strong) UIView *footerView;

@property (nonatomic, assign) CGFloat originY;

@property (nonatomic, strong) UIImageView *rewardImageView;
@property (nonatomic, strong) UILabel *distanceLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *companyLabel;

@property (nonatomic, strong) UIButton *confirmButton;

@property (nonatomic, strong) MKAnnotationView *selectedAnnotationView;

@end

@implementation RewardsDetailViewController

@synthesize scrollView, mapView, coordinate, manager;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isRedeemPush = FALSE;
    _isFirstLoad = TRUE;
    [self createLocationManager];
    
    [self configureView];
    [self addAnnotations];
    [super zoomToFitMapAnnotations];
}

- (void)configureView{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    if(!_isRedeemPush && _isFirstLoad){
        
        scrollView = [[LoqivaScrollView alloc] initWithFrame:CGRectMake(0., -20., WIDTH, HEIGHT + 20)];
        scrollView.delegate=self;
        
        _rewardImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,WIDTH,[UtilManager height:290])];
        
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],_currentReward.mediaurl]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        [_rewardImageView setImageWithURLRequest:imageRequest placeholderImage:[UtilManager imageWithColor:GRAY_TEXT_COLOR] success:nil failure:nil];
        [_rewardImageView setContentMode:UIViewContentModeScaleAspectFill];
        [[_rewardImageView layer] setMasksToBounds:YES];
        [scrollView addSubview:_rewardImageView];
        
        self.whiteView = [[UIView alloc] init];
        self.whiteView.frame = CGRectMake(0, _rewardImageView.frame.size.height, self.view.frame.size.width, 100);
        [scrollView addSubview:self.whiteView];
        
        [self.whiteView setBackgroundColor:[UIColor whiteColor]];
        
        
        UIImage *pinImage;
        pinImage = [UIImage imageNamed:@"icon-pin-small"];

        UIImageView *pinImageView;
        pinImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:20.], [UtilManager height:15], pinImage.size.width, pinImage.size.height)];
        [pinImageView setImage:pinImage];
        [self.whiteView addSubview:pinImageView];
        
        _distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x + pinImageView.frame.size.width + [UtilManager width:10.], pinImageView.frame.origin.y, [UtilManager width:180.], [UtilManager height:20])];
        [_distanceLabel setTextColor:[UIColor DistanceLabelTextColor]];
        [_distanceLabel setCenter:CGPointMake(_distanceLabel.center.x, pinImageView.center.y)];
        
        [_distanceLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:11]];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:_currentReward.latitude.doubleValue longitude:_currentReward.longitude.doubleValue];
        
        NSString *distance = _currentReward.distance;
        if ([distance isKindOfClass:[NSNumber class]]) {
            distance = [(NSNumber *)distance stringValue];
        }
        if(distance.length < 7) {
            [_distanceLabel setText:[distance distanceString]];
        }
        else {
            CGFloat distance = [LocationManager getDistanceFrom:location];
            NSString *distanceString = [NSString stringWithFormat:@"%.0f", distance];
            
            if (location && distanceString.length < 7) {
                [_distanceLabel setText:[[NSString stringWithFormat:@"%.0f", [LocationManager getDistanceFrom:location]] distanceString]];
            }
            else {
                [_distanceLabel setText:@""];
            }
        }
        [_distanceLabel setAdjustsFontSizeToFitWidth:YES];
        
        [self.whiteView addSubview:_distanceLabel];
        
        
        
        // Seperator
        UIView *firstSeparatorView;
        firstSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _distanceLabel.frame.origin.y + _distanceLabel.frame.size.height + [UtilManager height:10], WIDTH - 2 * pinImageView.frame.origin.x, 1)];
        [firstSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [self.whiteView addSubview:firstSeparatorView];
        
        

        // Title Label
        CGFloat titleHeight = [UtilManager heightForText:_currentReward.title havingWidth:firstSeparatorView.frame.size.width andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE]];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, firstSeparatorView.frame.origin.y + [UtilManager height:20.], firstSeparatorView.frame.size.width, titleHeight)];
        
        [_titleLabel setAttributedText:[Utility convertHtmlToPlainText: _currentReward.title] withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:TITLE_FONT_KERN withLineBreak:NO];
        
        CGRect rect = [_titleLabel.attributedText boundingRectWithSize:CGSizeMake(_titleLabel.frame.size.width, _titleLabel.frame.size.height) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        
        [_titleLabel setFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y, _titleLabel.frame.size.width, rect.size.height)];
        [_titleLabel setNumberOfLines:0];
        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
        [_titleLabel setBackgroundColor:TEST_BG_ONE];
        [self.whiteView addSubview:_titleLabel];

        
        
        // Seperator
        UIView *secondSeparatorView;
        secondSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + [UtilManager height:15], WIDTH - 2 * pinImageView.frame.origin.x, 1)];
        [secondSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [self.whiteView addSubview:secondSeparatorView];
        
        
        
        // Company Label
        CGFloat companyHeight = [UtilManager heightForText:_currentReward.company havingWidth:firstSeparatorView.frame.size.width andFont:[UIFont fontWithName:SEMIBOLD_FONT size:CONTACT_FONT_SIZE]];
        _companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, secondSeparatorView.frame.origin.y + [UtilManager height:15], secondSeparatorView.frame.size.width, companyHeight)];
        [_companyLabel setAttributedText:[Utility convertHtmlToPlainText: _currentReward.company] withLineHeight:CONTACT_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:SEMIBOLD_FONT size:CONTACT_FONT_SIZE] andColor:[UIColor CompanyNameTextColour] andKern:CONTACT_FONT_KERN withLineBreak:NO];
        CGRect companyRect = [_companyLabel.attributedText boundingRectWithSize:CGSizeMake(_companyLabel.frame.size.width, _companyLabel.frame.size.height) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        [_companyLabel setFrame:CGRectMake(_companyLabel.frame.origin.x, _companyLabel.frame.origin.y, _companyLabel.frame.size.width, companyRect.size.height)];
        [_companyLabel setNumberOfLines:0];
        [_companyLabel setAdjustsFontSizeToFitWidth:YES];
        [_companyLabel setBackgroundColor:TEST_BG_TWO];
        [self.whiteView addSubview:_companyLabel];
        
        
        // Invisible seperator added as there is issues with the ordering of the Address
        UIView *addressSeparatorView;
        addressSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _companyLabel.frame.origin.y + _companyLabel.frame.size.height + [UtilManager height:0], WIDTH - 2 * pinImageView.frame.origin.x, 1)];
        [addressSeparatorView setBackgroundColor:TRANSPARENT_ALPHA];
        [self.whiteView addSubview:addressSeparatorView];
        
        
        // Address Label
        CGFloat addressHeight = [UtilManager heightForText:_currentReward.address havingWidth:firstSeparatorView.frame.size.width andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE]];
        _addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, addressSeparatorView.frame.origin.y + [UtilManager height:0], secondSeparatorView.frame.size.width, addressHeight)];
        [_addressLabel  setAttributedText:[Utility convertHtmlToPlainText: _currentReward.address] withLineHeight:CONTACT_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:SEMIBOLD_FONT size:CONTACT_FONT_SIZE] andColor:GRAY_DARK_TEXT_COLOR andKern:CONTACT_FONT_KERN withLineBreak:NO];
        CGRect addressRect = [_addressLabel.attributedText boundingRectWithSize:CGSizeMake(_addressLabel.frame.size.width, _addressLabel.frame.size.height) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        [_addressLabel setFrame:CGRectMake(_addressLabel.frame.origin.x, _addressLabel.frame.origin.y, _addressLabel.frame.size.width, addressRect.size.height)];
        [_addressLabel setNumberOfLines:0];
        [_addressLabel setAdjustsFontSizeToFitWidth:YES];
        [_addressLabel setBackgroundColor:TEST_BG_ONE];
        [self.whiteView addSubview:_addressLabel];
        
        
        
    
        
        // Date Label
        _dateLabel= [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _addressLabel.frame.origin.y + _addressLabel.frame.size.height + [UtilManager height:25], firstSeparatorView.frame.size.width, [UtilManager height:16])];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:DATE_FORMATTER];
        [_dateLabel setTextColor:GRAY_TEXT_HOME_COLOR];
        [_dateLabel setFont:[UIFont fontWithName:REGULAR_FONT size:10]];
        [_dateLabel setNumberOfLines:0];
        [_dateLabel setBackgroundColor:TEST_BG_TWO];
        [_dateLabel setAdjustsFontSizeToFitWidth:YES];
        
        id timeArray = [_currentReward.timetables valueForKey:@"opening"];
        if (timeArray != [NSNull null]) {
            NSArray *timeTables = [_currentReward.timetables valueForKey:@"opening"];
            NSMutableString *timeTable = [[NSMutableString alloc] initWithString:@""];
            
            int count = 0;
            for(NSDictionary *hoursDictionary in timeTables){
                if(count == 2 || count == 4 || count == 6){
                    [timeTable appendFormat:@"\n%@",[hoursDictionary valueForKey:@"hours"]];
                }else{
                    if([timeTable isEqualToString:@""])
                        [timeTable appendFormat:@"%@",[hoursDictionary valueForKey:@"hours"]];
                    else
                        [timeTable appendFormat:@", %@",[hoursDictionary valueForKey:@"hours"]];
                }
                count ++;
            }
            
            CGFloat timetableHeight = [UtilManager heightForText:timeTable havingWidth:_dateLabel.frame.size.width andFont:_dateLabel.font];
            [_dateLabel setFrame:CGRectMake(_dateLabel.frame.origin.x, _dateLabel.frame.origin.y, _dateLabel.frame.size.width, timetableHeight)];
            [_dateLabel setBackgroundColor:TEST_BG_ONE];
            [_dateLabel setText:timeTable];
        }
        
    
        [self.whiteView addSubview:_dateLabel];
 
        UIView *thirdSeparatorView;
        
        thirdSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, _dateLabel.frame.origin.y + _dateLabel.frame.size.height + [UtilManager height:20], WIDTH - 2 * pinImageView.frame.origin.x, 1)];
        [thirdSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [self.whiteView addSubview:thirdSeparatorView];
        
        UIImage *redeemRewardImage = [UIImage imageNamed:@"btn-redeem-reward"];
        
        UIButton *redeemRewardButton;
        
        redeemRewardButton = [[UIButton alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, thirdSeparatorView.frame.origin.y + [UtilManager height:20], [UtilManager width:160], [UtilManager height:40])];
        [redeemRewardButton setImage:redeemRewardImage forState:UIControlStateNormal];
        [redeemRewardButton addTarget:self action:@selector(redeemRewardAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.whiteView addSubview:redeemRewardButton];
        
        UIImage *getDirectionsImage = [UIImage imageNamed:@"btn-get-directions"];
        
        UIButton *getDirectionsButton;
        if(!_isRedeemPush && _isFirstLoad)
        {
            getDirectionsButton =[[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:203] , redeemRewardButton.frame.origin.y, [UtilManager width:140], redeemRewardButton.frame.size.height)];
            [getDirectionsButton setImage:getDirectionsImage forState:UIControlStateNormal];
            [getDirectionsButton addTarget:self action:@selector(getDirectionsAction) forControlEvents:UIControlEventTouchUpInside];
            [self.whiteView addSubview:getDirectionsButton];
        }
        
        _originY = getDirectionsButton.frame.origin.y + getDirectionsButton.frame.size.height + [UtilManager height:15];
    }
    else if(!_isRedeemPush && !_isFirstLoad){
        
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],_currentReward.mediaurl]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        [_rewardImageView setImageWithURLRequest:imageRequest placeholderImage:nil success:nil failure:nil];

        [_addressLabel setAttributedText:_currentReward.address withLineHeight:CONTACT_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:SEMIBOLD_FONT size:CONTACT_FONT_SIZE] andColor:GRAY_DARK_TEXT_COLOR andKern:CONTACT_FONT_KERN withLineBreak:NO];
        
        CGRect addressRect = [_addressLabel.attributedText boundingRectWithSize:CGSizeMake(_addressLabel.frame.size.width, _addressLabel.frame.size.height) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        [_addressLabel setFrame:CGRectMake(_addressLabel.frame.origin.x, _addressLabel.frame.origin.y, _addressLabel.frame.size.width, addressRect.size.height)];
        [_addressLabel setNumberOfLines:0];
        [_addressLabel setAdjustsFontSizeToFitWidth:YES];
        [self.whiteView addSubview:_addressLabel];
    }
    
    if(!_isRedeemPush && _isFirstLoad)
    {
        _redeemUserView = [self redeemUserViewWithFrame:CGRectMake(0., _originY, scrollView.frame.size.width, [UtilManager height:434])];
    }
    
    if(_isRedeemPush){
        
        [self.whiteView addSubview:_redeemUserView];
        
        [self.scrollView scrollRectToVisible:CGRectMake(0, 890, 10, 10) animated:YES];
    }else if(!_isRedeemPush && !_isFirstLoad)
        [_redeemUserView removeFromSuperview];
    
    if(!_isRedeemPush && _isFirstLoad)
    {
        _fourSeparatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20.], _originY + 3, WIDTH - 2 * [UtilManager width:20.], 1)];
        [_fourSeparatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [self.whiteView addSubview:_fourSeparatorView];
    }
    else {
        [_fourSeparatorView setFrame:CGRectMake([UtilManager width:20.], _originY + 3, WIDTH - 2 * [UtilManager width:20.], 1)];
    }
    
    NSString *summaryString;
    summaryString = [Utility convertHtmlToPlainText: _currentReward.summary];
    CGFloat summaryHeight = [UtilManager heightForText:summaryString havingWidth:_fourSeparatorView.frame.size.width andFont:[UIFont fontWithName:LIGHT_FONT size:16]];
    if(!_isRedeemPush && _isFirstLoad){
        self.summaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(_fourSeparatorView.frame.origin.x,_fourSeparatorView.frame.origin.y + [UtilManager height:20] , _fourSeparatorView.frame.size.width, summaryHeight)];
    }else{
        [self.summaryLabel setFrame:CGRectMake(_fourSeparatorView.frame.origin.x,_fourSeparatorView.frame.origin.y + [UtilManager height:20] , _fourSeparatorView.frame.size.width, summaryHeight)];
    }
    
    [self.summaryLabel setTextAlignment:NSTextAlignmentLeft];
    [self.summaryLabel setAttributedText:summaryString withLineHeight:BODY_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:BODY_FONT_SIZE] andColor:GRAY_DARK_TEXT_COLOR andKern:BODY_FONT_KERN withLineBreak:NO];
    CGRect rect = [self.summaryLabel.attributedText boundingRectWithSize:CGSizeMake(self.summaryLabel.frame.size.width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    [self.summaryLabel setFrame:CGRectMake(self.summaryLabel.frame.origin.x, self.summaryLabel.frame.origin.y, self.summaryLabel.frame.size.width, rect.size.height)];
    [self.summaryLabel setNumberOfLines:0];
    [self.summaryLabel setBackgroundColor:TEST_BG_ONE];
    [self.whiteView addSubview:self.summaryLabel];

    if(!_isRedeemPush && _isFirstLoad)
    {
        [self addMap];
    } else {
        [mapView setFrame:CGRectMake(0., self.summaryLabel.frame.origin.y + self.summaryLabel.frame.size.height + [UtilManager height:30], WIDTH, [UtilManager height:180])];
    }
    
    if(!_isRedeemPush && _isFirstLoad){
        _footerView = [self rewardOnMapViewWithOriginY:(mapView.frame.origin.y + mapView.frame.size.height)];
        _footerView.frame = CGRectMake(0, self.view.frame.size.height - _footerView.frame.size.height, _footerView.frame.size.width, _footerView.frame.size.height);
        [self.view addSubview:_footerView];
    } else {
        _footerView.frame = CGRectMake(0, self.view.frame.size.height - _footerView.frame.size.height, _footerView.frame.size.width, _footerView.frame.size.height);
    }
    
    self.whiteView.frame = CGRectMake(self.whiteView.frame.origin.x, self.whiteView.frame.origin.y, self.whiteView.frame.size.width, mapView.frame.origin.y + mapView.frame.size.height);
    [scrollView setContentSize:CGSizeMake(WIDTH, self.whiteView.frame.origin.y + self.whiteView.frame.size.height + _footerView.frame.size.height)];
    [scrollView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:scrollView];
    
    [self setupHeaderForSection:CMSectionTypeRewardsDetail];
    
    [self.headerNav removeFromSuperview];
    
    [self.view insertSubview:self.headerNav aboveSubview:scrollView];
    
    _isFirstLoad = FALSE;
}

- (void)addAnnotations{
    CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(_currentReward.latitude.doubleValue, _currentReward.longitude.doubleValue) title:_currentReward.title summary:_currentReward.summary subtitle:@"" pin:@"image-pin-reward"];
    [annotation setAccessibilityLabel:_currentReward.title];
    [annotation setType:@"Attraction"];
    [annotation setImage:@""];
    
    [mapView addAnnotations:@[annotation]];
}

- (UIView *)redeemUserViewWithFrame:(CGRect)frame{
    UIView *redeemView = [[UIView alloc] initWithFrame:frame];
    [redeemView setBackgroundColor:[UIColor redeemRewardViewBGColor]];
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:35], [UtilManager width:35], WIDTH - [UtilManager width:70], [UtilManager height:20])];
    [infoLabel setText:NSLocalizedString(@"Please show this screen to redeem your reward", nil)];
    [infoLabel setTextColor:[UIColor whiteColor]];
    [infoLabel setFont:[UIFont fontWithName:REGULAR_FONT size:14]];
    [infoLabel setAdjustsFontSizeToFitWidth:YES];
    
    [redeemView addSubview:infoLabel];
    
    UIImageView *profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.,infoLabel.frame.origin.y + infoLabel.frame.size.height + [UtilManager height:20], [UtilManager height:130], [UtilManager height:130])];
    [profileImageView setCenter:CGPointMake(redeemView.center.x, profileImageView.center.y)];
    [[profileImageView layer] setMasksToBounds:YES];
    [[profileImageView layer] setCornerRadius:profileImageView.frame.size.width/2];
    [profileImageView setContentMode:UIViewContentModeScaleAspectFit];
    
    NSString *profileImageURL = [[[SessionManager shared] user] sourceImage];
    
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],profileImageURL]]
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [profileImageView setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageNamed:@"no-profile-image"] success:nil failure:nil];
    
    [redeemView addSubview:profileImageView];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., profileImageView.frame.origin.y + profileImageView.frame.size.height + [UtilManager height:20], WIDTH, [UtilManager height:45])];
    [nameLabel setTextAlignment:NSTextAlignmentCenter];
    [nameLabel setFont:[UIFont fontWithName:REGULAR_FONT size:30]];
    [nameLabel setTextColor:[UIColor whiteColor ]];
    [nameLabel setText:[NSString stringWithFormat:@"%@ %@",[[[SessionManager shared] user] firstName],[[[SessionManager shared] user] lastName]]];
    [nameLabel setAdjustsFontSizeToFitWidth:YES];
    
    [redeemView addSubview:nameLabel];
    
    UILabel *memberLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., nameLabel.frame.origin.y + nameLabel.frame.size.height + [UtilManager height:8], WIDTH, [UtilManager height:20])];
    [memberLabel setTextAlignment:NSTextAlignmentCenter];
    [memberLabel setText:NSLocalizedString(@"Membership Number", nil)];
    [memberLabel setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    [memberLabel setAdjustsFontSizeToFitWidth:YES];
    [memberLabel setTextColor:[UIColor whiteColor]];
    
    // [redeemView addSubview:memberLabel];
    
    UILabel *numberMemberLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., memberLabel.frame.origin.y + memberLabel.frame.size.height + [UtilManager height:8], WIDTH, [UtilManager height:20])];
    [numberMemberLabel setTextAlignment:NSTextAlignmentCenter];
    [numberMemberLabel setText:NSLocalizedString(@"#0123456789", nil)];
    [numberMemberLabel setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    [numberMemberLabel setAdjustsFontSizeToFitWidth:YES];
    [numberMemberLabel setTextColor:[UIColor whiteColor]];
    
    // [redeemView addSubview:numberMemberLabel];
    
    _confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(0., numberMemberLabel.frame.origin.y + numberMemberLabel.frame.size.height - [UtilManager height:15], [UtilManager width:240], [UtilManager height:64])];
    [_confirmButton setCenter:CGPointMake(redeemView.center.x, _confirmButton.center.y)];
    [_confirmButton setBackgroundImage:[UIImage imageNamed:@"btn-reward-accept"] forState:UIControlStateNormal];
    [_confirmButton addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    [redeemView addSubview:_confirmButton];
    
    return redeemView;
}

/**- (UIView *)navigationView{
 
 // NavCustomView *navCustomView = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) andUIBlurEffectStyle:UIBlurEffectStyleLight];
 NavCustomView *navCustomView = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT) gradienAndUIBlurEffectStyle:UIBlurEffectStyleLight];
 
 
 
 [[navCustomView icon] setImage:[UIImage imageNamed:@"icon-rewards"]];
 [[navCustomView iconLabel] setAttributedText:NSLocalizedString(@"Rewards", nil) withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:23] andColor:[UIColor whiteColor] andKern:-0.71];
 [[navCustomView backButton] setHidden:FALSE];
 [[navCustomView backButtonImage] setHidden:FALSE];
 [[navCustomView notificationLabel] setHidden:TRUE];
 [[navCustomView notificationButtonImage] setHidden:TRUE];
 [navCustomView setBackOption];
 
 return navCustomView;
 }**/

- (UIView *)rewardOnMapViewWithOriginY:(CGFloat)originY{
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., originY, WIDTH, [UtilManager height:145])];
    [footerView setBackgroundColor:[UIColor footerColor]];
    
    UILabel *newsLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:20], WIDTH - [UtilManager width:6], [UtilManager height:20])];
    [newsLabel setAttributedText:NSLocalizedString(@"Want to see your rewards on a map?", nil) withLineHeight:16 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:16] andColor:[UIColor footerTextAcentcolour] andKern:-0.6 withLineBreak:NO];
    [footerView addSubview:newsLabel];
    
    UILabel *updateLabel = [[UILabel alloc] initWithFrame:CGRectMake(newsLabel.frame.origin.x, newsLabel.frame.origin.y + newsLabel.frame.size.height + [UtilManager height:5], newsLabel.frame.size.width, newsLabel.frame.size.height)];
    [updateLabel setAttributedText:NSLocalizedString(@"Click the button below", nil) withLineHeight:16 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:16] andColor:[UIColor HeaderTextColour] andKern:-0.6 withLineBreak:NO];
    [footerView addSubview:updateLabel];

    
    UIImage *orangeButtonImage = [UIImage imageNamed:@"btn-rewards-map"];
    
    UIButton *preferencesButton = [[UIButton alloc] initWithFrame:CGRectMake(newsLabel.frame.origin.x, updateLabel.frame.origin.y + updateLabel.frame.size.height + [UtilManager height:15], orangeButtonImage.size.width, orangeButtonImage.size.height)];
    [preferencesButton setImage:orangeButtonImage forState:UIControlStateNormal];
    [preferencesButton addTarget:self action:@selector(rewardsMapAction) forControlEvents:UIControlEventTouchUpInside];
    //[preferencesButton setTitle:NSLocalizedString(@"Update My Preferences", nil) forState:UIControlStateNormal];
    
    [footerView addSubview:preferencesButton];
    
    return footerView;
}

- (void)redeemRewardAction:(id)sender{
    UIButton *rewardButton = (UIButton *)sender;
    
    if(_isRedeemPush){
        _isRedeemPush = FALSE;
        [rewardButton setImage:[UIImage imageNamed:@"btn-redeem-reward"] forState:UIControlStateNormal];
        _originY = _originY - [UtilManager height:434];
    } else {
        _isRedeemPush = TRUE;
        [rewardButton setImage:[UIImage imageNamed:@"btn-redeem-reward-push"] forState:UIControlStateNormal];
        _originY = _originY + [UtilManager height:434];
    }
    
    [self configureView];
}

- (void)rewardsMapAction {
    NSDictionary *notifDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"mapViewController",VIEWCONTROLLER_NOTIFICATION,
                                     [NSNumber numberWithInt:0],@"firstParam",nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:VIEWCONTROLLER_NOTIFICATION object:notifDictionary];
}

- (void)getDirectionsAction {
    MapViewController *mapViewController = [[MapViewController alloc] init];
    [mapViewController setCurrentReward:_currentReward];
    [self.navigationController pushViewController:mapViewController animated:YES];
}

- (void)confirmAction {
    if (![AppDelegate showAlertIfDisconnected]) {
        [WebServiceManager postRedeemRewardsItem:[NSString stringWithFormat:@"%d",_currentReward.mapitempoint.intValue] andDelegate:self];
        [_confirmButton setEnabled:FALSE];
    } 
}

- (void)backAction{
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float y = scrollView.contentOffset.y;
    CGRect imageFrame = self.rewardImageView.frame;
    imageFrame.origin.y = y/2;
    self.rewardImageView.frame = imageFrame;
}

#pragma mark WebServiceManagerDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    if(webServiceType == WebServiceTypeRewardsDetail){
        
    } else if(webServiceType == WebServiceTypeRewardsDetail){
        
    }else if(webServiceType == WebServiceTypeRedeemRewardsDetail){
        [_confirmButton setEnabled:YES];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    if(webServiceType == WebServiceTypeRewardsDetail){
        
    } else if(webServiceType == WebServiceTypeRewardsDetail){
        
    } else if(webServiceType == WebServiceTypeRedeemRewardsDetail){
        [_confirmButton setEnabled:YES];
    }
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    if(webServiceType == WebServiceTypeRewardsDetail){
        
    } else if(webServiceType == WebServiceTypeRewardsDetail){
        [_confirmButton setEnabled:YES];
        
    }else if(webServiceType == WebServiceTypeRedeemRewardsDetail){
        NSString *alertMessage = (NSString *)object;
        [UtilManager presentAlertWithMessage:alertMessage];
    }
}

@end
