//
//  RewardsViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "RewardsViewController.h"
#import "RewardsDetailViewController.h"
#import "MapViewController.h"
#import "RewardsCell.h"
#import "Reward.h"
#import "NavCustomView.h"
#import "AppContext.h"
#import "LocationManager.h"
#import "TemporaryUserViewController.h"
#import "Utility.h"
#import "NSString+HTML.h"
#import "UIColor+Helper.h"
#import "WebServiceManager.h"
#import "AppDelegate.h"
#import "loqiva-Swift.h"

@interface RewardsViewController ()<UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate>

@property (nonatomic, strong) NSArray *rewards;
@property (nonatomic, strong) NavCustomView *navView;

@end

@implementation RewardsViewController

@synthesize rewardsTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _rewards = [[RewardCache shared] cachedRewards];

    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor orangeAppColor];
    [self.refreshControl addTarget:self action:@selector(refreshControlAction) forControlEvents:UIControlEventValueChanged];
    [self.rewardsTableView addSubview:self.refreshControl];
    [self configureView];
    
    [self refreshControlAction];
}

- (void)refreshControlAction {
    [self refreshRewardsFromServer];
}

- (void)configureView{
    [self.view setBackgroundColor:TEST_BG_ONE];

    rewardsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 48, WIDTH, HEIGHT - 48) style:UITableViewStylePlain];

    [rewardsTableView setDelegate:self];
    [rewardsTableView setDataSource:self];
    [rewardsTableView registerClass:[RewardsCell class] forCellReuseIdentifier:@"rewardsCell"];
    [rewardsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.view addSubview:rewardsTableView];

    [self setupHeaderForSection:CMSectionTypeRewards];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([AppDelegate showAlertIfDisconnected]) {
            return;
        }

    if(![[SessionManager shared] user]){
    
            __weak RewardsViewController * weakSelf = self;
            self.tempUserViewController = [[TemporaryUserViewController alloc] initWithDismissBlock:^{
                    [weakSelf.tempUserViewController.view removeFromSuperview];
        
        
                } andCallToSignForFree:^{
                        [weakSelf.tempUserViewController.view removeFromSuperview];
                        [weakSelf presentSignForFreeController];
            
                    }];
    
            [self.view addSubview:self.tempUserViewController.view];
    
            return;
        }

    if(indexPath.section == 1){
            [rewardsTableView deselectRowAtIndexPath:indexPath animated:YES];
            RewardsDetailViewController *rewardsDetailViewController = [[RewardsDetailViewController alloc] init];
            Reward *currentReward = [_rewards objectAtIndex:indexPath.row];
            [rewardsDetailViewController setCurrentReward:currentReward];
    
            [self.navigationController pushViewController:rewardsDetailViewController animated:YES];
        }
}

#pragma mark UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        
                case 0:
                    return 0;
                case 1:
                    return _rewards.count;
                default:
                    return 0;
                    break;
        }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RewardsCell *cell = [rewardsTableView dequeueReusableCellWithIdentifier:@"rewardsCell"];

    switch (indexPath.section) {
        
                case 1:
                {
                        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
                        Reward *reward = [_rewards objectAtIndex:indexPath.row];
                        CGFloat summaryHeight = [UtilManager heightForText:reward.title havingWidth:cell.summaryLabel.frame.size.width andFont:[UIFont fontWithName:REGULAR_FONT size:15]];
                        [[cell titleLabel] setText:reward.company];
            
                        if(summaryHeight > 65)
                                summaryHeight = 65;
            
                        [[cell summaryLabel] setFrame:CGRectMake(cell.summaryLabel.frame.origin.x, cell.titleLabel.frame.origin.y + [UtilManager height:22], cell.summaryLabel.frame.size.width, [UtilManager height:65])];
                        [[cell summaryLabel] setAttributedText:[Utility convertHtmlToPlainText: reward.title] withLineHeight:17 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:REGULAR_FONT size:14] andColor:BLACK_TEXT_COLOR andKern:-0.2 withLineBreak:NO];
                        [[cell summaryLabel] sizeToFit];
                        [[cell summaryLabel] setNumberOfLines:3];
                        [[cell summaryLabel] setBackgroundColor:TEST_BG_ONE];
            
            
                        NSString *distance = reward.distance;
                        if ([distance isKindOfClass:[NSNumber class]]) {
                                distance = [(NSNumber *)distance stringValue];
                            }
                        [[cell distanceLabel] setText:[distance distanceString]];
            
            
                        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],reward.mediaurl]]
                                                                                                                 cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                                                                             timeoutInterval:60];
            
            
                        [cell.imageView setImageWithURLRequest:imageRequest placeholderImage:[UtilManager imageWithColor:GRAY_TEXT_COLOR] success:nil failure:nil];
                        [cell.imageView setContentMode:UIViewContentModeScaleAspectFill];
                        [[cell.imageView layer] setMasksToBounds:YES];
            
                    }
                    break;
                default:
                    break;
        }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [UtilManager height:130];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
   return [UtilManager height:130];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    switch (section) {
        
                case 0:
                    return [UtilManager height:0];
                    break;
                case 1:
                    return [UtilManager height:130];
                    break;
                case 2:
                    return [UtilManager height:140];
                    break;
                default:
                    return 0;
                    break;
        }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        
                case 0:
                    return [UtilManager height:0];
                    break;
                case 1:
                    return [UtilManager height:130];
                    break;
                case 2:
                    return [UtilManager height:140];
                    break;
                default:
                    return 0;
                    break;
        }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (section) {
        
        
                case 1:
                {
                        return [self headerTitleView:@"Check out our Great Rewards"];
                    }
                    break;
                case 2:
                {
                        return [self preferencesView];
                    }
                    break;
                default:
                    return nil;
                    break;
        }
}

- (UIView *)headerTitleView:(NSString *)title{
    UIView *headerTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, [UtilManager height:130])];
    [headerTitleView setBackgroundColor:WHITE_TEXT_COLOR];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:35], [UtilManager width:310], [UtilManager height:80])];
    [titleLabel setNumberOfLines:0];
    [titleLabel setText:title];
    [titleLabel setAdjustsFontSizeToFitWidth:NO];
    [titleLabel setBackgroundColor:TEST_BG_ONE];
    [titleLabel setAttributedText:title withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:[UIColor HeaderTextColour] andKern:TITLE_FONT_KERN withLineBreak:NO];

    [headerTitleView addSubview:titleLabel];

    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., headerTitleView.frame.size.height - 1, WIDTH, 1)];
    [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
    [headerTitleView addSubview:separatorView];

    return headerTitleView;
}

- (UIView *)navigationView{

    if(self.navView == nil){
    
       self.navView = [[NavCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_BAR_HEIGHT)];
        self.navView.parentController = self;
        [self.navView setBackgroundColor:[UIColor navigationBackgroundColor]];
        [[self.navView iconLabel] setAttributedText:NSLocalizedString(@"Rewards", nil) withLineHeight:32 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:10] andColor:[UIColor whiteColor] andKern:-0.71 withLineBreak:NO];
        [[self.navView separatorView] setBackgroundColor:[UIColor navigationSeparatorColor]];
        [[self.navView separatorView] setAlpha:SEPARATOR_VIEW_ALPHA];
        [[self.navView backButton] setHidden:TRUE];
    
    
    #ifdef LOQIVA
        [[self.navView icon] setImage:[UIImage imageNamed:@"icon-rewards-orange"]];
        [[self.navView slideButton] setImage:[UIImage imageNamed:@"btn-slider-orange"]];
       // [[navCustomView notificationButton] setImage:[UIImage imageNamed:@"btn-notification-orange"]];
    #elif OLDSEAPORT
        [[self.navView icon] setImage:[UIImage imageNamed:@"icon-rewards"]];
        [[self.navView slideButton] setImage:[UIImage imageNamed:@"btn-slider"]];
        [[self.navView notificationButtonImage]setImage:[UIImage imageNamed:@"btn-notification"]];
    
    #endif
    
    
        }

    return self.navView;
}

//Footer
- (UIView *)preferencesView{
    UIView *preferencesView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:140])];

    [preferencesView setBackgroundColor:[UIColor footerColor]];

    UILabel *newsLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:20], WIDTH - [UtilManager width:6], [UtilManager height:20])];
    [newsLabel setAttributedText:NSLocalizedString(@"Want to see your rewards on a map?", nil) withLineHeight:16 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:16] andColor:[UIColor footerTextAcentcolour] andKern:-0.6 withLineBreak:NO];
    [preferencesView addSubview:newsLabel];
    
    UILabel *updateLabel = [[UILabel alloc] initWithFrame:CGRectMake(newsLabel.frame.origin.x, newsLabel.frame.origin.y + newsLabel.frame.size.height + [UtilManager height:5], newsLabel.frame.size.width, newsLabel.frame.size.height)];
    [updateLabel setAttributedText:NSLocalizedString(@"Click the button below", nil) withLineHeight:16 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:16] andColor:[UIColor HeaderTextColour] andKern:-0.6 withLineBreak:NO];
    [preferencesView addSubview:updateLabel];

    UIImage *orangeButtonImage = [UIImage imageNamed:@"btn-rewards-map"];

    UIButton *preferencesButton = [[UIButton alloc] initWithFrame:CGRectMake(newsLabel.frame.origin.x, updateLabel.frame.origin.y + updateLabel.frame.size.height + [UtilManager height:10], orangeButtonImage.size.width, orangeButtonImage.size.height)];
    [preferencesButton setImage:orangeButtonImage forState:UIControlStateNormal];
    [preferencesButton addTarget:self action:@selector(preferencesAction) forControlEvents:UIControlEventTouchUpInside];

    [preferencesView addSubview:preferencesButton];

    return preferencesView;
}

- (void)preferencesAction{
    MapViewController *mapViewController = [[MapViewController alloc] init];
    [mapViewController setMapSelectType:MapSelectTypeRewards];
    [self.navigationController pushViewController:mapViewController animated:YES];
}

@end
