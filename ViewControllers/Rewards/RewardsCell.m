//
//  RewardsCell.m
//  loqiva
//
//  Created by Manuel Manzanera on 29/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "RewardsCell.h"
#import "UIColor+Helper.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "LocationManager.h"
#import "NSString+HTML.h"
#import "Utility.h"
#import "loqiva-Swift.h"

@implementation RewardsCell

@synthesize imageView, titleLabel, summaryLabel, separatorView, iconPin, distanceLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];

    if(self){
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH/2, 0., WIDTH/2, [UtilManager height:130])];
        [imageView setBackgroundColor:[UIColor clearColor]];
        [imageView setImage:[UIImage imageNamed:@"temp-rewards-cell"]];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [self.contentView addSubview:imageView];






        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20.], [UtilManager height:9], [UtilManager width:160], [UtilManager height:15])];

//        [titleLabel setAttributedText:[Utility convertHtmlToPlainText: _currentInterestDetail.title] withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:10] andColor:[UIColor orangeButtonColor] andKern:TITLE_FONT_KERN];

        [titleLabel setTextColor:[UIColor CompanyNameTextColour]];
        [titleLabel setTextAlignment:NSTextAlignmentLeft];
        [titleLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:10]];

        [self.contentView addSubview:titleLabel];






        //CGFloat summarySize = [UtilManager heightForText:@"Save \u00c2\u00a35 when you spend \u00c2\u00a320 at Fusion" havingWidth:titleLabel.frame.size.width andFont:[UIFont fontWithName:REGULAR_FONT size:15]];

        summaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y + titleLabel.frame.size.height, [UtilManager width:150], [UtilManager height:150])];
        [summaryLabel setNumberOfLines:0];
        [summaryLabel setTextColor:BLACK_TEXT_COLOR];
        [summaryLabel setTextAlignment:NSTextAlignmentLeft];
        [summaryLabel setFont:[UIFont fontWithName:REGULAR_FONT size:14]];
      //  [summaryLabel sizeToFit];

        [self.contentView addSubview:summaryLabel];





        UIImage *pinImage;
        pinImage = [UIImage imageNamed:@"icon-pin-small"];
        iconPin = [[UIImageView alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, [UtilManager height:108], [UtilManager width:9], [UtilManager height:11])];
         NSLog(@"GTBBBBB:: %@",iconPin);
        [iconPin setImage:pinImage];
        [self.contentView addSubview:iconPin];




//        CGFloat titleHeight = [UtilManager heightForText:_currentReward.title havingWidth:firstSeparatorView.frame.size.width andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE]];
//        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(pinImageView.frame.origin.x, firstSeparatorView.frame.origin.y + [UtilManager height:20.], firstSeparatorView.frame.size.width, titleHeight)];
//        [_titleLabel setAttributedText:_currentReward.title withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:[UIColor orangeButtonColor] andKern:TITLE_FONT_KERN];
//        CGRect rect = [_titleLabel.attributedText boundingRectWithSize:CGSizeMake(_titleLabel.frame.size.width, _titleLabel.frame.size.height) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
//        [_titleLabel setFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y, _titleLabel.frame.size.width, rect.size.height)];
//        [_titleLabel setNumberOfLines:0];
//        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
//        [_titleLabel setBackgroundColor:TEST_BG_ONE];
//        [scrollView addSubview:_titleLabel];


        distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(iconPin.frame.origin.x + iconPin.frame.size.width + [UtilManager width:5], iconPin.frame.origin.y + [UtilManager width:-1], [UtilManager width:180.], iconPin.frame.size.height)];

//        [distanceLabel setAttributedText:_currentReward.title withLineHeight:TITLE_FONT_LINEHEIGHT andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:TITLE_FONT_SIZE] andColor:[UIColor orangeButtonColor] andKern:TITLE_FONT_KERN];

        [distanceLabel setTextColor:[UIColor DistanceLabelTextColor]];
        [distanceLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:10]];
        [distanceLabel setAdjustsFontSizeToFitWidth:YES];

        [self.contentView addSubview:distanceLabel];





        separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., [UtilManager height:129], WIDTH, 1)];
        [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];

        [self.contentView addSubview:separatorView];

    }

    return self;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
