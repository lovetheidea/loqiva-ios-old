//
//  RewardsViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ParentViewController.h"

@interface RewardsViewController : ParentViewController 
@property (nonatomic, strong) UITableView *rewardsTableView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@end
