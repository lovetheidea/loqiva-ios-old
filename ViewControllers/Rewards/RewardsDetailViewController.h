//
//  RewardsDetailViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 26/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "POIDetailViewController.h"
#import "Reward.h"

@interface RewardsDetailViewController : POIDetailViewController<UIScrollViewDelegate>

@property (nonatomic, assign) BOOL isFromMenu;
@property (nonatomic, strong) Reward *currentReward;

// private (to expose methods in swift)
- (void)configureView;
@end
