//
//  RewardsCell.h
//  loqiva
//
//  Created by Manuel Manzanera on 29/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"

@interface RewardsCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *summaryLabel;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, strong) UIImageView *iconPin;
@property (nonatomic, strong) UILabel *distanceLabel;

@end
