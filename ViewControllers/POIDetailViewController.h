//
//  POIDetailViewController.h
//  loqiva
//
//  Created by Matt Harding on 01/08/2018.
//  Copyright © 2018 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "LoqivaScrollView.h"
#import <MapKit/Mapkit.h>

@interface POIDetailViewController : ParentViewController<MKMapViewDelegate>

@property (nonatomic, strong) LoqivaScrollView *scrollView;
@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) UIView *whiteView;
@property (nonatomic, assign) BOOL userIsLocated;
@property (nonatomic, strong) UILabel *summaryLabel;
@property (nonatomic, strong) CLLocationManager *manager;

-(void)addMap;
-(void)zoomToFitMapAnnotations;
-(void)createLocationManager;
@end
