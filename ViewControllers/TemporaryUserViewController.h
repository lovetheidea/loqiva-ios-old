//
//  TemporaryUserViewController.h
//  loqiva
//
//  Created by juan Jimenez on 08/12/2016.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^DismmissBlock)();
typedef void (^CallToSignForFreeBlock)();

@interface TemporaryUserViewController : UIViewController
-(id)initWithDismissBlock:(DismmissBlock)dismissBlock andCallToSignForFree:(CallToSignForFreeBlock)callToSignforFreeBlock;
@end
