//
//  InformationCell.m
//  loqiva
//
//  Created by Manuel Manzanera on 29/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "InformationCell.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "loqiva-Swift.h"

@implementation InformationCell

@synthesize titleLabel, rigthArrow;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], 0, WIDTH - [UtilManager width:20] * 2, self.contentView.frame.size.height)];
        [titleLabel setTextAlignment:NSTextAlignmentLeft];
        [titleLabel setTextColor:[UIColor darkGrayColor]];
        [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:16]];
        [self.contentView addSubview:titleLabel];
        
        rigthArrow  =[[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:9], [UtilManager height:17])];
        [rigthArrow setImage:[UIImage imageNamed:@"image-arrow-orange"]];
        [rigthArrow setCenter:CGPointMake(WIDTH - [UtilManager width:20], self.contentView.center.y)];
        
        [self.contentView addSubview:rigthArrow];
        
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., self.frame.size.height - 1, WIDTH, 1)];
        [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
        [self.contentView addSubview:separatorView];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
