//
//  InformationDetailViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 26/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "InformationDetailViewController.h"
#import "WebServiceManager.h"
#import "NavCustomView.h"
#import "UIColor+Helper.h"
#import "AppDelegate.h"
#import "loqiva-Swift.h"

@interface InformationDetailViewController ()<WebServiceManagerDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, strong) UILabel *descriptionLabel;

@end

@implementation InformationDetailViewController

@synthesize scrollView;


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (![AppDelegate showAlertIfDisconnected]) {
        [WebServiceManager getInformationItem:[NSString stringWithFormat:@"%d",_currentInfoItem.mapitemid.intValue] andDelegate:self];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];

    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0., -20., WIDTH, HEIGHT + 20)];
    
    CGFloat titleHeight = [UtilManager heightForText:NSLocalizedString(@"The network of transportation in Aberdeen is widespread and complex like that of any major city.", nil) havingWidth:[UtilManager width:330] andFont:[UIFont fontWithName:LIGHT_FONT size:27]];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], NAV_BAR_HEIGHT + [UtilManager height:20],[UtilManager width:330], titleHeight)];
    [_titleLabel setNumberOfLines:0];
    [_titleLabel setTextColor:[UIColor HeaderTextColour]];
    [_titleLabel setTextAlignment:NSTextAlignmentLeft];
    [_titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:27]];
    [scrollView addSubview:_titleLabel];
    
    _separatorView = [[UIView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y + [UtilManager height:20.], _titleLabel.frame.size.width, 1)];
    [_separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
    [scrollView addSubview:_separatorView];
    
    CGFloat descriptionHeight = [UtilManager heightForText:NSLocalizedString(@"The Festival aims to show the very best in mounatain-based film, across the broadest spectrum.\n\nFilms are screened in eleven competitive categories, with winners judged by a panel of film and mountain luminaries and - in the case of the People´s Choice prize - the audience itself.\n\nA trophy scupted by Andy Parkin and cash prizes up to 1000 will be awared for the best films in the categories", nil) havingWidth:(WIDTH - [UtilManager width:10] * 2) andFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    
    _descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(_separatorView.frame.origin.x, _separatorView.frame.origin.y + [UtilManager height:25], _separatorView.frame.size.width, descriptionHeight)];
    [_descriptionLabel setNumberOfLines:0];
    [_descriptionLabel setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    [_descriptionLabel setTextColor:GRAY_TEXT_COLOR];
    
    [scrollView addSubview:_descriptionLabel];
    
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, _descriptionLabel.frame.origin.y + _descriptionLabel.frame.size.height + [UtilManager height:20])];
    [self.view addSubview:scrollView];
    
    [self setupHeaderForSection:CMSectionTypeInfoDetail];
}

#pragma mark WebServiceManagerDelegateMethods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    InfoItem *responseItem = (InfoItem *)object;
    _currentInfoItem = responseItem;
    
    CGFloat height = [UtilManager heightForText:_currentInfoItem.title havingWidth:_titleLabel.frame.size.width andFont:_titleLabel.font];
    [_titleLabel setFrame:CGRectMake([UtilManager width:20], NAV_BAR_HEIGHT + [UtilManager height:20],[UtilManager width:330], height)];
    [_titleLabel setText:_currentInfoItem.title];
    
    height = [UtilManager heightForText:_currentInfoItem.summary havingWidth:_descriptionLabel.frame.size.width andFont:_descriptionLabel.font];
    [_separatorView setFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + [UtilManager height:20.], _titleLabel.frame.size.width, 1)];
    
    [_descriptionLabel setText:_currentInfoItem.summary];
    [_descriptionLabel setFrame:CGRectMake(_separatorView.frame.origin.x, _separatorView.frame.origin.y + [UtilManager height:25], _separatorView.frame.size.width, height)];
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, _descriptionLabel.frame.origin.y + _descriptionLabel.frame.size.height + [UtilManager height:20])];
}

@end
