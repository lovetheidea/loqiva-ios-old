//
//  InformationDetailViewController.h
//  loqiva
//
//  Created by Manuel Manzanera on 26/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ParentViewController.h"
#import "InfoItem.h"

@interface InformationDetailViewController : ParentViewController

@property (nonatomic, strong) InfoItem *currentInfoItem;

@end
