//
//  InformationViewController.m
//  loqiva
//
//  Created by Manuel Manzanera on 21/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "InformationViewController.h"
#import "InformationDetailViewController.h"
#import "WebServiceManager.h"
#import "InformationCell.h"
#import "NavCustomView.h"
#import "InfoItem.h"
#import "Utility.h"
#import "UIColor+Helper.h"
#import "AppDelegate.h"
#import "AppContext.h"
#import "loqiva-Swift.h"

@interface InformationViewController ()<UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate>

@property (nonatomic, strong) UITableView *informationTableView;
@property (nonatomic, strong) NSArray *infoItems;
@property (nonatomic, strong) UIRefreshControl *control;

@end

@implementation InformationViewController

@synthesize informationTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self refreshData];
    
    [self configureView];
}

- (void)refreshData {
    if (![AppDelegate showAlertIfDisconnected]) {
        [WebServiceManager getInformationDetails:self];
        _infoItems = [[[AppContext sharedInstance] infoItems] mutableCopy];
    
        [informationTableView reloadData];
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType {
    [self.control endRefreshing];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object {
    if (webServiceType == WebServiceTypeInformationDetails) {
        _infoItems = [[[AppContext sharedInstance] infoItems] mutableCopy];
        [informationTableView reloadData];
    }
    
    [self.control endRefreshing];
}

- (void)configureView{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    informationTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., [UtilManager height:60], WIDTH, HEIGHT  -[UtilManager height:70]) style:UITableViewStylePlain];
    [informationTableView setDelegate:self];
    [informationTableView setDataSource:self];
    [informationTableView registerClass:[InformationCell class] forCellReuseIdentifier:@"informationCell"];
    [informationTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:informationTableView];
    
    self.control = [[UIRefreshControl alloc] init];
    self.control.tintColor = [UIColor orangeAppColor];
    [self.control addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    
    NSString *reqSysVer = @"10.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending) {
        informationTableView.refreshControl = self.control;
    }
    else {
        [self.informationTableView addSubview:self.control];
        [self.informationTableView setAlwaysBounceVertical:YES];
    }
    
    
    [self setupHeaderForSection:CMSectionTypeInformation];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[SessionManager shared] isLoggedIn] == false){
        
        __weak InformationViewController * weakSelf = self;
        self.tempUserViewController = [[TemporaryUserViewController alloc] initWithDismissBlock:^{
            [weakSelf.tempUserViewController.view removeFromSuperview];
            
            
        } andCallToSignForFree:^{
            [weakSelf.tempUserViewController.view removeFromSuperview];
            [weakSelf presentSignForFreeController];
            
        }];
        
        [self.view addSubview:self.tempUserViewController.view];
        
        return;
    }
    
    [informationTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    InformationDetailViewController *informationDetailViewController = [[InformationDetailViewController alloc] init];
    
    InfoItem *infoItem = [_infoItems objectAtIndex:indexPath.row];
    [informationDetailViewController setCurrentInfoItem:infoItem];
    [self.navigationController pushViewController:informationDetailViewController animated:YES];
}

#pragma mark UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    [self.control endRefreshing];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    switch (section) {
       
        case 0:
            return _infoItems.count;
            break;
            
        default:
            break;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    switch (section) {
      
        case 0:
            return [UtilManager height:160];
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
     
        case 0:
            return [UtilManager height:160];
            break;
        default:
            return 0;
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (section) {
       
        case 0:
        {
            return [self headerTitleView:@"Need some advice, browse our information section"];
        }
            break;
        default:
            return nil;
            break;
    }
}

- (UIView *)headerTitleView:(NSString *)title{
    UIView *headerTitleView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:140])];
    [headerTitleView setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], 10, headerTitleView.frame.size.width - 2 * [UtilManager width:20], [UtilManager height:160])];
    [titleLabel setNumberOfLines:0];
     [titleLabel setAttributedText:[Utility convertHtmlToPlainText: NSLocalizedString(title, nil)] withLineHeight:30 andTextAlignment:NSTextAlignmentLeft andFont:[UIFont fontWithName:LIGHT_FONT size:28] andColor:[UIColor HeaderTextColour] andKern:-0.6 withLineBreak:NO];
    

   // [titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    [headerTitleView addSubview:titleLabel];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., [UtilManager height:159], WIDTH, 1)];
    [separatorView setBackgroundColor:GRAY_SEPARATOR_COLOR];
    [headerTitleView addSubview:separatorView];
    
    return headerTitleView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    InformationCell *cell = [informationTableView dequeueReusableCellWithIdentifier:@"informationCell"];
    
    InfoItem *infoItem = [_infoItems objectAtIndex:indexPath.row];
    [[cell titleLabel] setText:infoItem.title];
    
    return cell;
}



@end
