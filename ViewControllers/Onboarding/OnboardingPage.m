//
//  OnboardingPage.m
//  loqiva
//
//  Created by Emu on 15/09/2017.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OnboardingPage.h"
#import "loqiva-Swift.h"

@interface OnboardingPage()

@property (nonatomic, strong) IBOutlet UIImageView *imageView;

@end

@implementation OnboardingPage

- (void)setupPageWithImage:(NSString *)image {
    self.imageView.image = [UIImage imageNamed:image];
}

@end
