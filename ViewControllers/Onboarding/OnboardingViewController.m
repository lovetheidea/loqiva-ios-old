//
//  OnboardingViewController.m
//  loqiva
//
//  Created by Emu on 15/09/2017.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OnboardingViewController.h"
#import "OnboardingPage.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@interface OnboardingViewController () <UIScrollViewDelegate>

@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) NSArray *imageArray;

@end

@implementation OnboardingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageArray = @[@"onboarding1", @"onboarding2", @"onboarding3", @"onboarding4", @"onboarding5", @"onboarding6"];
    
    [self.view setBackgroundColor:[UIColor orangeAppColor]];
}

- (void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    [self setupOnboardingScreens];
}

- (void)setupOnboardingScreens {
    for (NSInteger count = 0; count < 6; count++) {
        OnboardingPage *page = [[[NSBundle mainBundle] loadNibNamed:@"OnboardingPage" owner:self options:nil] lastObject];
        page.frame = CGRectMake(count * self.view.frame.size.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
        [page setupPageWithImage:self.imageArray[count]];
        [self.scrollView addSubview:page];
    }
    
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width * 6, 10);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
}

- (IBAction)nextPressed:(id)sender {
    NSInteger currentPage = self.pageControl.currentPage + 1;
    if (currentPage == 6) {
        [self skipPressed:sender];
    }
    [self.scrollView scrollRectToVisible:CGRectMake(currentPage * self.view.frame.size.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height) animated:YES];
}

- (IBAction)skipPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
