//
//  OnboardingPage.h
//  loqiva
//
//  Created by Emu on 15/09/2017.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnboardingPage : UIView

- (void)setupPageWithImage:(NSString *)image;

@end
