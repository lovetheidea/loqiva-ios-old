//
//  ATBeaconAlertStrategySuperior.h
//  ATLocationBeacon
//
//  Created by sarra srairi on 17/02/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATBeaconAlertStrategyEqual.h"

/**
 * Strategy Superior :
 **/

#define SUPERIOR_OBJECT_KEY @">="
@interface ATBeaconAlertStrategySuperior : ATBeaconAlertStrategyEqual
@end
