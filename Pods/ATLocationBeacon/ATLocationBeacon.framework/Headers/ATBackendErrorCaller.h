//
//  ATBackendErrorCaller.h
//  ATLocationBeacon
//
//  Created by Stevens Olivier on 25/04/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ATBackendErrorCaller : NSObject{
    int minErrorNumbers;
    int timeWaitingBeforeNewCall;
}

@property (nonatomic, assign) int minErrorNumbers;
@property (nonatomic, assign) int timeWaitingBeforeNewCall;

-(id) initWithMinErrorNumbers:(int)_minErrorNumbers andTimeWaitingBeforeNewCall:(int)_timeWaitingBeforeNewCall;

@end
