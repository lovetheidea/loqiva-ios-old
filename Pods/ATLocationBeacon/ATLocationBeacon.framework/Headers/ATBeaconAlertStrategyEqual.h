//
//  ATBeaconAlertStrategyEqual.h
//  ATLocationBeacon
//
//  Created by sarra srairi on 17/02/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATBeaconAlertStrategy.h"
#import <ATConnectionHttp/ATConnectionHttp.h>
#define EQUAL_OBJECT_KEY @"="

/**
 * Strategy Equal :
 **/
 
@interface ATBeaconAlertStrategyEqual : ATBeaconAlertStrategy {
    
    int maxCountProximityToReset;
    int maxCountProximityBeforeAction;
}

@property  int maxCountProximityToReset;
@property  int maxCountProximityBeforeAction;

-(id)initWithParameter:(int)_maxCountProximityBeforeAction
         maxCountProximityToReset:(int)_maxCountProximityToReset
               maxTimeBeforeReset:(int)_maxTimeBeforeReset;

-(id)init;

@end
