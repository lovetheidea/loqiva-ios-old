//
//  FirstNotificationTesterTimer.h
//  ATLocationBeacon
//
//  Created by Stevens Olivier on 25/02/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATFirstNotificationTesterDelegate.h"

#define TIME_TO_BE_FIRST_NOTIFICATION @"com.connecthings.beacon.defaultNotification.timeToBeFirstNotification"
#define NEARBY_BEACONS @"com.connecthings.beacon.defaultNotification.nearbyBeacons"

/**
 *  The default implementation of the ATFirstNotificationTesterDelegate
 
 A first notification is defined has : "the notification that is displayed xx times after the last notification displayed"
 */
@interface ATFirstNotificationTesterTimer : NSObject<ATFirstNotificationTesterDelegate>{
    
    double timeSinceLastNotificationToSayItsFirst;
    double timeToBeFirstNotification;
    bool areThereNearbyBeacons;
    
}
/**
 *  the time where the next notification will be a first notification
 */
@property(nonatomic) double timeToBeFirstNotification;

/**
 *
 *  @param _timeSinceLastNotificationToSayItsFirst the minimum time since the last beacon notification displayed to consider a notifiation as the first notification.
 *
 *  @return A ATFirstNotificationTesterTimer instance
 */
- (id)initWithTimeSinceLastNotificationToSayItsFirst:(long) _timeSinceLastNotificationToSayItsFirst;




@end
