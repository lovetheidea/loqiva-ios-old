//
//  ATBeaconAlertStrategyDelegate.h
//  ATLocationBeacon
//
//  Created by sarra srairi on 17/02/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ATConnectionHttp/ATConnectionHttp.h>

/**
 * Protocol delegate helps you create your own Alert Strategy with some kind of method. You can customize whatever and whenever you create your action
 **/
@protocol ATBeaconAlertStrategyDelegate <NSObject>


/**
 *
 * UpdateBeaconContent : will check generate the default beaconContent update
 *
 * This will update the : isReadyForAction methode / the isConditionValid and the maxTimeBeforeResetingIsActionDone
 */
@required
-(void) updateBeaconContent:(ATBeaconContent *) _beaconContent;

/**
 *
 * createBeaconAlertParameter : will initilize a beaconAlertParameter
 *
 * this will be automatically called every upateBeaconContent callback when the beaconContent dosen't hava an associated beaconAlertParamter
 */
@optional
-(id) getBeaconAlertParameterClass;

/**
 *
 * getKey : Return the NSstring Key assiciated to the strategy created
 *
 * this key will be saved as the same name in the adtag platfrom as the strategy related key
 *
 * @return strategy key
 */
-(NSString *) getKey;

/**
 *
 * isConditionValid : Return a Bool Value
 *
 * this will check if the proximity condition and the maxTime is always valid it's up to you to customize you're own valid condition to create an alert
 *
 * @return true if the condition is valid
 */
-(bool) isConditionValid:(ATBeaconAlertParameter *) _beaconParameter;

/**
 * isReadyForAction : Return a Bool Value
 *
 * this will check if the beaconContent is flaged as ATBeaconActionStatusWaitingForAction and isConditionValid is valid
 *
 * @return true if the alert action could be realized
 */
-(bool) isReadyForAction:(ATBeaconAlertParameter *) _beaconParameter;


@end

