//
//  ATAppDelegate.h
//  Pods
//
//  Created by Stevens Olivier on 13/04/2015.
//  Copyright (c) 2015 ct. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <ATConnectionHttp/ATConnectionHttp.h>
#import <ATLocationBeacon/ATLocationBeacon.h>

/**
 * ATBeaconAppDelegate manages the configuration of the AppDelegate to detect beacon when application is on background
 */
@interface ATBeaconAppDelegate : UIResponder <CLLocationManagerDelegate>{
    ATBeaconManager *adtagBeaconManager;
    CLLocationManager *locationManager;
    BOOL versionLogDisplayTest ;
}
@property (strong, nonatomic) CLLocationManager *locationManager;
/**
 *  Init the various parameter necessary to connect to the Adtag platform
 *
 *  @param urlType  AdTag Platfrom you would like to connect (Preprod, Prod)
 *  @param login    manages the authorization to connect to the Adtag platform
 *  @param password manages the authorization to connect to the Adtag platform
 *  @param company  company's name
 *  @param uuid     the specific and unique id for ibeacon
 */
- (void) initAdtagInstanceWithUrlType:(ATUrlType) urlType
                            userLogin:(NSString *)login
                         userPassword:(NSString *)password
                          userCompany:(NSString *)company
                           beaconUuid:(NSString *)uuid;
- (void)cleanAttribute;
- (void)applicationWillResignActive:(UIApplication *)application;
- (void)applicationDidBecomeActive:(UIApplication *)application;
/**
 *  To Add a Notification Strategy.
 *  A Notification Strategy permits to modify the algorithm that manages the display of the notification.
 *  The Notification Strategy is added to a chain of responsability.
 *  The first added Notification Stategy will be the first be executed
 *
 *  @param _strategyNotification the Notification Strategy to add
 */
-(void)addNotificationStrategy:(id<ATBeaconNotificationStrategyDelegate>) _notificationStrategy ;

-(void)registerAlertDelegate:(id<ATBeaconAlertDelegate>)_delegateAlert;
/**
 *  Add a Default Notification.
 *  A default notification permits to replace the "first" beacon notification.
 *  The notion of "first" notification is managed by ATFirstNotificaitonTesterDelegate
 *
 *  @param defaultNotification the default notification to add
 */
-(void) addWelcomeNotification:(ATBeaconWelcomeNotification *) welcomeNotification;
/**
 *  Permits to replace the current FirstNotificationTester implementation to use your own.
 *
 *  @param firstNotificationTester the firstNotificationTester to use
 */
-(void) replaceFirstNotificationTester:(id<ATFirstNotificationTesterDelegate>)firstNotificationTester;

/**
 *  Permits to add a custom alert to the alert strategy. this will allow you to define the custom strategy to the beacon directly via adtag
 *
 *  @param _strategyAlert the strategy to use
 */
- (void)addAlertStrategy: (id<ATBeaconAlertStrategyDelegate>) _strategyAlert;
@end
