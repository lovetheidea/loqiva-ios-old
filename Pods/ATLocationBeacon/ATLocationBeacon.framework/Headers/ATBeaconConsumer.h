//
//  AdtagBeaconConsumer.h
//  CtbeaconManagerLib
//
//  Created by Stevens Olivier on 03/04/2015.
//  Copyright (c) 2015 ct. All rights reserved.
 


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <ATConnectionHttp/ATConnectionHttp.h>
#import "ATBeaconRangeDelegate.h"
#import "ATPhoneModeDelegate.h"
#import "ATBeaconUtils.h"
#import "ATBeaconUtils.h"
#import <ATAnalytics/ATAnalytics.h>
#import "ATBeaconNotificationStrategyDelegate.h"
#import  "ATBeaconAlertStrategyDelegate.h"
#import  "ATBeaconAlertDelegate.h"
#import  "ATBeaconRegiterAlertDelegate.h"
#import  "ATBeaconNotificationChain.h"
#import "ATBackendErrorManager.h"

@interface ATBeaconConsumer : NSObject <CLLocationManagerDelegate, ATPhoneModeDelegate>{
//,ATBeaconRegiterAlertDelegate
    BOOL isActive;
    BOOL isLocationStarted;
    BOOL requireAlwaysAuthorization;
    BOOL downloadContentInProgress;
    ATRangeFeedStatus feedStatus;
    CLBeaconRegion *uuidRegion;
    NSString *uuidRegionIdentifier;
    ATConnectionBeacon *connectionBeacon;
    NSCache *beaconCache;
    NSMutableDictionary *beaconInformationDataInProgress;
    NetworkStatus status;
    ATLogManager *logManager;
    int rangeCount ;
    ATBackendErrorManager *backendErrorManager;
    NSDictionary * strategyDictionnary;
    ATRangeInformationStatus beaconInformationStatus ;
}

@property(nonatomic, assign) BOOL isActive, isLocationStarted, requireAlwaysAuthorization;
@property(nonatomic, assign) ATRangeFeedStatus feedStatus;
@property (strong, nonatomic) NSString *uuidRegionIdentifier;
@property (strong, nonatomic) CLBeaconRegion *uuidRegion;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) id <ATBeaconNotificationStrategyDelegate>  strategyNotification;
@property (strong, nonatomic) NSDictionary* strategyDictionnary;
@property (atomic, copy) NSArray* lastBeaconDetected;
+ (ATBeaconConsumer *)sharedInstance;
- (id)initStringUUID:(NSString *)_uuid
         beaconCache:(NSCache *) _beaconCache
 strategyDictionnary:(NSDictionary *)_strategyDic;

-(void)startLocationManager;
-(void)didRangeBeacons:(NSArray *)_beacons
    BeaconInformations:(NSArray *)_beaconInformations
     InformationStatus:(ATRangeInformationStatus)informationStatus
            feedStatus:(ATRangeFeedStatus)feedstatus
              InRegion:(CLRegion *)region;

-(void)onForeground;
-(void)onBackground;
-(void)addNotificationSrategy:(id<ATBeaconNotificationStrategyDelegate>) _strategyNotification;

@end
