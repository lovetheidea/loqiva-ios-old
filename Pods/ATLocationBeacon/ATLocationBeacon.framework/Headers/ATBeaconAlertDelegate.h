//
//  ATBeaconAlertDelegate.h
//  ATLocationBeacon
//
//  Created by sarra srairi on 17/02/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <ATConnectionHttp/ATConnectionHttp.h>

/**
 * Implement this protocol in you're controller or application will permits you to create or remove Alert Action
 **/
@protocol ATBeaconAlertDelegate <NSObject>

/**
 * CreateBeaconAlert callback will be called when all the confitions are valid 
 * Bool returned value, true value means that you have create your own alert action * this will flag the action status as in progress
 * @return true if the beacon alert has already create the alert
 **/
@required
-(BOOL)createBeaconAlert:(ATBeaconContent *)_beaconContent;

/**
 * removeBeaconAlert callback will be called once the beacon detected leaves the proximity zone saved in adtag to manage action  *this will flas the action status as 
 * action is done
 * @return true if the beacon alerthas already remove the alert
 **/
@optional
-(BOOL)removeBeaconAlert:(ATBeaconContent *)_beaconContent
            actionStatus:(ATBeaconRemoveStatus)_actionStatus;

/**
 * onNetworkError callback will be called when a network error will be detected
 **/
@optional
-(void)onNetworkError:(ATRangeFeedStatus )_feedStatus;

@end