//
//  AdTagBeaconNotificationDelegate.h
//  Pods
//
//
//

#import <Foundation/Foundation.h>
#import <ATConnectionHttp/ATConnectionHttp.h>

/**
 *   Notification methodes delegation
 */
@protocol ATBeaconWelcomeNotificationDelegate <NSObject>

-(UILocalNotification*) createWelcomeNotification:(ATBeaconWelcomeNotification*) _beaconDefaultNotification;
@end
 