//
//  BeaconDefaultNotificationManager.h
//  ATLocationBeacon
//
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ATConnectionHttp/ATConnectionHttp.h>
#import "ATFirstNotificationTesterDelegate.h"
 
#import "NSUserDefaults.h"

#define DEFAULT_TIME_SINCE_LAST_NOTIFICATION_TO_SAY_ITS_FIRST 43200000
#define TIME_TO_REMOVE_NOTIFICATION @"com.connecthings.beacon.defaultNotification.timeToRemoveNotification"
#define WELCOME_NOTIF_TYPE @"com.connecthings.beacon.defaultNotification.welcomeNotifType"
/**
 *  Manage the display of the default notifications taking care of the type of the notification and the ATFirstNotificationTesterDelegate strategy.
 */
@interface ATBeaconWelcomeNotificationManager : NSObject{
    double timeToRemoveNotification;
    ATReachability* atReachability;
    id<ATFirstNotificationTesterDelegate> firstNotificationTester;
    NSMutableDictionary *notifications;
    ATBeaconWelcomeNotificationType currentWelcomeNotifType;
}

/**
 *  Initialize the ATBeaconDefaultNotificationManager with a ATFirstNotificationTesterDelegate
 *
 *  @param _firstNotificationTester The ATFirstNotificationTesterDelegate strategy
 *
 *  @return a ATBeaconDefaultNotificationManager instance
 */
-(id) initWithFirstNotificationTester:(id<ATFirstNotificationTesterDelegate>) _firstNotificationTester;

/**
 *  To replace the ATFirstNotificationTesterDelegate strategy
 *
 *  @param _firstNotificationTester the new first notifcation tester strategy
 */
-(void) replaceFirstNotificationTesterDelegate:(id<ATFirstNotificationTesterDelegate>) _firstNotificationTester;
/**
 *  Permit to add a default notification
 *
 *  @param notification the default notification to add
 */
- (void)addNotification:(ATBeaconWelcomeNotification*) notification;
/**
 *  @return the default notification depending of the internet connection status. Return nil if no default notification has been registered
 */
- (ATBeaconWelcomeNotification*) findWelcomeNotification;
/**
 *  Permits to load parameters registered before an application kill
 *
 *  @param userDefaults to load the data
 */
- (void) load:(NSUserDefaults*) userDefaults;
/**
 *  Permits to save parameters that are necessary if the application is killed
 *
 *  @param userDefaults to save the date
 */
- (void) save:(NSUserDefaults*) userDefaults;
/**
 *  indicate if a default notification is currently displayed
 *
 *  @return true if a default notification is displayed
 */
- (bool) isWelcomeNotificationInProgress;
/**
 *
 *  @return true if it's time to remove a default notification
 */
- (bool) isTimeToRemoveWelcomeNotification;
/**
 *  call when a default notification is canceled
 */
- (void) onCancelWelcomeNotification;
/**
 *
 *  @return true if it's time to display a default notification
 */
- (bool) isTimeToShowWelcomeNotification;
/**
 *  Call when there SDK is rangin in background
 *
 *  @param areThereNearbyBeacons true if the SDK detects nearby beacons
 */
-(void) areThereNearbyBeacons:(bool) areThereNearbyBeacons;
/**
 *  Call when the SDK switch from foreground to background
 *
 *  @param areThereNearbyBeacons true if there are nearby beacons
 */
-(void) onBackground:(bool)areThereNearbyBeacons;
/**
 * return true if we display at the same time the current beacon notification and the welcome notification
 */
-(bool) displayCurrentBeaconNotification;
/**
 *  Call when a default notification try to be created
 *
 *  @param showNotif    true if the notification is displayed
 *  @param notification the default notification used to create the notification
 */
- (void) onShowWelcomeNotification:(bool) showNotif withNotification:(ATBeaconWelcomeNotification*) notification;
/**
 *  return true if the current welcome notification is of the same status then the actual network status
 */
- (bool)isNotificationStillActual;

@end
