//
//  ATBeaconAlertStrategy.h
//  ATLocationBeacon
//
//  Created by sarra srairi on 11/05/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATBeaconAlertStrategyDelegate.h"
#import <ATConnectionHttp/ATConnectionHttp.h>

@protocol ATBeaconAlertStrategyEqual @end

@interface ATBeaconAlertStrategy :NSObject <ATBeaconAlertStrategyDelegate> {
 
    double maxTimeBeforeReset;
}

@property double maxTimeBeforeReset;

- (instancetype)initWithMaxTime:(double)_maxTimeBeforeReset;

/**
 * updateAlertParameter : allow you to synchronize your ATBeaconAlertParameter with the new beaconContent information
 */
-(void) updateAlertParameter:(ATBeaconContent *) _beaconContent;

@end
