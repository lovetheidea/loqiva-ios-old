//
//  ATLogBeaconDefaultNotification.h
//  ATLocationBeacon
//
//  Created by Stevens Olivier on 03/03/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ATAnalytics/ATAnalytics.h>
#import <ATConnectionHttp/ATConnectionHttp.h>

@interface ATLogBeaconWelcomeNotification : ATLog

- initWithWelcomeNotification:(ATBeaconWelcomeNotification *)defaultNotification andBeacons:(NSArray<ATBeacon> *) beacons;

@end
