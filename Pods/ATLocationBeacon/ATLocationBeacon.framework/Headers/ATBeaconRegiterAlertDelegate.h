             //
//  ATBeaconRegiterAlertDelegate.h
//  ATLocationBeacon
//
//  Created by sarra srairi on 29/02/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//


#import "ATBeaconAlertDelegate.h"
#import <Foundation/Foundation.h>
#import <ATConnectionHttp/ATConnectionHttp.h>

/**
 *   alert resgister delegation
 */
@protocol ATBeaconRegiterAlertDelegate <NSObject>
//registerAdtagBeaconAlertDelegate : will allow you to register ATBeaconAlertDelegate your application to manage alert Delegate protocol
@required
-(void) registerAdtagBeaconAlertDelegate:(__autoreleasing id<ATBeaconAlertDelegate>)_adtagBeaconAlertDelegate;

@end