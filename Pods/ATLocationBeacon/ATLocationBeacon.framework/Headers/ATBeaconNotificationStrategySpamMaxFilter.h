//
//   .h
//  ATLocationBeacon
//
//  Created by sarra srairi on 25/01/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATBeaconNotificationStrategyDelegate.h"
#define MAX_NUMBER_NOTIFICATION @"com.connecthings.notificationRegionFilter.maxNumberNotification"
#define TIME_BETWEEN_NOTIFICATION @"com.connecthings.notificationRegionFilter.timeBetweenNotification"
#define SAVESTATUS @"saveStatus"
#define LAST_NOTIFICATION_NUMBER @"com.connecthings.notificationRegionFilter.lastNotificationNumber"


/**
 * Filter type based on max notification number in defined lapse time.
 * &n number of notification in &x defined lapse time. 'Milliseconds'
 */
@interface ATBeaconNotificationStrategySpamMaxFilter : NSObject  <ATBeaconNotificationStrategyDelegate> {
    
    NSInteger maxNumberNotifications;
    double timeBetweenNotification;
    NSInteger notificationNumber;
    double nextTimeToDisplayNotification;
    BOOL isSavedToLoad ;
}
/**
 *  Integer number
 */
@property (nonatomic) NSInteger maxNumberNotifications;
/**
 *  Time in Milliseconds
 */
@property (nonatomic) double timeBetweenNotification;


/**
 * The filter will be defined by 2 attributes : _maxNumberNotification and _timeBetweenNotification
 * @param _maxNumberNotification : is the the maximum number of notifications
 * @param _field :the time between the next notifications stream in Milliseconds
 */

- (instancetype)initWithNotificationMaxNumber:(NSInteger)_maxNumberNotification
                          timeBtwNotification:(long)_timeBetweenNotification;


@end
