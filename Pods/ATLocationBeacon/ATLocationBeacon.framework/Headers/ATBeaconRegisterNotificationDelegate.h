//
//  ATBeaconRegisterNotificationDelegate
//  Copyright (c) 2015 ct. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <ATConnectionHttp/ATConnectionHttp.h>
#import "ATBeaconNotificationDelegate.h"
#import "ATBeaconWelcomeNotificationDelegate.h"


/**
 *To Switch Notification detection process
 */
@protocol ATBeaconRegisterNotificationDelegate <NSObject>

-(void) registerAdtagBeaconNotificationDelegate:(__autoreleasing id<ATBeaconNotificationDelegate>)_adtagBeaconNotificationDelegate;

@optional
-(void) registerAdtagBeaconWelcomeNotificationDelegate:(__autoreleasing id<ATBeaconWelcomeNotificationDelegate>)_adtagBeaconNotificationDelegate;

-(ATBeaconContent *) beaconContentNotification;

-(ATBeaconWelcomeNotification *) currentWelcomeNotification;

-(void) configureLocationManager:(CLLocationManager *) locationManager_;

@end

