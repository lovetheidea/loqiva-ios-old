//
//  ATLogDataBeaconDefaultNotification.h
//  ATLocationBeacon
//
//  Created by Stevens Olivier on 03/03/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ATAnalytics/ATAnalytics.h>
#import <ATConnectionHttp/ATConnectionHttp.h>

@interface ATLogDataBeaconWelcomeNotification : ATAdtagLogDataContent {
    NSString *beacon_welcome_notification_type;
    NSArray<ATBeacon> *beacons_around;
}

@property (strong, nonatomic) NSString *beacon_default_notification_type;
@property (strong, nonatomic) NSArray <ATBeacon> *beacons_around;

- initWithWelcomeNotification:(ATBeaconWelcomeNotification *)welcomeNotification andBeacons:(NSArray<ATBeacon> *) beacons;

@end
