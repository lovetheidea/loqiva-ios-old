//
//  FirstNotificationTester.h
//  ATLocationBeacon
//
//  Created by Stevens Olivier on 24/02/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ATConnectionHttp/ATConnectionHttp.h>
#import "NSUserDefaults.h"
/**
 *ATFirstNotificationDelegate permits to write the rule that determines when a notification is the FIRST notification.
 
 For example, your could define that the First Notification is notification displayed 12 hours after the last one.
 
 The SDK comes with a default implement, ATFirstNotificationTesterTimer that you could replace with your own.
 */
@protocol ATFirstNotificationTesterDelegate <NSObject>


/**
 *  To know if it's the right moment to show a welcome notification
 *
 *  @return true if all paramters to display notification are true
 */
-(bool) isTimeForNotification;
/**
 *  To be notified when there are nearby beacons (on backgroud) - called after an exit or an entry in a beacon region
 *
 *  @param areThereNearbyBeacons true if there are nearby beaconss
 */
-(void) areThereNearbyBeacons:(bool) areThereNearbyBeacons;

/**
 *  To be notified when a welcome notification is created
 *
 *  @param isCreated true if a welcome notification is created, false if it is the time to create a notification but the welcome notification was not created
 */
-(void) onWelcomeNotificationCreated:(bool) isCreated;

/**
 *  Notify when the application switches from foreground to background,
 *  and there are nearby beacons
 *
 *  @param areThereNearbyBeacons true if there are nearby beacons
 */
-(void) onBackground:(bool) areThereNearbyBeacons;
/**
 *  Permits to load parameters when the application starts (in particular after to be killed)
 *
 *  @param userDefaults permits to load your date
 */
-(void) load:(NSUserDefaults *) userDefaults;

/**
 *  Permits to save the necessary parameters.
 *
 *  @param userDefaults permit to save your data
 */
-(void) save:(NSUserDefaults *) userDefaults;


@end
