//
//  ATBeaconNotificationStrategyFilter.h
//  ATLocationBeacon
//
//  Created by sarra srairi on 12/01/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSUserDefaults.h"
#import "ATBeaconNotificationStrategyDelegate.h"
#import "ATLocalNotificationManager.h"

#define LAST_REGION @"com.connecthings.notificationRegionFilter.lastRegion"


/**
 * Filter type based on Region Spam
 * Permits to display a notification per beacon region.
 * A flexible use case : you can manage beacon region dynamically from the Adtag Platform<
 */
@interface ATBeaconNotificationStrategySpamRegionFilter : NSObject <ATBeaconNotificationStrategyDelegate> {
    NSString *category;
    NSString *field ;
    NSString *lastRegion;
}

/**
 * The region will be defined by 2 attributes : category and field
 * @param _category : Name of the category as defined into Adtag to identiy the beacon region;
 * @param _field : Name of the Field as defined into Adtag to identify the beacon region.
 */
- (id)initWithCategoryAndField:(NSString *)_category
                         field:(NSString *)_field;

@property (strong, nonatomic) ATBeaconContent *lastBeaconDetected;
@property (strong, nonatomic) NSString *category;
@property (strong, nonatomic) NSString *field;
@property (strong, nonatomic) NSString *lastRegion;
@end
