//
//  BeaconInformationRangeBackground.h
//  Pods
//
//  Created by sarra srairi on 21/01/2016.
//  Copyright (c) 2015 ct. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ATConnectionHttp/ATConnectionHttp.h>
#import "ATBeaconRangeDelegate.h"
#import <CoreLocation/CoreLocation.h>

@interface ATBeaconFullInformation : NSObject {
    NSLock *lock;
    NSArray<CLBeacon *> *beacons;
    
}

-(void) reset;
-(CLBeacon *) getNearestBeacon;
-(void) update:(NSArray<CLBeacon *> *) _beacons;
-(bool) findRegion:(CLBeaconRegion *) region;
-(bool) hasBeacons;

@end
