//
//  ATBackendErrorManager.h
//  ATLocationBeacon
//
//  Created by Stevens Olivier on 25/04/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATBackendErrorCaller.h"

@interface ATBackendErrorManager : NSObject {
    int currentPosition;
    int countError;
    int nextTimeBackend;
    NSArray *beaconErrorCallers;
}

- (void) reset;
- (bool) isTimeToCallBackend;
- (void) addError;
@end
