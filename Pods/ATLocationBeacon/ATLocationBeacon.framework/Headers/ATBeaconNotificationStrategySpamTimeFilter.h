//
//  ATBeaconNotificationStrategySpamTimeFilter.h
//  Pods
//
//  Created by sarra srairi on 19/01/2016.
//  Copyright (c) 2015 ct. All rights reserved.
//
#import "NSUserDefaults.h"
#import <Foundation/Foundation.h>
#import "ATBeaconNotificationStrategyDelegate.h"
#define MIN_TIME_BEFORE_CREATING_NOTIFICATION @"com.connecthings.notificationRegionFilter.minTimeBeforeCreatingNotification"
#define MIN_TIME_BETWEEN_TOW_NOTIFICATION @"com.connecthings.notificationRegionFilter.minTimeBetweenTwoNotifications"

/**
 * Filter type based on time Spam
 * Avoid creating a new notification if the application recently goes to background, or if a notification has been created few Milliseconds before.
 * The time in Milliseconds
 */
@interface ATBeaconNotificationStrategySpamTimeFilter : NSObject <ATBeaconNotificationStrategyDelegate> {
    
    double minTimeBetweenTwoNotifications;
    double minTimeBeforeCreatingNotificationWhenAppEntersInBackground;
    double timeToCreateNotificationAfterAppEntersInBackground, timeToCreateNextNotification;
    BOOL isSavedToLoad ;
}
/**
 *  Time in Milliseconds
 */
@property (nonatomic) double minTimeBetweenTwoNotifications;
/**
 *  Time in Milliseconds
 */
@property (nonatomic) double minTimeBeforeCreatingNotificationWhenAppEntersInBackground;
/**
 *  Time in Milliseconds
 */
@property (nonatomic) double timeToCreateNotificationAfterAppEntersInBackground;
/**
 *  Time in Milliseconds
 */
@property (nonatomic) double timeToCreateNextNotification;

/**
 * The region will be defined by 2 attributes : min time before creating notification when app enter in background and the min time between two notification
 * @param The time spam _minTimeBeforeCreatingNotificationWhenAppEntersInBackground and _minTimeBetweenTwoNotifications will define when application will allow user to display notification
 * @param The time in Milliseconds
 */
- (id)initWithMinTimeBeforeCreatingNotificationWhenAppEnterInBackground:(long)_minTimeBeforeCreatingNotificationWhenAppEntersInBackground
                                          minTimeBetweenTwoNotification:(long)_minTimeBetweenTwoNotifications;
@end
