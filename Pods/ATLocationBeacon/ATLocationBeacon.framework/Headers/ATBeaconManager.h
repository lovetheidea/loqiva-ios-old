//
//  AdtagBeaconManager.h
//  CtbeaconManagerLib
//
//  Created by Sarra Srairi on 30/03/2015.
//  Copyright (c) 2015 ct. All rights reserved.
//
#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
#import "ATBeaconRangeConsumer.h"
#import "ATBeaconRegisterNotificationDelegate.h"
#import "ATPhoneModeDelegate.h"
#import "ATBeaconNotificationConsumer.h"
#import "ATBeaconNotificationRangeConsumer.h"
#import "ATBeaconBleLocationStatus.h"
#import "ATBeaconNotificationStrategyDelegate.h"
#import "NSUserDefaults.h"
#import "ATBeaconAlertConsumer.h"

/**
 *  ATBeaconManager
 *  allows you to register your delegate for the ranging / notification / BleLocation
 */
@interface ATBeaconManager : NSObject{
    ATBeaconRangeConsumer *beaconRangeConsumer;
    id<ATBeaconRegisterNotificationDelegate> beaconNotificationConsumer;
    NSCache *cacheBeacon;
    ATLogManager *logManager;
    ATBeaconBleLocationStatus * adTagBeaconBleLocationStatus;
    NSArray *lastBeaconInformationsForAlert;
    NSString *beaconUUIDString;
    ATBeaconConsumer *beaconConsumer;
    NSMutableDictionary *strategyDictionnary;
    ATBeaconAlertConsumer *alertConsumer ;
}
@property (strong, nonatomic)  NSMutableDictionary *strategyDictionnary;

@property (strong, nonatomic) id<ATBeaconRegisterNotificationDelegate> beaconNotificationConsumer;
@property (strong, nonatomic)  id<ATBeaconNotificationStrategyDelegate> strategyNotification;
/**
 *  Get the singleton insatance
 *
 *  @return ATBeaconManger object
 */
+ (ATBeaconManager *)sharedInstance;


/**
 *  Initialize the ATBeaconManager with the specific property
 *
 *  @param beaconUUIDString the beacon's UUID
 *
 *  @return ATBeaconManger object
 */
+ (id) initInstanceUUIDStringIdentifier:(NSString *)beaconUUIDString;


/**
 *  Register the delegate for AdTagRange Delegate with the object that will consume this delegate
 *
 *  @param object your delegate object
 */
-(void)registerAdtagRangeDelegate: (id<ATRangeDelegate>) object;
/**
 *  Register the delegate for Beacon Notification Delegate with the object that will consume this delegate
 *
 *  @param object your delegate object
 */
-(void)registerBeaconNotificationDelegate: (id) object;

/**
 *  Register the delegate for Beacon Default Notification with the object that will consume this delegate
 *
 *  @param object your delegate object
 */
-(void)registerAdtagBeaconDefaultNotificationDelegate: (id) object;


/**
 *  Register the delegate for Beacon bluetooth & location Delegate with the object that will consume this delegate
 *
 *  @param object your delegate object
 */
-(void)registerAdtagBeaconBleLocationDelegate: (id)object;
/**
 *  If your app does not use ATBeaconAppDelegate, call this method when your app is going the foreground to notify the SDK about it
 */
-(void)onBackgroundState;
/**
 *  If your app does not use ATBeaconAppDelegate, call this method when your app is going the foreground to notify the SDK about it
 */
-(void)onForegroundState;

/**
 *  Permits to send an alert log to the Adtag Platform.
 *  An alert log must be send when application is on foreground, each time you display an information from a beaconContent
 *
 *  @param beaconContents array of beaconContent display to the user
 */
-(void)sendAlertLogs:(NSArray *)beaconContents;

/**
 *  Permits to send an alert log to the Adtag Platform.
 *  An alert log must be send when application is on foreground, each time you display an information from a beaconContent
 *
 *  @param beaconContent the beaconContent which uses to display the alert to the user
 */
-(void)sendAlertLog:(ATBeaconContent *)beaconContent;

/**
 *  Permits to send a redirect log.
 *  A redirect log must be generated when displaying the main information page about the beacon from a beacon notification, beacon alert or beacon action.
 *  @param beaconContent beacon content / all the information about beacon such as UUID major, minor, poi , acess
 *  @param redirectType  the type of the redirect. It could be : ATBeaconRedirectTypeAlert,ATBeaconRedirectTypeWebView, ATBeaconRedirectTypeNotification
 *  @param from          descripte the current controller
 */

-(void)sendRedictLog:(ATBeaconContent *)beaconContent
        redirectType:(ATBeaconRedirectType)redirectType
                from: (NSString *) from;


/**
 *  Permits to send a redirect log.
 *  A redirect log must be generated when displaying the main information page about the beacon from a beacon notification, beacon alert or beacon action.
 *  @param beaconContent beacon content / all the information about beacon such as UUID major, minor, poi , acess
 *  @param redirectType  the type of the redirect. It could be : ATBeaconRedirectTypeAlert,ATBeaconRedirectTypeWebView, ATBeaconRedirectTypeNotification
 *  @param from          descripte the current controller
 */

-(void)sendRedictLog:(ATBeaconWelcomeNotification *)beaconDefaultNotification from:(NSString *) from;

/**
 * Clean memory cache
 */
-(void)cleanCache;
/**
 *  This return the beacon information, when a user is getting a notification
 *
 *  @return ATBeaconContent
 */
-(ATBeaconContent *)getNotificationBeaconContent;

/**
 *  This return the beacon information, when a user is getting a notification
 *
 *  @return ATBeaconContent
 */
-(ATBeaconWelcomeNotification *) getBeaconWelcomeNotification;

/**
 * Normally your app is notified about notification creation when an zone entry is detected.
 * If you activate this option, your app is notified about creating a notification when a new beacon is detected using the ranging method.
 * Warning: This could dry the user battery.
 * Warning: You need to activate the location background mode capabilities to get it works.
 * Warning: This feature is experimental
 */
-(ATBeaconManager *)activateRangingForNotification;
/**
 *  To Add a Notification Strategy.
 A Notification Strategy permits to modify the algorithm that manages the display of the notification.
 The Notification Strategy is added to a chain of responsability.
 The first added Notification Stategy will be the first be executed
 *
 *  @param _strategyNotification the Notification Strategy to add
 */
-(void)addNotificationStrategy:(id<ATBeaconNotificationStrategyDelegate>) _strategyNotification;
///**
// *    *
// *  @param _strategyAlert _strategyAlert description
// */
//- (void)registerAlertStrategy:(id<ATBeaconAlertStrategyDelegate>) _strategyAlert;

-(void)registerBeaconAlertDelgate:(id<ATBeaconAlertDelegate>) object;
/**
 *  Add a Default Notification.
 A default notification permits to replace the "first" beacon notification.
 The notion of "first" notification is managed by ATFirstNotificaitonTesterDelegate
 *
 *  @param defaultNotification the default notification to add
 */
-(void) addWelcomeNotification:(ATBeaconWelcomeNotification *) defaultNotification;
/**
 *  Permits to replace the current FirstNotificationTester implementation to use your own.
 *
 *  @param firstNotificationTester the firstNotificationTester to use
 */
-(void) replaceFirstNotificationTester:(id<ATFirstNotificationTesterDelegate>) firstNotificationTester;
/**
 *  Permits to add a custom alert to the alert strategy. this will allow you to define the custom strategy to the beacon directly via adtag
 *
 *  @param _strategyAlert the strategy to use
 */
- (void)addAlertStrategy:(id<ATBeaconAlertStrategyDelegate>) _strategyAlert;
@end
