//
//  AdtagBeaconNotification.h
//  Pods
//
//  Created by Sarra Srairi on 26/03/2015.
//  Copyright (c) 2015 ct. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ATAnalytics/ATAnalytics.h>
#import "ATBeaconConsumer.h"
#import "ATBeaconNotificationDelegate.h"
#import "ATBeaconRegisterNotificationDelegate.h"
#import "ATBeaconWelcomeNotificationDelegate.h"
#import "ATLocalNotificationManager.h"
#import "ATBeaconNotificationStrategySpamRegionFilter.h"
#import "ATBeaconNotificationChain.h"
#import "ATBeaconFullInformation.h"
#import "ATBeaconWelcomeNotificationManager.h"
 

#define MAX_EMPTY_RANGE 30
#define NOTIFICATION_BEACON_CONTENT @"com.connecthings.notificationBeaconContent"
#define WELCOME_NOTIFICATION_CONTENT @"com.connecthings.welcomeNotificationContent"
#define CURRENT_WELCOME_NOTIFICATION @"com.connecthings.currentWelcomeNotification"
#define NOTIFICATION_REGION @"com.connecthings.notificationRegion"
#define PREVIOUS_NOTIFICATION_REGION @"com.connecthings.previousNotificationRegion"
#define NOTIFICATION_REGION_IDENTIFIER @"com.connecthings.notificationRegionIdentifier"

@interface ATBeaconNotificationConsumer : ATBeaconConsumer <ATBeaconRegisterNotificationDelegate>{
    NSObject *adtagBeaconNotificationDelegateLock;
    id<ATBeaconNotificationDelegate> adtagBeaconNotificationDelegate;
    id<ATBeaconWelcomeNotificationDelegate> adtagBeaconWelcomeNotificationDelegate;
    CLBeaconRegion *notificationRegion;
    CLBeaconRegion *previousNotificationRegion;
    ATBeaconContent *beaconContentNotification;
    ATBeaconWelcomeNotification *currentWelcomeNotification;
    UIApplication *app;
    UIBackgroundTaskIdentifier bgTask;
    BOOL isFirstTime;
    BOOL isRanging;
    BOOL isNotificationShown;
    BOOL isOnForgroundMode;
    BOOL isBgTaskStarted;
    BOOL needToLoadData;
    int countNoBeacon;
    NSTimer *timerForBgTask;
    NSString *notificationRegionIdentifier;
    ATLocalNotificationManager *localBeaconNotificationManager;
    ATLocalNotificationManager *localWelcomeNotificationManager;
    ATBeaconContent * lastBeaconContentDetected;
    NSArray *beaconsRanged;
    ATBeaconFullInformation *beaconFullInforamtion;
    ATBeaconNotificationChain *strategyChain;
    ATBeaconWelcomeNotificationManager *welcomeNotificationManager;
    NSUserDefaults *dataStore;
 
}
@property (strong, nonatomic)   CLBeaconRegion *notificationRegion ;
@property (strong, nonatomic)   CLBeaconRegion *previousNotificationRegion ;
@property (strong, nonatomic)   NSString *notificationRegionIdentifier;
@property (strong) NSUserDefaults *dataStore;
- (void) registerAdtagBeaconNotificationDelegate:(id<ATBeaconNotificationDelegate>) adtagBeaconNotificationDelegate;
- (void) registerAdtagBeaconWelcomeNotificationDelegate:(id<ATBeaconWelcomeNotificationDelegate>) adtagBeaconDefaultNotificationDelegate;
//- (void)startRangingInBackgroundTask:(BOOL) deleteNotification;
- (id)initStringUUID:(NSString *)_uuid
         beaconCache:(NSCache *) _beaconCache;
-(void) addWelcomeNotification:(ATBeaconWelcomeNotification *) welcomeNotification;
-(void) replaceFirstNotificationTesterDelegate:(id<ATFirstNotificationTesterDelegate>) firstNotificationTester;

@end
