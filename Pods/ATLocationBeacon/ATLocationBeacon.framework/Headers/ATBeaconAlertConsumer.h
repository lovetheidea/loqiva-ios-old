//
//  ATBeaconAlertConsumer.h
//  ATLocationBeacon
//
//  Created by sarra srairi on 04/03/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATBeaconConsumer.h"
#import <ATConnectionHttp/ATConnectionHttp.h>
#import "ATBeaconRangeDelegate.h"
#import "ATBeaconRangeConsumer.h"

@interface ATBeaconAlertConsumer :NSObject <ATRangeDelegate>{
    
    BOOL deleteAlert;
    
    BOOL popupIsCreated,actionIsInProgress;
    
    BOOL isNetworkError;
    
    int countNoBeacon;
    
    id<ATBeaconAlertDelegate> adtagBeaconAlertDelegate;
    
    NSObject *adtagBeaconAlertDelegateLock;
    
    ATBeaconContent *currentBeaconInformationAction;
}

//-(void)registerAlertStrategy:(id<ATBeaconAlertStrategyDelegate>) _strategyAlert;

-(void)registerAdtagBeaconAlertDelegate:(__autoreleasing id<ATBeaconAlertDelegate>)_adtagBeaconAlertDelegate;
 
@end
