//
//  ATBeaconAlertParameter.h
//  ATConnectionHttp
//
//  Created by sarra srairi on 02/02/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//
#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
#import "ATBeaconAlertParameter.h"


/**
 *  ATBeaconAlertParameter Object
 *  Manage the proximity attributes such as the current proximity and the last proximity
 *  This will allow you check your beacon proximity region, flag it the isInProximityForAction
 */

@interface ATBeaconAlertParameterProximity : ATBeaconAlertParameter {
    
    int countSameProximity;
    CLProximity currentActionProximity;
    CLProximity lastProximity;
    BOOL isInProximityForAction;

}
/**
 *  countSameProximity (int) : count the number of valid proximity  
 */ 
@property  int countSameProximity;
/**
 *  currentActionProximity (CLProximity): iBeacon current region proximity
 */
@property  CLProximity currentActionProximity;
/**
 *  lastProximity (CLProximity) : iBeacon current region proximity detected
 */
@property  CLProximity lastProximity;
/**
 *  isInProximityForAction : check if the ibeacon region proximity is the same as describe in the adtag platfrom to create alert
 */
@property  BOOL isInProximityForAction;
 
- (void) countSameProximityPlusOne;

- (void) countSameProximityReset;

- (CLProximity )getProximity;


@end

