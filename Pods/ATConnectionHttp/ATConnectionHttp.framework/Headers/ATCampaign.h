//
//  Campaign.h
//  AdTagConnectionHttpLibrary
//
//  Created by Stevens Olivier on 09/04/2015.
//  Copyright (c) 2015 ct. All rights reserved.
//

#import "ATEntity.h"

@interface ATCampaign : ATEntity

@property (strong, nonatomic) NSString *name;

@end
