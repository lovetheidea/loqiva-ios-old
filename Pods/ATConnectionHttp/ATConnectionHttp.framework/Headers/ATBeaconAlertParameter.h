//
//  ATBeaconAlertParameter.h
//  Pods
//
//  Created by sarra srairi on 11/05/2016.
//
//

 
#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
typedef enum{
    ATBeaconActionStatusActionDone,
    ATBeaconActionStatusActionInProgress,
    ATBeaconActionStatusWaitingForAction
}ATBeaconActionStatus;

typedef enum{
    ATBeaconRemoveStatusBeaconExit,
    ATBeaconRemoveStatusConditionInvalid
}ATBeaconRemoveStatus;

/**
 *  ATBeaconAlertParameter Object
 *
 *  @param actionStatus     : this is the typedef enum. The default value is ATBeaconActionStatusWaitingForAction
 *  @param maxTimeBeforeResetingIsActionDone : the needed time to reseat the action done
 *  @param isReadyForAction : check if the ibeacon detected is ReadyForAction: this will check the valid proximity and the isActionDone flag
 *  @param isConditionValid : this check if proximity is valid
 *  @return a default notification
 */

@interface ATBeaconAlertParameter : NSObject{
    
    ATBeaconActionStatus actionStatus;
    
    long maxTimeBeforeResetingIsActionDone;
    
    BOOL isReadyForAction;
    
    BOOL isConditionValid;
}
/**
 *  ATBeaconActionStatus : typedef enum default value is ATBeaconActionStatusWaitingForAction
 */
@property  ATBeaconActionStatus actionStatus;
/**
 *  maxTimeBeforeResetingIsActionDone : time to reseate the isActionDone flag
 */
@property  long maxTimeBeforeResetingIsActionDone;
/**
 *  isReadyForAction :check if the ibeacon detected is ReadyForAction: this will check the valid proximity and the isActionDone flag
 */
@property  BOOL isReadyForAction;
/**
 *  isConditionValid :this check if proximity is valid
 */
@property  BOOL isConditionValid;
/*
 * iBeacon is flagged as isActiondone when ibeacon action was triggered
 */
- (void) actionIsDone;
/*
 * iBeacon is flagged as isActionInProgress when the action is still actif or running (still not removed)
 */
- (void) actionIsInProgress;
@end
