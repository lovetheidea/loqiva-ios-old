#import <Foundation/Foundation.h>
#import "ATJSONModel.h"
#import "ATLogData.h"
#import "ATBeaconContent.h"
#import "ATBeacon.h"
#import "ATBeaconWelcomeNotification.h"
#import "ATAdtagLogDataContent.h"

@interface ATLogDataBeacon : ATAdtagLogDataContent{
    
    ATBeacon   *beacon;
    NSString *beacon_welcome_notification_type;
 
}

@property(nonatomic,retain) ATBeacon   *beacon;
@property(nonatomic,retain) NSString   *beacon_welcome_notification_type;

- (id)initWithBeaconContent:(ATBeaconContent *)beaconContent
                    subType:(ATLogSubtype)subtype;

- (id)initWithBeaconContent:(ATBeaconContent *)beaconContent
               redirectType:(ATBeaconRedirectType)_redirectType
                       from: (NSString *) from;

- (id)initWithWelcomeNotification:(ATBeaconWelcomeNotification *) welcomeDefaultNotification
                       from: (NSString *) from;

@end

