

//
//  BeaconDefaultNotification.h
//  ATLocationBeacon
//
//  Created by Stevens Olivier on 24/02/2016.
//  Copyright © 2016 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Defines the type of default notifications
 */

typedef enum{
    /**
     *  Flag a default notification to be shown when the internet connection is up
     */
    ATBeaconWelcomeNotificationTypeNetworkOn = 0,
    /**
     *  Flag a default notification to be shown when the internet connection is down
     */
    ATBeaconWelcomeNotificationTypeNetworkOff
}ATBeaconWelcomeNotificationType;

#define DEFAULT_NOTIFICATION_TYPES @[@"ATBeaconDefaultNotificationTypeNetworkOn", @"ATBeaconDefaultNotificationTypeNetworkError" ]
#define ENCODER_TITLE @"title"
#define ENCODER_DESCRIPTION @"description"
#define ENCODER_MIN_DISPLAY_TIME @"minDisplayTime"
#define ENCODER_DEFAULT_NOTIFICATION_TYPE @"defaultNotificationType"

/**
 *  A default notification replaces the "first" default notification.
 
 The notion of "first" is defined by "ATFirstNotifiationTesterDelegate"
 
 A default notification could be used for example to display:
 * a generic welcoming message
 * a message to notify about an available free wifi
 
 You can define two default notifications, one when the internet connection is down,
 and an other when the internet connection is up.
 
 A default notification is displayed for minimum of time  until there is still beacons around the device.
 **/
@interface ATBeaconWelcomeNotification : NSObject<NSCoding>{
    NSString *title;
    NSString *description;
    float minDisplayTime;
    bool displayCurrentBeaconNotification;
    ATBeaconWelcomeNotificationType welcomeNotificationType;
}
/**
 *  The title of the notification
 */
@property(strong, nonatomic) NSString *title;
/**
 *  the description of the notification
 */
@property(strong, nonatomic) NSString *description;
/**
 *  the minimum time to display the notification
 */
@property(nonatomic) bool displayCurrentBeaconNotification;
/**
 *  the minimum time to display the notification
 */
@property(nonatomic) float minDisplayTime;
/**
 *  the type of the notification, is that a notification when the interconnection is down or
 when the internet connection is up
 */
@property(nonatomic) ATBeaconWelcomeNotificationType welcomeNotificationType;
/**
 *  Initalise a welcome notification
 *
 *  @param title                   title of the notification
 *  @param description             description of the notification
 *  @param iconeId                 iconeId of the notification
 *  @param minDisplayTime          minimum time to display the notification in ms
 *  @param welcomeNotificationType the type of the notification
 *  @param displayCurrentBeaconNotification if true display the current beacon notification and the welcome notification
 *  @return a welcome notification
 */
- (id)initTitle:(NSString *) title
    description:(NSString *) description
 minDisplayTime:(int) minDisplayTime
welcomeNotificationType:(ATBeaconWelcomeNotificationType) welcomeNotificationType
displayCurrentBeaconNotification:(bool) displayCurrentBeaconNotification;
/**
 *  Initalise a welcome notification
 *
 *  @param title                   title of the notification
 *  @param description             description of the notification
 *  @param iconeId                 iconeId of the notification
 *  @param minDisplayTime          minimum time to display the notification in ms
 *  @param defaultNotificationType the type of the notification
 *
 *  @return a welcome notification
 */
- (id)initTitle:(NSString *) title
    description:(NSString *) description
 minDisplayTime:(int) minDisplayTime
welcomeNotificationType:(ATBeaconWelcomeNotificationType) welcomeNotificationType;
/**
 *  helper to get the notification type enum as String
 *
 *  @return a string value for the notification type enum
 */
-(NSString *) stringForWelcomeNotificationType;
/**
 *  Helper to get the notification type enum as String
 *
 *  @param notificationType a notification type to convert to string
 *
 *  @return a string value for the notification type enum
 */
+(NSString *) stringForWelcomeNotificationType:(ATBeaconWelcomeNotificationType) notificationType;

+ (ATBeaconWelcomeNotificationType) welcomeNotificationTypeFromString:(NSString *)type;

@end
