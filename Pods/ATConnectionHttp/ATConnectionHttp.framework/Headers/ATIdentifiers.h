//
//  ATIdentifier.h
//  ATConnectionHttp
//
//  Created by Sarra Srairi on 02/06/2015.
//  Copyright (c) 2015 R&D connecthings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATJSONModel.h"
@interface ATIdentifiers : ATJSONModel


@property ( nonatomic) NSArray* identifiers;



@end
