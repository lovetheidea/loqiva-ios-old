//
//  ATLogDataBeaconAround.h
//  Pods
//
//  Created by Sarra Srairi on 02/12/2015.
//
//
#import <Foundation/Foundation.h>
#import "ATJSONModel.h"
#import "ATLogData.h"
#import "ATBeaconContent.h"
#import "ATBeacon.h"
#import "ATAdtagLogDataContent.h"
#import "ATBeaconList.h"
@interface ATLogDataBeaconAround : ATAdtagLogDataContent{
    
    NSArray <ATBeacon> *beacons_around;
}

@property (strong, nonatomic) NSArray <ATBeacon> *beacons_around;

- (id)initWithBeacon:(NSArray <ATBeacon>  *)_beaconsAround
             subType:(ATLogSubtype)sub;
@end
