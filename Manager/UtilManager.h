//
//  UtilManager.h
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UtilManager : NSObject

+ (CGFloat)height:(CGFloat)pixels;
+ (CGFloat)width:(CGFloat)pixels;

+ (UIViewController*)presentTeporaryUserViewController;
+ (void)presentAlertWithMessage:(NSString *)message;
+ (void)presentAlertWithMessage:(NSString *)message andTitle:(NSString *)title;
+ (CGFloat)heightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font;

+ (UIColor *)colorwithHexString:(NSString *)hexStr alpha:(CGFloat)alpha;
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageFromText:(NSString *)text;
+ (UIImage*)imageWithImage:(UIImage*)sourceImage scaledToWidth:(float)i_width andMinHeigth:(float)heigth;

+ (NSMutableAttributedString *)attributeStringWithText:(NSString *)text andFont:(NSString *)font andSize:(NSInteger)size;

+ (NSString *)youtubeImagePathWithURL:(NSString *)url;

@end
