//
//  UtilManager.m
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "UtilManager.h"
#import "AppConstants.h"
#import "TemporaryUserViewController.h"
#import "loqiva-Swift.h"

//The Mockup with Sketch is doing with iPhone 6 size
#define iPHONE_6_WIDTH 375
#define iPHONE_6_HEIGHT 667

@implementation UtilManager

+ (CGFloat)height:(CGFloat)pixels{
    return (pixels /iPHONE_6_HEIGHT) * HEIGHT;
}

+ (CGFloat)width:(CGFloat)pixels{
    return (pixels /iPHONE_6_WIDTH) * WIDTH;
}

+ (CGFloat)heightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font
{
    CGFloat result = font.pointSize + 4;
    if (text)
    {
        CGSize textSize = { widthValue, CGFLOAT_MAX };
        CGSize size;
        CGRect frame = [text boundingRectWithSize:textSize
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{ NSFontAttributeName:font }
                                          context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height+1);
        
        result = MAX(size.height, result);
    }
    return result;
}


+ (void)presentAlertWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
    [alert show];
}

+ (void)presentAlertWithMessage:(NSString *)message andTitle:(NSString *)title
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
    [alert show];
}

+ (UIColor *)colorwithHexString:(NSString *)hexStr alpha:(CGFloat)alpha;
{
    unsigned int hexint = 0;

    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    [scanner setCharactersToBeSkipped:[NSCharacterSet
                                       characterSetWithCharactersInString:@"#"]];
    [scanner scanHexInt:&hexint];
    
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    
    return color;
}

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage *)imageFromText:(NSString *)text
{
    NSDictionary *attributes;
    // set the font type and size
   
    attributes = @{NSFontAttributeName: [UIFont fontWithName:LIGHT_FONT size:28],NSForegroundColorAttributeName:ORANGE_TEXT_COLOR};
    CGSize size  = [text sizeWithAttributes:attributes];
    
    if (&UIGraphicsBeginImageContextWithOptions != NULL)
        UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    
    [text drawAtPoint:CGPointMake(0.0, 0.0) withAttributes:attributes];
    
    // transfer image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+(UIImage*)imageWithImage:(UIImage*)sourceImage scaledToWidth:(float)i_width andMinHeigth:(float)heigth
{
    float oldWidth = sourceImage.size.width;
    float scaleWidthFactor = i_width / oldWidth;
    float scaleHeightFactor = heigth / sourceImage.size.height;
    
    float scaleFactor;
    
    if(scaleWidthFactor > scaleHeightFactor)
        scaleFactor = scaleWidthFactor;
    else
        scaleFactor = scaleHeightFactor;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (NSMutableAttributedString *)attributeStringWithText:(NSString *)text andFont:(NSString *)font andSize:(NSInteger)size{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];

    [attributedString addAttribute:NSKernAttributeName value:@(0.1) range:NSMakeRange(0, [text length])];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont fontWithName:font size:size]
                             range:NSMakeRange(0, [text length])];
    
    return attributedString;
}

+ (NSString *)youtubeImagePathWithURL:(NSString *)url{
    
    NSString *string = @"http://www.youtube.com/embed/";
    NSRange range = NSMakeRange(0, [string length]);
    
    NSString *youtubeId = [url substringFromIndex:range.length];
    range = [youtubeId rangeOfString:@"?"];
    
    youtubeId = [youtubeId substringToIndex:range.location];
    
    return [NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg",youtubeId];
}

@end
