//
//  ParserManager.h
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "InfoItem.h"
#import "Reward.h"
#import "Event.h"
#import "InterestDetail.h"
#import "Tours.h"
#import "GeoLocationMessage.h"

@class MapItem;
@class User;

@interface ParserManager : NSObject

+ (BOOL)parseHomePageResources:(NSDictionary *)response;
+ (BOOL)parseInformationDetails:(NSDictionary *)response;
+ (InfoItem *)parseInfoItem:(NSDictionary *)response;
+ (NSArray *)parseRewards:(NSDictionary *)response;
+ (Reward *)parseRewardsDetail:(NSDictionary *)response;
+ (BOOL)parseEvents:(NSDictionary *)response;
+ (Event *)parseEventDetail:(NSDictionary *)response;
+ (BOOL)paymentTypes:(NSDictionary *)response;
+ (BOOL)transportationTypes:(NSDictionary *)response;
+ (BOOL)parseInterests:(NSDictionary *)response;
+ (NSArray <MapItem *>*)parseMapItems:(NSDictionary *)response;
+ (NSArray <Tours *> *)parseTours:(NSDictionary *)response;
+ (InterestDetail *)parseInterestsDetails:(NSDictionary *)response;
+ (Tours *)parseTourDetails:(NSDictionary *)response;
+ (GeoLocationMessage *)parseGeolocationMessage:(NSDictionary *)response;
+ (BOOL)parseNotifications:(NSDictionary *)response;
+ (NSArray *)parseSurvey:(NSDictionary *)response;
+ (BOOL)parseTempUser:(NSDictionary *)response;
+ (User *)parseUser:(NSDictionary *)response;
@end
