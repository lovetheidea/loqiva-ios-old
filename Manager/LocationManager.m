//
//  LocationManager.m
//  loqiva
//
//  Created by Manuel Manzanera on 9/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "LocationManager.h"
#import "WebServiceManager.h"
#import "loqiva-Swift.h"

@implementation LocationManager

+(LocationManager *) sharedInstance
{
    static LocationManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc]init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if(self != nil) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
#warning DEV = 1 PROD = 10
        self.locationManager.distanceFilter = 1;
        self.locationManager.delegate = self;
        _firstLocation = FALSE;
    }
    return self;
}

- (void)startUpdatingLocation
{
    NSLog(@"Starting location updates");
    [self.locationManager startUpdatingLocation];
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Location service failed with error %@", error);
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray*)locations
{
    
    CLLocation *location = [locations lastObject];
    
    if(_firstLocation && self.currentLocation){
        CLLocationDistance distance = [location distanceFromLocation:self.currentLocation];
        #warning DEV = 1 PROD = 10
        if(distance > 1){
            self.currentLocation = location;
            [WebServiceManager getGeoAlertNotificationWithLatitude:[NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.latitude] andLongitude:[NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.longitude] andDelegate:nil];
        }
    }
    
    NSLog(@"Latitude %+.6f, Longitude %+.6f\n",
          location.coordinate.latitude,
          location.coordinate.longitude);
    
    
    if(!_firstLocation){
        CLLocation *currentLocation = [[LocationManager sharedInstance] currentLocation];
        CLLocationDegrees latitude = currentLocation.coordinate.latitude;
        CLLocationDegrees longitude = currentLocation.coordinate.longitude;
        
        _firstLocation = TRUE;
        [WebServiceManager getHomePageResourcesWithLatitude:[NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.latitude]  andLongitude:[NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.longitude] andDelegate:nil];
        [WebServiceManager getEventsWithLatitude:[NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.latitude] andLongitude:[NSNumber numberWithDouble:[[LocationManager sharedInstance] currentLocation].coordinate.longitude] andDelegate:nil];
        [WebServiceManager getInformationDetails:nil];

        [self requestRewardsWithLatitude:latitude longitude:longitude];
        [self requestToursWithLatitude:latitude longitude:longitude];
        [self requestAttractionsWithLatitude:latitude longitude:longitude];
        [self requestServicesWithLatitude:latitude longitude:longitude];

        self.currentLocation = location;
    }
}

+ (CLLocationDistance)getDistanceFrom:(CLLocation *)location{
    return [location distanceFromLocation:[LocationManager sharedInstance].currentLocation]/1000 * 0.621371;
}

@end
