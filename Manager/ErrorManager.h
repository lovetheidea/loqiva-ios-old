//
//  ErrorManager.h
//  loqiva
//
//  Created by Manuel Manzanera on 3/6/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorManager : NSObject

+ (NSString *)errorMessaggeWithCode:(NSNumber *)errorCode;

@end
