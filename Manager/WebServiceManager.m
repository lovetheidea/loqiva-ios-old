//
//  WebServiceManager.m
//  loqiva
//
//  Created by Manuel Manzanera on 26/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "WebServiceManager.h"
#import "ParserManager.h"
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "NSString+Encryption.h"
#import "AppContext.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "LocationManager.h"
#import "GeoLocationMessage.h"
#import "CustomNotification.h"
#import "loqiva-Swift.h"
#import "MapItem.h"
#import "User.h"

@implementation WebServiceManager


static NSString *CREATE_TEMP_USER = @"service/phonejson/createtempuser";
static NSString *REGISTER = @"service/phonejson/register";
static NSString *LOGIN = @"service/phonejson/appuserlogin";
static NSString *DESTROY_SESSION = @"service/phonejson/destroysession";
static NSString *USER_DETAILS = @"service/phonejson/getuserdetails";
static NSString *SAVE_PROFILE = @"service/phonejson/setuserdetails";
static NSString *SAVE_PROFILE_IMAGE = @"service/phonejson/save-image";
static NSString *FORGOT_PASSWORD = @"service/phonejson/forgottenpassword";
static NSString *RESET_PASSWORD = @"service/phonejson/passwordreset";
static NSString *NOTIFICATIONS = @"service/phonejson/getinappmessages";
static NSString *DELETE_NOTIFICATIONS = @"service/phonejson/deleteinappmessage/messageid";
static NSString *GET_INTERESTS = @"service/phonejson/loadinterests";
static NSString *POST_DEVTOKEN = @"service/phonejson/updateuserapptoken";
static NSString *SAVE_INTEREST = @"service/phonejson/saveuserinterests";
static NSString *REPORT_KEY = @"service/phonejson/generate-reporting-key";
static NSString *REPORT_IMAGE = @"service/phonejson/upload-image";
static NSString *ISSUE_REPORT = @"service/phonejson/save-issue-report";
static NSString *GET_TOURS = @"service/phonejson/walklist";
static NSString *GET_TOURS_DETAILS = @"service/phonejson/walkroute/walkid";
static NSString *HOME_DETAILS = @"service/phonejson/homepagedetails";
static NSString *INFORMATION = @"service/phonejson/infolist";
static NSString *INFORMATION_DETAILS = @"service/phonejson/infoitem/infoitemid";
static NSString *REWARDS = @"service/phonejson/listofrewarditems";

// this is the old code
//static NSString *REWARDS_DETAILS = @"service/phonejson/rewarditemdetails/rewardid";
static NSString *REWARDS_DETAILS = @"service/phonejson/rewardetailsbylatlng/rewardid";

static NSString *REWARD_REDEEM = @"service/phonejson/redeemreward/rewardid";
static NSString *BEACON_REWARD = @"service/phonejson/getbeaconbasedreward";
static NSString *EVENTS = @"service/phonejson/eventslist/";
static NSString *EVENT_DETAIL = @"service/phonejson/eventdetails/eventId";
static NSString *PAYMENT_LIST = @"service/phonejson/paymenttypeslist";
static NSString *PAYMENT_TRANSPORT_LIST = @"service/phonejson/transportation";
static NSString *MAP_ITEMS_ATTRACTION = @"service/phonejson/listofmappoints/locationtype/attraction";
static NSString *MAP_ITEMS_SERVICE = @"service/phonejson/listofmappoints/locationtype/service";
static NSString *GEO_LOCATION = @"service/phonejson/geolocationmessage";
static NSString *INTEREST_DETAILS = @"service/phonejson/interestdetails/interestid";
static NSString *USER_AGE_GROUP = @"service/phonejson/getagegroup";
static NSString *USER_TITLE_GROUP = @"service/phonejson/gettitle";
static NSString *SURVEY_LIST = @"service/phonejson/surveyalert";


+ (NSString *)urlPath {
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    NSString *urlPath = infoPlistDict[@"AppEndpointURL"];
    if (urlPath.length) {
        return urlPath;
    }
    
    return @"http://chippingnorton.loqiva.com/";
}

#pragma mark USER
+ (void)getTitleGroup:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], USER_TITLE_GROUP];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeTitle andObject:[dataDictionary objectForKey:@"title"]];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeTitle];
    }];
}

+ (void)getAgeGroup:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], USER_AGE_GROUP];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeAgeGroupUser andObject:[dataDictionary objectForKey:@"agegroup"]];
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeAgeGroupUser];
    }];
}

+ (void)createTempUser:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], CREATE_TEMP_USER];
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        [ParserManager parseTempUser:dataDictionary];
        
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeTempUser andObject:nil];
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeTempUser];
    }];
}

+(void)registerNewUser:(User *)user success:(void (^)(User * _Nsonnull))success failure:(void (^)(NSError *))failure {
    
    NSArray* unfilteredPoscode = [user.postcode componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* userPostcode = [unfilteredPoscode componentsJoinedByString:@""];

    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/title/%@/firstname/%@/lastname/%@/age/%@/email/%@/password/%@/postcode/%@/mobile/%@", [self urlPath], REGISTER, user.title, user.firstName, user.lastName, user.age, user.mail, user.password, userPostcode, user.phoneNumber];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments error:&jsonError];
        
        if (jsonError != nil) {
            NSDictionary *userInfo = @{ NSLocalizedDescriptionKey:jsonError,
                                       NSLocalizedFailureReasonErrorKey:jsonError
                                       };
            failure([[NSError alloc] initWithDomain:@"loqiva" code:-1 userInfo:userInfo]);
            return;
        }
        
        if([[dataDictionary valueForKey:@"success"] isEqual:[NSNumber numberWithInt:1]]){
            NSString *token = [dataDictionary valueForKey:@"token"];
            user.userToken = token;
            success(user);
        } else {
            NSString *errorMessage = [dataDictionary valueForKey:@"errormsg"];
            if (errorMessage == nil) {
                errorMessage = @"Unable to complete request";
            }
            NSInteger errorCode = [[dataDictionary valueForKey:@"errorcode"] integerValue];
            
            NSDictionary *userInfo = @{
                                       NSLocalizedDescriptionKey:errorMessage,
                                       NSLocalizedFailureReasonErrorKey:errorMessage
                                       };
            failure([[NSError alloc] initWithDomain:@"loqiva" code:errorCode userInfo:userInfo]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

+ (void)loginWithUsername:(NSString  *)username andPassword:(NSString *)password success:(void (^)(User * _Nsonnull))success failure:(void (^)(NSError * _Nonnull))failure {
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/username/%@/password/%@", [self urlPath], LOGIN, username,password];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        // TODO why do we not recieve the user object in the response. This second request is adds unnecessary complexity. Ask the server team to change the logic here
        if([[dataDictionary valueForKey:@"Login"] isEqualToString:@"True"]){
                NSString * usertoken = [dataDictionary valueForKey:@"usertoken"];
            
            // call to get the users profile.
            [self getLoggedInUsersProfileWithSuccess:^(User *user) {
                user.userToken = usertoken;
                user.password = password;
                success(user);
            } failure:^(NSError *error) {
                failure(error);
            }];
        }
        else {
            failure([NSError new]);
        }

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

+ (void)logoutWithSuccess:(void (^)(void))success failure:(void (^)(NSError * _Nonnull))failure {
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], DESTROY_SESSION];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"devToken"];
        [self postDevToken:nil];
        
        success();
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

+ (void)getLoggedInUsersProfileWithSuccess:(void (^)(User * _Nsonnull))success failure:(void (^)(NSError * _Nonnull))failure {
    
    /*
     NOTE: The current system uses cookies to request the users details.
     This is an unusual approach and may be worth asking the server team to use a more traditional method of using user identifiers
     */
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], USER_DETAILS];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        User *user = [ParserManager parseUser: dataDictionary];
        success(user);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

+ (void)deleteNotification:(NSString *)identifier {
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/%@/token/%@", [self urlPath], DELETE_NOTIFICATIONS, identifier, [[[SessionManager shared] user] userToken]];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

+ (void)getNotificationsDelegate:(id<WebServiceManagerDelegate>)delegate {
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/token/%@", [self urlPath], NOTIFICATIONS, [[[SessionManager shared] user] userToken]];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        
        [ParserManager parseNotifications:dataDictionary];
       
        if(jsonError == nil){
            if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
                [delegate dataDidDownload:WebServiceTypeProfileSave andObject:dataDictionary];
        } else {
            if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
                [delegate dataDidDownloadWithFail:WebServiceTypeProfileSave];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeProfileSave];
    }];
}

+ (void)saveProfile:(NSDictionary *)profileDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/firstname/%@/lastname/%@/email/%@/password/%@/postcode/%@/mobile/%@", [self urlPath], SAVE_PROFILE, [profileDictionary valueForKey:@"firstname"], [profileDictionary valueForKey:@"lastname"], [profileDictionary valueForKey:@"email"], [profileDictionary valueForKey:@"password"], [profileDictionary valueForKey:@"postcode"], profileDictionary[@"mobile"]];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if([[dataDictionary valueForKey:@"success"] isEqual:[NSNumber numberWithInt:1]]){
            if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
                [delegate dataDidDownload:WebServiceTypeProfileSave andObject:nil];
        } else {
            if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
                [delegate dataDidDownloadWithFail:WebServiceTypeProfileSave];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeProfileSave];
    }];
}

+ (void)updateProfileImage:(UIImage *)image success:(void (^)(NSString *))success failure:(void (^)(NSError *))failure {
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/token/%@/", [self urlPath], SAVE_PROFILE_IMAGE, [[[SessionManager shared] user] userToken]];
    
    [[NetworkingManager shared] performMulitPartRequestForUrlString:urlPath parameters:nil propertyKey:@"mediaimage" image:image mimeType:@"image/jpeg" success:^(NSURLSessionDataTask *task, id responseObject) {
        if(responseObject != nil){
    
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                bool duplicatedSuccessStatus = [[(NSDictionary *)responseObject objectForKey:@"success"] boolValue];
                NSString *imageURL = [(NSDictionary *)responseObject valueForKey:@"imageName"];
                if(duplicatedSuccessStatus){
                    success(imageURL);
                }
                else {
                    failure([NSError new]);
                }
            }
            else {
                failure ([NSError new]);
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

+ (void)forgottenPassWord:(NSString *)email andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/email/%@/device/iphone", [self urlPath], FORGOT_PASSWORD, email];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeForgotPassword andObject:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeForgotPassword];
    }];

}

+ (void)resetPassword:(NSString *)newPassword andConfirmPassword:(NSString *)confirmPassword andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *userToken = [UserDefaultManager getWithKey:@"usernewtoken"];
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/usertoken/%@/newpassword/%@/confirmpassword/%@", [self urlPath], RESET_PASSWORD, userToken, newPassword, confirmPassword];
     NSLog(@"%@", urlPath);
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeForgotPassword andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeForgotPassword];
    }];
}

+ (void)postDevToken:(id<WebServiceManagerDelegate>)delegate {
    
    NSString *devToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"devToken"];
    if (!devToken.length) {
        devToken = @"NA";
    }
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/userId/%@/apptoken/%@", [self urlPath], POST_DEVTOKEN, [[[SessionManager shared] user] userToken], devToken];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        [ParserManager parseInterests:dataDictionary];
        
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeInterests andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeInterests];
    }];
}

#pragma mark INTERESTS

+ (void)getInterests:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/token/%@", [self urlPath], GET_INTERESTS, [[[SessionManager shared] user] userToken]];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        [ParserManager parseInterests:dataDictionary];
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeInterests andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeInterests];
    }];
}

+ (void)saveInterestWithId:(NSNumber *)interestId andStatus:(NSNumber *)status andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/token/%@/interest/%@/status/%d", [self urlPath], SAVE_INTEREST, [[[SessionManager shared] user] userToken], interestId,status.intValue];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeInterestSave andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeInterestSave];
    }];
}

#pragma mark REPORTING

+ (void)generateReportKey:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], REPORT_KEY];
    
    [[NetworkingManager shared] performRequest:HTTPMethodPOST urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if(responseObject!=nil){
            NSError *jsonError;
            NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
            bool success = [[dataDictionary objectForKey:@"success"] boolValue];
            if(success){
                NSString *reportId = [dataDictionary objectForKey:@"reportid"];
                reportId = [reportId stringByRemovingPercentEncoding];
                if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
                    [delegate dataDidDownload:WebServiceTypeGeneratingReport andObject:reportId];
            }
            else{
                [UtilManager presentAlertWithMessage:NSLocalizedString(@"Unable to submit the report at the moment, please try again later", nil)];
                [delegate dataDidDownloadWithFail:WebServiceTypeGeneratingReport];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeGeneratingReport];
    }];
}

+ (void)postReportImage:(UIImage *)image andReportId:(NSString *)reportId andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSDictionary *parameters = @{@"reportid": reportId};
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/", [self urlPath], REPORT_IMAGE];
    [[NetworkingManager shared] performMulitPartRequestForUrlString:urlPath parameters:parameters propertyKey:@"uploadFile" image:image mimeType:@"image/jpeg" success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if(responseObject!=nil){
            NSError *jsonError;
            NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
            bool success = [[dataDictionary objectForKey:@"success"] boolValue];
            if(success){
                [delegate dataDidDownload:WebServiceTypeImageReport andObject:nil];
            }
            else{
                [UtilManager presentAlertWithMessage:NSLocalizedString(@"Unable to upload the image at the moment, please try again later", nil)];
                [delegate dataDidDownloadWithFail:WebServiceTypeImageReport];
            }
        }
        
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        
        if(httpResponse.statusCode == 200){
            [delegate dataDidDownload:WebServiceTypeImageReport andObject:nil];
        }
        else if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeImageReport];
    }];
}

+ (void)postReportFields:(NSDictionary *)reportDictionary andReportId:(NSString *)reportId andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], ISSUE_REPORT];
    
    [[NetworkingManager shared] performRequest:HTTPMethodPOST urlString:urlPath parameters:reportDictionary success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeHomePage andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeHomePage];
    }];
}

#pragma mark TOURS

+ (void)getToursWithLatitude:(CLLocationDegrees)latitude andLongitude:(CLLocationDegrees)longitude success:(void (^)(NSArray <Tours *> * _Nsonnull))success failure:(void (^)(NSError * _Nonnull))failure {
    
    NSString *urlPath = [self generateURLPathForEndPoint:GET_TOURS latitude:latitude longitude:longitude];
    [NSString stringWithFormat:@"%@%@", [self urlPath], GET_TOURS];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        NSArray <Tours *> *tours = [ParserManager parseTours:dataDictionary];
        success(tours);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        failure(error);
    }];
}

+ (void)getToursDetails:(NSString *)walkid andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/%@", [self urlPath], GET_TOURS_DETAILS,walkid];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        Tours *toursDetail = [ParserManager parseTourDetails:dataDictionary];
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeToursDetail andObject:toursDetail];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeToursDetail];
    }];
}

#pragma mark HOME PAGE

+ (void)getHomePageResourcesWithLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    
    NSString *usertime = [dateFormatter stringFromDate:[NSDate date]];
    NSString *urlPathWithoutEscapes = [NSString stringWithFormat:@"%@%@/latitude/%@/longitude/%@/usertime/%@", [self urlPath], HOME_DETAILS, latitude, longitude,usertime];
    NSString *urlPath = [urlPathWithoutEscapes stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        [ParserManager parseHomePageResources:dataDictionary];
        
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)]) {
            [delegate dataDidDownload:WebServiceTypeHomePage andObject:nil];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)]) {
           [delegate dataDidDownloadWithFail:WebServiceTypeHomePage];
        }
    }];
}

#pragma mark INFORMATION

+ (void)getInformationDetails:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], INFORMATION];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        [ParserManager parseInformationDetails:dataDictionary];
        
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeInformationDetails andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeInformationDetails];
    }];
}

+ (void)getInformationItem:(NSString *)infoid andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/%@", [self urlPath], INFORMATION_DETAILS, infoid];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        InfoItem *responseInfoItem = [ParserManager parseInfoItem:dataDictionary];
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeInfoItem andObject:responseInfoItem];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeInfoItem];
    }];
}

+ (void)getSurveys:(id<WebServiceManagerDelegate>)delegate {
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], SURVEY_LIST];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        [ParserManager parseSurvey:dataDictionary];
        
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeSurveyList andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeSurveyList];
    }];
}

#pragma mark REWARDS

+ (void)getRewardsWithLatitude:(CLLocationDegrees)latitude andLongitude:(CLLocationDegrees)longitude success:(void (^)(NSArray<Reward *> * _Nonnull))success failure:(void (^)(NSError * _Nonnull))failure {
    
    //this is the old code that does not work.
    //NSString *urlPath = [NSString stringWithFormat:@"%@%@/latitude/%@/longitude/%@", [self urlPath], REWARDS, [NSString stringWithFormat:@"%f",latitude], [NSString stringWithFormat:@"%f",longitude]];

    NSString *urlPath = [NSString stringWithFormat:@"%@%@/latitude/%@/longitude/%@", [self urlPath], REWARDS,
                   [NSNumber numberWithFloat:[[NSUserDefaults standardUserDefaults] floatForKey:@"USERLAT"]], [NSNumber numberWithFloat:[[NSUserDefaults standardUserDefaults] floatForKey:@"USERLNG"]]];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        NSArray *rewards = [ParserManager parseRewards:dataDictionary];
        success(rewards);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

+ (void)getReward:(NSString *)rewardId success:(void (^)(Reward * _Nonnull))success failure:(void (^)(NSError * _Nonnull))failure {
    //this is old code
    //NSString *urlPath = [NSString stringWithFormat:@"%@%@/%@", [self urlPath], REWARDS_DETAILS, rewardId];
    
    //new code wasiq
    //retriving the current user long lat and passing the long lat of the user position on start of the app.
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/%@/latitude/%@/longitude/%@", [self urlPath], REWARDS_DETAILS, rewardId, [NSNumber numberWithFloat:[[NSUserDefaults standardUserDefaults] floatForKey:@"USERLAT"]], [NSNumber numberWithFloat:[[NSUserDefaults standardUserDefaults] floatForKey:@"USERLNG"]]];
    
//    NSString *urlPath = [NSString stringWithFormat:@"%@%@/%@/latitude/%@/longitude/%@", [self urlPath], REWARDS_DETAILS, rewardId, [NSString stringWithFormat:@"%f",latitude], [NSString stringWithFormat:@"%f",longitude]];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        
        Reward *reward = [ParserManager parseRewardsDetail:dataDictionary];
        success(reward);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        
        failure(error);
    }];
}

/* //this is the old code. This does not work
+ (void)getReward:(NSString *)rewardId success:(void (^)(Reward * _Nonnull))success failure:(void (^)(NSError * _Nonnull))failure {
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/%@", [self urlPath], REWARDS_DETAILS, rewardId];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        
        Reward *reward = [ParserManager parseRewardsDetail:dataDictionary];
        success(reward);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        
        failure(error);
    }];
}
*/

+ (void)getBeaconRewardWithBeaconDictionary:(NSDictionary *)beaconDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], BEACON_REWARD];
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:beaconDictionary success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        NSArray *resultset = [dataDictionary valueForKey:@"resultset"];
        NSDictionary *resultsetDictionary;
        if(resultset && resultset.count > 0)
        {
            resultsetDictionary = [resultset objectAtIndex:0];
        }
        
    [delegate dataDidDownload:WebServiceTypeBeaconReward andObject:resultsetDictionary];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
        [delegate dataDidDownloadWithFail:WebServiceTypeBeaconReward];
    }];
}

+ (void)postRedeemRewardsItem:(NSString *)rewardsId andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/%@", [self urlPath], REWARD_REDEEM,rewardsId];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if([[dataDictionary valueForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]){
            if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
                [delegate dataDidDownload:WebServiceTypeRedeemRewardsDetail andObject:NSLocalizedString(@"Success!", nil)];
        } else{
            if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
                [delegate dataDidDownload:WebServiceTypeRedeemRewardsDetail andObject:NSLocalizedString(@"Fail!", nil)];
        }
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeRedeemRewardsDetail];
    }];
}

#pragma mark EVENTS

+ (void)getEventsWithLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY"];
    
    NSString *usertime = [dateFormatter stringFromDate:[NSDate date]];
    
    NSString *urlPathWithEscapes = [NSString stringWithFormat:@"%@%@latitude/%@/longitude/%@/date/%@", [self urlPath], EVENTS, latitude, longitude, usertime];
    NSString *urlPath = [urlPathWithEscapes stringByReplacingOccurrencesOfString:@" "
                                                          withString:@"%20"];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        NSInteger statusCode = 0;
        
        if ([task.response isKindOfClass:[NSHTTPURLResponse class]]) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
            statusCode = httpResponse.statusCode;
        }
        
        if (statusCode == 200) {
            [ParserManager parseEvents:dataDictionary];
        }
        
        
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeEvents andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeEvents];
    }];
}

+ (void)getEventsDetail:(NSString *)eventId andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@/%@", [self urlPath], EVENT_DETAIL, eventId];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeEventDetail andObject:[ParserManager parseEventDetail:dataDictionary]];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeEventDetail];
    }];
}

#pragma mark PAYMENTS

+ (void)getPaymentTypes:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], PAYMENT_LIST];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
    
        [ParserManager paymentTypes:dataDictionary];
        
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypePaymentTypes andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypePaymentTypes];
    }];
}

+ (void)getPaymentTransports:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], PAYMENT_TRANSPORT_LIST];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        [ParserManager transportationTypes:dataDictionary];
        
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeTransportationTypes andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
    
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeTransportationTypes];
    }];
}

#pragma mark MAP

+ (void)getAttractionsWithLatitude:(CLLocationDegrees)latitude andLongitude:(CLLocationDegrees)longitude success:(void (^)(NSArray <MapItem *> * _Nsonnull))success failure:(void (^)(NSError * _Nonnull))failure {
    
    NSString *urlPath = [self generateURLPathForEndPoint: MAP_ITEMS_ATTRACTION latitude: latitude longitude: longitude];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        NSArray * attractions = [ParserManager parseMapItems:dataDictionary];
        success(attractions);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

+ (void)getServicesWithLatitude:(CLLocationDegrees)latitude andLongitude:(CLLocationDegrees)longitude success:(void (^)(NSArray <MapItem *> * _Nsonnull))success failure:(void (^)(NSError * _Nonnull))failure {
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", [self urlPath], MAP_ITEMS_SERVICE];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        NSArray * services = [ParserManager parseMapItems:dataDictionary];
        
        success(services);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

+ (void)getGeoAlertNotificationWithLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath;
    if(!latitude)
        urlPath = [NSString stringWithFormat:@"%@%@/", [self urlPath], GEO_LOCATION];
    else
    {
        if(![[[SessionManager shared] user] userToken])
        {
            if([[[AppContext sharedInstance] geoLocationMessage] userId]){
                urlPath = [NSString stringWithFormat:@"%@%@/latitude/%@/longitude/%@/userid/%@", [self urlPath], GEO_LOCATION, latitude, longitude, [[[AppContext sharedInstance] geoLocationMessage] userId]];
            } else {
                urlPath = [NSString stringWithFormat:@"%@%@/latitude/%@/longitude/%@", [self urlPath], GEO_LOCATION, latitude, longitude];
            }
        }
        else{
           urlPath = [NSString stringWithFormat:@"%@%@/latitude/%@/longitude/%@/userid/%@", [self urlPath], GEO_LOCATION, latitude, longitude, [[[SessionManager shared] user] userToken]];
        }
    }
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        GeoLocationMessage *geoLocationMessagge = [ParserManager parseGeolocationMessage:dataDictionary];
        
        if(geoLocationMessagge.alerttype.intValue == 1){
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            
            localNotification.fireDate = [NSDate date];
            localNotification.alertAction = geoLocationMessagge.msgtitle;
            localNotification.alertBody = geoLocationMessagge.welcomeMsg;
            
            localNotification.soundName = @"alarm.wav";
            localNotification.applicationIconBadgeNumber = 0;
            
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            
            CustomNotification *customNotification = [[CustomNotification alloc] init];
            [customNotification setTitle:geoLocationMessagge.msgtitle];
            [customNotification setSummary:geoLocationMessagge.welcomeMsg];
            [customNotification setNotificationId:geoLocationMessagge.rewardId];
            [customNotification setNotificationDate:[NSDate date]];
            [customNotification setIsRead:[NSNumber numberWithBool:FALSE]];
            [customNotification setCustomNotificationType:[NSNumber numberWithInt:CustomNotificationTypeGeoAlert]];
            [customNotification setUrl:geoLocationMessagge.url];
            
            [CustomNotification saveCustomNotification:customNotification];
            
            NSMutableArray *notificationsArray = [[AppContext sharedInstance] notifications];
           
            if(notificationsArray.count == 0) {
                notificationsArray = [[NSMutableArray alloc] initWithArray:[CustomNotification getCustomNotifications]];
            }
        }
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeGeoNotification andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeGeoNotification];
    }];
}

+ (void)deleteInAppMessage:(NSString *)messageId andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@service/ios/deleteinappmessage/messageid/%@", [self urlPath], messageId];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeDeleteInAppMessage andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeDeleteInAppMessage];
    }];
}

#pragma mark INTERESTS

+ (void)getInterestDetails:(NSString *)interestId success:(void (^)(InterestDetail * _Nonnull))success
                   failure:(void (^)(NSError * _Nonnull))failure {
    
    NSString *urlPath;
    if([[LocationManager sharedInstance] currentLocation])
        urlPath = [NSString stringWithFormat:@"%@%@/%@/latitude/%@/longitude/%@", [self urlPath], INTEREST_DETAILS, interestId, [NSNumber numberWithFloat:[[LocationManager sharedInstance] currentLocation].coordinate.latitude], [NSNumber numberWithFloat:[[LocationManager sharedInstance] currentLocation].coordinate.longitude]];
    else
        urlPath = [NSString stringWithFormat:@"%@%@/%@", [self urlPath], INTEREST_DETAILS, interestId];
    
    [[NetworkingManager shared] performRequest:HTTPMethodGET urlString:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        InterestDetail *interestDetail = [ParserManager parseInterestsDetails:dataDictionary];
        if (interestDetail != nil) {
            success(interestDetail);
        } else {
            failure([NSError new]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}


@end
