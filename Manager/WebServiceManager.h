//
//  WebServiceManager.h
//  loqiva
//
//  Created by Manuel Manzanera on 26/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import "InterestDetail.h"
#import "Reward.h"
@class MapItem;
@class Tours;
@class User;

@protocol WebServiceManagerDelegate;

typedef enum {
    WebServiceTypeTitle,
    WebServiceTypeAgeGroupUser,
    WebServiceTypeTempUser,
    WebServiceTypeRegisterUser,
    WebServiceTypeUserLogin,
    WebServiceTypeDestroySession,
    WebServiceTypeProfile,
    WebServiceTypeProfileSave,
    WebServiceTypeProfileSaveImage,
    WebServiceTypeForgotPassword,
    WebServiceTypeResetPassword,
    WebServiceTypeHomePage,
    WebServiceTypeInformationDetails,
    WebServiceTypeInfoItem,
    WebServiceTypeRewards,
    WebServiceTypeRewardsDetail,
    WebServiceTypeRedeemRewardsDetail,
    WebServiceTypeEvents,
    WebServiceTypeEventDetail,
    WebServiceTypePaymentTypes,
    WebServiceTypeTransportationTypes,
    WebServiceTypeBenefitInformation,
    WebServiceTypeCongrats,
    WebServiceTypeDeleteInAppMessage,
    WebServiceTypeTours,
    WebServiceTypeToursDetail,
    WebServiceTypeInterests,
    WebServiceTypeInterestSave,
    WebServiceTypeGeoNotification,
    WebServiceTypeMapItems,
    WebServiceTypeInfoMapItems,
    WebServiceTypeInterestDetails,
    WebServiceTypeGeneratingReport,
    WebServiceTypeImageReport,
    WebServiceTypeBeaconReward,
    WebServiceTypeSurveyList,
} WebServiceType;


@interface WebServiceManager : NSObject

//private - exposed for swift
+ (NSString *)urlPath;

// USER
+ (void)getTitleGroup:(id<WebServiceManagerDelegate>)delegate;
+ (void)getAgeGroup:(id<WebServiceManagerDelegate>)delegate;
+ (void)createTempUser:(id<WebServiceManagerDelegate>)delegate;

+(void)registerNewUser:(User *)user success:(void (^)(User *))success failure:(void (^)(NSError *))failure;

+ (void)logoutWithSuccess:(void (^)(void))success failure:(void (^)(NSError *))failure;
+ (void)getLoggedInUsersProfileWithSuccess:(void (^)(User * _Nsonnull))success failure:(void (^)(NSError *))failure;
+ (void)loginWithUsername:(NSString  *)username andPassword:(NSString *)password success:(void (^)(User *))success failure:(void (^)(NSError *))failure;
+ (void)saveProfile:(NSDictionary *)profileDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate;

+ (void)updateProfileImage:(UIImage *)image success:(void (^)(NSString *))success failure:(void (^)(NSError *))failure;

+ (void)forgottenPassWord:(NSString *)email andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)resetPassword:(NSString *)newPassword andConfirmPassword:(NSString *)confirmPassword andDelegate:(id<WebServiceManagerDelegate>)delegate;

// INTERESTS
+ (void)postDevToken:(id<WebServiceManagerDelegate>)delegate;
+ (void)deleteNotification:(NSString *)identifier;
+ (void)getNotificationsDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)getInterests:(id<WebServiceManagerDelegate>)delegate;
+ (void)saveInterestWithId:(NSNumber *)interestId andStatus:(NSNumber *)status andDelegate:(id<WebServiceManagerDelegate>)delegate;

// REPORTING
+ (void)generateReportKey:(id<WebServiceManagerDelegate>)delegate;
+ (void)postReportImage:(UIImage *)reportImage andReportId:(NSString *)reportId andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)postReportFields:(NSDictionary *)reportDictionary andReportId:(NSString *)reportId andDelegate:(id<WebServiceManagerDelegate>)delegate;

//Tours

+ (void)getToursWithLatitude:(CLLocationDegrees)latitude andLongitude:(CLLocationDegrees)longitude success:(void (^)(NSArray <Tours *> *))success failure:(void (^)(NSError *))failure;
+ (void)getToursDetails:(NSString *)walkid andDelegate:(id<WebServiceManagerDelegate>)delegate;

//Rewards

+ (void)getSurveys:(id<WebServiceManagerDelegate>)delegate;

+ (void)getRewardsWithLatitude:(CLLocationDegrees)latitude andLongitude:(CLLocationDegrees)longitude success:(void (^)(NSArray<Reward *> *))success failure:(void (^)(NSError *))failure;
+ (void)getReward:(NSString *)rewardId success:(void (^)(Reward *))success failure:(void (^)(NSError *))failure;
+ (void)getBeaconRewardWithBeaconDictionary:(NSDictionary *)beaconDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate;

+ (void)getHomePageResourcesWithLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)getInformationDetails:(id<WebServiceManagerDelegate>)delegate;
+ (void)getInformationItem:(NSString *)infoid andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)postRedeemRewardsItem:(NSString *)rewardsId andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)getEventsWithLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)getEventsDetail:(NSString *)eventId andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)getPaymentTypes:(id<WebServiceManagerDelegate>)delegate;
+ (void)getPaymentTransports:(id<WebServiceManagerDelegate>)delegate;

+ (void)getServicesWithLatitude:(CLLocationDegrees)latitude andLongitude:(CLLocationDegrees)longitude success:(void (^)(NSArray <MapItem *> * _Nsonnull))success failure:(void (^)(NSError *))failure;
+ (void)getAttractionsWithLatitude:(CLLocationDegrees)latitude andLongitude:(CLLocationDegrees)longitude success:(void (^)(NSArray <MapItem *> *))success failure:(void (^)(NSError *))failure;
        
        
+ (void)deleteInAppMessage:(NSString *)messageId andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)getGeoAlertNotificationWithLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude andDelegate:(id<WebServiceManagerDelegate>)delegate;

+ (void)getInterestDetails:(NSString *)interestId success:(void (^)(InterestDetail *))success failure:(void (^)(NSError *))failure;

@end

@protocol WebServiceManagerDelegate <NSObject>

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object;
- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType;

@optional
- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object;

@end
