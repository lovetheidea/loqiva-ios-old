//
//  RegularExpressionsManager.h
//  loqiva
//
//  Created by Manuel Manzanera on 16/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegularExpressionsManager : NSObject

+ (BOOL)checkMail:(NSString *)checkString;

@end
