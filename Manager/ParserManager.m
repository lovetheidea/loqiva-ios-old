//
//  ParserManager.m
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ParserManager.h"

#import "HomePage.h"
#import "ContextualMenu.h"
#import "Event.h"
#import "LowerMenu.h"
#import "LowerMenuItem.h"
#import "InfoItem.h"
#import "Reward.h"
#import "PaymentType.h"
#import "Transport.h"
#import "Interest.h"
#import "TempUser.h"
#import "MapItem.h"
#import "Tours.h"
#import "CustomNotification.h"
#import "Notification.h"
#import "GeoLocationMessage.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "Survey.h"

@implementation ParserManager

+ (BOOL)parseHomePageResources:(NSDictionary *)response{
    
    NSArray *resultset = [response valueForKey:@"resultset"];
    
    if(resultset && resultset.count > 0)
    {
        NSDictionary *resultsetDictionary = [resultset objectAtIndex:0];
        
        NSArray *contextualMenus = [resultsetDictionary valueForKey:@"contextualmenu"];
        
        NSMutableArray *contextualMenuToSave = [[NSMutableArray alloc] init];
        NSMutableArray *lowerMenuToSave = [[NSMutableArray alloc] init];
        
        
        for(int i = 0; i < contextualMenus.count; i++){
            NSDictionary *contextualMenuDict = [contextualMenus objectAtIndex:i];
            ContextualMenu *contextualMenu = [[ContextualMenu alloc] init];
            switch (i) {
                case 0:
                {
                    NSDictionary *firstItemContextualDictionary = [contextualMenuDict valueForKey:@"firstitem"];
                    [contextualMenu setContextualMenuId:[NSNumber numberWithInt:[[firstItemContextualDictionary valueForKey:@"id"] intValue]]];
                    [contextualMenu setHeadlinetext:[firstItemContextualDictionary valueForKey:@"headlinetext"]];
                    [contextualMenu setTitle:[firstItemContextualDictionary valueForKey:@"title"]];
                    [contextualMenu setSummary:[firstItemContextualDictionary valueForKey:@"description"]];
                    [contextualMenu setDistance:[firstItemContextualDictionary valueForKey:@"distance"]];
                    [contextualMenu setLatitude:[firstItemContextualDictionary valueForKey:@"latitude"]];
                    [contextualMenu setLongitude:[firstItemContextualDictionary valueForKey:@"longitude"]];
                    [contextualMenu setImage:[firstItemContextualDictionary valueForKey:@"image"]];
                    [contextualMenu setOpeningtime:[firstItemContextualDictionary valueForKey:@"openingtime"]];
                    [contextualMenu setClosingtime:[firstItemContextualDictionary valueForKey:@"closingtime"]];
                    [contextualMenu setWeather:[firstItemContextualDictionary valueForKey:@"weather"]];
                    [contextualMenu setTemp:[firstItemContextualDictionary valueForKey:@"temp"]];
                    [contextualMenu setIcon:[firstItemContextualDictionary valueForKey:@"headicon"]];
                    
                    [contextualMenuToSave addObject:contextualMenu];
                }
                    break;
                case 1:
                {
                    NSDictionary *secondItemContextualDictionary = [contextualMenuDict valueForKey:@"seconditem"];
                    [contextualMenu setContextualMenuId:[NSNumber numberWithInt:[[secondItemContextualDictionary valueForKey:@"id"] intValue]]];
                    [contextualMenu setHeadlinetext:[secondItemContextualDictionary valueForKey:@"headlinetext"]];
                    [contextualMenu setTitle:[secondItemContextualDictionary valueForKey:@"title"]];
                    [contextualMenu setSummary:[secondItemContextualDictionary valueForKey:@"description"]];
                    [contextualMenu setDistance:[secondItemContextualDictionary valueForKey:@"distance"]];
                    [contextualMenu setLatitude:[secondItemContextualDictionary valueForKey:@"latitude"]];
                    [contextualMenu setLongitude:[secondItemContextualDictionary valueForKey:@"longitude"]];
                    [contextualMenu setImage:[secondItemContextualDictionary valueForKey:@"image"]];
                    [contextualMenu setOpeningtime:[secondItemContextualDictionary valueForKey:@"openingtime"]];
                    [contextualMenu setClosingtime:[secondItemContextualDictionary valueForKey:@"closingtime"]];
                    [contextualMenu setWeather:[secondItemContextualDictionary valueForKey:@"weather"]];
                    [contextualMenu setTemp:[secondItemContextualDictionary valueForKey:@"temp"]];
                    [contextualMenu setIcon:[secondItemContextualDictionary valueForKey:@"headicon"]];
                    
                    [contextualMenuToSave addObject:contextualMenu];
                    break;
                }
                case 2:{
                    NSDictionary *thirdItemContextualDictionary = [contextualMenuDict valueForKey:@"thirditem"];
                    [contextualMenu setContextualMenuId:[NSNumber numberWithInt:[[thirdItemContextualDictionary valueForKey:@"id"] intValue]]];
                    [contextualMenu setHeadlinetext:[thirdItemContextualDictionary valueForKey:@"headlinetext"]];
                    [contextualMenu setTitle:[thirdItemContextualDictionary valueForKey:@"title"]];
                    [contextualMenu setSummary:[thirdItemContextualDictionary valueForKey:@"description"]];
                    [contextualMenu setDistance:[thirdItemContextualDictionary valueForKey:@"distance"]];
                    [contextualMenu setLatitude:[thirdItemContextualDictionary valueForKey:@"latitude"]];
                    [contextualMenu setLongitude:[thirdItemContextualDictionary valueForKey:@"longitude"]];
                    [contextualMenu setImage:[thirdItemContextualDictionary valueForKey:@"image"]];
                    [contextualMenu setOpeningtime:[thirdItemContextualDictionary valueForKey:@"openingtime"]];
                    [contextualMenu setClosingtime:[thirdItemContextualDictionary valueForKey:@"closingtime"]];
                    [contextualMenu setWeather:[thirdItemContextualDictionary valueForKey:@"weather"]];
                    [contextualMenu setTemp:[thirdItemContextualDictionary valueForKey:@"temp"]];
                    [contextualMenu setIcon:[thirdItemContextualDictionary valueForKey:@"headicon"]];
                    
                    [contextualMenuToSave addObject:contextualMenu];
                }
                    break;
                default:
                    break;
            }
        }
        
        NSString *lowerMenuText = [resultsetDictionary valueForKey:@"lowermenutext"];
        NSArray *lowerMenus = [resultsetDictionary valueForKey:@"lowermenu"];
        
        if (lowerMenus.count > 0) {
            NSArray *lowerMenuItems = [lowerMenus objectAtIndex:0];
            
            for(NSDictionary *lowerDictionary in lowerMenuItems){
                NSDictionary *featureDictionary = [lowerDictionary valueForKey:@"feature"];
                NSArray *feeds = [lowerDictionary valueForKey:@"feeds"];
                
                if(![featureDictionary isKindOfClass:[NSArray class]]){
                    
                    LowerMenu *lowerMenu = [[LowerMenu alloc] init];
                    
                    [lowerMenu setTitle:[featureDictionary valueForKey:@"title"]];
                    [lowerMenu setSummary:[featureDictionary valueForKey:@"Description"]];
                    
                    if(![[featureDictionary valueForKey:@"MediaType"] isKindOfClass:[NSNull class]]) {
                        [lowerMenu setMediaType:[NSNumber numberWithInteger:[[featureDictionary valueForKey:@"MediaType"] integerValue]]];
                    }
                    
                    [lowerMenu setUrl:[featureDictionary valueForKey:@"Url"]];
                    [lowerMenu setLatitude:[featureDictionary valueForKey:@"Latitude"]];
                    [lowerMenu setLongitude:[featureDictionary valueForKey:@"Longitude"]];
                    [lowerMenu setAddress:[featureDictionary valueForKey:@"Address"]];
                    [lowerMenu setDistance:[featureDictionary valueForKey:@"distance"]];
                    [lowerMenu setMediaurl:[featureDictionary valueForKey:@"mediaurl"]];
                    [lowerMenu setLowerMenuId:[featureDictionary valueForKey:@"id"]];
                    
                    [lowerMenuToSave addObject:lowerMenu];
                    
                    NSMutableArray *lowerMenuItemsToSave = [[NSMutableArray alloc] init];
                    
                    for(NSDictionary *lowerMenuItemDict in feeds){
                        
                        LowerMenuItem *lowerMenuItem = [[LowerMenuItem alloc] init];
                        [lowerMenuItem setContent:[lowerMenuItemDict valueForKey:@"content"]];
                        [lowerMenuItem setImage:[lowerMenuItemDict valueForKey:@"image"]];
                        [lowerMenuItem setSourceLink:[lowerMenuItemDict valueForKey:@"sourceLink"]];
                        [lowerMenuItem setSourceTitle:[lowerMenuItemDict valueForKey:@"sourceTitle"]];
                        [lowerMenuItem setTime:[lowerMenuItemDict valueForKey:@"time"]];
                        [lowerMenuItem setTitle:[lowerMenuItemDict valueForKey:@"title"]];
                        [lowerMenuItem setType:[lowerMenuItemDict valueForKey:@"type"]];
                        NSLog(@"HYHY  :: %@", lowerMenuItem.type);
                        NSLog(@"HYHY  :: %@", lowerMenuItem.title);
                        NSLog(@"HYHY  :: %@", lowerMenuItem.sourceTitle);
                        
                        if([lowerMenuItem.type isEqualToString:@"youtube"]){
                            NSLog(@"HYHY  :: %@", lowerMenuItemDict);
                        }
                        
                        [lowerMenuItemsToSave addObject:lowerMenuItem];
                    }
                    LowerMenu *lastlowerMenu = [lowerMenuToSave objectAtIndex:lowerMenuToSave.count - 1];
                    [lastlowerMenu setLowerMenuItems:lowerMenuItemsToSave];
                }
                else {
                    NSMutableArray *lowerMenuItemsToSave = [[NSMutableArray alloc] init];
                    for(NSDictionary *lowerMenuItemDict in feeds){
                        LowerMenuItem *lowerMenuItem = [[LowerMenuItem alloc] init];
                        [lowerMenuItem setContent:[lowerMenuItemDict valueForKey:@"content"]];
                        [lowerMenuItem setImage:[lowerMenuItemDict valueForKey:@"image"]];
                        [lowerMenuItem setSourceLink:[lowerMenuItemDict valueForKey:@"sourceLink"]];
                        [lowerMenuItem setSourceTitle:[lowerMenuItemDict valueForKey:@"sourceTitle"]];
                        [lowerMenuItem setTime:[lowerMenuItemDict valueForKey:@"time"]];
                        [lowerMenuItem setTitle:[lowerMenuItemDict valueForKey:@"title"]];
                        [lowerMenuItem setType:[lowerMenuItemDict valueForKey:@"type"]];
                        NSLog(@"HYHY  :: %@", lowerMenuItem.type);
                        NSLog(@"HYHY  :: %@", lowerMenuItem.title);
                        NSLog(@"HYHY  :: %@", lowerMenuItem.sourceTitle);
                        
                        if([lowerMenuItem.type isEqualToString:@"youtube"]){
                            NSLog(@"HYHY  :: %@", lowerMenuItemDict);
                        }
                        
                        [lowerMenuItemsToSave addObject:lowerMenuItem];
                    }
                    LowerMenu *lastlowerMenu = [lowerMenuToSave objectAtIndex:lowerMenuToSave.count - 1];
                    NSMutableArray *lowerMenuItems = [lastlowerMenu.lowerMenuItems mutableCopy];
                    [lowerMenuItems addObjectsFromArray:lowerMenuItemsToSave];
                    [lastlowerMenu setLowerMenuItems:lowerMenuItems];
                }
            }
        }
        
        if([resultsetDictionary valueForKey:@"alerttype"]){
            GeoLocationMessage *geolocationMessage = [[GeoLocationMessage alloc] init];
            
            [geolocationMessage setAlerttype:[resultsetDictionary valueForKey:@"alerttype"]];
            [geolocationMessage setUserId:[resultsetDictionary valueForKey:@"userid"]];
            [geolocationMessage setUserlatitude:[resultsetDictionary valueForKey:@"userlatitude"]];
            [geolocationMessage setUserlongitude:[resultsetDictionary valueForKey:@"userlongitude"]];
            [geolocationMessage setMsgtitle:[resultsetDictionary valueForKey:@"msgtitle"]];
            [geolocationMessage setWelcomeMsg:[resultsetDictionary valueForKey:@"welcomemsg"]];
            [geolocationMessage setGoodbyeMsg:[resultsetDictionary valueForKey:@"goodbyemsg"]];
        
            [[AppContext sharedInstance] setGeoLocationMessage:geolocationMessage];
            
        }
        
        if(contextualMenuToSave && contextualMenuToSave.count > 0){
            HomePage *homePage = [[HomePage alloc] init];
            [homePage setContextualMenus:contextualMenuToSave];
            [homePage setLowerMenuText:lowerMenuText];
            [homePage setLowerMenu:lowerMenuToSave];
        
            [[AppContext sharedInstance] setHomePage:homePage];
        }
    
    }
    
    return YES;
}


+ (BOOL)parseInformationDetails:(NSDictionary *)response{
    
    NSArray *resultset = [response valueForKey:@"resultset"];
    
    if(resultset && resultset.count > 0)
    {
        NSDictionary *resultsetDictionary = [resultset objectAtIndex:0];
        
        NSArray *infoItems = [resultsetDictionary valueForKey:@"infoitem"];
        NSMutableArray *infoItemsToSave = [[NSMutableArray alloc] init];
        for(NSDictionary *infoItemDict in infoItems){
            InfoItem *infoItem = [[InfoItem alloc] init];
            [infoItem setMapitemid:[infoItemDict valueForKey:@"mapitemid"]];
            [infoItem setTitle:[infoItemDict valueForKey:@"title"]];
            
            [infoItemsToSave addObject:infoItem];
        }
        
        [[AppContext sharedInstance] setInfoItems:infoItemsToSave];
    }
    
    return YES;
}

+ (InfoItem *)parseInfoItem:(NSDictionary *)response{
    
    NSArray *resultset = [response valueForKey:@"resultset"];
    InfoItem *infoItem;
    if(resultset && resultset.count > 0)
    {
        NSDictionary *resultsetDictionary = [resultset objectAtIndex:0];
        
        infoItem = [[InfoItem alloc] init];
        [infoItem setMapitemid:[resultsetDictionary valueForKey:@"infoid"]];
        [infoItem setTitle:[resultsetDictionary valueForKey:@"title"]];
        [infoItem setInfoid:[resultsetDictionary valueForKey:@"infoid"]];
        [infoItem setAddress:[resultsetDictionary valueForKey:@"address"]];
        [infoItem setMediatype:[resultsetDictionary valueForKey:@"mediatype"]];
        [infoItem setInterestphone:[resultsetDictionary valueForKey:@"interestphone"]];
        [infoItem setInterestmail:[resultsetDictionary valueForKey:@"interestemail"]];
        [infoItem setImagetype:[resultsetDictionary valueForKey:@"imagetype"]];
        [infoItem setSummary:[resultsetDictionary valueForKey:@"description"]];
        [infoItem setName:[resultsetDictionary valueForKey:@"name"]];
        [infoItem setMediaurl:[resultsetDictionary valueForKey:@"mediaurl"]];
        
    }
    
    return infoItem;
}

+ (NSArray *)parseSurvey:(NSDictionary *)response {
    NSArray *resultset = [response valueForKey:@"resultset"];
    NSMutableArray *surveysToSave = [[NSMutableArray  alloc] init];
    
    for (NSDictionary *dict in resultset) {
        Survey *survey = [[Survey alloc] init];
        [survey setDesc:[dict valueForKey:@"description"]];
        [survey setExpires:[dict valueForKey:@"expires"]];
        [survey setLink:[dict valueForKey:@"link"]];
        [survey setTitle:[dict valueForKey:@"title"]];
        
        [surveysToSave addObject:survey];
    }
    
    if (surveysToSave.count) {
        [[AppContext sharedInstance] setSurveys:surveysToSave];
    }
    else {
        [[AppContext sharedInstance] setSurveys:nil];
    }
    
    return surveysToSave;
}

+ (NSArray *)parseRewards:(NSDictionary *)response{
    
    NSArray *resultset = [response valueForKey:@"resultset"];
    
    NSMutableArray *rewardsToSave = [[NSMutableArray  alloc] init];
    
    if(resultset && resultset.count > 0)
    {
        NSDictionary *resultsetDictionary = [resultset objectAtIndex:0];
        
        NSArray *rewardItems = [resultsetDictionary valueForKey:@"mapitempoint"];
        
        for(NSDictionary *rewardItemDict in rewardItems){
            Reward *reward = [[Reward alloc] init];
            
            [reward setMapitempoint:[rewardItemDict valueForKey:@"mapitemid"]];
            [reward setTitle:[rewardItemDict valueForKey:@"title"]];
            [reward setImageType:[rewardItemDict valueForKey:@"imagetype"]];
            [reward setLatitude:[rewardItemDict valueForKey:@"latitude"]];
            [reward setLongitude:[rewardItemDict valueForKey:@"longitude"]];
            [reward setMediaurl:[rewardItemDict valueForKey:@"mediaurl"]];
            [reward setDistance:[rewardItemDict valueForKey:@"distance"]];
            [reward setCompany:[rewardItemDict valueForKey:@"company"]];
            [reward setTimetables:[rewardItemDict valueForKey:@"timetable"]];
            
            [rewardsToSave addObject:reward];
        }
        
//        [[AppContext sharedInstance] setRewardsItems:rewardsToSave];
        return rewardsToSave;
    }
    
    return [NSArray new];
}

+ (Reward *)parseRewardsDetail:(NSDictionary *)response{
    
    NSArray *resultset = [response valueForKey:@"resultset"];
    Reward *reward;
    if(resultset && resultset.count > 0)
    {
        NSDictionary *resultsetDictionary = [resultset objectAtIndex:0];
        
        reward = [[Reward alloc] init];
            
        [reward setMapitempoint:[resultsetDictionary valueForKey:@"mapitemid"]];
        [reward setTitle:[resultsetDictionary valueForKey:@"title"]];
        [reward setImageType:[resultsetDictionary valueForKey:@"imagetype"]];
        [reward setLatitude:[resultsetDictionary valueForKey:@"latitude"]];
        [reward setLongitude:[resultsetDictionary valueForKey:@"longitude"]];
        [reward setAddress:[resultsetDictionary valueForKey:@"address"]];
        [reward setSummary:[resultsetDictionary valueForKey:@"description"]];
        [reward setInterestURL:[resultsetDictionary valueForKey:@"interesturl"]];
        [reward setInterestPhone:[resultsetDictionary valueForKey:@"interestphone"]];
        [reward setMediatype:[resultsetDictionary valueForKey:@"mediatype"]];
        [reward setMediaurl:[resultsetDictionary valueForKey:@"mediaurl"]];
        [reward setDistance:[resultsetDictionary valueForKey:@"distance"]];
        [reward setCompany:[resultsetDictionary valueForKey:@"company"]];
        [reward setTimetables:[resultsetDictionary valueForKey:@"timetable"]];
    }
    
    return reward;
}

+ (BOOL)parseEvents:(NSDictionary *)response {
    NSArray *resultset = [response valueForKey:@"resultset"];
    
    NSMutableArray *eventsToSave = [[NSMutableArray  alloc] init];
    
    for(NSDictionary *eventItemDict in resultset){
        Event *event = [[Event alloc] init];
            
        [event setEventId:[NSNumber numberWithInt:[[eventItemDict valueForKey:@"eventid"] intValue]]];
        [event setTitle:[eventItemDict valueForKey:@"title"]];
        [event setSummary:[eventItemDict valueForKey:@"description"]];
        [event setMediatype:[NSNumber numberWithInt:[[eventItemDict valueForKey:@"mediatype"] intValue]]];
        [event setMediaURL:[eventItemDict valueForKey:@"mediaurl"]];
        [event setSecondaryimage:[eventItemDict valueForKey:@"secondaryimage"]];
        [event setUrl:[eventItemDict valueForKey:@"url"]];
        [event setLatitude:[eventItemDict valueForKey:@"latitude"]];
        [event setLongitude:[eventItemDict valueForKey:@"longitude"]];
        [event setEventcategory:[eventItemDict valueForKey:@"eventcategory"]];
        [event setAddress:[eventItemDict valueForKey:@"address"]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:DATE_FORMATTER];
        
        NSDate *date = [dateFormatter dateFromString:[eventItemDict valueForKey:@"startdate"]];
        [event setStartDate:date];
        
        [event setVenueurl:[eventItemDict valueForKey:@"venueurl"]];
        [event setTicketsurl:[eventItemDict valueForKey:@"ticketsurl"]];
        
        [eventsToSave addObject:event];
    }
    
    if (eventsToSave.count) {
        [[AppContext sharedInstance] setEvents:eventsToSave];
    }
    else {
        [[AppContext sharedInstance] setEvents:nil];
    }
    
    return YES;
}


+ (Event *)parseEventDetail:(NSDictionary *)response{
    NSArray *resultset = [response valueForKey:@"resultset"];
    Event *event;
    if(resultset && resultset.count > 0)
    {
        NSDictionary *resultsetDictionary = [resultset objectAtIndex:0];
        
        event = [[Event alloc] init];
        
        [event setEventId:[resultsetDictionary valueForKey:@"eventid"]];
        [event setTitle:[resultsetDictionary valueForKey:@"title"]];
        [event setSummary:[resultsetDictionary valueForKey:@"description"]];
        [event setMediatype:[resultsetDictionary valueForKey:@"mediatype"]];
        [event setMediaURL:[resultsetDictionary valueForKey:@"mediaurl"]];
        [event setSecondaryimage:[resultsetDictionary valueForKey:@"secondaryimage"]];
        [event setUrl:[resultsetDictionary valueForKey:@"url"]];
        [event setLatitude:[resultset valueForKey:@"latitude"]];
        [event setLongitude:[resultset valueForKey:@"longitude"]];
        [event setEventcategory:[resultset valueForKey:@"eventcategory"]];
        [event setAddress:[resultset valueForKey:@"address"]];
        [event setStartDate:[resultset valueForKey:@"startdate"]];
        [event setVenueurl:[resultset valueForKey:@"venueurl"]];
    }
    
    return event;
}

+ (BOOL)paymentTypes:(NSDictionary *)response{
    
    NSArray *resultset = [response valueForKey:@"resultset"];
    NSMutableArray *paymentTypesToSave = [[NSMutableArray  alloc] init];
    
    if(resultset && resultset.count > 0)
    {
        for(NSDictionary *paymentTypeDictionary in resultset){
            
            PaymentType *paymentType = [[PaymentType alloc] init];
            [paymentType setPaymentTypeId:[paymentTypeDictionary valueForKey:@"paymenttypeid"]];
            [paymentType setPaymentTypeTitle:[paymentTypeDictionary valueForKey:@"paymenttypetitle"]];
            [paymentType setPaymentURL:[paymentTypeDictionary valueForKey:@"paymenturl"]];
            
            [paymentTypesToSave addObject:paymentType];
        }
        
        [[AppContext sharedInstance] setPaymentTypes:paymentTypesToSave];
    }
    
    return YES;
}

+ (BOOL)transportationTypes:(NSDictionary *)response{
    
    NSArray *resultset = [response valueForKey:@"resultset"];
    NSMutableArray *transportsToSave = [[NSMutableArray  alloc] init];
    
    if(resultset && resultset.count > 0)
    {
        for(NSDictionary *transportDictionary in resultset){
            
            Transport  *transport = [[Transport alloc] init];
            [transport setTitle:[transportDictionary valueForKey:@"title"]];
            [transport setUrl:[transportDictionary valueForKey:@"url"]];
            [transport setIcon:[transportDictionary valueForKey:@"icon"]];
            
            [transportsToSave addObject:transport];
        }
        
        [[AppContext sharedInstance] setTransports:transportsToSave];
    }
    
    return YES;
}

+ (BOOL)parseNotifications:(NSDictionary *)response {
    NSArray *resultset = [response valueForKey:@"resultset"];
    NSMutableArray *notificationsToSave = [[NSMutableArray  alloc] init];
    
    if(resultset && resultset.count > 0)
    {
        for(NSDictionary *interestDictionary in resultset){
            
            Notification *notification = [[Notification alloc] init];
            notification.title = interestDictionary[@"title"];
            notification.date = interestDictionary[@"date"];
            notification.documentId = interestDictionary[@"documentid"];
            notification.hyperLink = interestDictionary[@"hyperlink"];
            notification.imageLink = interestDictionary[@"image"];
            notification.notificationBody = interestDictionary[@"inappbody"];
            notification.notificationHeader = interestDictionary[@"inappheader"];
            notification.thumbnailImageLink = interestDictionary[@"thumbnail"];
            notification.identifier = interestDictionary[@"id"];
            notification.mapId = interestDictionary[@"mapitemid"];
            notification.surveyId = interestDictionary[@"surveyid"];
            notification.templateId = interestDictionary[@"templatemessageid"];
            
            [notificationsToSave addObject:notification];
        }
        
        [[AppContext sharedInstance] setNotifications:notificationsToSave];
    }
    
    return YES;
}

+ (BOOL)parseInterests:(NSDictionary *)response{
    NSArray *resultset = [response valueForKey:@"resultset"];
    NSMutableArray *interestsToSave = [[NSMutableArray  alloc] init];
    
    if(resultset && resultset.count > 0)
    {
        for(NSDictionary *interestDictionary in resultset){
            
            Interest *interest = [[Interest alloc] init];
            
            [interest setInterestId:[interestDictionary valueForKey:@"InterestId"]];
            [interest setName:[interestDictionary valueForKey:@"InterestName"]];
            [interest setStatus:[interestDictionary valueForKey:@"Status"]];
            
            [interestsToSave addObject:interest];
        }
        
        [[AppContext sharedInstance] setInterests:interestsToSave];
    }
    
    return YES;
}

+(User *) parseUser:(NSDictionary *)response {
    NSDictionary *userDictionary = [[response valueForKey:@"resultset"] firstObject];
    User *user = [[User alloc] init];
    
    [user setFirstName:[userDictionary valueForKey:@"firstname"]];
    [user setLastName:[userDictionary valueForKey:@"lastname"]];
    [user setPostcode:[userDictionary valueForKey:@"postcode"]];
    [user setMail:[userDictionary valueForKey:@"email"]];
    [user setSourceImage:[userDictionary valueForKey:@"profilepicture"]];
    [user setPhoneNumber:[userDictionary valueForKey:@"mobile"]];
    
    return user;
}

+ (BOOL)parseTempUser:(NSDictionary *)response{
    NSArray *resultset = [response valueForKey:@"resultset"];
    TempUser *tempUser;
    if(resultset && resultset.count > 0)
    {
        NSDictionary *resultsetDictionary = [resultset objectAtIndex:0];
        
        tempUser = [[TempUser alloc] init];
        [tempUser setUserToken:[resultsetDictionary valueForKey:@"usertoken"]];
        [tempUser setSessionToken:[resultsetDictionary  valueForKey:@"sessiontoken"]];
    }
    
    return tempUser;
}

+ (NSArray <Tours *> *)parseTours:(NSDictionary *)response {
    
    NSArray *resultset = [response valueForKey:@"resultset"];
    NSMutableArray *toursToSave = [[NSMutableArray  alloc] init];
    
    if(resultset && resultset.count > 0)
    {
        for(NSDictionary *tourDictionary in resultset){
            
            Tours *tours = [[Tours alloc] init];
            
            [tours setWalkId:[NSNumber numberWithInt:[[tourDictionary valueForKey:@"walkid"] intValue]]];
            [tours setTitle:[tourDictionary valueForKey:@"title"]];
            [tours setStartPointName:[tourDictionary valueForKey:@"Start"]];
            [tours setEndPointName:[tourDictionary valueForKey:@"End"]];
            [tours setStartPointLat:[NSNumber numberWithDouble:[[tourDictionary valueForKey:@"startpointlat"] doubleValue]]];
            [tours setEndPointLat:[NSNumber numberWithDouble:[[tourDictionary valueForKey:@"endpointlat"] doubleValue]]];
            [tours setStartPointLong:[NSNumber numberWithDouble:[[tourDictionary valueForKey:@"startpointlong"] doubleValue]]];
            [tours setEndPointLong:[NSNumber numberWithDouble:[[tourDictionary valueForKey:@"endpointlong"] doubleValue]]];
            [tours setWalkColour:[tourDictionary valueForKey:@"walkcolour"]];
            [tours setMidPointLat:[NSNumber numberWithDouble:[[tourDictionary valueForKey:@"midpointlat"] doubleValue]]];
            [tours setMidPointLong:[NSNumber numberWithDouble:[[tourDictionary valueForKey:@"midpointlong"] doubleValue]]];
            [tours setWalkRoutePolyline:[tourDictionary valueForKey:@"walkroutepolyline"]];
            [tours setWalkListOrder:[NSNumber numberWithInt:[[tourDictionary valueForKey:@"walklistorder"] intValue]]];
            [tours setImageURL:[tourDictionary objectForKey:@"mediaurl"]];
            [toursToSave addObject:tours];
        }
    }
    
    return toursToSave;
}

+ (NSArray <MapItem *>*)parseMapItems:(NSDictionary *)response{
    NSArray *resultset = [response valueForKey:@"resultset"];
    NSMutableArray *mapsToSave = [[NSMutableArray  alloc] init];
    
    if(resultset && resultset.count > 0)
    {
        for(NSDictionary *mapIntemDictionary in resultset){
            
            MapItem *mapItem = [[MapItem alloc] init];
            
            [mapItem setMapItemId:[NSNumber numberWithInt:[[mapIntemDictionary valueForKey:@"mapitemid"] intValue]]];
            [mapItem setTitle:[mapIntemDictionary valueForKey:@"title"]];
            //[mapItem setImageType:[NSNumber numberWithInt:[[mapIntemDictionary valueForKey:@"imagetype"] intValue]]];
            [mapItem setImageType:[mapIntemDictionary valueForKey:@"imagetype"]];
            [mapItem setLatitude:[NSNumber numberWithFloat:[[mapIntemDictionary valueForKey:@"latitude"] floatValue]]];
            [mapItem setLongitude:[NSNumber numberWithFloat:[[mapIntemDictionary valueForKey:@"longitude"] floatValue]]];
            
            [mapsToSave addObject:mapItem];
        }
    }
    return mapsToSave;
}

+ (InterestDetail *)parseInterestsDetails:(NSDictionary *)response{
    NSArray *resultset = [response valueForKey:@"resultset"];
    
    if(resultset && resultset.count > 0)
    {
        for(NSDictionary *mapIntemDictionary in resultset){
            
            InterestDetail *interestDetail = [[InterestDetail alloc] init];
            
            [interestDetail setInterestId:[mapIntemDictionary valueForKey:@"interestid"]];
            [interestDetail setAddress:[mapIntemDictionary valueForKey:@"address"]];
            [interestDetail setSummary:[mapIntemDictionary valueForKey:@"description"]];
            [interestDetail setInterestURL:[mapIntemDictionary valueForKey:@"interesturl"]];
            [interestDetail setLatitude:[mapIntemDictionary valueForKey:@"latitude"]];
            [interestDetail setLongitude:[mapIntemDictionary valueForKey:@"longitude"]];
            [interestDetail setMediaType:[mapIntemDictionary valueForKey:@"mediatype"]];
            [interestDetail setMediaURL:[mapIntemDictionary valueForKey:@"mediaurl"]];
            [interestDetail setSecondaryImage:[mapIntemDictionary valueForKey:@"secondaryimage"]];
            [interestDetail setTitle:[mapIntemDictionary valueForKey:@"title"]];
            [interestDetail setWalkId:[mapIntemDictionary valueForKey:@"walkid"]];
            [interestDetail setTimetables:[mapIntemDictionary valueForKey:@"timetable"]];
            [interestDetail setDistance:[mapIntemDictionary valueForKey:@"distance"]];
            [interestDetail setImageType:[mapIntemDictionary valueForKey:@"imagetype"]];
            
            return interestDetail;
        }
    }
    
    
    return nil;
}

+ (Tours *)parseTourDetails:(NSDictionary *)response{
    NSArray *resultset = [response valueForKey:@"resultset"];
    
    if(resultset && resultset.count > 0)
    {
        for(NSDictionary *mapIntemDictionary in resultset){
            
            Tours *tourDetail = [[Tours alloc] init];
            
            [tourDetail setWalkId:[mapIntemDictionary valueForKey:@"walkid"]];
            [tourDetail setTitle:[mapIntemDictionary valueForKey:@"title"]];
            [tourDetail setEndPointLat:[mapIntemDictionary valueForKey:@"endpointlat"]];
            [tourDetail setEndPointLong:[mapIntemDictionary valueForKey:@"endpointlong"]];
            [tourDetail setStartPointLat:[mapIntemDictionary valueForKey:@"startpointlat"]];
            [tourDetail setStartPointLong:[mapIntemDictionary valueForKey:@"startpointlong"]];
            [tourDetail setWalkColour:[mapIntemDictionary valueForKey:@"walkcolour"]];
            [tourDetail setWalkRoutePolyline:[mapIntemDictionary valueForKey:@"walkroutepolyline"]];
            
            NSArray *interests = [mapIntemDictionary valueForKey:@"interestpoint"];
            NSMutableArray *interestsTour = [[NSMutableArray alloc] initWithCapacity:0];
            
            for(NSDictionary *interestDictionary in interests){
                InterestDetail *interestDetail = [[InterestDetail alloc] init];
                [interestDetail setInterestId:[interestDictionary valueForKey:@"interestid"]];
                [interestDetail setLatitude:[interestDictionary valueForKey:@"latitude"]];
                [interestDetail setLongitude:[interestDictionary valueForKey:@"longitude"]];
                [interestDetail setTitle:[interestDictionary valueForKey:@"title"]];
                [interestDetail setDistance:[interestDictionary valueForKey:@"distance"]];
                
                [interestsTour addObject:interestDetail];
            }
            
            [tourDetail setInterestPoints:interestsTour];
            
            return tourDetail;
        }
    }
    
    
    return nil;
}

+ (GeoLocationMessage *)parseGeolocationMessage:(NSDictionary *)response{
    NSArray *resultset = [response valueForKey:@"resultset"];
    if(resultset && resultset.count > 0){
        
        NSDictionary *resultsetDictionary = [resultset objectAtIndex:0];
        
        if([resultsetDictionary valueForKey:@"alerttype"]){
            GeoLocationMessage *geolocationMessage = [[GeoLocationMessage alloc] init];
            
            [geolocationMessage setAlerttype:[resultsetDictionary valueForKey:@"alerttype"]];
            [geolocationMessage setUserId:[resultsetDictionary valueForKey:@"userid"]];
            [geolocationMessage setUserlatitude:[resultsetDictionary valueForKey:@"userlatitude"]];
            [geolocationMessage setUserlongitude:[resultsetDictionary valueForKey:@"userlongitude"]];
            [geolocationMessage setMsgtitle:[resultsetDictionary valueForKey:@"msgtitle"]];
            [geolocationMessage setWelcomeMsg:[resultsetDictionary valueForKey:@"welcomemsg"]];
            [geolocationMessage setGoodbyeMsg:[resultsetDictionary valueForKey:@"goodbyemsg"]];
            [geolocationMessage setIcon:[resultsetDictionary valueForKey:@"icon"]];
            [geolocationMessage setUrl:[resultsetDictionary valueForKey:@"url"]];
            [geolocationMessage setRewardId:[resultsetDictionary valueForKey:@"rewardid"]];
            
            [[AppContext sharedInstance] setGeoLocationMessage:geolocationMessage];
            
            return geolocationMessage;
            
        }
    }
        return nil;
}

@end
