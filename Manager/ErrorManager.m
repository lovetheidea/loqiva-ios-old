//
//  ErrorManager.m
//  loqiva
//
//  Created by Manuel Manzanera on 3/6/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "ErrorManager.h"
#import "loqiva-Swift.h"

@implementation ErrorManager

+ (NSString *)errorMessaggeWithCode:(NSNumber *)errorCode{
    switch (errorCode.intValue) {
        case 100:
            return NSLocalizedString(@"Invalid username and password", nil);
            break;
        case 101:
            return NSLocalizedString(@"Invalid Username", nil);
            break;
        case 102:
            return NSLocalizedString(@"Invalid Password", nil);
            break;
        case 103:
            return NSLocalizedString(@"User is not active. Please confirm sign up", nil);
            break;
        case 104:
            return NSLocalizedString(@"User details not found. Please sign up.", nil);
            break;
        case 105:
            return NSLocalizedString(@"Email has already been taken. Please try another.", nil);
            break;
        case 106:
            return NSLocalizedString(@"Invalid email address. Please check again.", nil);
            break;
        case 107:
            return NSLocalizedString(@"The password you have entered is not valid. Passwords need to be a minimum of 6 characters. No special characters are allowed.", nil);
            break;
        default:
            return @"";
            break;
    }
}

@end
