//
//  UserDefaultManager.m
//  loqiva
//
//  Created by Manuel Manzanera on 27/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "UserDefaultManager.h"
#import "loqiva-Swift.h"

@implementation UserDefaultManager

+(id)getWithKey:(NSString *)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (standardUserDefaults && [standardUserDefaults objectForKey:key]!=nil)
    {
        return [standardUserDefaults objectForKey:key];
    }
    return nil;
}

+(void)setWithKey:(NSString *)key WithObject:(id)obj
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (standardUserDefaults)
    {
        [standardUserDefaults setObject:obj forKey:key];
        [standardUserDefaults synchronize];
    }
}

+(void)removeKey:(NSString *)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (standardUserDefaults)
    {
        [standardUserDefaults removeObjectForKey:key];
        [standardUserDefaults synchronize];
    }
}

+(void)setWithdictionary:(NSDictionary *)dict
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (standardUserDefaults)
    {
        for (NSString * key in dict)
        {
            [standardUserDefaults setObject:[dict objectForKey:key] forKey:key];
        }
        [standardUserDefaults synchronize];
    }
}

@end
