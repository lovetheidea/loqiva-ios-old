//
//  UserDefaultManager.h
//  loqiva
//
//  Created by Manuel Manzanera on 27/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultManager : NSObject

+(id)getWithKey:(NSString *)key;
+(void)setWithKey:(NSString *)key WithObject:(id)obj;
+(void)setWithdictionary:(NSDictionary *)dict;
+(void)removeKey:(NSString *)key;

@end
