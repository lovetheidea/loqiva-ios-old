//
//  RegularExpressionsManager.m
//  loqiva
//
//  Created by Manuel Manzanera on 16/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "RegularExpressionsManager.h"
#import "loqiva-Swift.h"

@implementation RegularExpressionsManager

+ (BOOL)checkMail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end
