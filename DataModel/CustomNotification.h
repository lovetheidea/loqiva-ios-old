//
//  CustomNotification.h
//  loqiva
//
//  Created by Manuel Manzanera on 16/11/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    CustomNotificationTypeBeacon,
    CustomNotificationTypeGeoAlert
} CustomNotificationType;

@interface CustomNotification : NSObject <NSCoding>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *summary;
@property (nonatomic, strong) NSDate *notificationDate;
@property (nonatomic, strong) NSString *notificationId;
@property (nonatomic, strong) NSNumber *isRead;
@property (nonatomic, strong) NSNumber *customNotificationType;
@property (nonatomic, strong) NSString *url;

+ (BOOL)saveCustomNotification:(CustomNotification *)customNotification;
+ (NSMutableArray *)getCustomNotifications;
+ (BOOL)deleteCustomNotification:(CustomNotification *)customNotifiaction;

@end
