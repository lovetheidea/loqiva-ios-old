//
//  Notification.h
//  loqiva
//
//  Created by Emu on 15/08/2017.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notification : NSObject

@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *documentId;
@property (nonatomic, strong) NSString *hyperLink;
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *mapId;
@property (nonatomic, strong) NSString *surveyId;
@property (nonatomic, strong) NSString *templateId;
@property (nonatomic, strong) NSString *imageLink;
@property (nonatomic, strong) NSString *notificationBody;
@property (nonatomic, strong) NSString *notificationHeader;
@property (nonatomic, strong) NSString *thumbnailImageLink;
@property (nonatomic, strong) NSString *title;

@end
