//
//  LowerMenuItem.h
//  loqiva
//
//  Created by Manuel Manzanera on 11/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LowerMenuItem : NSObject

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *sourceTitle;
@property (nonatomic, strong) NSString *sourceLink;

@end
