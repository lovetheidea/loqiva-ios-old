//
//  Tours.h
//  loqiva
//
//  Created by Manuel Manzanera on 5/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tours : NSObject

@property (nonatomic, strong) NSNumber *walkId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *startPointName;
@property (nonatomic, strong) NSString *endPointName;
@property (nonatomic, strong) NSNumber *startPointLat;
@property (nonatomic, strong) NSNumber *startPointLong;
@property (nonatomic, strong) NSNumber *endPointLat;
@property (nonatomic, strong) NSNumber *endPointLong;
@property (nonatomic, strong) NSString *walkColour;
@property (nonatomic, strong) NSNumber *midPointLat;
@property (nonatomic, strong) NSNumber *midPointLong;
@property (nonatomic, strong) NSString *walkRoutePolyline;
@property (nonatomic, strong) NSNumber *walkListOrder;
@property (nonatomic, strong) NSString *imageURL;

@property (nonatomic, strong) NSArray *interestPoints;

@end
