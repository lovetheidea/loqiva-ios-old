//
//  TempUser.h
//  loqiva
//
//  Created by Manuel Manzanera on 10/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TempUser : NSObject

@property (nonatomic, strong) NSString *userToken;
@property (nonatomic, strong) NSString *sessionToken;

@end
