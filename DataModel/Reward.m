//
//  Reward.m
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "Reward.h"
#import "AppContext.h"
#import "loqiva-Swift.h"

@implementation Reward

+ (Reward *)getRewardWithMapItemPoint:(NSString *)mapItemPoint {
    for(Reward *reward in [[RewardCache shared] cachedRewards]){
        if ([mapItemPoint isKindOfClass:[NSString class]]) {
            NSString *rewardId = [NSString stringWithFormat:@"%i", reward.mapitempoint.intValue];
            if([rewardId isEqualToString:mapItemPoint]) {
                return reward;
            }
        }
        else {
            int rewardId = reward.mapitempoint.intValue;
            int mapId = [(NSNumber *)mapItemPoint intValue];
            if (rewardId == mapId) {
                return reward;
            }
        }
        
    }
    
    return nil;
}

+ (Reward *)getRewardWithTitle:(NSString *)rewardTitle{
    for(Reward *reward in [[RewardCache shared] cachedRewards]) {
        if([reward.title isEqualToString:rewardTitle])
            return reward;
    }
    
    return nil;
}

@end
