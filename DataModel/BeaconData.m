//
//  BeaconData.m
//  loqiva
//
//  Created by Wasiq on 10/10/2016.
//  Copyright © 2016 inup. All rights reserved.
//

#import "BeaconData.h"

#import "UserDefaultManager.h"
#import "loqiva-Swift.h"

@implementation BeaconData

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    
    if(self){
        [self setBeaconUUID:[aDecoder decodeObjectForKey:@"beaconUUID"]];
        [self setTitleNotification:[aDecoder decodeObjectForKey:@"titleNotification"]];
        [self setBodyNotification:[aDecoder decodeObjectForKey:@"bodyNotification"]];
        [self setRewardId:[aDecoder      decodeObjectForKey:@"rewardId"]];
        [self setHeading:[aDecoder decodeObjectForKey:@"heading"]];
        [self setIcon:[aDecoder decodeObjectForKey:@"icon"]];
        [self setImageData:[aDecoder decodeObjectForKey:@"imageData"]];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
#warning THE UUID is HARDCODE
    [aCoder encodeObject:@"1" forKey:@"beaconUUID"];
    [aCoder encodeObject:_titleNotification forKey:@"titleNotification"];
    [aCoder encodeObject:_bodyNotification forKey:@"bodyNotification"];
    [aCoder encodeObject:_icon forKey:@"icon"];
    [aCoder encodeObject:_rewardId  forKey:@"rewardId"];
    [aCoder encodeObject:_heading forKey:@"heading"];
    [aCoder encodeObject:_imageData forKey:@"imageData"];
}

+ (void)saveBeacon:(BeaconData *)beaconData{
    
    NSData *beaconInfo = [NSKeyedArchiver archivedDataWithRootObject:beaconData];
    [UserDefaultManager setWithKey:@"BEACON" WithObject:beaconInfo];
}

+ (BeaconData *)getBeaconData{
    
    NSData *beaconInfo = [UserDefaultManager getWithKey:@"BEACON"];
    BeaconData *lastBeaconReceived = [NSKeyedUnarchiver unarchiveObjectWithData:beaconInfo];
    
    return lastBeaconReceived;
}

@end
