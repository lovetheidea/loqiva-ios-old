//
//  BeaconData.h
//  loqiva
//
//  Created by Wasiq on 10/10/2016.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeaconData : NSObject <NSCoding>

@property (nonatomic, strong) NSString *beaconUUID;
@property (nonatomic, strong) NSString *titleNotification;
@property (nonatomic, strong) NSString *bodyNotification;
@property (nonatomic, strong) NSString *rewardId;
@property (nonatomic, strong) NSString *heading;
@property (nonatomic, strong) NSString *icon;

@property (nonatomic, strong) NSData *imageData;

+ (void)saveBeacon:(BeaconData *)beaconData;
+ (BeaconData *)getBeaconData;

@end
