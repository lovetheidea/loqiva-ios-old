//
//  Event.m
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "Event.h"
#import "AppContext.h"
#import "loqiva-Swift.h"

@implementation Event

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    
    if(self){
        [self setEventId:[aDecoder decodeObjectForKey:@"eventId"]];
        [self setTitle:[aDecoder decodeObjectForKey:@"title"]];
        [self setSummary:[aDecoder decodeObjectForKey:@"summary"]];
        [self setMediatype:[aDecoder decodeObjectForKey:@"mediatype"]];
        [self setMediaURL:[aDecoder decodeObjectForKey:@"mediaURL"]];
        [self setSecondaryimage:[aDecoder decodeObjectForKey:@"secondaryimage"]];
        [self setUrl:[aDecoder decodeObjectForKey:@"url"]];
        [self setLatitude:[aDecoder decodeObjectForKey:@"latitude"]];
        [self setLongitude:[aDecoder decodeObjectForKey:@"longitude"]];
        [self setEventcategory:[aDecoder decodeObjectForKey:@"eventcategory"]];
        [self setAddress:[aDecoder decodeObjectForKey:@"address"]];
        [self setStartDate:[aDecoder decodeObjectForKey:@"startDate"]];
        [self setVenueurl:[aDecoder decodeObjectForKey:@"venueurl"]];
        [self setTicketsurl:[aDecoder decodeObjectForKey:@"ticketsurl"]];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_eventId forKey:@"eventId"];
    [aCoder encodeObject:_title forKey:@"title"];
    [aCoder encodeObject:_summary forKey:@"summary"];
    [aCoder encodeObject:_mediatype forKey:@"mediatype"];
    [aCoder encodeObject:_mediaURL forKey:@"mediaURL"];
    [aCoder encodeObject:_secondaryimage forKey:@"secondaryimage"];
    [aCoder encodeObject:_url forKey:@"url"];
    [aCoder encodeObject:_latitude forKey:@"latitude"];
    [aCoder encodeObject:_longitude forKey:@"longitude"];
    [aCoder encodeObject:_eventcategory forKey:@"eventcategory"];
    [aCoder encodeObject:_address forKey:@"address"];
    [aCoder encodeObject:_startDate forKey:@"startDate"];
    [aCoder encodeObject:_venueurl forKey:@"venueurl"];
    [aCoder encodeObject:_ticketsurl forKey:@"ticketsurl"];
}

+ (NSArray *)eventInDate:(NSDate *)date{
    
    NSArray *events = [[AppContext sharedInstance] events];
    
    NSLog(@"Final %@",[NSDate dateWithTimeInterval:1 sinceDate:[Event endOfDay:date]]);
    NSLog(@"Comienzo %@",[NSDate dateWithTimeInterval:1 sinceDate:[Event beginOfDay:date]]);
    
    NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"startDate >= %@ AND startDate<=%@",[NSDate dateWithTimeInterval:1 sinceDate:[Event beginOfDay:date]],[NSDate dateWithTimeInterval:1 sinceDate:[Event endOfDay:date]]];
    
    return [events filteredArrayUsingPredicate:datePredicate];
}

+ (NSDate *)beginOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
    
    [components setHour:0];
    [components setMinute:00];
    [components setSecond:00];
    
    return [cal dateFromComponents:components];
    
}

+ (NSDate *)endOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
    
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    
    return [cal dateFromComponents:components];
    
}

@end
