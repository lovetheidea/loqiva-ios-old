//
//  User.m
//  loqiva
//
//  Created by Manuel Manzanera on 6/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "User.h"
#import "loqiva-Swift.h"
#import "AppConstants.h"

@implementation User

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.firstName forKey:@"title"];
    [coder encodeObject:self.age forKey:@"age"];
    [coder encodeObject:self.firstName forKey:@"firstName"];
    [coder encodeObject:self.lastName forKey:@"lastName"];
    [coder encodeObject:self.mail forKey:@"mail"];
    [coder encodeObject:self.password forKey:@"password"];
    [coder encodeObject:self.postcode forKey:@"postcode"];
    [coder encodeObject:self.phoneNumber forKey:@"phoneNumber"];
    [coder encodeObject:self.sourceImage forKey:@"sourceImage"];
    [coder encodeObject:self.userToken forKey:@"userToken"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.age = [aDecoder decodeObjectForKey:@"age"];
        self.firstName = [aDecoder decodeObjectForKey:@"firstName"];
        self.lastName = [aDecoder decodeObjectForKey:@"lastName"];
        self.mail = [aDecoder decodeObjectForKey:@"mail"];
        self.password = [aDecoder decodeObjectForKey:@"password"];
        self.postcode = [aDecoder decodeObjectForKey:@"postcode"];
        self.phoneNumber = [aDecoder decodeObjectForKey:@"phoneNumber"];
        self.sourceImage = [aDecoder decodeObjectForKey:@"sourceImage"];
        self.userToken = [aDecoder decodeObjectForKey:@"userToken"];
    }
    return self;
}
@end
