//
//  CustomNotification.m
//  loqiva
//
//  Created by Manuel Manzanera on 16/11/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "CustomNotification.h"
#import "UserDefaultManager.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "loqiva-Swift.h"

@implementation CustomNotification

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    
    if(self){
        
        [self setTitle:[aDecoder decodeObjectForKey:@"title"]];
        [self setSummary:[aDecoder decodeObjectForKey:@"summary"]];
        [self setNotificationDate:[aDecoder decodeObjectForKey:@"notificationDate"]];
        [self setNotificationId:[aDecoder decodeObjectForKey:@"notificationId"]];
        [self setIsRead:[aDecoder decodeObjectForKey:@"isRead"]];
        [self setCustomNotificationType:[aDecoder decodeObjectForKey:@"customNotificationType"]];
        [self setUrl:[aDecoder decodeObjectForKey:@"url"]];
        
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:_title forKey:@"title"];
    [aCoder encodeObject:_summary forKey:@"summary"];
    [aCoder encodeObject:_notificationDate forKey:@"notificationDate"];
    [aCoder encodeObject:_notificationId forKey:@"notificationId"];
    [aCoder encodeObject:_isRead forKey:@"isRead"];
    [aCoder encodeObject:_customNotificationType forKey:@"customNotificationType"];
    [aCoder encodeObject:_url forKey:@"url"];
}

+ (NSMutableArray *)getCustomNotifications{
    NSData *customNotificationData = [UserDefaultManager getWithKey:@"CUSTOMNOTIFICATIONS"];
    
    NSMutableArray *customNotifications = [[NSMutableArray alloc] initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:customNotificationData]];
    
    return customNotifications;
}

+ (BOOL)saveCustomNotification:(CustomNotification *)customNotification{
    
    NSData *customNotificationData = [UserDefaultManager getWithKey:@"CUSTOMNOTIFICATIONS"];
    
    NSMutableArray *customNotifications = [[NSMutableArray alloc] initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:customNotificationData]];
    
    NSPredicate *namesBeginningWithLetterPredicate = [NSPredicate predicateWithFormat:@"title == %@",customNotification.title];
    NSMutableArray *existMessages = [[NSMutableArray alloc] initWithArray:customNotifications];
    
    [existMessages filterUsingPredicate:namesBeginningWithLetterPredicate];
    
    if(existMessages.count == 0){
        [customNotifications addObject:customNotification];
        
        NSMutableArray *notificationsArray = [[AppContext sharedInstance] notifications];
        NSInteger elementCount = notificationsArray.count;
        //set notifications object to nil if there isn't any object exist. -- wasiq
        if (elementCount == 0) {
            notificationsArray = nil;
            [[[AppContext sharedInstance] notifications] addObject:notificationsArray];
            [[[AppContext sharedInstance] notifications] addObject:customNotification];
        }
        
    }
    else{
        return FALSE;
    }
    customNotificationData = [NSKeyedArchiver archivedDataWithRootObject:(NSArray *)customNotifications];
    if(customNotificationData != NULL){
         [UserDefaultManager setWithKey:@"CUSTOMNOTIFICATIONS" WithObject:customNotificationData];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:DID_UPDATE_BEACON_NOTIFICATION object:nil];
    
    return YES;
}

+ (BOOL)deleteCustomNotification:(CustomNotification *)customNotifiaction{
    
    NSData *customNotificationData = [UserDefaultManager getWithKey:@"CUSTOMNOTIFICATIONS"];
    
    NSMutableArray *customNotifications = [[NSMutableArray alloc] initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:customNotificationData]];
    
    
    CustomNotification * auxNotificationToDelete;
    for(CustomNotification *customAuxNotification in customNotifications){
        if([customAuxNotification.title isEqualToString:customNotifiaction.title]){
            auxNotificationToDelete = customAuxNotification;
        }
    }
    
    if(auxNotificationToDelete!=nil)
      [ customNotifications removeObject:auxNotificationToDelete];
    
    
    customNotificationData = [NSKeyedArchiver archivedDataWithRootObject:(NSArray *)customNotifications];
    if(customNotificationData != NULL)
        [UserDefaultManager setWithKey:@"CUSTOMNOTIFICATIONS" WithObject:customNotificationData];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:DID_UPDATE_BEACON_NOTIFICATION object:nil];
    
    return YES;
}



@end
