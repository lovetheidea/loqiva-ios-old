//
//  Reward.h
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Reward : NSObject

@property (nonatomic, strong) NSNumber *mapitempoint;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSNumber *imageType;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, strong) NSString *company;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSDictionary *timetables;

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *summary;
@property (nonatomic, strong) NSString *interestURL;
@property (nonatomic, strong) NSString *interestPhone;
@property (nonatomic, strong) NSNumber *mediatype;
@property (nonatomic, strong) NSString *mediaurl;


+ (Reward *)getRewardWithMapItemPoint:(NSString *)mapItemPoint;
+ (Reward *)getRewardWithTitle:(NSString *)rewardTitle;

@end
