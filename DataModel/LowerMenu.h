//
//  LowerMenu.h
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LowerMenu : NSObject


@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *summary;    //description
@property (nonatomic, strong) NSNumber *MediaType;
@property (nonatomic, strong) NSString *Url;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSArray *lowerMenuItems;
@property (nonatomic, strong) NSString *mediaurl;
@property (nonatomic, strong) NSNumber *lowerMenuId;

@end
