//
//  Survey.h
//  loqiva
//
//  Created by Emu on 08/10/2017.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Survey : NSObject

@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *expires;

@end
