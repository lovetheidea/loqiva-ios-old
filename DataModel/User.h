//
//  User.h
//  loqiva
//
//  Created by Manuel Manzanera on 6/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UserDefaultManager.h"

@interface User : NSObject<NSCoding>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *age;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *mail;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *postcode;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *sourceImage;
@property (nonatomic, strong) NSString *userToken;

@end
