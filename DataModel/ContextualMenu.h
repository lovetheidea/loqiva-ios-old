//
//  ContextualMenu.h
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContextualMenu : NSObject

/** Contextual Menu **/

@property (nonatomic, strong) NSNumber *contextualMenuId;         //id
@property (nonatomic, strong) NSString *headlinetext;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *summary;            //description
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *openingtime;
@property (nonatomic, strong) NSString *closingtime;
@property (nonatomic, strong) NSString *weather;
@property (nonatomic, strong) NSNumber *temp;
@property (nonatomic, strong) NSString *icon;

@end
