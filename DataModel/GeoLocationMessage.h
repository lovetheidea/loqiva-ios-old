//
//  GeoLocationMessage.h
//  loqiva
//
//  Created by Manuel Manzanera on 23/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GeoLocationMessage : NSObject

@property (nonatomic, strong) NSNumber *alerttype;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSNumber *userlatitude;
@property (nonatomic, strong) NSNumber *userlongitude;
@property (nonatomic, strong) NSString *msgtitle;
@property (nonatomic, strong) NSString *welcomeMsg;
@property (nonatomic, strong) NSString *goodbyeMsg;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *rewardId;
@property (nonatomic, strong) NSString *url;

@end
