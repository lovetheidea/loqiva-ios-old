//
//  InterestDetail.h
//  loqiva
//
//  Created by Manuel Manzanera on 24/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InterestDetail : NSObject

@property (nonatomic, strong) NSNumber *interestId;
@property (nonatomic, strong) NSString *summary;            //description
@property (nonatomic, strong) NSString *address;           
@property (nonatomic, strong) NSString *interestPhone;
@property (nonatomic, strong) NSString *interestURL;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, strong) NSNumber *mediaType;
@property (nonatomic, strong) NSString *mediaURL;
@property (nonatomic, strong) NSString *secondaryImage;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSNumber *walkId;
@property (nonatomic, strong) NSDictionary *timetables;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *imageType;

@end
