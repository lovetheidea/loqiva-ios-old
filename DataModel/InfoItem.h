//
//  InfoItem.h
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InfoItem : NSObject

@property (nonatomic, strong) NSNumber *mapitemid;          //mapitemId
@property (nonatomic, strong) NSString *title;              //title

@property (nonatomic, strong) NSString *address;            //address
@property (nonatomic, strong) NSNumber *imagetype;          //imagetype
@property (nonatomic, strong) NSString *summary;            //description
@property (nonatomic, strong) NSString *interesturl;        //interesturl
@property (nonatomic, strong) NSString *interestphone;      //interestphone
@property (nonatomic, strong) NSString *interestmail;       //interestemail
@property (nonatomic, strong) NSNumber *mediatype;          //mediatype
@property (nonatomic, strong) NSString *name;               //name
@property (nonatomic, strong) NSString *mediaurl;           //mediaurl

@property (nonatomic, strong) NSNumber *infoid;             //infoid

@end
