//
//  Event.h
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject <NSCoding>

@property (nonatomic, strong) NSNumber *eventId;                //eventid
@property (nonatomic, strong) NSString *title;                  //title
@property (nonatomic, strong) NSString *summary;                //description
@property (nonatomic, strong) NSNumber *mediatype;              //mediatype
@property (nonatomic, strong) NSString *mediaURL;               //mediaurl
@property (nonatomic, strong) NSString *secondaryimage;         //secondaryimage
@property (nonatomic, strong) NSString *url;                    //url
@property (nonatomic, strong) NSNumber *latitude;               //latitude
@property (nonatomic, strong) NSNumber *longitude;              //longitude
@property (nonatomic, strong) NSNumber *eventcategory;          //eventcategory
@property (nonatomic, strong) NSString *address;                //address
@property (nonatomic, strong) NSDate *startDate;                //startdate
@property (nonatomic, strong) NSString *venueurl;               //venueurl
@property (nonatomic, strong) NSString *ticketsurl;             //ticketsurl

+ (NSArray *)eventInDate:(NSDate *)date;

@end
