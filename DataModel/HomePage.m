//
//  HomePage.m
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "HomePage.h"
#import "AppContext.h"
#import "ContextualMenu.h"
#import "loqiva-Swift.h"

@implementation HomePage

@synthesize contextualMenus;

+ (NSArray *)activateContextualMenus{
    
    NSMutableArray *contextualMenuActive = [[NSMutableArray alloc] init];
    
    for(ContextualMenu *contextualMenu in [[[AppContext sharedInstance] homePage] contextualMenus]){
        if(contextualMenu.contextualMenuId)
            [contextualMenuActive addObject:contextualMenu];
    }
    
    return contextualMenuActive;
}

@end
