//
//  HomePage.h
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomePage : NSObject

@property (nonatomic, strong) NSArray *contextualMenus;
@property (nonatomic, strong) NSString *lowerMenuText;
@property (nonatomic, strong) NSArray *lowerMenu;

+ (NSArray *)activateContextualMenus;

@end
