//
//  PaymentType.h
//  loqiva
//
//  Created by Manuel Manzanera on 27/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentType : NSObject

@property (nonatomic, strong) NSNumber *paymentTypeId;
@property (nonatomic, strong) NSString *paymentTypeTitle;
@property (nonatomic, strong) NSString *paymentURL;

@end
