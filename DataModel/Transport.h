//
//  Transport.h
//  loqiva
//
//  Created by Manuel Manzanera on 14/9/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Transport : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *icon;

@end
