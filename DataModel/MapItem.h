//
//  MapItem.h
//  loqiva
//
//  Created by Manuel Manzanera on 10/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapItem : NSObject

@property (nonatomic, strong) NSNumber *mapItemId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imageType;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;

@end
