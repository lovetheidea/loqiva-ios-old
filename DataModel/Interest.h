//
//  Interest.h
//  loqiva
//
//  Created by Manuel Manzanera on 9/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Interest : NSObject

@property (nonatomic, strong) NSNumber *interestId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *status;

@end
