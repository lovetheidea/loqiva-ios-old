//
//  Array+Extensions.swift
//  loqiva
//
//  Created by Matt Harding on 12/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

extension Array
{
    subscript (safe index: Int) -> Element? {
        return self.indices ~= index ? self[index] : nil
    }
}
