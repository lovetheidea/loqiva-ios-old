//
//  ResultEnum.swift
//  loqiva
//
//  Created by Matt Harding on 12/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

/*
 This enum is perfect for returning one values that can have two states. Success and Failure. In both scenarios we can attach an object. If we do not want to attach an object simply use Void as said object
 */
enum Result<T, E> {
    case success(T)
    case error(E)
}
