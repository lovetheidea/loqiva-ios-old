import Foundation

@objc class DataConnectionManager: NSObject {
    static let shared = DataConnectionManager()
    
    let reachability = Reachability()
    
    var isOffline: Bool {
        return (reachability?.connection ?? .none) == .none
    }
    
    var isConnected: Bool {
        return !isOffline
    }
    
    override init() {
        
        do {
            try reachability?.startNotifier()
        } catch {
            
        }
    }
    
    func addObserver(closure: @escaping (Bool)->Void) {
        NotificationCenter.default.addObserver(forName: Notification.Name.reachabilityChanged, object: nil, queue: nil) { [weak self] _ in
            let isConnected = self?.isConnected ?? false
            
            DispatchQueue.main.async {
                closure(isConnected)
            }
        }
    }
}
