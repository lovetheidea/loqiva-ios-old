//
//  AppDelegate+Extensions.swift
//  loqiva
//
//  Created by Matt Harding on 19/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

// MARK:- Network Activity

extension AppDelegate {
    
    func startObservingNetworkRequests() {
        self.networkRequestMonitor = NetworkRequestMonitor()
        self.networkRequestMonitor.startObserving()
    }
}
