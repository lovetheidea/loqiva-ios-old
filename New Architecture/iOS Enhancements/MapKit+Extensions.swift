//
//  MapKit+Extensions.swift
//  loqiva
//
//  Created by Matt Harding on 21/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation
import MapKit

extension MKMapView {
    
    func zoomInOnAllAnnotations() {
        showAnnotations(annotations, animated: true)
    }
}

extension MKCoordinateRegion{
    var mapRect:MKMapRect {
        get{
            let a = MKMapPoint.init(CLLocationCoordinate2DMake(
                self.center.latitude + self.span.latitudeDelta / 2,
                self.center.longitude - self.span.longitudeDelta / 2))
            
            let b = MKMapPoint.init(CLLocationCoordinate2DMake(
                self.center.latitude - self.span.latitudeDelta / 2,
                self.center.longitude + self.span.longitudeDelta / 2))
            
            return MKMapRect.init(x: min(a.x,b.x), y: min(a.y,b.y), width: abs(a.x-b.x), height: abs(a.y-b.y))
        }
    }
}

extension MKMapView {
    func setVisibleRegion(mapRegion: MKCoordinateRegion, edgePadding insets: UIEdgeInsets, animated animate: Bool) {
        self.setVisibleMapRect(mapRegion.mapRect, edgePadding: insets , animated: animate)
    }
}
