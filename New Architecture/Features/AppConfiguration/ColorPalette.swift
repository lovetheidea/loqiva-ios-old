//
//  ColorPalette.swift
//  loqiva
//
//  Created by Matt Harding on 31/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

@objc class ColorPalette: NSObject {
    static let orangetext = UtilManager.colorwithHexString("12ff00", alpha:1)
    static let navigationBar = UtilManager.colorwithHexString("000000", alpha:1)
    static let orangeContextualTint = UtilManager.colorwithHexString("000000", alpha:0.3)
    static let headerSeperator = UtilManager.colorwithHexString("ffffff", alpha:0.35)
    static let seperator = UIColor(red: 214/255, green:214/255, blue:214/255, alpha:0.8)
    
    static let greyBackground = UIColor(red:214/255, green:214/255, blue:214/255, alpha:0.3)
    static let greyPicker = UIColor(red:0.941, green:0.941, blue:0.941, alpha:1)
    static let lowerMenuBorder:UIColor = UtilManager.colorwithHexString("979797", alpha:1)
    static let greyTextHome:UIColor = UtilManager.colorwithHexString("717171", alpha:1)
    static let greyDarkTextHome:UIColor = UtilManager.colorwithHexString("474747", alpha:1)
    static let greyText:UIColor = UtilManager.colorwithHexString("787878", alpha:1)
    static let greyReportBackground = UIColor(red:0.400, green:0.400, blue:0.400, alpha:1)
    static let blackText:UIColor = UtilManager.colorwithHexString("474747", alpha:1)
    static let whiteText:UIColor = UtilManager.colorwithHexString("ffffff", alpha:1)
}
