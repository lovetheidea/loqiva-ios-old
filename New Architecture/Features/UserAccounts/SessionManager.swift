//
//  SessionManager.swift
//  loqiva
//
//  Created by Matt Harding on 20/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation
    
class SessionManager: NSObject {
    
    static let shared = SessionManager()
    
    private(set) var user: User?
    
    override init() {
        super.init()
        user = loadUserFromDisk()
        // TODO how we handle user sessions (using cookies) when we restart the app?
    }
    
    var isLoggedIn: Bool {
        return user != nil
    }
    
    func logout(_ completion: @escaping (Result<Void, POIRequestError>) -> Void) {
        
        WebServiceManager.logout(success: { [weak self] in
    
            self?.removeUserFromDisk()
            self?.user = nil
    
            completion(.success(()))
        }) { (error) in
            completion(.error(.unknown))
        }
    }
    
    func registerNewUser(_ user: User, completion: @escaping (Result<User, POIRequestError>) -> Void) {
        WebServiceManager.registerNewUser(user, success: { [weak self] user in
            self?.userDidLoginToRemoteServer(user!)
            completion(Result<User, POIRequestError>.success(user!))
        }) { (error) in
            
            let objcerror = error! as NSError
            let requestError = POIRequestError.generateFromServerErrorCode(objcerror.code)
            completion(Result<User, POIRequestError>.error(requestError))
        }
    }
    
    func login(withUserName username: String, password: String, completion: @escaping (Result<User, POIRequestError>) -> Void) {
        WebServiceManager.login(withUsername: username, andPassword: password, success: { [weak self] (user) in
            self?.userDidLoginToRemoteServer(user!)
            completion(Result<User, POIRequestError>.success(user!))
            
        }) { (error) in
            completion(Result<User, POIRequestError>.error(.unknown))
        }
    }
    
    private func userDidLoginToRemoteServer(_ user: User) {
        
        self.user = user
        storeUserToDisk(user)
    }
    
    func loginUsingStoredDetails(_ completion: @escaping (Result<User, POIRequestError>) -> Void) {
        if
            let user = self.user,
            let username = user.mail,
            let password = user.password,
            username.count > 0,
            password.count > 0 {
            login(withUserName: username, password: password, completion: completion)
        } else {
            completion(.error(.unknown))
        }
    }
    
    func updateUserFromServer(_ completion: @escaping (Result<User, POIRequestError>) -> Void) {
        
        guard
            let loggedInUser = user,
            let userToken = loggedInUser.userToken,
            let password = loggedInUser.password
            else {
            completion(.error(.unknown))
            return
        }
        
        WebServiceManager.getLoggedInUsersProfile(success: { [weak self] (userFromServer) in
            let newUser = userFromServer
            newUser?.userToken = userToken
            newUser?.password = password
            
            self?.user = newUser
            completion(.success(userFromServer!))
        }) { error in
            completion(.error(.unknown))
        }
    }

    func storeLoggedInUserToDisk() {
        guard let user = self.user else {
            return
        }
        storeUserToDisk(user)
    }
    
    private func storeUserToDisk(_ user: User) {
        
        let data = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.setValue(data, forKey: "UserData")
    }
    
    func loadUserFromDisk() -> User? {
        guard let data = UserDefaults.standard.value(forKey: "UserData") as? Data,
            let user = NSKeyedUnarchiver.unarchiveObject(with: data) as? User else {
            return nil
        }
        return user
    }
    
    private func removeUserFromDisk() {
        UserDefaults.standard.removeObject(forKey: "UserData")
    }
    
    func updateProfileImage(with image: UIImage, completion: @escaping (Result<Void, POIRequestError>) -> Void) {
        let imageData = image.jpegData(compressionQuality: 0.5)
        WebServiceManager.updateProfileImage(image, success: { [weak self] imageURL in
            if
                let strongSelf = self,
                let user = strongSelf.user {
                user.sourceImage = imageURL
                strongSelf.userDidLoginToRemoteServer(user)
            }
            completion(.success(()))
        }) { (error) in
            completion(.error(.unknown))
        }
    }
}
