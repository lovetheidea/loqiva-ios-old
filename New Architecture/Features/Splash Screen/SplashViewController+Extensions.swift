//
//  SplashViewController+Extensions.swift
//  loqiva
//
//  Created by Matt Harding on 21/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

extension SplashViewController {
    
    var currentLocation: (latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let latitude = LocationManager.sharedInstance().currentLocation?.coordinate.latitude ?? 0
        let longitude = LocationManager.sharedInstance().currentLocation?.coordinate.longitude ?? 0
        return (latitude, longitude)
    }
    
    func requestRewards() {
        
        let locationData = currentLocation
        RewardCache.shared.getRewards(nearLatitude: locationData.latitude, longitude: locationData.longitude) { _ in
            
        }
    }
    
    func requestAttractions() {
        
        let locationData = currentLocation
        AttractionCache.shared.getAttractions(nearLatitude: locationData.latitude, longitude: locationData.longitude) { _ in
            
        }
    }
    
    func requestServices() {
        
        let locationData = currentLocation
        ServiceCache.shared.getServices(nearLatitude: locationData.latitude, longitude: locationData.longitude) { _ in
            
        }
    }
    
    func requestTours() {
        
        let locationData = currentLocation
        TourCache.shared.getTours(nearLatitude: locationData.latitude, longitude: locationData.longitude) { [weak self] _ in
            
        }
    }
}

// MARK:- User Session

extension SplashViewController {

    func loginIfNeeded() {
        
        // Due to having to use cookies we must re-login each time the application launches
        if SessionManager.shared.isLoggedIn {
            SessionManager.shared.loginUsingStoredDetails { [weak self] result in
                switch result {
                case .success(_):
                    WebServiceManager.getInterests(nil)
                    self?.presentHomeViewController()
                case .error(_):
                    self?.presentSign()
                }
            }
        } else {
            presentSign()
        }
    }

}
