//
//  RewardsViewController+Extensions.swift
//  loqiva
//
//  Created by Matt Harding on 21/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

extension RewardsViewController {
    
    func refreshRewardsFromServer() {
        
        self.refreshControl?.beginRefreshing()
        
        let latitude = LocationManager.sharedInstance().currentLocation?.coordinate.latitude ?? 0
        let longitude = LocationManager.sharedInstance().currentLocation?.coordinate.longitude ?? 0
        RewardCache.shared.getRewards(nearLatitude: latitude, longitude: longitude, shouldReDownloadFromServer: true) { [weak self] _ in
            self?.refreshControl?.endRefreshing()
            self?.rewardsTableView.reloadData()
        }
    }
}
