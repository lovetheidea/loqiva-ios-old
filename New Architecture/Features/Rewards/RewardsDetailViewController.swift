//
//  RewardsDetailViewController.swift
//  loqiva
//
//  Created by Matt Harding on 17/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

extension RewardsDetailViewController {
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let identifier = String(describing: self.currentReward.mapitempoint.intValue)
        
        RewardDetailCache.shared.getValue(forIdentifier: identifier) { [weak self] result in
            switch result {
            case .success(let reward):

                self?.currentReward = reward
                self?.configureView()
                
            case .error(_): break

            }
        }
    }
}
