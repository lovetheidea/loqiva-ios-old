//
//  CachedStorageManager.swift
//  loqiva
//
//  Created by Matt Harding on 12/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

class CachedStorageManager<T, E: Error>: NSObject {
    
    private var timestamp: Date?
    private var cache: [String: Any] = [:]
    private var isLoadingFromWebservice = false
    private var completionHandlerStore: [String: [(Result<T, E>) -> Void]] = [:]
    private let remoteFetchHandler: (String, (@escaping (Result<T, E>) -> Void)) -> Void
    
    init(remoteFetchHandler: @escaping (String, (@escaping (Result<T, E>) -> Void)) -> Void) {
        self.remoteFetchHandler = remoteFetchHandler
        super.init()
        setup()
    }
    
    private func setup() {
        registerForNotifications()
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(forName: UIApplication.didReceiveMemoryWarningNotification, object: nil, queue: nil, using: didRecieveMemoryWarning)
    }
    
    private func didRecieveMemoryWarning(_ notification: Notification) {
        cache.removeAll()
    }
    
    private func checkCache(forIdentifier identifier: String) -> T? {
        return cache[identifier] as? T
    }
    
    private var isCacheOutOfDate: Bool {
        guard let dateOfCache = timestamp else {
            return false
        }
        let allowedDurationForCache: Double = 60 * 5 // 5 minutes
        return dateOfCache.timeIntervalSinceNow < -allowedDurationForCache
    }
    
    func getValue(forIdentifier identifier: String, completion: @escaping (Result<T, E>) -> Void) {
        
        if isCacheOutOfDate {
            cache.removeAll()
        }
        
        if let cachedValue = checkCache(forIdentifier: identifier) {
            completion(.success(cachedValue))
            return
        }
        
        addCompletionHandler(completion, forIdentifier: identifier)
        
        guard isLoadingFromWebservice == false else {
            return
        }
        
        let webRequestDidComplete: (Result<T, E>) -> Void = { [weak self] result in
            self?.isLoadingFromWebservice = false
            
            switch result {
            case .success (let remoteData):
                self?.cache[identifier] = remoteData
                self?.timestamp = Date()
                
                self?.triggerCompletionHandlers(forIdentifier: identifier) { completionHandler in
                    completionHandler(.success(remoteData))
                }
            case .error (let error):
                
                self?.triggerCompletionHandlers(forIdentifier: identifier) { completionHandler in
                    completionHandler(.error(error))
                }
            }
        }
        
        isLoadingFromWebservice = true
        remoteFetchHandler(identifier, webRequestDidComplete)
    }
    
    private func addCompletionHandler(_ completion: @escaping (Result<T, E>) -> Void, forIdentifier identifier: String) {
        var completionHandlers: [(Result<T, E>) -> Void] = {
            guard let completionHandlers = completionHandlerStore[identifier] else {
                return []
            }
            return completionHandlers
        }()
        
        completionHandlers.append(completion)
        completionHandlerStore[identifier] = completionHandlers
    }
    
    private func triggerCompletionHandlers(forIdentifier identifier: String, execution: ((Result<T, E>) -> Void) -> Void) {
        
        guard let completionHandlers = completionHandlerStore[identifier] else {
            return
        }
        
        completionHandlers.forEach({ completionHandler in
            execution(completionHandler)
        })
        
        completionHandlerStore.removeValue(forKey: identifier)
    }
}


