//
//  POIRequestError.swift
//  loqiva
//
//  Created by Matt Harding on 17/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

enum POIRequestError: Error {
    case unknown
    case invalidUserNameAndPassword
    case invalidUserName
    case invalidPassword
    case unauthorized
    case userNotFound
    case emailAlreadyTaken
    case invalidEmailAddress
    case passwordValidation
    case invalidUserType
    
    static func generateFromServerErrorCode(_ serverErrorCode: Int) -> POIRequestError {
        switch serverErrorCode {
        case 100: return .invalidUserNameAndPassword
        case 101: return .invalidUserName
        case 102: return .invalidPassword
        case 103: return .unauthorized
        case 104: return .userNotFound
        case 105: return .emailAlreadyTaken
        case 106: return .invalidEmailAddress
        case 107: return .passwordValidation
        case 108: return .invalidUserType
        default: return .unknown
        }
    }
    
    var localizedMessage: String {
        
        switch self {
        case .invalidUserNameAndPassword: return "Invalid username and password"
        case .invalidUserName: return "Invalid Username"
        case .invalidPassword: return "Invalid Password"
        case .unauthorized: return "User is not active. Please confirm sign up"
        case .userNotFound: return "User details not found. Please sign up."
        case .emailAlreadyTaken: return "Email has already been taken. Please try another."
        case .invalidEmailAddress: return "Invalid email address. Please check again."
        case .passwordValidation: return "The password you have entered is not valid. Passwords need to be a minimum of 6 characters. No special characters are allowed."
        case .invalidUserType: return "Sorry, This app is not for Business users. Feel free to sign up as a general user."
        default: return "Unfortunatley an error has occurred"
        }
    }
}
