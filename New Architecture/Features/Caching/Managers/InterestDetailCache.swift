//
//  InterestDetailCache.swift
//  loqiva
//
//  Created by Matt Harding on 17/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

class InterestDetailCache: NSObject {
    
    static let shared = InterestDetailCache()
    
    let cachedStorageManager: CachedStorageManager<InterestDetail, POIRequestError> = {
        
        let remoteFetchHandler: (String, @escaping ((Result<InterestDetail, POIRequestError>) -> Void)) -> Void = { identifier, completion in
            WebServiceManager.getInterestDetails(identifier, success: { interestDetail in
                completion(Result<InterestDetail, POIRequestError>.success(interestDetail!))
            }, failure: { error in
                completion(.error(.unknown))
            })
        }
        return CachedStorageManager<InterestDetail, POIRequestError>(remoteFetchHandler: remoteFetchHandler)
    }()
    
    func getValue(forIdentifier identifier: String, completion: @escaping (Result<InterestDetail, POIRequestError>) -> Void) {
        cachedStorageManager.getValue(forIdentifier: identifier, completion: completion)
    }
}
