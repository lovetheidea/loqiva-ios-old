//
//  ServiceCache.swift
//  loqiva
//
//  Created by Matt Harding on 22/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

@objc class ServiceCache: NSObject {
    
    static let shared = ServiceCache()
    
    private var cache: [String: MapItem] = [:]
    
    private var timestamp: Date?
    private var isLoadingFromWebservice = false
    private var completionHandlerStore: [String: [(Result<[MapItem], POIRequestError>) -> Void]] = [:]
    
    override init() {
        super.init()
        setup()
    }
    
    private func setup() {
        registerForNotifications()
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(forName: UIApplication.didReceiveMemoryWarningNotification, object: nil, queue: nil, using: didRecieveMemoryWarning)
    }
    
    private func didRecieveMemoryWarning(_ notification: Notification) {
        cache.removeAll()
    }
    
    var cachedMapItems: [MapItem] {
        return cache.map { return $1 }
    }
    
    private var isCacheOutOfDate: Bool {
        guard let dateOfCache = timestamp else {
            return false
        }
        let allowedDurationForCache: Double = Double(Constant.cacheTime) // 5 minutes
        return dateOfCache.timeIntervalSinceNow < -allowedDurationForCache
    }
    
    var cachedMapItemsWithNonZeroLocations: [MapItem] {
        return cachedMapItems.compactMap { mapItem in
            // TODO 0,0 is actually a valid location. But copying existing logic
            if mapItem.latitude.doubleValue != 0 && mapItem.longitude.doubleValue != 0 {
                return mapItem
            }
            return nil
        }
    }
    
    func checkCache(for identifier: String) -> MapItem? {
        return cache[identifier]
    }
    
    func getServices(nearLatitude latitude: CLLocationDegrees, longitude: CLLocationDegrees, shouldReDownloadFromServer: Bool = false, completion: @escaping (Result<[MapItem], POIRequestError>) -> Void) {
        
        if isCacheOutOfDate {
            cache.removeAll()
        }
        
        if shouldReDownloadFromServer == false {
            
            let cachedMapItems = self.cachedMapItems
            if cachedMapItems.count > 0 {
                completion(Result<[MapItem], POIRequestError>.success(cachedMapItems))
                return
            }
        }
        
        addCompletionHandler(completion, forIdentifier: "mapItems")
        
        guard isLoadingFromWebservice == false else {
            return
        }
        
        let webRequestDidComplete: (Result<[MapItem], POIRequestError>) -> Void = { [weak self] result in
            self?.isLoadingFromWebservice = false
            
            switch result {
            case .success (let remoteData):
                remoteData.forEach { mapItem in
                    let identifier = String(describing: mapItem.mapItemId)
                    self?.cache[identifier] = mapItem
                }
                self?.timestamp = Date()
                
                self?.triggerCompletionHandlers(forIdentifier: "mapItems") { completionHandler in
                    completionHandler(.success(remoteData))
                }
            case .error (let error):
                
                self?.triggerCompletionHandlers(forIdentifier: "mapItems") { completionHandler in
                    completionHandler(.error(error))
                }
            }
        }
        
        isLoadingFromWebservice = true
        WebServiceManager.getServicesWithLatitude(latitude, andLongitude: longitude, success: { mapItems in
            let newMapItems = mapItems ?? []
            webRequestDidComplete(Result<[MapItem], POIRequestError>.success(newMapItems))
        }) { (error) in
            webRequestDidComplete(Result<[MapItem], POIRequestError>.error(.unknown))
        }
    }
    
    private func addCompletionHandler(_ completion: @escaping (Result<[MapItem], POIRequestError>) -> Void, forIdentifier identifier: String) {
        
        // TODO store requests identified by gps coordinates?
        var completionHandlers: [(Result<[MapItem], POIRequestError>) -> Void] = {
            guard let completionHandlers = completionHandlerStore[identifier] else {
                return []
            }
            return completionHandlers
        }()
        
        completionHandlers.append(completion)
        completionHandlerStore[identifier] = completionHandlers
    }
    
    private func triggerCompletionHandlers(forIdentifier identifier: String, execution: ((Result<[MapItem], POIRequestError>) -> Void) -> Void) {
        
        guard let completionHandlers = completionHandlerStore[identifier] else {
            return
        }
        
        completionHandlers.forEach({ completionHandler in
            execution(completionHandler)
        })
        
        completionHandlerStore.removeValue(forKey: identifier)
    }
}
