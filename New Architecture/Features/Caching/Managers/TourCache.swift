//
//  TourCache.swift
//  loqiva
//
//  Created by Matt Harding on 22/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

@objc class TourCache: NSObject {
    
    static let shared = TourCache()
    
    private var cache: [String: Tours] = [:]
    
    private var timestamp: Date?
    private var isLoadingFromWebservice = false
    private var completionHandlerStore: [String: [(Result<[Tours], POIRequestError>) -> Void]] = [:]
    
    override init() {
        super.init()
        setup()
    }
    
    private func setup() {
        registerForNotifications()
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(forName: UIApplication.didReceiveMemoryWarningNotification, object: nil, queue: nil, using: didRecieveMemoryWarning)
    }
    
    private func didRecieveMemoryWarning(_ notification: Notification) {
        cache.removeAll()
    }
    
    var cachedTours: [Tours] {
        return cache.map { return $1 }
    }
    
    var cachedToursWithNonZeroLocations: [Tours] {
        return cachedTours.compactMap { tours in
            // TODO 0,0 is actually a valid location. But copying existing logic
            if tours.startPointLat.doubleValue != 0 && tours.startPointLong.doubleValue != 0 {
                return tours
            }
            return nil
        }
    }
    
    func checkCache(for identifier: String) -> Tours? {
        return cache[identifier]
    }
    
    private var isCacheOutOfDate: Bool {
        guard let dateOfCache = timestamp else {
            return false
        }
        let allowedDurationForCache: Double = Double(Constant.cacheTime) // 5 minutes
        return dateOfCache.timeIntervalSinceNow < -allowedDurationForCache
    }
    
    func getTours(nearLatitude latitude: CLLocationDegrees, longitude: CLLocationDegrees, shouldReDownloadFromServer: Bool = false, completion: @escaping (Result<[Tours], POIRequestError>) -> Void) {
        
        if isCacheOutOfDate {
            cache.removeAll()
        }
        
        if shouldReDownloadFromServer == false {
            
            let cachedTours = self.cachedTours
            if cachedTours.count > 0 {
                completion(Result<[Tours], POIRequestError>.success(cachedTours))
                return
            }
        }
        
        addCompletionHandler(completion, forIdentifier: "tourItems")
        
        guard isLoadingFromWebservice == false else {
            return
        }
        
        let webRequestDidComplete: (Result<[Tours], POIRequestError>) -> Void = { [weak self] result in
            self?.isLoadingFromWebservice = false
            
            switch result {
            case .success (let remoteData):
                remoteData.forEach { tours in
                    let identifier = String(describing: tours.walkId)
                    self?.cache[identifier] = tours
                }
                self?.timestamp = Date()
                
                self?.triggerCompletionHandlers(forIdentifier: "tourItems") { completionHandler in
                    completionHandler(.success(remoteData))
                }
            case .error (let error):
                
                self?.triggerCompletionHandlers(forIdentifier: "tourItems") { completionHandler in
                    completionHandler(.error(error))
                }
            }
        }
        
        isLoadingFromWebservice = true
        WebServiceManager.getToursWithLatitude(latitude, andLongitude: longitude, success: { tours in
            let newTours = tours ?? []
            webRequestDidComplete(Result<[Tours], POIRequestError>.success(newTours))
        }) { (error) in
            webRequestDidComplete(Result<[Tours], POIRequestError>.error(.unknown))
        }
    }
    
    private func addCompletionHandler(_ completion: @escaping (Result<[Tours], POIRequestError>) -> Void, forIdentifier identifier: String) {
        
        // TODO store requests identified by gps coordinates?
        var completionHandlers: [(Result<[Tours], POIRequestError>) -> Void] = {
            guard let completionHandlers = completionHandlerStore[identifier] else {
                return []
            }
            return completionHandlers
        }()
        
        completionHandlers.append(completion)
        completionHandlerStore[identifier] = completionHandlers
    }
    
    private func triggerCompletionHandlers(forIdentifier identifier: String, execution: ((Result<[Tours], POIRequestError>) -> Void) -> Void) {
        
        guard let completionHandlers = completionHandlerStore[identifier] else {
            return
        }
        
        completionHandlers.forEach({ completionHandler in
            execution(completionHandler)
        })
        
        completionHandlerStore.removeValue(forKey: identifier)
    }
}
