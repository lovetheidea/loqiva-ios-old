//
//  RewardDetailCache.swift
//  loqiva
//
//  Created by Matt Harding on 17/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

class RewardDetailCache: NSObject {
    
    static let shared = RewardDetailCache()
    
    let cachedStorageManager: CachedStorageManager<Reward, POIRequestError> = {
        
        let remoteFetchHandler: (String, @escaping ((Result<Reward, POIRequestError>) -> Void)) -> Void = { identifier, completion in
            WebServiceManager.getReward(identifier, success: { reward in
                completion(Result<Reward, POIRequestError>.success(reward!))
            }, failure: { error in
                completion(.error(.unknown))
            })
        }
        return CachedStorageManager<Reward, POIRequestError>(remoteFetchHandler: remoteFetchHandler)
    }()
    
    func getValue(forIdentifier identifier: String, completion: @escaping (Result<Reward, POIRequestError>) -> Void) {
        cachedStorageManager.getValue(forIdentifier: identifier, completion: completion)
    }
}
