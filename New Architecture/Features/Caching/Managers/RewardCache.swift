//
//  RewardCache.swift
//  loqiva
//
//  Created by Matt Harding on 20/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

        // TODO list - next steps
        // 1. change the web request method to have completion handlers
        // 2. change the web request to accept CLLocationDegrees
        // 3. Subclass this cache so we can customise it, by taking into account a lat and long, then caching each rewards by their identifiers.
        // 4. how do we decide when to use the cache and when to not? if we have data use it, otherwise do not.
        // 5. The POI Manager can decide to clear this cache if the user changes location.


@objc class RewardCache: NSObject {
    
    static let shared = RewardCache()
    
    private var cache: [String: Reward] = [:]
    
    private var timestamp: Date?
    private var isLoadingFromWebservice = false
    private var completionHandlerStore: [String: [(Result<[Reward], POIRequestError>) -> Void]] = [:]
    
    override init() {
        super.init()
        setup()
    }
    
    private func setup() {
        registerForNotifications()
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(forName: UIApplication.didReceiveMemoryWarningNotification, object: nil, queue: nil, using: didRecieveMemoryWarning)
    }
    
    private func didRecieveMemoryWarning(_ notification: Notification) {
        cache.removeAll()
    }
    
    var cachedRewards: [Reward] {
        return cache.map { return $1 }
    }
    
    private var isCacheOutOfDate: Bool {
        guard let dateOfCache = timestamp else {
            return false
        }
        let allowedDurationForCache: Double = Double(Constant.cacheTime) // 5 minutes
        return dateOfCache.timeIntervalSinceNow < -allowedDurationForCache
    }
    
    var cachedRewardsWithNonZeroLocations: [Reward] {
        return cachedRewards.compactMap { reward in
            // TODO 0,0 is actually a valid location. But copying existing logic
            if reward.latitude.doubleValue != 0 && reward.longitude.doubleValue != 0 {
                return reward
            }
            return nil
        }
    }
    
    func checkCache(for identifier: String) -> Reward? {
        return cache[identifier]
    }
    
    func getRewards(nearLatitude latitude: CLLocationDegrees, longitude: CLLocationDegrees, shouldReDownloadFromServer: Bool = false, completion: @escaping (Result<[Reward], POIRequestError>) -> Void) {
        
        if isCacheOutOfDate {
            cache.removeAll()
        }
        
        if shouldReDownloadFromServer == false {
            
            let cachedRewards = self.cachedRewards
            if cachedRewards.count > 0 {
                completion(Result<[Reward], POIRequestError>.success(cachedRewards))
                return
            }
        }
        
        addCompletionHandler(completion, forIdentifier: "rewards")
        
        guard isLoadingFromWebservice == false else {
            return
        }
        
        let webRequestDidComplete: (Result<[Reward], POIRequestError>) -> Void = { [weak self] result in
            self?.isLoadingFromWebservice = false
            
            switch result {
            case .success (let remoteData):
                remoteData.forEach { reward in
                    let identifier = String(describing: reward.mapitempoint.intValue)
                    self?.cache[identifier] = reward
                }
                self?.timestamp = Date()
                
                self?.triggerCompletionHandlers(forIdentifier: "rewards") { completionHandler in
                    completionHandler(.success(remoteData))
                }
            case .error (let error):
                
                self?.triggerCompletionHandlers(forIdentifier: "rewards") { completionHandler in
                    completionHandler(.error(error))
                }
            }
        }
        
        isLoadingFromWebservice = true
        WebServiceManager.getRewardsWithLatitude(latitude, andLongitude: longitude, success: { (rewards) in
            let newRewards = rewards ?? []
            webRequestDidComplete(Result<[Reward], POIRequestError>.success(newRewards))
        }) { (error) in
            webRequestDidComplete(Result<[Reward], POIRequestError>.error(.unknown))
        }
    }
    
    private func addCompletionHandler(_ completion: @escaping (Result<[Reward], POIRequestError>) -> Void, forIdentifier identifier: String) {
        
        // TODO store requests identified by gps coordinates?
        var completionHandlers: [(Result<[Reward], POIRequestError>) -> Void] = {
            guard let completionHandlers = completionHandlerStore[identifier] else {
                return []
            }
            return completionHandlers
        }()
        
        completionHandlers.append(completion)
        completionHandlerStore[identifier] = completionHandlers
    }
    
    private func triggerCompletionHandlers(forIdentifier identifier: String, execution: ((Result<[Reward], POIRequestError>) -> Void) -> Void) {
        
        guard let completionHandlers = completionHandlerStore[identifier] else {
            return
        }
        
        completionHandlers.forEach({ completionHandler in
            execution(completionHandler)
        })
        
        completionHandlerStore.removeValue(forKey: identifier)
    }
}

