//
//  AttractionViewController+extensions.swift
//  loqiva
//
//  Created by Matt Harding on 16/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation
import UIKit

extension AttractionViewController {
 
    func loadInterestDetails(withIdentifier identifier: String) {
        InterestDetailCache.shared.getValue(forIdentifier: identifier) { [weak self] result in
            switch result {
            case .success(let interestDetail):
                
                self?.currentInterestDetail = interestDetail
                self?.configureView()
                self?.addAnnotations()
                self?.internalZoomToFitMapAnnotations()
                
            case .error(_):
                self?.configureView()
                self?.addAnnotations()
                self?.internalZoomToFitMapAnnotations()
            }
        }
    }
    
    private func internalZoomToFitMapAnnotations() {
        super.zoomToFitMapAnnotations()
    }
}
