//
//  MapViewController+extension.swift
//  loqiva
//
//  Created by Matt Harding on 11/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation
import UIKit
import MapKit

// MARK:- Callout Views

extension MapViewController: RewardCalloutDetailViewDelegate {
    
    private func displaySignUpScreenViewController() {
        
        let signUpViewController: TemporaryUserViewController = TemporaryUserViewController(dismiss: { [weak self] in
            self?.tempUserViewController.view.removeFromSuperview()
        }) { [weak self] in
            self?.tempUserViewController.view.removeFromSuperview()
            self?.presentSignForFreeController()
        }
        self.tempUserViewController = signUpViewController
        view.addSubview(signUpViewController.view)
    }
    
    func userDidSelect(_ calloutDetailView: RewardCalloutDetailView) {
        
        guard SessionManager.shared.isLoggedIn else {
            displaySignUpScreenViewController()
            return
        }
        
        guard let annotation = calloutDetailView.annotation else {
            return
        }
        
        switch annotation.type {
        case "Attraction":
            if currentEvent == nil {
                if let viewController = AttractionViewController(currentInterestDetail: currentInterestDetail, andInterestDetailID: nil) {
                    navigationController?.pushViewController(viewController, animated: true)
                }
            }
            
        case "Reward":
            let viewController = RewardsDetailViewController()
            viewController.currentReward = Reward.getWithMapItemPoint(annotation.interestId)
            navigationController?.pushViewController(viewController, animated: true)
            
        case "Service":
            let viewController = ServiceDetailViewController()
            viewController.currentInterestDetail = currentInterestDetail
            navigationController?.pushViewController(viewController, animated: true)
            
        case "Tours":
            if let delegate = self as? WebServiceManagerDelegate {
                WebServiceManager.getToursDetails(annotation.interestId, andDelegate: delegate)
            }
            
        case "Tours Information":
            let viewController = ToursDetailViewController()
            viewController.currentInterestDetail = currentInterestDetail
            navigationController?.pushViewController(viewController, animated: true)
            
        default: break
            
        }
    }
}

//MARK:- Annotations

extension MapViewController {

    fileprivate func calculateimageURL(from urlSuffix: String) -> URL? {
        let baseURLString = UIColor.imagePath() ?? ""
        let urlString = baseURLString + urlSuffix
        return URL(string: urlString)
    }
    
    @objc func createRewardCalloutDetailView(from annotation: CustomAnnotation) -> RewardCalloutDetailView? {
        
        guard let calloutDetailView = RewardCalloutDetailView.loadFromNib() else {
            return nil
        }
        
        calloutDetailView.delegate = self
        calloutDetailView.annotation = annotation
        let imageURL = calculateimageURL(from: annotation.image)
        var summary = ""
        if annotation.summary != nil {
            summary = annotation.summary
        }
        
        let configuration = MapCalloutConfiguration(title: annotation.subtitle, subtitle: summary, imageURL: imageURL)
        calloutDetailView.update(from: configuration)
        let widthConstraint = NSLayoutConstraint(item: calloutDetailView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 240)
        calloutDetailView.addConstraint(widthConstraint)
        
        let heightConstraint = NSLayoutConstraint(item: calloutDetailView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 270)
        calloutDetailView.addConstraint(heightConstraint)
        
        return calloutDetailView
    }
    
    func clearSelectedAnnotationData() {
        
        self.interestDetail = nil
        self.currentReward = nil
        self.currentInterestDetail = nil
        self.currentEvent = nil
        self.selectedAnnotationView = nil
    }
    
    func updateImageForSelectedAnnotationView(url: URL, interestDetail: InterestDetail) {
        guard
            let selectedAnnotationView = self.selectedAnnotationView,
            let rewardDetailView = selectedAnnotationView.detailCalloutAccessoryView as? RewardCalloutDetailView else {
                return
        }
        
        rewardDetailView.subtitleLabel?.text = interestDetail.summary
        rewardDetailView.imageView?.setImageWith(url)
    }
    
    func updateImageForSelectedAnnotationView(url: URL, reward: Reward) {
        
        guard
            let selectedAnnotationView = self.selectedAnnotationView,
            let rewardDetailView = selectedAnnotationView.detailCalloutAccessoryView as? RewardCalloutDetailView else {
                return
        }
        
        rewardDetailView.subtitleLabel?.text = reward.summary
        rewardDetailView.imageView?.setImageWith(url)
    }
    
    func mapViewDidSelectAnnotationView(annotationView: MKAnnotationView) {
        print("Annotation selected")
        self.selectedAnnotationView = annotationView
    
        guard let customAnnotation = annotationView.annotation as? CustomAnnotation else {
            return
        }
        
        if customAnnotation.type == "Reward" {
            loadRewardDetails(withIdentifier: customAnnotation.interestId)
        } else {
            loadInterestDetails(withIdentifier: customAnnotation.interestId)
        }
    }

}

// MARK:- Map Loading

extension MapViewController {
    
    func loadInterestDetails(withIdentifier identifier: String) {
        InterestDetailCache.shared.getValue(forIdentifier: identifier) { [weak self] result in
            switch result {
            case .success(let interestDetail):
                
                guard let strongSelf = self else {
                    return
                }
                strongSelf.currentInterestDetail = interestDetail
                
                if let imageURL = strongSelf.calculateimageURL(from: interestDetail.mediaURL) {
                    strongSelf.updateImageForSelectedAnnotationView(url: imageURL, interestDetail: interestDetail)
                }
                
            case .error(_): break
               
            }
        }
    }
    
    var currentLocation: (latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let latitude = LocationManager.sharedInstance().currentLocation?.coordinate.latitude ?? 0
        let longitude = LocationManager.sharedInstance().currentLocation?.coordinate.longitude ?? 0
        return (latitude, longitude)
    }
    
    func requestRewards() {
        
        let locationData = currentLocation
        RewardCache.shared.getRewards(nearLatitude: locationData.latitude, longitude: locationData.longitude) { [weak self] _ in
            
            self?.configureView()
        }
    }
    
    func requestAttractions() {
        
        let locationData = currentLocation
        AttractionCache.shared.getAttractions(nearLatitude: locationData.latitude, longitude: locationData.longitude) { [weak self] _ in
            
            self?.configureView()
        }
    }
    
    func requestServices() {
        
        let locationData = currentLocation
        ServiceCache.shared.getServices(nearLatitude: locationData.latitude, longitude: locationData.longitude) { [weak self] _ in
            
            self?.configureView()
        }
    }
    
    func requestTours() {
        
        let locationData = currentLocation
        TourCache.shared.getTours(nearLatitude: locationData.latitude, longitude: locationData.longitude) { [weak self] _ in
            
            self?.configureView()
        }
    }
    
    func loadRewardDetails(withIdentifier identifier: String) {
        RewardDetailCache.shared.getValue(forIdentifier: identifier) { [weak self] result in
            switch result {
            case .success(let reward):

                guard let strongSelf = self else {
                    return
                }
                strongSelf.currentReward = reward

                if let imageURL = strongSelf.calculateimageURL(from: reward.mediaurl) {
                    strongSelf.updateImageForSelectedAnnotationView(url: imageURL, reward: reward)
                }

            case .error(_): break

            }
        }
    }
    
    func generateAnnotationsForRewards() -> [CustomAnnotation] {
        
        let rewards = RewardCache.shared.cachedRewardsWithNonZeroLocations
        return rewards.map { reward in
            let coordinate = CLLocationCoordinate2D(latitude: reward.latitude.doubleValue, longitude: reward.longitude.doubleValue)
            let annotation = CustomAnnotation(coordinate: coordinate, title: reward.title, summary: reward.summary, subtitle: "", pin: "image-pin-reward")
            annotation?.accessibilityLabel = reward.title
            annotation?.image = reward.mediaurl
            annotation?.type = "Reward"
            annotation?.interestId = "\(reward.mapitempoint.intValue)"
            annotation?.data = reward
            return annotation!
        }
    }
    
    func addMapAnnotationsForRewards() {
        self.mapAnnotations.addObjects(from: generateAnnotationsForRewards())
    }
}

// MARK:- Menu Animations

extension MapViewController {

    @objc func toggleMenuDrawer() {
        
        if mapRoundView.isCollapsed {
            openMenuDrawer()
        } else {
            closeMenuDrawer(true)
        }
    }
    
    func openMenuDrawer() {
        let mapRoundView = self.mapRoundView
        mapRoundView?.isCollapsed = false
        UIView.animate(withDuration: 0.17, delay: 0, options: .curveEaseOut, animations: {
            mapRoundView?.transform = CGAffineTransform.identity
        }) { finished in
            if finished {
                mapRoundView?.revealButtons()
            }
        }
    }
    
    func closeMenuDrawer(_ animated: Bool) {
        mapRoundView?.isCollapsed = true
        let animations: () -> Void = {
            let scale: CGFloat = 0.25
            self.mapRoundView.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
        
        let onCompletion: (Bool) -> Void = { [weak self] finished in
            if finished {
                self?.mapRoundView.hideButtons()
            }
        }
        
        if animated {
            UIView.animate(withDuration: 0.12, delay: 0, options: .curveLinear, animations: {
                animations()
            }) { finished in
                onCompletion(finished)
            }
        } else {
            animations()
            onCompletion(true)
        }
    }
    
    func setupMenuDrawer() {
        containerMapView.backgroundColor = UIColor.white
//        addBlurView(to: containerMapView)
        let layer = containerMapView.layer
        layer.shadowColor =  ColorPalette.greyTextHome.cgColor
        layer.shadowOffset = CGSize(width:0,height:0)
        layer.shadowRadius = 2
        layer.shadowOpacity = 0.8
        layer.masksToBounds = false
        closeMenuDrawer(false)
    }
    
//    private func addBlurView(to view: UIView) {
//        let visualAffect = UIBlurEffect(style: .extraLight)
//        let blurView = UIVisualEffectView(effect: visualAffect)
//        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        blurView.frame = view.bounds
//        view.addSubview(blurView)
//    }
}
