//
//  MapRoundView+Extensions.swift
//  loqiva
//
//  Created by Matt Harding on 31/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

extension MapRoundView {
    
    func revealButtons() {
        let scale: CGFloat = 1
        let animated = true
        let duration: TimeInterval = 0.1
        let delayIncrement: TimeInterval = 0.04
        setScaleOnView(rewardsButton, scale: scale, animated: animated, duration: duration, delay: delayIncrement * 0)
        setScaleOnView(infoButton, scale: scale, animated: animated, duration: duration, delay: delayIncrement * 1)
        setScaleOnView(tourButton, scale: scale, animated: animated, duration: duration, delay: delayIncrement * 2)
        setScaleOnView(friendButton, scale: scale, animated: animated, duration: duration, delay: delayIncrement * 3)
    }
    
    func hideButtons() {
        let scale: CGFloat = 0
        let animated = false
        let duration: TimeInterval = 0
        let delayIncrement: TimeInterval = 0
        setScaleOnView(rewardsButton, scale: scale, animated: animated, duration: duration, delay: delayIncrement * 0)
        setScaleOnView(infoButton, scale: scale, animated: animated, duration: duration, delay: delayIncrement * 1)
        setScaleOnView(tourButton, scale: scale, animated: animated, duration: duration, delay: delayIncrement * 2)
        setScaleOnView(friendButton, scale: scale, animated: animated, duration: duration, delay: delayIncrement * 3)
    }
    
    func setScaleOnView(_ view: UIView, scale: CGFloat, animated: Bool, duration: TimeInterval, delay: TimeInterval) {
        
        let animations: () -> Void = {
            view.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
        
        if animated {
            UIView.animate(withDuration: duration, delay: delay, options: .curveLinear, animations: {
                animations()
            }) { finished in
                
            }
        } else {
            animations()
        }
    }

    
}
