//
//  MapCalloutConfiguration.swift
//  loqiva
//
//  Created by Matt Harding on 11/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

@objc class MapCalloutConfiguration: NSObject {
    
    let title: String
    let subtitle: String
    let imageURL: URL?
    
    init(title: String, subtitle: String, imageURL: URL?) {
        self.title = title
        self.subtitle = subtitle
        self.imageURL = imageURL
    }
}

