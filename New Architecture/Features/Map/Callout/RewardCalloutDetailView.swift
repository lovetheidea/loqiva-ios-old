//
//  RewardCalloutDetailView.swift
//  loqiva
//
//  Created by Matt Harding on 11/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation
import UIKit

@objc protocol RewardCalloutDetailViewDelegate: class {
    func userDidSelect(_ calloutDetailView: RewardCalloutDetailView)
}

@objc class RewardCalloutDetailView: UIView {
    
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var subtitleLabel: UILabel?
    @IBOutlet var imageView: UIImageView?
    
    weak var annotation: CustomAnnotation?
    weak var delegate: RewardCalloutDetailViewDelegate?
    
    static func loadFromNib() -> RewardCalloutDetailView? {
        return Bundle.main.loadNibNamed("RewardCalloutDetailView", owner: nil, options: [:])?.first as? RewardCalloutDetailView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        imageView?.layer.masksToBounds = true
    }
    
    func update(from configuration: MapCalloutConfiguration) {
        titleLabel?.text = configuration.title
        if let url = configuration.imageURL {
            imageView?.setImageWith(url)
        }
    }
    
    @IBAction private func tapGestureUpdate(tapGesture: UITapGestureRecognizer) {
        delegate?.userDidSelect(self)
    }
}
