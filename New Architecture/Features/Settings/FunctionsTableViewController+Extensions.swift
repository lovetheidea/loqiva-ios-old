//
//  FunctionsTableViewController+Extensions.swift
//  loqiva
//
//  Created by Matt Harding on 25/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation
import UIKit

extension FunctionsTableViewController {
    
    @objc func logout() {
        
        if(SessionManager.shared.isLoggedIn) {
            SessionManager.shared.logout { [weak self] result in
                switch result {
                case .success:
                    self?.backToStartApplication()
                case .error(_):
                    UtilManager.presentAlert(withMessage: "Sorry, we were unable to log you out due to a request failure. Please try again")
                }
            }
        } else {
            backToStartApplication()
        }
    }
}
