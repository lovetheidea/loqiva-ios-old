//
//  WebserviceManager+Extensions.swift
//  loqiva
//
//  Created by Matt Harding on 22/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

extension WebServiceManager {
    
    static func appendQueryStringParams(for urlPath: String, latitude: CLLocationDegrees, longitude: CLLocationDegrees) -> String {
        return "\(urlPath)/\(latitude)/\(longitude)"
    }
    
    @objc static func generateURLPath(forEndPoint endPoint: String, latitude: CLLocationDegrees, longitude: CLLocationDegrees) -> String {
        let baseURL = WebServiceManager.urlPath()
        return baseURL! + "/\(endPoint)/\(latitude)/\(longitude)"
    }
}
