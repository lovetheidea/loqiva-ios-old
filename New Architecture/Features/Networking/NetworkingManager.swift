//
//  NetworkingManager.swift
//  loqiva
//
//  Created by Matt Harding on 19/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

@objc enum HTTPMethod: Int {
    case GET
    case POST
    case PUT
    case DELETE
}

@objc class NetworkingManager: NSObject {

    static let networkRequestStartedNotificationName = Notification.Name("LoqivaNetworkingRequestStarted")
    static let networkRequestCompletedNotificationName = Notification.Name("LoqivaNetworkingRequestCompleted")
    
    static let shared = NetworkingManager()
    
    private func generateSessionManager(withSerialisers: Bool = true) -> AFHTTPSessionManager {
        let manager = AFHTTPSessionManager()
        if withSerialisers {
            manager.requestSerializer = AFHTTPRequestSerializer()
            manager.responseSerializer = AFHTTPResponseSerializer()
        }
        return manager
    }

    @objc func performRequest(_ httpMethod: HTTPMethod, urlString: String, parameters: Any?, success: @escaping (URLSessionDataTask, Any?) -> Void, failure: @escaping (URLSessionDataTask?, Error) -> Void) {
        
        let sessionManager = generateSessionManager()
        
        postStartingRequestNotification()
        let localSuccessHandler: (URLSessionDataTask, Any?) -> Void = { (sessionDataTask, response) in
            self.postCompletedRequestNotification()
            success(sessionDataTask, response)
        }
        
        let localFailureHandler: (URLSessionDataTask?, Error) -> Void = { (sessionDataTask, error) in
            self.postCompletedRequestNotification()
            failure(sessionDataTask, error)
        }
        
        switch httpMethod {
        case .DELETE:
            sessionManager.delete(urlString, parameters: parameters, success: localSuccessHandler, failure: localFailureHandler)
            case .POST:
                sessionManager.post(urlString, parameters: parameters, success: localSuccessHandler, failure: localFailureHandler)
            case .PUT:
                sessionManager.put(urlString, parameters: parameters, success: localSuccessHandler, failure: localFailureHandler)
            case .GET:
                sessionManager.get(urlString, parameters: parameters, success: localSuccessHandler, failure: localFailureHandler)
        }
        
    }
    
    @objc func performMulitPartRequest(forUrlString urlString: String, parameters: Any?, propertyKey: String, image: UIImage, mimeType: String, success: @escaping (URLSessionDataTask, Any?) -> Void, failure: @escaping (URLSessionDataTask?, Error) -> Void) {
        
        let sessionManager = generateSessionManager(withSerialisers: true)

        postStartingRequestNotification()
        let localSuccessHandler: (URLSessionDataTask, Any?) -> Void = { (sessionDataTask, response) in
            self.postCompletedRequestNotification()
            success(sessionDataTask, response)
        }

        let localFailureHandler: (URLSessionDataTask?, Error) -> Void = { (sessionDataTask, error) in
            self.postCompletedRequestNotification()
            failure(sessionDataTask, error)
        }

        sessionManager.requestSerializer.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")

        let urlImage :URL = saveImage(image: image)!

        sessionManager.post(urlString, parameters: parameters, constructingBodyWith: { (multipartFormData) in
            try! multipartFormData.appendPart(withFileURL: urlImage, name: propertyKey, fileName: "temp.jpeg", mimeType: mimeType)
        }, progress: nil, success: localSuccessHandler, failure: localFailureHandler)

    }
    
    func saveImage(image: UIImage) -> URL? {
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            return nil
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return nil
        }
        do {
            let url :URL = directory.appendingPathComponent("fileName.png")!
            try data.write(to: url)
            return url
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    private func postStartingRequestNotification() {
        NotificationCenter.default.post(name: NetworkingManager.networkRequestStartedNotificationName, object: self)
    }
    
    private func postCompletedRequestNotification() {
        NotificationCenter.default.post(name: NetworkingManager.networkRequestCompletedNotificationName, object: self)
    }
}
