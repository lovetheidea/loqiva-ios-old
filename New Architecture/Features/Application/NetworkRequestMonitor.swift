//
//  NetworkRequestMonitor.swift
//  loqiva
//
//  Created by Matt Harding on 19/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

@objc class NetworkRequestMonitor: NSObject {
    
    private var numberOfRequestsInFlight: Int = 0 { didSet {
            UIApplication.shared.isNetworkActivityIndicatorVisible = numberOfRequestsInFlight > 0
        }
    }
    
    func startObserving() {
        NotificationCenter.default.addObserver(forName: NetworkingManager.networkRequestStartedNotificationName, object: nil, queue: nil) { [weak self] _ in
            self?.numberOfRequestsInFlight += 1
        }
        
        NotificationCenter.default.addObserver(forName: NetworkingManager.networkRequestCompletedNotificationName, object: nil, queue: nil) { [weak self] _ in
            self?.numberOfRequestsInFlight -= 1
        }
    }
    
}
