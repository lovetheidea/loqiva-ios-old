//
//  LocationManager+Extensions.swift
//  loqiva
//
//  Created by Matt Harding on 21/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

extension LocationManager {
    
    func requestRewards(withLatitude latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        RewardCache.shared.getRewards(nearLatitude: latitude, longitude: longitude) { _ in
            
        }
    }
    
    func requestAttractions(withLatitude latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        AttractionCache.shared.getAttractions(nearLatitude: latitude, longitude: longitude) { [weak self] _ in
            
        }
    }
    
    func requestServices(withLatitude latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        ServiceCache.shared.getServices(nearLatitude: latitude, longitude: longitude) { [weak self] _ in
            
        }
    }
    
    func requestTours(withLatitude latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        TourCache.shared.getTours(nearLatitude: latitude, longitude: longitude) { [weak self] _ in
            
        }
    }
}
