//
//  RegisterUserViewController+Extensions.swift
//  loqiva
//
//  Created by Matt Harding on 26/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation
import UIKit

extension RegisterUserViewController {

    @objc func registerNewUser(_ user: User, profileImage: UIImage?) {
        
        saveButton.isEnabled = false
        SessionManager.shared.registerNewUser(user) { [weak self] (result) in
            switch result {
            case .success(_):
                WebServiceManager.postDevToken(nil)
                if let profileImage = profileImage {
                    SessionManager.shared.updateProfileImage(with: profileImage, completion: { (_) in
                    })
                }
                
                let interestsViewController = InterestsViewController()
                self?.navigationController?.pushViewController(interestsViewController, animated: true)
                
            case .error(let error):
                let message = error.localizedMessage
                    UtilManager.presentAlert(withMessage: message)
            }
            self?.saveButton.isEnabled = true
        }
    }
}
