//
//  SignUpViewController+Extensions.swift
//  loqiva
//
//  Created by Matt Harding on 25/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation

extension SignUpViewController {
  
    @objc func login(withUsername username: String, password: String) {
        SessionManager.shared.login(withUserName: username, password: password) { [weak self] result in
            switch result {
            case .success(_):
                
                // TODO what is this dev token?
                WebServiceManager.postDevToken(nil)
                WebServiceManager .getInterests(nil)
                
                self?.presentHomeViewController()
                
            case .error(_):
                let errorMessage = NSLocalizedString("An error has occurred, please enter your details again", comment: "")
                UtilManager.presentAlert(withMessage: errorMessage)
            }
        }
    }
}
