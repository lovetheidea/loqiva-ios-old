//
//  ProfileViewController+Extensions.swift
//  loqiva
//
//  Created by Matt Harding on 25/07/2018.
//  Copyright © 2018 inup. All rights reserved.
//

import Foundation
import UIKit

extension ProfileViewController {
    
    func updateProfile() {
        refreshControl.beginRefreshing()
        
        SessionManager.shared.updateUserFromServer { [weak self] (result) in
            
            switch result {
            case .success(_):
                self?.interestView = nil
            case .error(_):
                break
            }
            self?.refreshControl.endRefreshing()
            self?.profileTableView.reloadData()
        }
    }
    
    func logout() {
        SessionManager.shared.logout { [weak self] result in
            switch result {
            case .success:
                self?.backToStartApplication()
            case .error(_):
                UtilManager.presentAlert(withMessage: "Sorry, we were unable to log you out due to a request failure. Please try again")
            }
        }
    }
    
    func uploadProfilePicture(_ image: UIImage) {
        SessionManager.shared.updateProfileImage(with: image) { [weak self] (result) in
            
            self?.refreshControl.endRefreshing()
            switch result {
            case .success(): break
            case .error(_): break
            }
        }
    }
}
