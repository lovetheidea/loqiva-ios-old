//
//  Utility.h
//  loqiva
//
//  Created by Captive Minds on 20/04/2017.
//  Copyright © 2017 inup. All rights reserved.
//

//#ifndef Utility_h
//#define Utility_h
//
//
//#endif /* Utility_h */
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@interface Utility: NSObject
+(NSString *)convertHtmlToPlainText:(NSString *)HTMLString;

@end
