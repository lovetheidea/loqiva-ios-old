//
//  Utility.m
//  loqiva
//
//  Created by Captive Minds on 20/04/2017.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Utility.h"
#import "loqiva-Swift.h"

@implementation Utility


+(NSString *)convertHtmlToPlainText:(NSString *)HTMLString
{
    
    
//    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[HTMLString dataUsingEncoding:NSUnicodeStringEncoding]
//                                                                    options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
//                                                         documentAttributes:nil error:nil];
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[HTMLString dataUsingEncoding:NSUTF8StringEncoding]
                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                               NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                          documentAttributes:nil error:nil];
    
    NSString *plainString = attrStr.string;
    
    return plainString;
}

@end
