//
//  UITextView+Extra.m
//  loqiva
//
//  Created by Captive Minds on 25/04/2017.
//  Copyright © 2017 inup. All rights reserved.
//
#import "UITextView+Extra.h"
#import "NSString+HTML.h"
#import "AppConstants.h"
#import "loqiva-Swift.h"


@implementation UITextView (HTML)


-(void) setAttributedTextForTextView:(NSString*)text withLineHeight:(float)lineHeight andTextAlignment:(NSTextAlignment)alignment andColor:(UIColor*)color andKern:(float)kern andFont:(UIFont*)font {
    
    if(text.length > 0){
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setMinimumLineHeight:lineHeight];
        [style setMaximumLineHeight:lineHeight];
        [style setAlignment:alignment];
        
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:text];
        if([attrStr respondsToSelector:@selector(addAttribute:value:range:)]){
            NSRange range = NSMakeRange(0, [attrStr length]);
            [attrStr addAttribute:NSFontAttributeName
                            value:font
                            range:range];
            [attrStr addAttribute:NSForegroundColorAttributeName value:color range:range];
            [attrStr addAttribute:NSKernAttributeName value:[NSNumber numberWithFloat:kern] range:range];
            [attrStr addAttribute:NSParagraphStyleAttributeName value:style range:range];
        }
        
        [self setAttributedText:attrStr];
    }
}


@end

