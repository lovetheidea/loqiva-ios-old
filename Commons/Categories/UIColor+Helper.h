//
//  UIColor+Helper.h
//  loqiva
//
//  Created by Emu on 16/08/2017.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Helper)

+ (NSString *)imagePath;

+ (UIColor *)homeContextualFirstItemBGColor;
+ (UIColor *)homeContextualSecondItemBGColor;
+ (UIColor *)homeContextualThirdItemBGColor;

+ (UIColor *)orangeButtonColor;
+ (UIColor *)orangeAppColor;
+ (UIColor *)footerColor;

+ (UIColor *)headerBackgroundColor;
+ (UIColor *)navigationSeparatorColor;
+ (UIColor *)DistanceLabelTextColor;
+ (UIColor *)navigationBackgroundColor;
+ (UIColor *)footerTextAcentcolour;
+ (UIColor *)redeemRewardViewBGColor;
+ (UIColor *)InterestTagBGColour;
+ (UIColor *)CompanyNameTextColour;
+ (UIColor *)HeaderTextColour;
+ (UIColor *)FeedTextColour;
+ (UIColor *)InterestTagNameSelected;
+ (UIColor *)EventMessageNoEventsColour;
+ (UIColor *)EventDateSelectorBGColour;
+ (UIColor *)ContextualNavCircleBGColour;
@end
