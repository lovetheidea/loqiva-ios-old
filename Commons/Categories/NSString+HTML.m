//
//  NSString+HTML.m
//  ipswich staff
//
//  Created by marco attanasio on 03/09/2014.
//  Copyright (c) 2014 Captive Health. All rights reserved.
//

#import "NSString+HTML.h"
#import "loqiva-Swift.h"

@implementation NSString (HTML)

- (NSString *)distanceString {
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    BOOL usesMiles = infoPlistDict[@"AppIsInMiles"];
    if (usesMiles) {
        return [NSString stringWithFormat:@"%@ %@", self, NSLocalizedString(@"Miles", nil)];
    }

    CGFloat floatValue = [self floatValue] * 1.609344;
    return [NSString stringWithFormat:@"%.1f %@", floatValue, NSLocalizedString(@"Km", nil)];
}

- (NSMutableArray*)stringsBetweenString:(NSString*)start andString:(NSString*)end
{
    
    NSMutableArray* strings = [NSMutableArray arrayWithCapacity:0];
    
    NSRange startRange = [self rangeOfString:start];
    
    for( ;; )
    {
        
        if (startRange.location != NSNotFound)
        {
            
            NSRange targetRange;
            
            targetRange.location = startRange.location + startRange.length;
            targetRange.length = [self length] - targetRange.location;
            
            NSRange endRange = [self rangeOfString:end options:0 range:targetRange];
            
            if (endRange.location != NSNotFound)
            {
                
                targetRange.length = endRange.location - targetRange.location;
                [strings addObject:[self substringWithRange:targetRange]];
                
                NSRange restOfString;
                
                restOfString.location = endRange.location + endRange.length;
                restOfString.length = [self length] - restOfString.location;
                
                startRange = [self rangeOfString:start options:0 range:restOfString];
                
            }
            else
            {
                break;
            }
            
        }
        else
        {
            break;
        }
        
    }
    
    return strings;
    
}

- (NSString *)stringByStrippingHTML
{
    NSRange r;
    
    NSString *s = [self copy];
    
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    
    return s;
}

@end
