//
//  NSDate+Extra.m
//  towerhamlets
//
//  Created by marco attanasio on 28/10/2014.
//  Copyright (c) 2014 Captive Minds. All rights reserved.
//

#import "NSDate+Extra.h"
#import "loqiva-Swift.h"

@implementation NSDate (Extra)

-(NSString*) daySuffix{
    NSInteger day = [[[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian] components:NSCalendarUnitDay fromDate:self] day];
    if (day >= 11 && day <= 13) {
        return @"th";
    } else if (day % 10 == 1) {
        return @"st";
    } else if (day % 10 == 2) {
        return @"nd";
    } else if (day % 10 == 3) {
        return @"rd";
    } else {
        return @"th";
    }
}

- (NSString *)daySuffixForDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger dayOfMonth = [calendar component:NSCalendarUnitDay fromDate:date];
    switch (dayOfMonth) {
        case 1:
        case 21:
        case 31: return @"st";
        case 2:
        case 22: return @"nd";
        case 3:
        case 23: return @"rd";
        default: return @"th";
    }
}

- (NSString *)currentDay
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday|NSCalendarUnitDay fromDate:self];
    int weekday = (int)[comps weekday];
    int day = (int)[comps day];
    
    NSMutableString *dateString = [[NSMutableString alloc] init];
    
    switch (weekday)
    {
        case 0:
            [dateString appendString:NSLocalizedString(@"Saturday", nil)];
            break;
        case 1:
            [dateString appendString:NSLocalizedString(@"Sunday", nil)];
            break;
        case 2:
            [dateString appendString:NSLocalizedString(@"Monday", nil)];
            break;
        case 3:
            [dateString appendString:NSLocalizedString(@"Tuesday", nil)];
            break;
        case 4:
            [dateString appendString:NSLocalizedString(@"Wednesday", nil)];
            break;
        case 5:
            [dateString appendString:NSLocalizedString(@"Thursday", nil)];
            break;
        case 6:
            [dateString appendString:NSLocalizedString(@"Friday", nil)];
            break;
        case 7:
            [dateString appendString:NSLocalizedString(@"Saturday", nil)];
            break;
        default:
            return @"";
            break;
    }
    
    [dateString appendString:[NSString stringWithFormat:@" %d",day]];
    
    return dateString;
}

- (NSString *)currentMonth
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitMonth fromDate:self];
    
    int month = (int)[comps month];
    
    NSMutableString *dateString = [[NSMutableString alloc] init];
    
    switch (month)
    {
        case 1:
            [dateString appendString:NSLocalizedString(@"January", nil)];
            break;
        case 2:
            [dateString appendString:NSLocalizedString(@"February", nil)];
            break;
        case 3:
            [dateString appendString:NSLocalizedString(@"March", nil)];
            break;
        case 4:
            [dateString appendString:NSLocalizedString(@"April", nil)];
            break;
        case 5:
            [dateString appendString:NSLocalizedString(@"May", nil)];
            break;
        case 6:
            [dateString appendString:NSLocalizedString(@"June", nil)];
            break;
        case 7:
            [dateString appendString:NSLocalizedString(@"July", nil)];
            break;
        case 8:
            [dateString appendString:NSLocalizedString(@"August", nil)];
            break;
        case 9:
            [dateString appendString:NSLocalizedString(@"September", nil)];
            break;
        case 10:
            [dateString appendString:NSLocalizedString(@"October", nil)];
            break;
        case 11:
            [dateString appendString:NSLocalizedString(@"November", nil)];
            break;
        case 12:
            [dateString appendString:NSLocalizedString(@"December", nil)];
            break;
        default:
            return @"";
            break;
    }
    
    return dateString;
}

@end
