//
//  UIImage+Crop.h
//  loqiva
//
//  Created by juan on 30/11/2016.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Crop)
- (UIImage *)crop:(CGRect)rect;
@end
