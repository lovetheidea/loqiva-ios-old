//
//  UILabel+HTML.h
//  ipswich staff
//
//  Created by marco attanasio on 03/09/2014.
//  Copyright (c) 2014 Captive Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (HTML)


-(void)setLineHeight:(CGFloat)lineHeight;
-(void) htmlWithBoldString:(NSString*)t lineHeight:(float)l standardFont:(UIFont*)sf boldFont:(UIFont*)bf color:(UIColor*)c;

-(void) setAttributedText:(NSString*)text withLineHeight:(float)lineHeight andTextAlignment:(NSTextAlignment)alignment andFont:(UIFont*)font andColor:(UIColor*)color andKern:(float)kern withLineBreak:(BOOL)lineBreak;
@end
