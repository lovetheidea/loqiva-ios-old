//
//  UIColor+Helper.m
//  loqiva
//
//  Created by Emu on 16/08/2017.
//  Copyright © 2017 inup. All rights reserved.
//

#import "UIColor+Helper.h"
#import "UtilManager.h"
#import "loqiva-Swift.h"

@implementation UIColor (Helper)

+ (NSString *)imagePath {
    return [self valueForKey:@"AppImageURLPath"];
}

+ (NSString *)valueForKey:(NSString *)key {
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    NSString *value = infoPlistDict[key];
    return value;
}

+ (NSString *)colorDictionary:(NSString *)key {
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    NSDictionary *dict = infoPlistDict[@"ApplicationColors"];
    NSString *value = dict[key];
    return value;
}

+ (UIColor *)colorForKey:(NSString *)key {
    NSString *value = [self colorDictionary:key];
    if (value.length) {
        return [UtilManager colorwithHexString:value alpha:1.0];
    }
    
    return [UIColor clearColor];
}

+ (UIColor *)homeContextualFirstItemBGColor {
    return [self colorForKey:@"HomeContextualFirstItemBGColor"];
}

+ (UIColor *)homeContextualSecondItemBGColor {
    return [self colorForKey:@"HomeContextualSecondItemBGColor"];
}

+ (UIColor *)homeContextualThirdItemBGColor {
    return [self colorForKey:@"HomeContextualThirdItemBGColor"];
}

+ (UIColor *)orangeButtonColor {
    return [self colorForKey:@"AppAccentColour"];
}

+ (UIColor *)orangeAppColor {
    return [self colorForKey:@"AppBgColour"];
}

+ (UIColor *)footerColor {
    
    return [self colorForKey:@"FooterBGColor"];
}

+ (UIColor *)DistanceLabelTextColor {
    return [self colorForKey:@"DistanceLabelTextColor"];
}

+ (UIColor *)InterestTagBGColour {
    return [self colorForKey:@"InterestTagBGColour"];
}

+ (UIColor *)navigationBackgroundColor {
    return [self colorForKey:@"NavigationBackgroundColor"];
}

+ (UIColor *)headerBackgroundColor {
    return [self colorForKey:@"HeaderBackgroundColor"];
}

+ (UIColor *)navigationSeparatorColor {
    //return [UIColor orangeButtonColor];
    return [self colorForKey:@"NavigationSeparatorColor"];
}

+ (UIColor *)footerTextAcentcolour {
    return [self colorForKey:@"FooterTextAcentcolour"];
}

+ (UIColor *)redeemRewardViewBGColor{
    return [self colorForKey:@"RedeemRewardViewBGColor"];
}
+ (UIColor *)CompanyNameTextColour{
    return [self colorForKey:@"CompanyNameTextColour"];
}
+ (UIColor *)HeaderTextColour{
    return [self colorForKey:@"HeaderTextColour"];
}
+ (UIColor *)FeedTextColour{
    return [self colorForKey:@"FeedTextColour"];
}
+ (UIColor *)InterestTagNameSelected{
    return [self colorForKey:@"InterestTagNameSelected"];
}
+ (UIColor *)EventMessageNoEventsColour{
    return [self colorForKey:@"EventMessageNoEventsColour"];
}
+ (UIColor *)EventDateSelectorBGColour{
    return [self colorForKey:@"EventDateSelectorBGColour"];
}
+ (UIColor *)ContextualNavCircleBGColour{
    return [self colorForKey:@"ContextualNavCircleBGColour"];
}
@end
