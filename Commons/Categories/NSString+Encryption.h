//
//  NSString+Encryption.h
//  loqiva
//
//  Created by Manuel Manzanera on 9/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSData+Encryption.h"

@interface NSString (Encryption)
- (NSString *)AES256EncryptWithKey:(NSString *)key;
- (NSString *)AES256DecryptWithKey:(NSString *)key;
- (NSString *) md5;
- (NSString *)urlencode;
@end
