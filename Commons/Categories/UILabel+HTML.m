//
//  UILabel+HTML.m
//  ipswich staff
//
//  Created by marco attanasio on 03/09/2014.
//  Copyright (c) 2014 Captive Health. All rights reserved.
//

#import "UILabel+Extra.h"
#import "NSString+HTML.h"
#import "loqiva-Swift.h"

@implementation UILabel (HTML) 

-(void)setLineHeight:(CGFloat)lineHeight{
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:self.text];
    NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
    
    
    style.lineSpacing = lineHeight;
    
    [attributeString addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0,self.text.length)];
    self.attributedText = attributeString;
    
}
-(void) htmlWithBoldString:(NSString*)t lineHeight:(float)l standardFont:(UIFont*)sf boldFont:(UIFont*)bf color:(UIColor*)c{
    [self setText:[t stringByStrippingHTML]];
    if([self respondsToSelector:@selector(setAttributedText:)]){
        
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineSpacing:l];
        
        //Apply bold font
        NSMutableArray* arr = [t stringsBetweenString:@"<b>" andString:@"</b>"];
        NSString *htmlStrippedText = [t stringByStrippingHTML];
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:htmlStrippedText];
        if([attrStr respondsToSelector:@selector(addAttribute:value:range:)]){
            NSRange range = NSMakeRange(0, [attrStr length]);
            [attrStr addAttribute:NSFontAttributeName
                            value:sf
                            range:range];
            [attrStr addAttribute:NSForegroundColorAttributeName value:c range:range];
        }
        
        //Find html b tag to make text bold
        if(arr!=nil && arr.count>0){
            for( NSString* str in arr )
            {
                NSRange range = [[t stringByStrippingHTML] rangeOfString:str];
                if( range.location != NSNotFound )
                {
                    if([attrStr respondsToSelector:@selector(addAttribute:value:range:)]){
                        [attrStr addAttribute:NSFontAttributeName
                                        value:bf
                                        range:range];
                    }
                }
            }
        }
        
        [attrStr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, [attrStr length])];
        self.attributedText = attrStr;
    }
}

-(void) setAttributedText:(NSString*)text withLineHeight:(float)lineHeight andTextAlignment:(NSTextAlignment)alignment andFont:(UIFont*)font andColor:(UIColor*)color andKern:(float)kern withLineBreak:(BOOL)lineBreak {
    
    if (![text isKindOfClass:[NSNull class]] && text.length > 0){
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setMinimumLineHeight:lineHeight];
        [style setMaximumLineHeight:lineHeight];
        [style setAlignment:alignment];
        
        if (lineBreak) {
            style.lineBreakMode = NSLineBreakByTruncatingTail;
        }
        
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:text];
        if([attrStr respondsToSelector:@selector(addAttribute:value:range:)]){
            NSRange range = NSMakeRange(0, [attrStr length]);
            [attrStr addAttribute:NSFontAttributeName
                            value:font
                            range:range];
            [attrStr addAttribute:NSForegroundColorAttributeName value:color range:range];
            [attrStr addAttribute:NSKernAttributeName value:[NSNumber numberWithFloat:kern] range:range];
            [attrStr addAttribute:NSParagraphStyleAttributeName value:style range:range];
        }
        
        [self setAttributedText:attrStr];
    }
}

@end
