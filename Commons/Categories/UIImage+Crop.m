//
//  UIImage+Crop.m
//  loqiva
//
//  Created by juan on 30/11/2016.
//  Copyright © 2016 inup. All rights reserved.
//

#import "UIImage+Crop.h"
#import "loqiva-Swift.h"

@implementation UIImage (Crop)
- (UIImage *)crop:(CGRect)rect {
    if (self.scale > 1.0f) {
        rect = CGRectMake(rect.origin.x * self.scale,
                          rect.origin.y * self.scale,
                          rect.size.width * self.scale,
                          rect.size.height * self.scale);
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}

@end
