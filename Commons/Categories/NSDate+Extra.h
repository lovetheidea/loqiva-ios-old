//
//  NSDate+Extra.h
//  towerhamlets
//
//  Created by marco attanasio on 28/10/2014.
//  Copyright (c) 2014 Captive Minds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extra)

-(NSString*) daySuffix;
- (NSString *)currentDay;
- (NSString *)currentMonth;
- (NSString *)daySuffixForDate:(NSDate *)date;

@end
