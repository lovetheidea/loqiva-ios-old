//
//  UIImage+Tint.h
//  loqiva
//
//  Created by Manuel Manzanera on 5/9/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Tint)
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
- (UIImage *)tintedImageWithColor:(UIColor *)tintColor;
-(UIImage*) imageWithGradientOverlay:(UIImage*)sourceImage color1:(UIColor*)color1 color2:(UIColor*)color2 color3:(UIColor*)color3 gradPointA:(CGPoint)pointA gradPointB:(CGPoint)pointB gradPointC:(CGPoint)pointC gradPointD:(CGPoint)pointD;

@end
