//
//  NSString+HTML.h
//  ipswich staff
//
//  Created by marco attanasio on 03/09/2014.
//  Copyright (c) 2014 Captive Health. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface NSString (HTML)

- (NSMutableArray*)stringsBetweenString:(NSString*)start andString:(NSString*)end;
- (NSString *)stringByStrippingHTML;

- (NSString *)distanceString;

@end
