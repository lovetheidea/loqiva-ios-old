//
//  SkTintedUIImageView.m
//  
//
//  Created by Gabriel Nica on 28/04/2012.
//  Copyright (c) 2012 Skybound Interactive Ltd. All rights reserved.
//

#import "SkTintedUIImageView.h"
#import "UIView+Framing.h"
#import "loqiva-Swift.h"

@implementation SkTintedUIImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    self.tintColor = [UIColor clearColor];
}

- (id) init
{
    self = [super init];
    if (self)
    {
        _tintColor = [UIColor clearColor];
    }
    return self;
}

- (id)initWithImage:(UIImage *)image
{
    self = [super initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    
    if(self)
    {
        self.image = image;
        
        //set the view to opaque
        self.opaque = NO;
    }
    
    return self;
}

- (void) presentAsNormalImageView
{
    _tintColor = nil;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:self.image];
    imageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    imageView.contentMode = self.contentMode;
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:imageView];    
}

- (void)setTintColor:(UIColor *)color
{
    
    [self setTintColor:color];
    
    //update every time the tint color is set
    [self setNeedsDisplay];
}

- (void) drawRect:(CGRect)rect
{
    if (_tintColor)
    {
        CGContextRef context = UIGraphicsGetCurrentContext();    
        CGRect imgRect = CGRectZero;
        
        if (self.contentMode == UIViewContentModeCenter)
        {
            imgRect.size = _image.size;
            imgRect.origin = CGPointMake(rect.size.width/2-_image.size.width/2, rect.size.height/2-_image.size.height/2);
        } else 
        {
            imgRect.origin = CGPointZero;
            if (self.contentMode == UIViewContentModeScaleAspectFill || self.contentMode == UIViewContentModeScaleAspectFit)
            {
                float aspect = _image.size.width/_image.size.height;
                if (_image.size.height > _image.size.width)
                {
                    
                    imgRect.size.height = self.height;
                    imgRect.size.width = self.height * aspect;
                    imgRect.origin.x = roundf(self.width/2.0f - imgRect.size.width/2.0f);
                } else {
                    
                    imgRect.size.width = self.width;
                    imgRect.size.height = self.width / aspect;
                    imgRect.origin.y = roundf(self.height/2.0f - imgRect.size.height/2.0f);
                }
                
                
            } else if (self.contentMode == UIViewContentModeScaleToFill)
            {
                imgRect.size = self.frame.size;
            }
            
        }
        
        CGContextScaleCTM(context, 1, -1);
        CGContextTranslateCTM(context, 0, -rect.size.height);
        
        //set the clipping area to the image
        
        CGContextClipToMask(context, imgRect, _image.CGImage);
        CGContextDrawImage(context, imgRect, _image.CGImage);
        CGContextSetBlendMode(context, kCGBlendModeOverlay);
        CGContextSetFillColorWithColor(context, [_tintColor CGColor]);
        
        CGContextFillRect(context, rect);    

    } else [super drawRect:rect];
}

@end
