//
//  UITextView+Extra.h
//  loqiva
//
//  Created by Captive Minds on 25/04/2017.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (HTML)

-(void) setAttributedTextForTextView:(NSString*)text withLineHeight:(float)lineHeight andTextAlignment:(NSTextAlignment)alignment andColor:(UIColor*)color andKern:(float)kern andFont:(UIFont*)font;

@end
