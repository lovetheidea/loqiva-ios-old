//
//  UIView+Framing.h
//  GeoCab
//
//  Created by David Fox on 12/05/2011.
//  Copyright 2011 None. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIView (Framing)

- (void) setXPosition:(float)aPos;
- (void) setYPosition:(float)aPos;
- (void) setWidth:(float)aSize;
- (void) setHeight:(float)aSize;
- (float) xPosition;
- (float) yPosition;
- (float) width;
- (float) height;

- (void) addFadeOut:(BOOL)bottom;

@end
