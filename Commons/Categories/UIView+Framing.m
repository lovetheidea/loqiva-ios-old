//
//  UIView+Framing.m
//  GeoCab
//
//  Created by David Fox on 12/05/2011.
//  Copyright 2011 None. All rights reserved.
//

#import "UIView+Framing.h"
#import "loqiva-Swift.h"


@implementation UIView (Framing)

- (void) addFadeOut:(BOOL)bottom {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    
    if (bottom) {
        gradient.colors = @[(id)[UIColor clearColor].CGColor, (id)[[UIColor blackColor] colorWithAlphaComponent:0.7].CGColor];
    }
    else {
        gradient.colors = @[(id)[[UIColor blackColor] colorWithAlphaComponent:0.7].CGColor, (id)[UIColor clearColor].CGColor];
    }
    
    [self.layer insertSublayer:gradient atIndex:0];
}

- (void) setXPosition:(float)aPos {
    [self setFrame:CGRectMake(aPos, self.frame.origin.y, self.frame.size.width, self.frame.size.height)];
}

- (void) setYPosition:(float)aPos {
    [self setFrame:CGRectMake(self.frame.origin.x, aPos, self.frame.size.width, self.frame.size.height)];
}

- (void) setWidth:(float)aSize {
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, aSize, self.frame.size.height)];
}

- (void) setHeight:(float)aSize {
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, aSize)];
}

- (float) xPosition {
    return self.frame.origin.x;
}

- (float) yPosition {
    return self.frame.origin.y;
}

- (float) width {
    return self.frame.size.width;
}

- (float) height {
    return self.frame.size.height;
}

@end
