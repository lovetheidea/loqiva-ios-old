//
//  SkTintedUIImageView.h
//  
//
//  Created by Gabriel Nica on 28/04/2012.
//  Copyright (c) 2012 Skybound Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SkTintedUIImageView : UIView

@property (nonatomic) UIImage * image;
@property (nonatomic) UIColor * tintColor;

- (id)initWithImage:(UIImage *)image;
- (void) presentAsNormalImageView;

@end
