//
//  UIImage+Tint.m
//  loqiva
//
//  Created by Manuel Manzanera on 5/9/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "UIImage+Tint.h"
#import "loqiva-Swift.h"

@implementation UIImage (Tint)
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
-(UIImage*) imageWithGradientOverlay:(UIImage*)sourceImage color1:(UIColor*)color1 color2:(UIColor*)color2 color3:(UIColor*)color3 gradPointA:(CGPoint)pointA gradPointB:(CGPoint)pointB gradPointC:(CGPoint)pointC gradPointD:(CGPoint)pointD {
    
    //CGSize size = CGSizeMake(300, 210);
    CGSize size = sourceImage.size;
    
    // Start context
    UIGraphicsBeginImageContext(size);
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(c, 0, sourceImage.size.height);
    CGContextScaleCTM(c, 1, -1);
    
    // Draw source image into context
    CGContextDrawImage(c, (CGRect){CGPointZero, size}, sourceImage.CGImage);
    
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    //bottom
    CGFloat gradLocs[] = {0,1};
    //top
    CGFloat gradLocs2[] = {0,1};
    NSArray* colors = @[(id)color1.CGColor, (id)color2.CGColor];
    NSArray* colors2 = @[(id)color2.CGColor, (id)color3.CGColor];
    
    // Create a simple linear gradient with the colors provided.
    CGGradientRef grad = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)colors, gradLocs);
    CGGradientRef grad2 = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)colors2, gradLocs2);
    CGColorSpaceRelease(colorSpace);
    
    // Draw gradient with multiply blend mode over the source image
    CGContextSetBlendMode(c, kCGBlendModeMultiply);
    CGContextDrawLinearGradient(c, grad, pointA, pointB, 0);
    CGGradientRelease(grad);
    CGContextSetBlendMode(c, kCGBlendModeMultiply);
    CGContextDrawLinearGradient(c, grad2, pointC, pointD, 0);
    CGGradientRelease(grad2);
    

    
    // Grab resulting image from context
    UIImage* resultImg = UIGraphicsGetImageFromCurrentImageContext();
    

    UIGraphicsEndImageContext();
    
    return resultImg;
}

- (UIImage *)tintedImageWithColor:(UIColor *)tintColor
{
    // It's important to pass in 0.0f to this function to draw the image to the scale of the screen
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);
    [tintColor setFill];
    CGRect bounds = CGRectMake(0, 0, self.size.width, self.size.height);
    UIRectFill(bounds);
    [self drawInRect:bounds blendMode:kCGBlendModeNormal alpha:1.0];
    
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
}

@end
