//
//  AppConstants.h
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//


#define isBLUECAT 1

////////////////////////// FRAMES /////////////////////////////

#define WINDOW_ROOT  [UIApplication sharedApplication].keyWindow.rootViewController.view
#define VIEW_FRAME CGRectMake(0., 0., [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)
#define WIDTH [[UIScreen mainScreen] bounds].size.width
#define HEIGHT [[UIScreen mainScreen] bounds].size.height

#define NAV_ICON_ORIGIN_X [UtilManager width:20]
#define NAV_ICON_ORIGIN_Y [UtilManager height:32]
#define NAV_ICON_WIDTH    [UtilManager width:28.]
#define NAV_ICON_HEIGHT   [UtilManager height:28.]

#define NAV_NOTIFICATION_ORIGIN_X   [UtilManager width:292.]
#define NAV_NOTIFICATION_ORIGIN_Y   [UtilManager height:35]
#define NAV_NOTIFICATION_WIDTH      [UtilManager width:18.]
#define NAV_NOTIFICATION_HEIGHT     [UtilManager height:21.]

#define NAV_SLIDE_ORIGIN_X   [UtilManager width:327.]
#define NAV_SLIDE_ORIGIN_Y   [UtilManager height:35]
#define NAV_SLIDE_WIDTH      [UtilManager width:28.]
#define NAV_SLIDE_HEIGHT     [UtilManager height:20.]

#define NAV_CLOSE_ORIGIN_X   [UtilManager width:320]
#define NAV_CLOSE_ORIGIN_Y   [UtilManager height:34]
#define NAV_CLOSE_WIDTH      [UtilManager width:22.]
#define NAV_CLOSE_HEIGHT     [UtilManager height:22.]

#define NAV_BAR_HEIGHT [UtilManager height:80]
#define MONT_CALENDAR_HEIGHT [UtilManager height:250]

#define UITEXTFIELD_HEIGHT 50
#define RADIUS 18
#define BUTTON_FONT_SIZE 13

#define TOPWINDOW  [UIApplication sharedApplication].keyWindow.rootViewController.view

// Accent Footer Text colour ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define FOOTER_ACCENT_TEXT [UtilManager colorwithHexString:@"F75411" alpha:1]

// Accent text colour IS THIS ACTUALLY USED? //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define ORANGE_TEXT_COLOR [UtilManager colorwithHexString:@"12ff00" alpha:1]

// Gradient colour for header //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define GRADIENT_DARK [UtilManager colorwithHexString:@"000000" alpha:.85]

// BG colour TINT CONTEXTUAL //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define ORANGE_BG_CONTEXTUAL_COLOR [UtilManager colorwithHexString:@"041e41" alpha:0.9]

#define HEADER_SEPARATOR_COLOR [UtilManager colorwithHexString:@"ffffff" alpha:.35]
#define GRAY_SEPARATOR_COLOR [UIColor colorWithRed:214/255. green:214./255. blue:214/255. alpha:0.8]
#define GRAY_BACKGROUND_COLOR [UIColor colorWithRed:214/255. green:214./255. blue:214/255. alpha:0.3]
#define GRAY_PIKCER_COLOR [UIColor colorWithRed:0.941 green:0.941 blue:0.941 alpha:1.00]
#define GRAY_LOWER_MENU_BORDER_COLOR [UtilManager colorwithHexString:@"979797" alpha:1]
#define GRAY_TEXT_HOME_COLOR [UtilManager colorwithHexString:@"717171" alpha:1]
#define GRAY_DARK_TEXT_COLOR [UtilManager colorwithHexString:@"474747" alpha:1]
#define GRAY_TEXT_COLOR [UtilManager colorwithHexString:@"787878" alpha:1]
#define GRAY_REPORT_BG [UIColor colorWithRed:0.400 green:0.400 blue:0.400 alpha:1.00]

#define BLACK_TEXT_COLOR [UtilManager colorwithHexString:@"474747" alpha:1]
#define WHITE_TEXT_COLOR [UtilManager colorwithHexString:@"ffffff" alpha:1]

#define SEPARATOR_VIEW_ALPHA 0.5
#define TRANSPARENT_ALPHA 0

// Test background colour ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define TEST_BG_ONE [UtilManager colorwithHexString:@"00ff3c" alpha:0]
#define TEST_BG_TWO [UtilManager colorwithHexString:@"ff006c" alpha:0]

#define DATE_FORMATTER @"YYYY-MM-dd HH:mm:ss"
#define DATE_FORMATTER_MONTH_VIEW @"MMMM YYYY"
#define DATE_FORMATTER_PRETTY @"h:mma, EEEE dd MMMM YYYY"
#define DATE_FORMATTER_FULL @"EEEE dd MMMM YYYY"
#define DATE_FORMATTER_NOTIFICATION @"dd MMMM YYYY @ HH:mm"


////////////////////////// FONTS /////////////////////////////

#define LIGHT_FONT @"OpenSans-Light"
#define REGULAR_FONT @"OpenSans"
#define CONDLIGHT_FONT @"OpenSans-CondensedLight"
#define SEMIBOLD_FONT @"OpenSans-Semibold"
#define ITALIC @"OpenSans-Italic"
#define LIGHT_ITALIC @"OpenSansLight-Italic"
#define CONDBOLD_FONT @"OpenSans-CondBold"
#define BOLD_FONT @"OpenSans-Bold"
#define EXTRA_BOLD_FONT @"OpenSans-Extrabold"
#define SEMI_BOLD_ITALIC @"OpenSans-SemiboldItalic"
#define COND_BOLD_ITALIC @"OpenSans-CondensedLightItalic"
#define BOLD_ITALIC @"OpenSans-BoldItalic"
#define EXTRA_BOLD_ITALIC_FONT @"OpenSans-ExtraboldItalic"



////////////////////////// MITCH FONT STYLES /////////////////////////////

#define NAV_CUSTOM_VIEW_FONT_SIZE 22
#define NAV_CUSTOM_VIEW_LINEHEIGHT 27
#define NAV_CUSTOM_VIEW_KERN -0.4

#define TITLE_FONT_SIZE 27
#define TITLE_FONT_LINEHEIGHT 30
#define TITLE_FONT_KERN -0.8

#define CONTACT_FONT_SIZE 12
#define CONTACT_FONT_LINEHEIGHT 16
#define CONTACT_FONT_KERN -0.4

#define BODY_FONT_SIZE 16
#define BODY_FONT_LINEHEIGHT 22
#define BODY_FONT_KERN -0.1

#define SMALL_FONT_SIZE 10
#define SMALL_FONT_LINEHEIGHT 10
#define SMALL_FONT_KERN -0.2


////////////////////////// NOTIFICATIONS /////////////////////////////

#define SLIDE_NOTIFICATION @"SlideNavigationAction"
#define ACTION_NOTIFICATION @"NotificationNavigationAction"
#define BACK_NOTIFICATION @"BackNavigationAction"
#define WEBVIEW_NOTIFICATION @"WebViewAction"
#define VIEWCONTROLLER_NOTIFICATION @"ViewControllerNavigationAction"
#define REWARD_DETAIL_NOTIFICATION @"RewardDetailNotification"
#define DID_UPDATE_BEACON_NOTIFICATION @"DidUpdateBecaonNotification"
#define DID_UPDATE_HOME_NOTIFICATION @"DidUpdateHomeNotification"


//////////////////////// USERDEFAULTMANAGER KEYS ///////////////////

#define USER_TOKEN @"usertoken"// TODO delete this value and all associated code

////////////////////////

#define IOS_10 [[UIDevice currentDevice] systemVersion].intValue>=10
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
