//
//  LoqivaScrollView.m
//  loqiva
//
//  Created by Emu on 06/02/2019.
//  Copyright © 2019 inup. All rights reserved.
//

#import "LoqivaScrollView.h"

@implementation LoqivaScrollView

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (self.hidden || !self.userInteractionEnabled || self.alpha < 0.01 || ![self pointInside:point withEvent:event]) {
        return nil;
    } else {
        BOOL hasReturned = NO;
        for (UIView *subview in [self.subviews reverseObjectEnumerator]) {
            UIView *hitView = [subview hitTest:[subview convertPoint:point fromView:self] withEvent:event];
            if (hitView) {
                hasReturned = YES;
                return hitView;
            }
        }
        
        if (!hasReturned) {
            for (UIView *subview in self.superview.subviews) {
                UIView *hitView = [subview hitTest:[subview convertPoint:point fromView:self] withEvent:event];
                if (hitView) {
                    hasReturned = YES;
                    return hitView;
                }
            }
        }
        
        return self;
    }
}

@end
