//
//  AppContext.m
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "AppContext.h"
#import "loqiva-Swift.h"

@implementation AppContext

+ (AppContext *)sharedInstance
{
    static dispatch_once_t once;
    static AppContext *appContext;
    dispatch_once(&once, ^{ appContext = [[self alloc] init]; });
    return appContext;
}

@end
