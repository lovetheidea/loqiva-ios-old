//
//  AppContext.h
//  loqiva
//
//  Created by Manuel Manzanera on 20/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HomePage.h"
#import "User.h"
#import "InterestDetail.h"
#import "GeoLocationMessage.h"

@interface AppContext : NSObject

@property (nonatomic, strong) HomePage *homePage;
@property (nonatomic, strong) NSMutableArray *infoItems;

@property (nonatomic, strong) NSMutableArray *paymentTypes;
@property (nonatomic, strong) NSMutableArray *transports;
@property (nonatomic, strong) NSMutableArray *events;
@property (nonatomic, strong) NSMutableArray *surveys;
@property (nonatomic, strong) NSMutableArray *interests;

@property (nonatomic, strong) NSMutableArray *notifications;

@property (nonatomic, strong) InterestDetail *selectedInterestDetail;
@property (nonatomic, strong) GeoLocationMessage *geoLocationMessage;

+ (AppContext *)sharedInstance;

@end
