//
//  BeaconNotificationStrategyFilter.m
//  Notification
//
//  Created by Stevens Olivier on 31/05/2016.
//  Copyright © 2016 sarra srairi. All rights reserved.
//

#import "BeaconNotificationStrategyFilter.h"
#import "UtilManager.h"
#import "BeaconData.h"
#import "AppContext.h"
#import <UserNotifications/UserNotifications.h>
#import "AppDelegate.h"
#import "AppConstants.h"
#import "UIColor+Helper.h"
#import "loqiva-Swift.h"

@implementation BeaconNotificationStrategyFilter

-(id) initWithMinTimeBetweenNotification:(long)minTime{
    self = [super init];
    if(self){
      
        minTimeBetweenNotification =   ((double)minTime / 1000);
        minNextTimeNotification = 0 ;
    }
    return self;
}

-(BOOL) createNewNotification:(ATBeaconContent *)newBeaconContent feedStatus:(ATRangeFeedStatus)feedStatus{
    NSLog(@"min time %f %f",minNextTimeNotification , CACurrentMediaTime());
    NSLog(@"min time %d ", minNextTimeNotification < CACurrentMediaTime());
    return minNextTimeNotification < CACurrentMediaTime();
    //return YES ;
}

-(void) onNotificationIsCreated:(ATBeaconContent *)beconContent notificationStatus:(BOOL)notificationStatus{
    if(notificationStatus){
        minNextTimeNotification = CACurrentMediaTime() + minTimeBetweenNotification;
    }
}

-(BOOL) deleteCurrentNotification:(ATBeaconContent *)newBeaconContent feedStatus:(ATRangeFeedStatus)feedStatus{
    return true;
}

-(void) onNotificationIsDeleted:(ATBeaconContent *)beconContent notificationStatus:(BOOL)notificationStatus{
    NSLog(@"Beacon Notification deleted");
}

-(void) onBackground{

}

-(void) onForeground{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APP_ACTIVE" object:nil];
}

-(void) onStartMonitoringRegion:(ATBeaconContent *)beaconContent feedStatus:(ATRangeFeedStatus)_feedStatus{
    //To check it
    // I think when App is enter in region call this method
    //[UtilManager presentAlertWithMessage:@"ENTER IN BACKGROUND"];
    
    //if(beaconContent){
    NSLog(@"%@",beaconContent.getNotificationDescription);
    
    //[notification setAlertBody:[beaconContent getNotificationDescription]];
    
    switch (_feedStatus) {
        case ATRangeFeedStatusInProgress:
            NSLog(@"IN PROGRESS");
//            if(!beaconContent)
//                [UtilManager presentAlertWithMessage:@"BeaconContent is nil" andTitle:@"Warning"];
            break;
        case ATRangeFeedStatusBackendError:
            NSLog(@"BACKEND ERROR");
            break;
        case ATRangeFeedStatusNetworkError:
            NSLog(@"NETWORK ERROR");
            break;
        case ATRangeFeedStatusBackendSuccess:
            NSLog(@"BACKEND SUCCESS");
            if(!beaconContent.getLatitude) {
                UILocalNotification *notification = [[UILocalNotification alloc] init];
                [notification setAlertBody:@"ADTag beacon response is null."];
                [notification setAlertTitle:@"Beacon Alert"];
                
                [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
            }
            break;
            
        default:
            break;
    }
    
    if(beaconContent){
    
        [WebServiceManager getBeaconRewardWithBeaconDictionary:[self beaconDictionary:beaconContent] andDelegate:self];
        
        BeaconData *lastBeaconData = [BeaconData getBeaconData];
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
            
            //Notification Content
            UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
            content.body =  [lastBeaconData bodyNotification];
            content.title = [lastBeaconData titleNotification];
            content.sound = [UNNotificationSound defaultSound];
            
            //Set Badge Number
            //content.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
            content.badge = @(1);
            
            // Deliver the notification in five seconds.
            UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
                                                          triggerWithTimeInterval:1.0f repeats:NO];
            
            //Notification Request
            UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"LocalNotification" content:content trigger:trigger];
            
            //schedule localNotification
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            //center.delegate = self;
            [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                if (!error) {
                    NSLog(@"add Notification Request succeeded!");
                }
            }];
            
        } else {
            
            if(IOS_10){
                UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
                content.title = [lastBeaconData titleNotification];
                content.body = [lastBeaconData bodyNotification];
                
                UNNotificationAttachment *attachment = [UNNotificationAttachment attachmentWithIdentifier:@"BeaconImage" URL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[UIColor imagePath],lastBeaconData.icon]] options:nil error:nil];
               
                content.attachments = @[attachment];
                
                // Deliver the notification in five seconds.
                UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger
                                                              triggerWithTimeInterval:5 repeats:NO];
                
                
                
                UNNotificationRequest* request = [UNNotificationRequest requestWithIdentifier:@"BeaconNotification"
                                                                                      content:content trigger:trigger];
                
                // Schedule the notification.
                UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
                [center addNotificationRequest:request withCompletionHandler:^(NSError *error){
                    if(!error){
                        
                        NSLog(@"Completion handler for notification");
                    }
                    
                }];
            }else{
                UILocalNotification *notification = [[UILocalNotification alloc] init];
                
                [notification setAlertBody:[lastBeaconData bodyNotification]];
                [notification setAlertTitle:[lastBeaconData titleNotification]];
                [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
            }
        }
    }
}

- (NSDictionary *)beaconDictionary:(ATBeaconContent *)beaconContent{
    
    NSDictionary *Localisation = [[[beaconContent.poi.newCategories valueForKey:@"Localisation"] valueForKey:@"fields"] valueForKey:@"Localisation"];
    NSArray *beaconLocation = [Localisation valueForKey:@"location"];
    
    NSNumber *latitude;
    NSNumber *longitude;
    if(beaconLocation.count == 2){
        latitude = [[NSNumber alloc] initWithFloat:[[beaconLocation objectAtIndex:0] floatValue]];
        longitude = [[NSNumber alloc] initWithFloat:[[beaconLocation objectAtIndex:1] floatValue]];
    } else {
        latitude = [[NSNumber alloc] initWithFloat:0];
        longitude = [[NSNumber alloc] initWithFloat:0];
    }
    
    NSString *pointUniqueId = beaconContent.poi.id;
    
    NSDictionary *beaconDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[[SessionManager shared] user].userToken,@"userid",pointUniqueId,@"pointuniqueid",beaconContent.uuid,@"beaconuuid",beaconContent.major,@"major",beaconContent.minor,@"minor",latitude,@"latitude",longitude,@"longitude", nil];
    
    return beaconDictionary;
}

#pragma marks WebServiceManagerDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    NSDictionary *resultset = (NSDictionary *)object;
    
    if(resultset.allKeys.count <4){
        NSLog(@"Resultset was empty");
    } else {
        
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        
        localNotification.fireDate = [NSDate date];
        localNotification.alertAction = [resultset valueForKey:@"title"];
        localNotification.alertBody = [resultset valueForKey:@"description"];
        
        localNotification.soundName = @"alarm.wav";
        //[localNotification setAlertLaunchImage:[UIImage imageNamed:@""]];
        
        localNotification.applicationIconBadgeNumber = 0;
        
        [localNotification setUserInfo:resultset];
        
        BeaconData *beaconData = [[BeaconData alloc] init];
        [beaconData setBodyNotification:[resultset valueForKey:@"description"]];
        [beaconData setTitleNotification:[resultset valueForKey:@"title"]];
        [beaconData setIcon:[resultset valueForKey:@"icon"]];
        [beaconData setHeading:[resultset valueForKey:@"heading"]];
        [beaconData setRewardId:[resultset valueForKey:@"id"]];
        
        [BeaconData saveBeacon:beaconData];
    }
}

-(void) load:(NSUserDefaults *)dataStore{
    minNextTimeNotification = [dataStore doubleForKey:MIN_NEXT_TIME_NOTIFICATION];
}

-(void) save:(NSUserDefaults *)dataStore{
    [dataStore setDouble:minNextTimeNotification forKey:MIN_NEXT_TIME_NOTIFICATION];
}

@end
